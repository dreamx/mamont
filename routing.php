<?php

session_start();
require_once("admin/classes/core.php");
$id_client=$_SESSION['usid'];
$client=client::getClient($id_client);
$chpu=isset($_GET['chpu'])?$_GET['chpu']:'';
$maschpu=explode('/',$chpu);
$content="";
$title_site='Интернет-магазин детских товаров Мамонтенок';
$q="SELECT * FROM `catalog` WHERE `public`='on' ORDER BY `num`";
$menu=DB::query($q,'a');
$tree=new tree('parent','id',$menu);
/*
 * домашняя страница
 */

    if($maschpu[0]==''|| $maschpu[0]=='home')
    { 
        $description="Интернет-магазин детских товаров Мамонтенок";
        $keywords="Интернет-магазин детских товаров Мамонтенок";
        $includ_page="html/home.php";
        //$includ_page="html/menu.php";
        $content="html/home_head.php";
        $q="SELECT `p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`small_desc`,
                    GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos        
            FROM `product` AS `p` 
            LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
            WHERE `p`.`public`='on' AND `p`.`slide`='on' 
            GROUP BY `p`.`id`
        ";
        $slide=$core->_list($q);

        $q="SELECT `p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`small_desc`,
                    GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos        
            FROM `product` AS `p` 
            LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
            WHERE `p`.`public`='on' AND `p`.`new`='on' 
            GROUP BY `p`.`id`
        ";
        $new=$core->_list($q);

        $q="SELECT `p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`small_desc`,
                    GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos        
            FROM `product` AS `p` 
            LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
            WHERE `p`.`public`='on' AND `p`.`hit`='on' 
            GROUP BY `p`.`id`
        ";
        $hit=$core->_list($q);

        include "html/head.php";
        //include "/var/www/html/html/menu2.php";
        exit;
   }
/* конец домашняя страница */ 

/* открытый каталог*/
$url=htmlspecialchars($maschpu[0],ENT_QUOTES);
$q="SELECT * FROM `catalog` WHERE `translit`=:trans";
$cat=DB::query($q,'a',array('trans'=>$url));
if($cat){
    $cat=$cat[0];
    $title_site.=' | '.$cat['title_seo'];
    $title_page=$cat['title']; // заголовок страницы
    $description=$cat['desc_seo'];
    $keywords=$cat['keywords_seo'];

    $child_cat=$tree->rec_round($tree->orig,intval($cat['id']));
    if(count($child_cat)>0){
        $includ_page="catalog_child_show.php";
        $content="html/home_head.php";
        $wh=array();
        foreach ($child_cat as $key => $value) {
            $wh[]="`c`.`id`='".$key."'";
        }
        $q="SELECT `c`.*, concat(`ph`.`id`,'.',`ph`.`Photos_ex`) AS photo
        FROM `catalog` AS `c`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`c`.`id` AND `ph`.`Photos_module`='catalog'
        WHERE (".implode(' or ',$wh).") AND `c`.`public`='on'
        GROUP BY `c`.`id`
        ORDER BY `c`.`num`";
        $category=$core->_list($q);
    }else{

        /*$wh_ch=array();// массив для фильтра по характеристикам
        foreach ($_GET as $key => $value) {
            preg_match('/^ch(\d+)$/', $key,$res);
            
            if(count($res)>0){
                $wh_ch[]=intVal($res[1]);
            }
        }
        $wh_prod='';
        //Если фильтр включен
        if(count($wh_ch)>0){
            $q="SELECT `cv`.`id_prod`,GROUP_CONCAT(DISTINCT `cv`.`id_char`) AS `chars`
                FROM `char_value` AS `cv`
                LEFT JOIN `product` AS `p` ON `cv`.`id_prod`=`p`.`id`
                WHERE `p`.`catalog`='".intval($cat['id'])."' AND `p`.`public`='on'
                GROUP BY  `cv`.`id_prod` ";
            $ch_arr=$core->_list($q);// массив характеристик   
            $wh_prod=array();
            if($ch_arr){
                foreach ($ch_arr as $prod_chars) {
                    $chars=explode(',',$prod_chars['chars']);
                    $flag=true;
                    foreach ($wh_ch as $value) {
                        
                        if(!in_array($value, $chars)){
                            $flag=false; 
                            break;
                        }
                    }
                    if($flag){
                       $wh_prod[]="`p`.`id`='".intval($prod_chars['id_prod'])."'"; 
                    }
                }
            }
            if(count($wh_prod)>0){
                $wh_prod=" AND (".implode(' or ',$wh_prod).")";
            }else{
                $wh_prod="";
            }
        }else{
             $wh_ch='';
        }
        $q="SELECT `p`.`catalog`,`cl`.`value_char`,`c`.`title` as `ctitle`, `c`.`si`,`c`.`id` AS `char`,`cl`.`id`,count(`p`.`id`) AS `prd`
            FROM `product` as `p`
            LEFT JOIN `char_value` AS `cv` ON `cv`.`id_prod`=`p`.`id`
            LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
            LEFT JOIN `char`  AS `c` ON `c`.`id`=`cl`.`id_char`
            WHERE `catalog`='".intval($cat['id'])."' AND `p`.`public`='on' AND  `cl`.`value_char` IS NOT NULL ".$wh_prod."
            GROUP BY `cl`.`id`";
        $f_arr=$products=$core->_list($q);// массив фильтра 
        */
        //print_r($q);
        $arr_sort=array(
            array('`p`.`title`','названию','ASC'),
            array('`p`.`price`','увеличению цены','ASC'),
            array('`p`.`price`','уменьшению цены','DESC')
            );
        $order='';
        if(isset($_GET['sort'])){
            if(isset($arr_sort[intval($_GET['sort'])])){
                $sel_sort='';
                $sel_sort=intval($_GET['sort']);
                if(isset($sel_sort)){
                    $order=" ORDER BY ".$arr_sort[$sel_sort][0]." ".$arr_sort[$sel_sort][2];
                }
            }
        }
        $includ_page="catalog_show.php";
        $content="html/home_head.php";
        $q="SELECT `p`.`id`,`p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`new`,`p`.`hit`,`p`.`action`,`p`.`action_price`,
                    GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos
        FROM `product` AS `p`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
        LEFT JOIN `char_value` AS `cv` ON `cv`.`id_prod`=`p`.`id`
        LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
        WHERE `catalog`='".intval($cat['id'])."' AND `p`.`public`='on' ".$wh_prod."
        GROUP BY `p`.`id`
        ".$order;
        $products=$core->_list($q);
    }
    include "html/head.php";
    exit;
}
/* конец открытый каталог*/

/* новинки */
if($maschpu[0]=='new'){
    $arr_sort=array(
            array('`p`.`title`','названию','ASC'),
            array('`p`.`price`','увеличению цены','ASC'),
            array('`p`.`price`','уменьшению цены','DESC')
            );
        $order='';
        if(isset($_GET['sort'])){
            if(isset($arr_sort[intval($_GET['sort'])])){
                $sel_sort='';
                $sel_sort=intval($_GET['sort']);
                if(isset($sel_sort)){
                    $order=" ORDER BY ".$arr_sort[$sel_sort][0]." ".$arr_sort[$sel_sort][2];
                }
            }
        }
    $q="SELECT `p`.`id`,`p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`new`,`p`.`hit`,`p`.`action`,`p`.`action_price`,
                    GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos
        FROM `product` AS `p`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
        LEFT JOIN `char_value` AS `cv` ON `cv`.`id_prod`=`p`.`id`
        LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
        WHERE `p`.`public`='on' AND `p`.`new`='on'
        GROUP BY `p`.`id` ".$order;
    $products=$core->_list($q);
    $title_site.=' | Новинки';
    $title_page=$cat['title']; // заголовок страницы
    $description=$cat['desc_seo'];
    $keywords=$cat['keywords_seo'];
    $includ_page="new_show.php";
    $content="html/home_head.php";
    include "html/head.php";
    exit;
}    
/* конец новинки */

/* хиты */
if($maschpu[0]=='hits'){
    $arr_sort=array(
            array('`p`.`title`','названию','ASC'),
            array('`p`.`price`','увеличению цены','ASC'),
            array('`p`.`price`','уменьшению цены','DESC')
            );
        $order='';
        if(isset($_GET['sort'])){
            if(isset($arr_sort[intval($_GET['sort'])])){
                $sel_sort='';
                $sel_sort=intval($_GET['sort']);
                if(isset($sel_sort)){
                    $order=" ORDER BY ".$arr_sort[$sel_sort][0]." ".$arr_sort[$sel_sort][2];
                }
            }
        }
    $q="SELECT `p`.`id`,`p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`new`,`p`.`hit`,`p`.`action`,`p`.`action_price`,
                    GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos
        FROM `product` AS `p`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
        LEFT JOIN `char_value` AS `cv` ON `cv`.`id_prod`=`p`.`id`
        LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
        WHERE `p`.`public`='on' AND `p`.`hit`='on'
        GROUP BY `p`.`id` ".$order;
    $products=$core->_list($q);
    $title_site.=' | Хиты продаж';
    $title_page=$cat['title']; // заголовок страницы
    $description=$cat['desc_seo'];
    $keywords=$cat['keywords_seo'];
    $includ_page="hits_show.php";
    $content="html/home_head.php";
    include "html/head.php";
    exit;
}    
/* конец хиты */

/* акции */
if($maschpu[0]=='actions'){
    $arr_sort=array(
            array('`p`.`title`','названию','ASC'),
            array('`p`.`price`','увеличению цены','ASC'),
            array('`p`.`price`','уменьшению цены','DESC')
            );
        $order='';
        if(isset($_GET['sort'])){
            if(isset($arr_sort[intval($_GET['sort'])])){
                $sel_sort='';
                $sel_sort=intval($_GET['sort']);
                if(isset($sel_sort)){
                    $order=" ORDER BY ".$arr_sort[$sel_sort][0]." ".$arr_sort[$sel_sort][2];
                }
            }
        }
    $q="SELECT `p`.`id`,`p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`new`,`p`.`hit`,`p`.`action`,`p`.`action_price`,
                    GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos
        FROM `product` AS `p`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
        LEFT JOIN `char_value` AS `cv` ON `cv`.`id_prod`=`p`.`id`
        LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
        WHERE `p`.`public`='on' AND `p`.`action`='on'
        GROUP BY `p`.`id` ".$order;
    $products=$core->_list($q);
    $title_site.=' | Хиты продаж';
    $title_page=$cat['title']; // заголовок страницы
    $description=$cat['desc_seo'];
    $keywords=$cat['keywords_seo'];
    $includ_page="actions_show.php";
    $content="html/home_head.php";
    include "html/head.php";
    exit;
}    
/* конец акции */

/*  поиск */
/*$url=htmlspecialchars($maschpu[0],ENT_QUOTES);
$q="SELECT * FROM `catalog` WHERE `translit`='".$url."'";
$cat=$core->_list($q);*/
if($maschpu[0]=='search'){
   
    if(!empty($_GET['f'])){ 
    $f = preg_replace("/(?<=[а-яё])(ы|у|ем|ым|ет|им|ам|ить|ий|ю|ый|ой|ая|ое|ые|ому|а|о|у|е|ого|ему|и|ых|ох|ия|ий|ь|я|он|ют|ат)(?![а-яё])/i",'',$_GET['f']);
    $f=htmlspecialchars($f,ENT_QUOTES);
    $f=explode(' ',$f);
    $wh="`p`.`public`='on' ";
    $wh_or=array();
    $wh_cat="`c`.`public`='on' ";
    $wh_cat_or=array();
    foreach ($f as $w) {
        if(mb_strlen($w,'utf-8')>3){
            array_push($wh_or,"`p`.`title` LIKE '%".$w."%'");
            array_push($wh_or,"`p`.`art` LIKE '%".$w."%'");
            array_push($wh_cat_or,"`c`.`title` LIKE '%".$w."%'");
        }
    }
    
    if(count($wh_cat_or)>0){
        $wh_cat.="and (".implode(' or ',$wh_cat_or).")";
    }
    $q="SELECT `c`.*, concat(`ph`.`id`,'.',`ph`.`Photos_ex`) AS photo
        FROM `catalog` AS `c`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`c`.`id` AND `ph`.`Photos_module`='catalog'
        WHERE ".$wh_cat."
        GROUP BY `c`.`id`
        ORDER BY `c`.`num`";
        $category=$core->_list($q);

    if(count($wh_or)>0){
        $wh.="and (".implode(' or ',$wh_or).")";
    }
    $q="SELECT `p`.`id`,`p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`new`,`p`.`hit`,`p`.`action`,`p`.`action_price`,
                GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos
    FROM `product` AS `p`
    LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
    WHERE ".$wh."
    GROUP BY `p`.`id`
    ORDER BY `p`.`num`";
    $products=$core->_list($q);
    }
    $title_site.=' | Результат поиска';
    $description="";
    $keywords="";
    $includ_page="search_show.php";
    $content="html/home_head.php";
    $title_page="Результат поиска"; // заголовок страницы
    include "html/head.php";
    exit;
}
/* конец  поиск */

/* открытый товар*/
$url=htmlspecialchars($maschpu[0],ENT_QUOTES);

$q="SELECT `p`.*,
                GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos
    FROM `product` AS `p`
    LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
    WHERE `p`.`title_translit`=:trans AND `p`.`public`='on'
    GROUP BY `p`.`id`
    ORDER BY `p`.`num`"; 
$prod=DB::query($q,'a',array('trans'=>$url));

if($prod){
    $prod=$prod[0];
    $q="SELECT `p`.`id`,`p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`new`,`p`.`hit`,`p`.`action`,`p`.`action_price`,
                    GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos
        FROM `product` AS `p`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
        LEFT JOIN `link_product_product` AS `l` ON `l`.`id_note2`=`p`.`id`
        WHERE `l`.`id_note1`='".intval($prod['id'])."' AND `p`.`public`='on'
        GROUP BY `p`.`id`
        ORDER BY `p`.`num`";
    $dop_products=$core->_list($q);//сопутствующие товары

    $q="SELECT * FROM `reviews` WHERE `public_review`='on' AND `prod`='".intval($prod['id'])."'";
    $reviews_products=$core->_list($q);//отзывы о товаре

    $q="SELECT * FROM `product_quest` WHERE `public_quest`='on' AND `product`='".intval($prod['id'])."'";
    $quest_products=$core->_list($q);//вопросы о товаре

    $q="SELECT * FROM `transport` ORDER BY `price`";
    $transport=$core->_list($q);// виды доставки

    $pr=Product::getInstance($prod['id']);//объект класса

    $title_site.=' | '.$prod['title_seo'];
    $description=$prod['desc_seo'];
    $keywords=$prod['content_seo'];
    $includ_page="product.php";
    $content="html/home_head.php";
    $title_page=$prod['title']; // заголовок страницы
    $photos=explode(',',$prod['photos']);
    include "html/head.php";
    exit;
}
/* конец открытый товар*/


/* танкю страница */
if($maschpu[0]=='thankyou')
{ 
   

    $description=$page_info['Pages_desc_seo'];
    $keywords=$page_info['Pages_content_seo'];
    //$includ_page="thank.php";
    $content="html/thank.php";
    $title_page="Спасибо"; // заголовок страницы
    //include "html/head.php";
}
/* конец танкю  */


/* корзина */
    if($maschpu[0]=='shoping_cart'&&empty($maschpu[1])){
    $q="SELECT * FROM `transport` ORDER BY `price`";
    $transport=$core->_list($q);// виды доставки
    $title_site.=' | Корзина и оформление заказа';
    $description='';
    $keywords='';
    $includ_page="shoping_cart.php";
    $content="html/home_head.php";
    $title_page='Корзина и оформление заказа'; // заголовок страницы
    include "html/head_cart.php";
    exit;
}
/* конец корзина */

/* страницы */
if($maschpu[0]=='page'&&!empty($maschpu[1])){ 
    $url=explode('-', $maschpu[1]);
    $id=$url[0];
    if(is_numeric($id))
    {
        $q="SELECT `p`.`id`,`p`.`Pages_title` AS `name`,`p`.`Pages_content` AS `content`,
                    `Pages_title_seo`,`Pages_desc_seo`,`Pages_content_seo`        
            FROM `Pages` AS `p`
            WHERE `p`.`Pages_public`='on' AND `p`.`id`='".intval($id)."' 
            GROUP BY `p`.`id`
            LIMIT 1";
        $page_info=$core->_list($q);
        if(count($page_info)>0)
        {
            $page_info=$page_info[0];
            $title_site.=' | '.$page_info['Pages_title_seo'];
            $description=$page_info['Pages_desc_seo'];
            $keywords=$page_info['Pages_content_seo'];
            $includ_page="page.php";
            $content="html/home_head.php";
            $title_page=$page_info['name']; // заголовок страницы
            
        }
    }
}
/* конец страницы */ 

/* бренды */
if($maschpu[0]=='brand'&&empty($maschpu[1])){ 
    if($_GET['view']=='abc'||empty($_GET['view'])){
        $q="SELECT `b`.`id`,`b`.`title`
        FROM `brand` AS `b`
        LEFT JOIN `product` AS `p` ON `p`.`brand`=`b`.`id`
        WHERE `p`.`public`='on' 
        GROUP BY `b`.`id`";
        $contl='';
        if($_GET['sort']=='rus'){
            $q.="ORDER BY  `b`.`title` REGEXP '^[a-z]' ASC, `b`.`title` ASC";
            $contl=' <a href="/brand?view=abc&sort=eng" id="eng_sort" class="noactive lg_sort">анг</a> <span id="rus_sort" class="active lg_sort">рус</span>';
        }else{
            $q.="ORDER BY  `b`.`title`";
            $contl=' <span id="eng_sort" class="active lg_sort">анг</span> <a href="/brand?view=abc&sort=rus" id="rus_sort" class="noactive lg_sort">рус</a>';
        }

        $brand=DB::query($q,'a');
    }
    if($_GET['view']=='pop'){
        $q="SELECT `b`.`id`,`b`.`title`,concat(`ph`.`id`,'.',`ph`.`Photos_ex`) AS `photo`
        FROM `brand` AS `b`
        LEFT JOIN `product` AS `p` ON `p`.`brand`=`b`.`id`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`b`.`id` AND `ph`.`Photos_module`='brand'
        WHERE `p`.`public`='on' 
        GROUP BY `b`.`id`
        ORDER BY `b`.`num`";
        $brand=DB::query($q,'a');
    }
       
    $title_site.=' | Бренды';
    $description='';
    $keywords='';
    $includ_page="brands.php";
    $content="html/home_head.php";
    $title_page="Бренды"; // заголовок страницы
    
}
/* конец бренды */ 


/* страница бренда*/
if($maschpu[0]=='brand'&&!empty($maschpu[1])&&empty($maschpu[2])){
   
    $id_brand=intval($maschpu[1]);
    $q="SELECT `b`.*,concat(`ph`.`id`,'.',`ph`.`Photos_ex`) AS `photo`
        FROM `brand` AS `b`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`b`.`id` AND `ph`.`Photos_module`='brand'
        WHERE `b`.`id`='".$id_brand."' 
        LIMIT 1";
    $brand=DB::query($q,'a'); 
    if($brand){
        $brand=$brand[0];
        $q="SELECT `p`.`id`,`p`.`title`,`p`.`title_translit`,`p`.`price`,`p`.`new`,`p`.`hit`,`c`.`title` AS `ctitle`,`c`.`translit` AS `clink`,`p`.`catalog`,
                    GROUP_CONCAT( DISTINCT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) ORDER BY `ph`.`Photos_num` ASC SEPARATOR ',') as photos
        FROM `product` AS `p`
        LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`p`.`id` AND `ph`.`Photos_module`='product'
        LEFT JOIN `catalog` AS `c` ON `c`.`id` = `p`.`catalog`
        WHERE `p`.`brand`='".$id_brand."'
        GROUP BY `p`.`id`
        ORDER BY `p`.`catalog`";
        $products=$core->_list($q);
        $title_site.=' | '.$brand['title'];
        $description="";
        $keywords="";
        $includ_page="brand_show.php";
        $content="html/home_head.php";
        $title_page=$brand['title']; // заголовок страницы
        include "html/head.php";
        exit;
    }
}
/* страница бренда */

if($maschpu[0]=='testchimp')
{
    include 'html/testchimp.php';
    exit;
}

/* личный кабинет */
if($maschpu[0]=='lc')
{
    define('_LCACCESS', 1);
    
    $title_site='Личный кабинет';
    $title_page='Личный кабинет';
    if($maschpu[1]=='login')
    {
        include "html/login.php";
        exit();
    }else if(!User::check_auth())
    {
        header(("location: /"));
        exit();
    }else{//клиент авторизован
        
        
        $includ_page="lc.php";
        $content="html/home_head.php";
        $title_page='Личный кабинет'; // заголовок страницы
        include "html/head.php";
        exit();
    }
 
    

    if($maschpu[1]=='exit'){
        User::log_out();
        header('Location:/');
        exit();
    }

    if($maschpu[1]=='load')
    {
      if(file_exists("ajax/".$maschpu[2].".php"))
      {
        include("ajax/".$maschpu[2].".php");
      }else
      {
        echo "Файл не найден";
      }
      exit();
    }
}
/* конец личный кабинет */





if($maschpu[0]=='load')
{
    define('_LCACCESS', 1);
  if(file_exists("ajax/".$maschpu[1].".php"))
  {
    include("ajax/".$maschpu[1].".php");
  }else
  {
    echo "Файл не найден";
  }
  exit();
}
if($content=="")
{ 
  //include "html/error404.php";
  header("Status: 404 Not Found");
  $title_page='Страница не найдена';
  $includ_page='';
  
  include "html/error404.php";

}else
{
  include "html/head.php"; 
}

  
?>


