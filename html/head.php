<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="/favicon.png" type="image/png" />

    <link rel="stylesheet" href="/css/fonts/font.css" />
    <link href="/css/vendors/bootstrap.css" rel="stylesheet" />

    <!-- Slider style sheet -->
    <link rel="stylesheet" href="/css/vendors/slider/masterslider.css" />
    <!-- Slider skin -->
    <link rel="stylesheet" href="/css/vendors/slider/style.css" />
    <link rel="stylesheet" href="/css/vendors/slider/style2.css" />

    <link rel="stylesheet" href="/css/main.css" />

    <script src="/js/core/libs/modernizr.js"></script>
    <script src="/js/core/libs/jquery2.js"></script>
    <script src="/js/jquery.cookie.js"></script>
    <script src="/js/core/libs/easing.js"></script>
    <script src="/js/core/libs/ui.js"></script>
    <script src="/js/core/libs/device.js"></script>
    <script src="/js/core/core.js"></script>
    <script src="/js/core/default.js"></script>
    <script src="/js/core/slider.js"></script>
    <script src="/js/core/jquery.maskedinput.min.js"></script>
    <script src="/js/core/libs/bootstrap.min.js"></script>
    <script src="/js/core/libs/slider/masterslider.min.js"></script>

    <!-- Prettyphoto Lightbox jQuery Plugin -->
    <link href="/css/vendors/slider/prettyPhoto.css"  rel='stylesheet' type='text/css'/>
    <script src="/js/core/libs/jquery.prettyPhoto.js"></script>
    <!-- MasterSlider Template Style -->
    <link href='/css/vendors/slider/ms-lightbox.css' rel='stylesheet' type='text/css'>

    <?php
    if(!empty($content)){
    	include $content;
    }
    ?>   

</body>
</html>