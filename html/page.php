<div class="row content_block">

  <div class="col-md-8 col-xs-8">
    <ol class="breadcrumb">
        <li><a href="/">Главная</a></li>
        <li class="active"><?=$title_page;?></li>
    </ol>
    
  </div>
</div>

<div class="row content_block">
  <div class="col-md-12">
    <h1 class="cat_title"><?=$title_page;?></h1>
    <?=$page_info['content'];?>
  </div>
</div>
