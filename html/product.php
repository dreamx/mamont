<div class="row content_block">

  <div class="col-md-8 col-xs-8">
    <?php

      model::get_path_link($prod['catalog'],$tree,true);
    ?>
    <script type="text/javascript">
      $(document).ready(function() {
        //console.log($('.catalog_items [href="/<?php echo $tree->orig_key[$prod['catalog']]['translit'];?>"]'));
         $('.catalog_items [href="/<?php echo $tree->orig_key[$prod['catalog']]['translit'];?>"]').addClass('active_item').parents('ul').show();
      
      });
    </script>
  </div>
  <div class="col-md-4 col-xs-4">
    <div class="cat_sort">
      
    </div>
  </div>

</div>



<div class="row content_block">
  <div class="col-md-4">
    <div class="ms-lightbox-template">
      <div class="ms-showcase2-template">
                
                <div class="master-slider ms-skin-light-3" id="masterslider5">
                
           <!--        <div class="ms-slide">
                       
                      
                      <img src="/css/img/blank.gif" data-src="/img/video/bann.jpg" alt="lorem ipsum dolor sit"/>     
                       
                     
                      <video data-autopause="false" data-mute="false" data-loop="false" data-fill-mode="fit">
                          <source id="mp4" src="/img/video/WOW_web.mp4" type="video/mp4"/>
                          <source id="ogv" src="/img/video/WOW_web.ogv" type="video/ogg"/>
                          <source id="webm" src="/img/video/WOW_web.webm" type="video/webm"/>
                          
                      </video>
               
                  </div>
                   -->

                  <!-- new slide -->
                  <?php
                  if($prod['video']){
                    $output_array=array();
                    $link='';
                    preg_match("/src=\"(.*?)\"/", $prod['video'], $output_array);
                    $link=isset($output_array[1])?$output_array[1].'?':'';
                    if(preg_match("/^http:\/\/www.youtube.com(.*?)$/", $prod['video'])){
                      $link=$prod['video'];
                    }
                  ?>
                  <!-- <div class="ms-slide" attll="<?php echo $link;?>">
                       
                      
                      <img src="/css/img/blank.gif" data-src="/img/<?php echo $photos[0];?>" alt="lorem ipsum dolor sit"/>     
                       
                      
                      <img class="ms-thumb" src="/img/thumbs/<?php echo $photos[0];?>" alt="lorem ipsum dolor sit"/>
                       
                      
                      <a href="<?php echo $link;?>&hd=1&wmode=opaque&controls=1&showinfo=0" data-type="video">Youtube video</a>
                       
                  </div> -->
                  <?php
                  }
                  ?>
                  <!-- end of slide -->
                  <?php
                  foreach ($photos as $ph) {
                    ?>
                    <div class="ms-slide">
                         <img src="/css/img/blank.gif" data-src="/img/<?=$ph;?>" alt="<?=$title_page;?>"/> 
                         <img class="ms-thumb" src="/img/thumbs/<?=$ph;?>" alt="<?=$title_page;?>" /> 
                         <a href="/img/<?=$ph;?>" class="ms-lightbox" rel="prettyPhoto[<?=$title_page;?>]" title="<?=$title_page;?>"> lightbox </a>
                    </div>
                    <?php
                   } 
                  ?>
                  </div>
                  
           </div>
      </div>
            
  </div>

  <div class="col-md-8">

      <div class="row">

        <div class="col-md-12">
          <h1 class="cat_title"><?=$title_page;?></h1>
          <?php
          //$disc=$pr->getMaxDisc(time());
          ?>
        </div>

        <div class="col-md-12">
          <div class="space20"></div>
          <div class="horizontal_line"></div>
          <div class="space20"></div>
        </div>

        <div class="col-md-6">

          <div class="product_art">
              Артикул товара: <?=$prod['art'];?>
          </div>
          <?php
          
          if($prod['action']=='on'){
          ?>
          <div class="old_price" id="old_price">
              Старая цена: <span><?=intval($prod['price']);?></span> руб
          </div>
          <?php
            }
          ?>
          <input type="hidden" id="prod" value="<?=intval($prod['id']);?>">
          <input type="hidden" id="price" value="<?=intval($prod['price']);?>">
          <div class="new_price" id="new_price">
             <span><?php if($prod['action']=='on'){echo intval($prod['action_price']);}else{echo intval($prod['price']);};?></span> руб
          </div>

          <div class="product_info">
               <?=$prod['short_desc'];?>
          </div>

          <div class="product_shipping_info">
            Информация о доставке именно этого товара
          </div>

          <div class="social_api">
            Социальные апи
          </div>

        </div>

        <div class="col-md-6">

          <div class="row">

              <div class="col-md-12">

              <div class="card_block">

                <div id="card_product_block">
                  <?php
                  /*
                  //print_r($pr);
                  if($pr->chars){
                  ?>
                  <div class="product_select">
                    <center>
                      <?php
                      foreach ($pr->chars as $ch => $values) {
                          $first=current($values);
                          //если есть фото делаем список с картинками
                          if(!empty($first['photo']))
                          {
                            ?>
                            <label><?=$ch;?></label>
                            <div class="dropdown select_color">
                              <input type="hidden" class="sel_val" rel="<?=intval($first['price']);?>" value="<?=key($values);?>">
                              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                <span class="select_cont">
                                <img width="40" src="/img/thumbs/<?=$first['photo'];?>"><?=$first['val'];?>
                                </span>
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <?
                                   foreach ($values as $id => $value) {
                                    ?>
                                    
                                    <li rel="<?=intval($value['price']);?>" value="<?=$id;?>" role="presentation"><img width="40" src="/img/thumbs/<?=$value['photo'];?>"> <?=$value['val'];?></li>
                                    <?
                                   }
                                  ?>
                              </ul>
                            </div>
                 <?php
                          }else if(!empty($first['valc']))//если есть номер цвета делаем список с квадратиками этого цвета
                          {
                            ?>
                            <label><?=$ch;?></label>
                            <div class="dropdown select_color">
                              <input type="hidden" class="sel_val" rel="<?=intval($first['price']);?>" value="<?=key($values);?>">
                              <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                <span class="select_cont">
                                <span class="color_label" style="background:<?=$first['valc'];?>;"></span><?=$first['val'];?>
                                </span>
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <?
                                   foreach ($values as $id => $value) {
                                    ?>
                                    
                                    <li rel="<?=intval($value['price']);?>" value="<?=$id;?>" role="presentation"><span class="color_label" style="background:<?=$value['valc'];?>;"></span> <?=$value['val'];?></li>
                                    <?
                                   }
                                  ?>
                              </ul>
                            </div>
                 <?php
                          }else{
                           ?>
                           <label><?=$ch;?></label>
                           <select class="form-control product_features"> 
                           <?
                           foreach ($values as $id => $value) {
                            ?>
                            <option rel="<?=intval($value['price']);?>" value="<?=$id;?>"><?=$value['val'];?></option>
                            <?
                           }
                           ?>
                           </select>
                           <?php
                          }
                       } 
                      ?>
                      
                    </center>
                  </div>
                  <?php
                  }
                  */
                  ?>

                  <div class="product_quantity"><label>Количество</label></div>

                  <center>
                  <div class="product_quantity_select">

                   <button class="add_plus" id="add_plus" >+</button><input type="text" class="form-control add_quantity num" id="count_prod" placeholder="1" value="1"><button class="add_plus" id="del_minus">-</button>

                  </div>
                  </center>

                  <div class="clear"></div>

                  <div class="product_add_to_cart" id="product_add_to_cart">
                    
                    <center><span class="btn_add_to_cart" onClick="addProd();">В корзину</span></center>

                  </div>
                  <span id="mes_add" style="display:none;"></span>

                   <div class="buy_click">

                    <center><span onclick="" data-toggle="modal" data-target="#one_click_modal">Купить в один клик</span></center>

                  </div>

                  <div class="payment_method"></div>

                  <div class="space20"></div>
                    <center><div class="product_horizontal_line"></div></center>

                  <!-- <div class="product_comparison">

                    <center><span onclick="login();">Добавить к сравнению</span></center>

                  </div> -->

                  <div class="product_shipping">

                    <center><span data-toggle="modal" data-target="#shipping_modal">Информация о доставке</span></center>

                  </div>
  
                </div>

              </div>

              </div>

          </div>

        </div>

     </div>

  </div>

</div>

<div class="row content_block">

  <div class="col-md-8">

    <ul class="nav nav-tabs" role="tablist" id="produst_tab">
      <!-- <li role="colors"><a href="#colors" aria-controls="colors" role="tab" data-toggle="tab">Расцветки</a></li> -->
      <?php
      if($prod['video']){ 
      ?>
      <li role="presentation" class="active"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Видео</a></li>
      <?php 
    }else{
      $cl='active';
    }
    ?>
      <li role="presentation" class="<?=$cl;?>"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Описание и характеристики</a></li>
      <!-- <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Обсуждение (<?=count($quest_products);?> ответа)</a></li> -->
      <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" class="reviews">Отзывы (<?=count($reviews_products);?> отзывов)</a></li>
    </ul>

    <div class="tab-content">
      <div role="tabpanel" class="tab-pane" id="colors">
        <div class="row">
          <div class="col-md-3">
            <div class="colors_img">
              <img src="/img/thumbs/1790.png" class="img-thumbnail">
            </div>
          </div>
          <div class="col-md-6">2</div>
          <div class="col-md-3">3</div>
        </div>
      </div>
      <?php
      if($prod['video']){ 
      ?>
      <div role="tabpanel" class="tab-pane active" id="profile">
        <?php
        
          echo $prod['video'];
          ?>

      </div>
      <?php } ?>
      <div role="tabpanel" class="tab-pane <?=$cl;?>" id="home"><?=$prod['desc'];?></div>
      
      <div role="tabpanel" class="tab-pane" id="messages">
        <?php
        if($reviews_products){
          foreach ($reviews_products as $review) {
            ?>
            <div>
              <b>дата отзыва:</b><?=$review['date'];?><br>
              <b>имя:</b><?=$review['name'];?><br>
              <b>оценка:</b><?=$review['bal'];?><br>
              <b>Отзыв:</b><?=$review['review'];?><br>
            </div>
            <?
          }
        }else{
          echo "Нет отзывов";
        }
        ?>
      </div>
      <div role="tabpanel" class="tab-pane" id="settings">
        <?php
        if($quest_products){
          foreach ($quest_products as $quest) {
            ?>
            <div>
              <b>дата вопроса:</b><?=$quest['date_quest'];?><br>
              <b>имя:</b><?=$quest['name'];?><br>
              <b>Вопрос:</b><?=$quest['quest'];?><br>
              <b>дата ответа:</b><?=$quest['date_reply'];?><br>
              <b>ответ:</b><?=$quest['reply'];?><br>
            </div>
            <?
          }
        }else{
          echo "Нет вопросов";
        }
        ?>
      </div>
    </div>

  </div>

  <div class="col-md-4">
    <?php 
    //если есть сопутствующие товары
    if($dop_products){
      ?>
    <center><div class="product_recommend">Рекомендуемые товары</div></center>
    <?php 
      foreach ($dop_products as $prod) {
                $ph=explode(',',$prod['photos']);
                ?>
                <div id="product_list">
              <div id="product_block">
               <div class="product_img">
                <a href="/<?=$prod['title_translit'];?>">
                <img src="/img/thumbs/<?=$ph[0];?>" class="img-rounded"></a>
               </div>
               <div class="product_name">
                <a href="/<?=$prod['title_translit'];?>">
                 <?=$prod['title'];?>
               </a>
               </div>
               <div class="product_bottom_line">
               </div>
               <div class="product_price">
                <?=intval($prod['price']);?> руб
               </div>
               <div class="add_cart">
                <span id="add_shoping_cart" class=""></span>
               </div>
               <div class="clear"></div>
              </div>
            </div>
            <?
            }
  } ?>
  </div>
</div>


<div class="modal fade" id="one_click_modal" tabindex="-1" role="dialog" aria-labelledby="one_click_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="userLabel">Быстрое оформление заказа</h4>
      </div>
      <div id="one_click_body" class="modal-body">
        <form>
          
        <div class="row">

          <div class="col-md-4 input_block">
            
            <input name="" id="client_name" class="form-control input user_input_info" type="text" placeholder="Ваше имя">

          </div>

          <div class="col-md-4 input_block">
           
            <input name="" id="client_phone" class="form-control input user_input_info" type="text" placeholder="Ваш номер телефона">

          </div>

          <div class="col-md-4 input_block">
           
            <input name="" id="client_email" class="form-control input user_input_info" type="text" placeholder="Ваш email">

          </div>

        </div>
        <div class="space10"></div>
        <div class="row">
          <div class="col-md-6 input_block">
            <label>Как Вы хотите получить заказ:</label>
            <select class="form-control user_city input" id="dost">
              <?php
              if($transport){
                foreach ($transport as $trans) {
                  ?>
                  <option price="<?php echo $trans['price'];?>" value="<?php echo $trans['id'];?>"><?php echo $trans['title'];?></option>
                  <?
                }
              }
              ?>
              
            </select>
          </div>
          <!-- <div class="col-md-6 input_block">
            <label>Как Вы хотите оплатить заказ:</label>
            <select class="form-control user_city input">
              <option>Наличными</option>
              <option>Банковской картой</option>
            </select>
          </div> -->
          <div class="col-md-6 tright">
           <h4 class="one_click_h4" >Стоимость доставки</h4>
          </div>
          <div class="col-md-6 tright">
                 <h4 class="one_click_h4" ><span id="transport_price_modal"><?php echo $transport[0]['price'];?></span> руб.</h4> 
          </div>
          
        </div>
        <div class="space20"></div>
        <div class="row">
            <div class="col-md-3 one_click_modal_img">
              
              <img src="/img/thumbs/<?php echo $photos[0]; ?>" class="img-thumbnail">
             
            </div>
            <div class="col-md-4">
              <h4 class="one_click_h4"><?=$title_page;?></h4>
            </div>
            <div class="col-md-5">
              <div class="row">
                <div class="col-md-6"></div>
                <div class="col-md-6 tright one_click_total">
                   <input type="number" id="count_prod_modal" class="form-control tcenter count_prod num" placeholder="1" value="1">
                </div>
                <div class="col-md-12 tright">
                 <h4 class="one_click_h4" id="product_price_modal"></h4>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 tright">
                 <h4 class="one_click_h4" id="itog_price_modal">Итого: <span></span> руб.</h4>
                </div>
              </div>
              
            </div>
            
          </div>
          
        </form>
      </div>
      <div class="modal-footer">
        <div id="mess"></div>
        <button type="button" id="send_order" class="btn btn_order">Оформить заказ</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="shipping_modal" tabindex="-1" role="dialog" aria-labelledby="shipping_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="userLabel">Информация о доставке</h4>
      </div>
      <div id="shipping_body" class="modal-body">
        <h4 class="c333">Доставка курьером (по г. Рязань)</h4>
        <p></p>
      </div>
      <div class="modal-footer">
        <p>В интернет-магазине “Мамонтёнок” вы найдете товары от более 100 ведущих производителей, таких как: Maxi-Cosi, Romer, Chicco, Baby Care, Inglesina, Jetem, Avent и многих других. Ваш выбор не ограничен наличием товара на складе, мы привезем любой понравившийся вам товар.</p>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">      
 
    var slider = new MasterSlider();
    var pr='';
    <?php
      /*if($disc['edinici']=='perc'){
        ?>
        pr=<?=$disc['value'];?>;
        function getNewprice(price){
          return price-(price*pr/100);
        }
        <?
      }else if($disc['edinici']=='rub'){
        ?>
        pr=<?=$disc['value'];?>;
        function getNewprice(price){
          return price-pr;
        }
        <?
      }else{?>
        function getNewprice(price){
          return price;
        }
        <?php
      }*/
    ?>
    slider.control('arrows'); 
    slider.control('lightbox');
    slider.control('thumblist' , {autohide:false ,dir:'h',align:'bottom', width:110, height:120, margin:10, space:0 , hideUnder:400});
 
    slider.setup('masterslider5' , {
        width: 650,
        layout: "fillwidth",
        height: 470,
        space:0,
        loop:true,
        view:'fade',
        heightLimit: 470,


    });

    /*function refreshPrice(){
      var price=parseInt($('#price').val());
      var chars=$('.product_select select option:selected,.product_select input.sel_val');
      var price_chars=0;
      $.each(chars, function(index, val) {console.log(parseInt($(val).attr('rel')));console.log($(val));
         price_chars+=parseInt($(val).attr('rel'));
      });
      $('#old_price span').html(price+price_chars);
      $('#new_price span').html(parseInt(getNewprice(price+price_chars)));
    }*/

    function itog_modal(){
      var count=parseInt($('#count_prod_modal').val());
      var price=parseFloat($('#product_price_modal span').html());
      var dost=parseFloat($('#transport_price_modal').html());
      $('#itog_price_modal span').html(count*price+dost);
    }
     
    $(document).ready(function(){
        $('#client_phone').mask("+7 (999) 999 99 99");
        //refreshPrice();

        $('#dost').change(function(event) {
          $('#transport_price_modal').html($(this).find('option:selected').attr('price'));
          itog_modal();
        });

        /*$('.select_color .dropdown-menu li').click(function(){
          var parent=$(this).parents('.select_color').eq(0);
          parent.find('.select_cont').html($(this).html());
          var sel=parent.find('input.sel_val').eq(0);
          sel.attr('rel',$(this).attr('rel'));
          sel.val($(this).attr('value'));
          refreshPrice();
        });

        $('.product_select select').change(function(event) {
          refreshPrice();
        });*/

        $('#count_prod,#count_prod_modal').change(function(event) {
          if(!isNumber($(this).val())||$(this).val()==''||$(this).val()<=0){
            $(this).val(1);
          }
        });
        $('#count_prod_modal').change(function(event) {
          itog_modal();
        });
        $('#add_plus').click(function(){
          var c=parseInt($('#count_prod').val());
          $('#count_prod').val(c+1);
        });
        $('#del_minus').click(function(){
          var c=parseInt($('#count_prod').val());
          if((c-1)<1){
            $('#count_prod').val(1);
          }else{
            $('#count_prod').val(c-1);
          }
        });
        $("a[rel^='prettyPhoto']").prettyPhoto();
        $('#produst_tab a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        });


        $('#one_click_modal').on('show.bs.modal', function (e) {
            $('#count_prod_modal').val($('#count_prod').val());
            $('#product_price_modal').html($('#new_price').html());
            itog_modal();
        });

        $('#send_order').click(function(event) {

          var bt=$(this);
          var cart=[];
          var pr={};
          pr['prod']=$('#prod').val();
          pr['count']=$('#count_prod_modal').val();
          //var chars=$('.product_select select option:selected,.product_select input.sel_val');
          //pr['chars']=[];
          // $.each(chars, function(index, val) {
          //    pr['chars'].push($(val).val());
          // });
          cart.push(pr);
          bt.hide();
          var st={};
          st['cart']=JSON.stringify(cart);
          st['dost']=$('#dost').val();
          st['pay_type']=$('#pay_type').val();
          st['name']=$('#client_name').val();
          st['phone']=$('#client_phone').val();
          st['email']=$('#client_email').val();
          if(st['name']==''){
            $('#mess').html('<div class="alert alert-danger" role="alert">Введите Ваше Имя</div>');
            bt.show();
            return false;
          }
          if(st['phone']==''){
            $('#mess').html('<div class="alert alert-danger" role="alert">Укажите номер телефона</div>');
            bt.show();
            return false;
          }
           if(st['email']==''){
            $('#mess').html('<div class="alert alert-danger" role="alert">Укажите Ваш email-адрес</div>');
            bt.show();
            return false;
          }

          //console.log(st);
          $.post('load/send_order', st, function(data) {//alert(data);
            try{
              data=JSON.parse(data);
              if(data.status){
                //clearCart();
                //window.location.href="/thankyou";
                $('#mess').html('<div class="alert alert-success" role="alert">Заказ принят! Спасибо за Ваш выбор. <br>Совсем скоро с Вами свяжется менеджер.</div>');
              }else{
                bt.show();
              }
            }catch(e){
              bt.show();
            }
          });

        });
    });


     
</script>
