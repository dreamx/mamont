<div id="thankyou">
	<div class="container">
		
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="top_text tcenter">
					<div class="main_logo" onclick="top.location.href = '/'"></div> 
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="top_text tcenter">
					<h1>Спасибо за заказ!</h1>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<div class="top_text tcenter">
					<h4>Номер вашего заказа: 234522</h4>
					<br>
					<p class="text_l">(Следите за статусом заказа в личном кабинете)</p>
					<br><br>
					<h4>В ближайшее время мы свяжемся с вами!</h4>
					<h4>Благодарим что выбрали интернет-магазин Мамонтенок!</h4>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="middle_text tcenter">
					<a href="/" class="btn btn_order" role="button">Вернуться в магазин</a>
					<a href="/lc" class="btn btn_enter" role="button">Перейти личный кабинет</a>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>	
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="middle_text tcenter">
				
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>	
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="middle_text tcenter">
					<h4>Получите доступ к дополнительным скидкам Мамонтенка, а также первым узнавать о новинках и акциях нашего интернет-магазина! Достаточно указать Ваш email адрес:</h4>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>	
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="input_text tcenter">
					<input id="add_mail_list" type="text" class="form-control input  user_input_info num" placeholder="Введите email-адрес">
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>	
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="input_text tcenter">
					<a href="#" class="btn btn_order" role="button">Отправить</a>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>	
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				<div class="middle_text tcenter">
				
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>	
	</div>
</div>

