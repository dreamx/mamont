<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="/favicon.png" type="image/png" />

    <link rel="stylesheet" href="/css/fonts/font.css" />
    <link href="/css/vendors/bootstrap.css" rel="stylesheet" />

    <link rel="stylesheet" href="/css/vendors/cal/glDatePicker.default.css" />
    <link rel="stylesheet" href="/css/vendors/cal/glDatePicker.flatwhite.css" />
    <link rel="stylesheet" href="/css/kendo.common-bootstrap.min.css" />
    <link rel="stylesheet" href="/css/main.css" />

    <script type="text/javascript" src="/js/core/libs/modernizr.js"></script>
    <script type="text/javascript" src="/js/core/libs/jquery2.js"></script>
    <script type="text/javascript" src="/js/jquery.cookie.js"></script>
    <script type="text/javascript" src="/js/core/libs/easing.js"></script>
    <script type="text/javascript" src="/js/core/libs/ui.js"></script>
    <script type="text/javascript" src="/js/core/libs/device.js"></script>
    <script type="text/javascript" src="/js/kendo.all.min.js"></script>
    <script type="text/javascript" src="/js/core/core.js"></script>
    <script type="text/javascript" src="/js/core/default.js"></script>
    <script src="/js/core/jquery.maskedinput.min.js"></script>

    <script src="/js/core/libs/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="/js/core/libs/cal/glDatePicker.js"></script>

    <?php
    if(!empty($content)){
    	include $content;
    }
    ?>   

</body>
</html>