
<div id="top_bar">

    <div class="row">
  
        <div class="col-md-8">
        
        <nav class="navbar navbar-default">
          <div class="container-fluid" style="z-index: 2000">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                 <div class="collaps_contact"><span class="top_name_mobile">Интернет-магазин Мамонтенок</span><!-- <span class="top_phone_mobile">8-800-256-54-41</span> --><div class="clear"></div></div>
              <button type="button" class="navbar-toggle collapsed mobile_collapsed" data-toggle="collapse" data-target="">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Интернет-магазин детских товаров Мамонтенок</a>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="mobile_toggle">
              <ul class="nav navbar-nav">
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Каталог товаров <span class="caret"></span></a>
                  <ul class="dropdown-menu" role="menu">
                    <li><a href="#">Something else here</a></li>
                    <li><a href="#">Separated link</a></li>
                    <li class="divider"></li>
                    
                  </ul>
                </li>  
                <li class="more_info"><a href="/brand">Бренды</a></li>
                <li class="more_info"><a href="#">Акции и скидки</a></li>
                <li class="more_info"><a href="#">Новинки</a></li>
                <li class="more_info"><a href="#">Хиты продаж</a></li>
                <li class="active"><a href="/page/36-dostavka_i_oplata">Доставка и оплата <span class="sr-only">(current)</span></a></li>
                <li class="downmenu"><a href="/page/42-garantiya_i_vozvrat">Гарантия и возврат</a></li>
                <li class="downmenu"><a href="/page/43-o_magazine">О магазине</a></li>
                <li class="downmenu"><a href="/page/44-kontakty">Контакты</a></li>
              </ul>
              <!-- City -->
              <!-- <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link</a></li>
              </ul> -->
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
         
        </div>
        <div class="col-md-4">
          <?php
          if(empty($id_client)){
            ?>
          <div id="user_cab" data-toggle="modal" data-target="#user_cab_modal"></div>
          <?php }else{
            ?>
            <div id="user_login"><span class="mif-user"> <a id="" href="/lc" >Личный кабинет</a></span> &nbsp;&nbsp;&nbsp; <span class="mif-cancel " id="user_exit"><a> Выход</a></span></div>
            <?php
          }
          ?>
          
          <!-- <div id="user_call"></div> -->

          <div id="adapt_cart" class="cart_right_screen">

                <a href="/shoping_cart">

                  <div id="shoping_cart">
                    <div class="fixed_cart">
                      <span></span> 
                    </div>
                  </div>

                </a>

          </div>

        </div>
        
    </div>   

</div>


<div class="top_pad"></div>


    <div id="left_menu">
                 
      <div class="main_logo" onclick="top.location.href = '/'"><!-- <span class="seo">Интернет-магазин детских товаров в Рязани Мамонтенок</span> --></div> 

                   
      <div class="main_catalog">
        <?php //var_dump($menu);
        if($menu){
          function childs($arr,$parent){
            foreach ($arr as $ar) {
              if($ar['parent']==$parent)
              {
                return true;
              }
            }
            return false;
          }
          function get_sub($arr,$parent){
            foreach ($arr as $ar) {
              if($ar['parent']==$parent){
                if(childs($arr,$ar['id']))
                {?>
                  <li class="items"><?php echo $ar['title'];?> <span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span>
                    <ul class="subs">
                    <?php
                    get_sub($arr,$ar['id']);
                    ?>
                    </ul>
                  </li>
                <?php
                }else{
                ?>
                <li><a href="/<?php echo $ar['translit'];?>"><?php echo $ar['title'];?></a></li> 
                <?
                }
              }
            }
          }





        ?>
        <ul class="catalog_items">
          <?php
          $i=0;
          /*foreach ($menu as $ar) {

              if($ar['parent']==0)
              {
                $i++;
                ?>
                <li class="items eee<?php echo $i;?>"><?echo $ar['title'];?>
                <?
                get_sub($menu,$ar['id']);
                ?>
                </li>
                <?php
              }
              
            }*/
          get_sub($menu,0);
          ?>
          <li class="items last">&nbsp;</li>
        </ul>  

           <!-- <ul class="catalog_items">
               <li class="items food">Питание и кормление</li>
                <ul class="subs">
                  <li> Стульчики для кормления <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span></li>
                </ul>
               <li class="items clears">Подгузники и гигиена</li>
               <li class="items floor">Детская комната</li>
               <li class="items adv">Прогулки и путешествия</li>
               <li class="items sport">Спорт и отдых</li>
               <li class="items games">Игры и игрушки</li>
               <li class="items sertificat">Подарочные сертификаты</li>
               <li class="items last">&nbsp;</li>
           </ul> -->
           <?php
         }
           ?>
      </div>
      
      <div id="smm">
           <div class="smm_text">
           Только для друзей Мамонтенка - эксклюзивные цены, приятные сюрпризы и увлекательные конкурсы для родителей и их малышей.
           </div>
           <div class="smm_icons">
           </div>
           <div class="smm_action">
           Присоединяйтесь!
           </div>
           <div class="smm_arrow_left"></div>
           <div class="smm_arrow_right"></div>
           <div class="smm_vk_icon"></div>
           <!-- <div class="smm_insta_icon"></div> -->   
      </div>
      
      <div id="footer_map">
           <div class="footer_text">
           390000, Рязань,<br>
           Горького, 30
           </div>
           <div class="map">
           <span onclick="top.location.href='/page/44-kontakty'">Посмотреть на карте</span>
           </div>
      </div> 
                    
    </div>

    <div id="content">
    
         
         <div class="row content_block">
              
              <div class="col-md-6 col-sm-12 a_logo">

                <div class="main_logo_v" onclick="top.location.href = '/'"><!-- <span class="seo">Интернет-магазин детских товаров в Рязани Мамонтенок</span> --></div> 
              
                   <div id="main_menu">
                     <div class="main_logo_v_l" onclick="top.location.href = '/'"><!-- <span class="seo">Интернет-магазин детских товаров в Рязани Мамонтенок</span> --></div> 
                        <ul class="horizontal_menu">
                          <li><a href="#" class="general_catalog open_catalog">Каталог в один клик</a></li>
                          <li><a href="/brand" >Бренды</a></li>
                          <li><a href="/actions">Акции</a></li>
                          <li><a href="/new">Новинки</a></li>
                          <li><a href="/hits">Хиты продаж</a></li>
                        </ul>
                        <div class="clear"></div>
                   </div>
              
              </div>
              <!-- контенер для брендов -->
              <div id="show_content" style="display:none;">
              </div>
              <!-- /контенер для брендов -->

              <div class="col-md-6 col-xs-6">
                   
                   <div id="search_form">
                        
                        <div class="row">
                             <div class="col-md-6 main_search">
                                <form action="/search" method="GET">
                                      <input name="f" class="form-control input-lg input_search" type="text" placeholder="Что будем искать?">
                                      <input type="submit" class="ico_search" value="">
                                </form>
                             </div>
                             <div class="col-md-6">
                                  <center>
                                    <div id="phone_free"><span>8 (4912) 21-27-47 <span><a data-toggle="modal" data-target="#recall_modal">Обратный звонок</a></span></span></div>
                                  </center>
                             </div>
                             
                        </div>     
                             
                   </div>
                   
              </div>
              
         </div>

      </div>

       <div id="general_catalog_big">

              <div class="close_catalog"><div class="close_catalog_button object05"></div></div>

              <div class="row">

                  <div id="menu_cat"class="col-md-3 cat_left_bg">

                     <div class="big_cat_name">Категория</div>

                       <ul>
                        <?php
                          if($menu){
                            foreach ($menu as $m) {
                              if($m['parent']==0){
                             ?>
                            <li class="p_item" id="p<?php echo $m['id'];?>">
                              <span class="small_cat_name"><?php echo $m['title'];?></span> <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                              <div class="clear"></div>
                            </li>
                             <?php
                              }
                            }
                        }
                        ?>
                       </ul>
                  </div>
                  <div id="menu_sub_cat" class="col-md-3 cat_left_bg">

                      <div class="big_cat_name ">Подкатегория</div>
                        <?php
                          if($menu){
                            foreach ($menu as $m) {
                              if($m['parent']==0){
                                if(childs($menu,$m['id'])){
                              ?>
                              <div class="chidls" id="item<?php echo $m['id'];?>">
                                <ul>
                                <?php
                                foreach ($menu as $it) {
                                  if($it['parent']==$m['id']){
                                  ?>
                                  
                                    <?php
                                    if(childs($menu,$it['id'])){
                                      ?>
                                      <li class="ch_item" id="ch_<?php echo $it['id'];?>">
                                    <span class="small_cat_name"><?php echo $it['title'];?></span> <span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                                    <div class="clear"></div>
                                      <ul>
                                      <?
                                      foreach ($menu as $chh) {
                                        if($it['id']==$chh['parent']){
                                        ?>
                                        <li class="ch_ch" id="$chh_<?php echo $chh['id'];?>">
                                        <a href="/<?=$chh['translit'];?>" class="small_cat_name"><?php echo $chh['title'];?></a> 
                                        <div class="clear"></div>
                                        </li>
                                        <?php
                                        }
                                      }
                                      ?>
                                      </ul>
                                    </li>
                                      <?php
                                    }else{
                                      ?>
                                      <li class="ch_item" id="ch_<?php echo $it['id'];?>">
                                        <a href="/<?=$it['translit'];?>" class="small_cat_name"><?php echo $it['title'];?></a> 
                                        <div class="clear"></div>
                                      </li>
                                    <?
                                    }
                                    ?>
                                  
                                  <?php
                                  }
                                }
                                ?>
                              </ul>
                              </div>
                             <?php
                               }
                              }
                            }
                        }
                        ?>
  
                  </div>
                  <div id="menu_brand" class="col-md-3 cat_left_bg">
  
                    
                  </div>
                  <div id="menu_action"  class="col-md-3">

                      
                  </div>

              </div>

           </div>


           <div id="mobile_footer">

           </div>




<div class="modal fade" id="user_cab_modal" tabindex="-1" role="dialog" aria-labelledby="user_cab_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="userLabel">Вход / Регистрация</h4>
      </div>
      <div class="modal-body">
        
         <form>
          
        <div class="row">
          <div class="col-md-5 input_block">
            <input name="" id="email_login" class="form-control input user_input_info" type="text" placeholder="Ваш email">
          </div>
          <div class="col-md-5 input_block">
            <input name="" id="pass_login" class="form-control input user_input_info" type="password" placeholder="Ваш пароль">
          </div>
          <div class="col-md-2 input_block">
            <button type="button" id="login_but" class="btn btn_enter">Войти</button>
          </div>
          <div class="col-md-12" id="mess_login">
          </div>
          <div class="col-md-6 input_block">
            <a id="recovery_modal_trigger" class="btn btn_lostpass" data-dismiss="modal">Забыли пароль?</a>
          </div>
        </div>

      </form>

        
      </div>
      <div class="modal-footer">
        <h4 class="register_text"><span>Получайте скидки и подарки</span></h4><button type="button" id="show_rigistration" class="btn btn_order fright">Регистрируйтесь</button>
        <div class="clear"></div>
        <div id="registration_body">
          <div class="row">
            <div class="col-md-12 tleft p_bonus_info">
              Регистрация в нашем интернет-магазине занимает 10 секунд, после чего Вам станут доступны скидки и возможность узнавать о выгодных предложениях от Мамонтенка!
            </div>
          </div>
          <div class="space20"></div>
          <form>
            <div class="row">
              <div class="col-md-6 input_block">
                <input name="" id="name_reg" class="form-control input user_input_info" type="text" placeholder="Ваше Имя">
              </div>
              <div class="col-md-6 input_block">
                <input name="" id="email_reg" class="form-control input user_input_info" type="text" placeholder="Ваш email">
              </div>
            </div>
             <div class="row">
              <div class="col-md-6 input_block">
                <input name="" id="pass_reg" class="form-control input user_input_info" type="password" placeholder="Придумайте пароль">
              </div>
              <div class="col-md-6 input_block">
                <input name="" id="pass_test_reg" class="form-control input user_input_info" type="password" placeholder="Повторите пароль">
              </div>
            </div>
            <div class="space20"></div>
             <div class="row">
              <div class="col-md-12" id="mess_reg">
                
              </div>
              <div class="col-md-12 input_block tcenter">
                <button type="button" id="reg_but" class="btn btn_enter">Зарегистрироваться</button>
              </div>
            </div>
          </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="recall_modal" tabindex="-1" role="dialog" aria-labelledby="recall_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="userLabel">Мы перезвоним</h4>
      </div>
      <div class="modal-body">

        <form>
          
        <div class="row">
          <div class="col-md-6 input_block">
            
            <input name="" id="recall_name" class="form-control input user_input_info" type="text" placeholder="Ваше Имя">

          </div>
          <div class="col-md-6 input_block">
           
            <input name="" id="recall_phone" class="form-control input user_input_info" type="text" placeholder="+7 (  ) ___ __ __" pattern="+7 ([0-9]{3}) [0-9]{3} [0-9]{2} [0-9]{2}">
          </div>
        </div>

      </form>

      </div>

      <div class="modal-footer">
        <div id="recall_mess"></div>
        <button type="button" data-loading-text="Отправка..." id="send_phone" class="btn btn_enter">Отправить</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="recovery_modal" tabindex="-1" role="dialog" aria-labelledby="recovery_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="userLabel">Восстановление пароля</h4>
      </div>
      <div class="modal-body">

        <div id="recovery_body">
            <div class="row">
              <div class="col-md-12 tcenter">
                <p>Укажите Ваш email-адрес, и мы вышлем инструкцию по востановленю пароля.</p>
              </div>
            </div>
            <div class="space20"></div>
          </div>

        <form>
          
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6 input_block">
            
            <input name="" class="form-control input user_input_info" type="text" placeholder="Ваш email-адрес">

          </div>
          <div class="col-md-3"></div>
        </div>

      </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn_enter">Восстановить</button>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
 $(document).ready(function(){

  $('#recall_phone').mask("+7 (999) 999 99 99");
  $('#send_phone').click(function(){
    var el=$(this);
    el.button('loading');
    var ph=$('#recall_phone').val();
    var nm=$('#recall_name').val();
    $('#recall_mess').html("");
    if(ph==''||nm==''){
      el.button('reset');
      $('#recall_mess').html("Заполните все поля");
      return false;
    }
    $.post('/load/send_mess', {phone:ph,name:nm }, function(rez) {
      try{
        data=$.parseJSON(rez);
        $('#recall_mess').html(data.html);
        if(data.status){
          el.remove();
        }else{
          el.button('reset');
        }
      }catch(e){
         el.button('reset');
      }
      
    });
  });
  $('#reg_but').click(function(){
    var name=$('#name_reg').val();
    var pass=$('#pass_reg').val();
    var pass_test=$('#pass_test_reg').val();
    var email=$('#email_reg').val();
    if(pass!==pass_test){
     $('#mess_reg').html("пароли не совпадают!");
      return false;
    }
    registr(email,pass,name);
  });
    $('#user_exit').click(function(){
      logout();
      location.reload();
    });
 }); 

  $('#login_but').click(function(event) {
    var pass=$('#pass_login').val();
    var email=$('#email_login').val();
    login(email,pass);
  });

  $('#recovery_modal_trigger').click(function() {
    $('#recovery_modal').modal();
  });

  var path=location.pathname;
  $('.catalog_items [href="'+path+'"]').addClass('active_item').parents('ul').show();
  $('.catalog_items > li').each(function(index, el) {
    $(el).addClass('item_ico'+(index+1));
  });
  $('#brands').click(function(){
    brand_show('abc','eng');
    return false;
  });
  $('#show_content').on('click','.lg_sort',function(){
    brand_show('abc',$(this).attr('id').slice(0, 3));
  });
  $('#show_content').on('click','#pop_sort',function(){
    brand_show('pop','');
    return false;
  });

  function brand_show(view,sort){
    $.post('/load/brands', {view: view,sort:sort}, function(res) {
      $('#show_content').html(res);
      
    });
  }
  $('#registration_body').hide();

  $('#show_rigistration').click(function(){
    $('#registration_body').show();
  });
  

</script>