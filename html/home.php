
<div class="space20"></div>

      <div class="space20"></div>
      
           <div class="row content_block">
           
                <div class="col-md-8 mainlists">
                    <?php  
                    if($slide){
      
                        
                    ?>
                    <!-- masterslider -->
                    <div class="master-slider ms-skin-light-3" id="masterslider">
                        <!-- new slide -->
                        <?php 
                        foreach ($slide as $sl) {
                            ?>
                        <div class="ms-slide" data-delay="6">
                             
                                <h1 class="ms-layer hps-amp"
                                    style="left: 50px; top: 50px;"
                                    data-type="text"
                                    data-delay="0"
                                    data-duration="700"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeOutExpo"
                                >
                                   <?php echo $sl['title'];?>
                                </h1>

                                <h2 class="ms-layer hps-amp"
                                    style="left: 140px; top: 170px;"
                                    data-type="text"
                                    data-delay="50"
                                    data-duration="700"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeOutBack"
                                >
                                    <?php echo $sl['small_desc'];?>
                                </h2>

                                <h2 class="ms-layer hps-amp"
                                    style="left: 140px; top: 270px;"
                                    data-type="text"
                                    data-delay="50"
                                    data-duration="700"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeOutBack"
                                >
                                   <span style="font-size: 18px; font-family: 'houschka_roundeddemibold'; text-transform: uppercase;">Стоимость сегодня всего: </span>
                                </h2>

                                <h3 class="ms-layer hps-amp"
                                    style="left: 176px; top: 300px;"
                                    data-type="text"
                                    data-delay="100"
                                    data-duration="700"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeInOutBack"
                                >
                                     <?php echo $sl['price'];?>
                                </h3>
                              <?php
                              $ph=explode(',',$sl['photos']);
                              ?>
                                <img src="/css/img/blank.gif" data-src="/img/<?php echo $ph[0];?>" alt="<?php echo $sl['title'];?>"
                                  style="right:-20px; top:0px; z-index:2;"
                                  class="ms-layer"
                                  data-type="image"
                                  data-delay="200"
                                  data-duration="1500"
                                  data-ease="easeOutExpo"
                                  data-effect="scalefrom(1.3,1.3,0,0)"
                                />

                                <!-- <h4 class="ms-layer hps-amp"
                                    style="left: 140px; top: 380px;"
                                    data-type="text"
                                    data-delay="100"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeInOutBack"
                                >
                                     Подробнее
                                </h4> -->

                                <!-- linked slide -->
                                <a href="/<?php echo $sl['title_translit'];?>"><?php echo $sl['title'];?></a>

                        </div>
                        <?php
                    }
                        ?>
                        <!-- end of slide -->
                    </div>
                    <!-- end of masterslider -->
                    <?php }
                    ?>
                </div>
                
                <div class="col-md-4 morelist">
                
                    <div class="row">

                        <div class="col-md-12 hitsales">
                            <?php
                    if($new){
                             
                    ?>
                          <!-- masterslider hit -->
                          <div class="master-slider ms-skin-light-5" id="masterslidernew">
                              <!-- new slide -->
                              <?php 
                              foreach ($new as $n) {
                                 
                              ?>
                              <div class="ms-slide" data-delay="12">
                             
                               <h5 class="ms-layer hps-amp-new"
                                    style="left: 20px; top: 20px;"
                                    data-type="text"
                                    data-delay="0"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeOutExpo"
                                >
                                    Новинка! 
                                </h5>

                                <h1 class="ms-layer hps-amp-name"
                                    style="left: 20px; top: 50px;"
                                    data-type="text"
                                    data-delay="0"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeOutExpo"
                                >
                                   <?php echo $n['title'];?>
                                </h1>

                                <h2 class="ms-layer hps-amp-info"
                                    style="left: 20px; top: 90px;"
                                    data-type="text"
                                    data-delay="50"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeOutBack"
                                >
                                <?php echo $n['small_desc'];?>
                                </h2>

                                <h3 class="ms-layer hps-amp-price"
                                    style="left: 20px; top: 180px;"
                                    data-type="text"
                                    data-delay="100"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeInOutBack"
                                >
                                    <?php echo $n['price'];?>
                                </h3>
                            <?php
                              $ph=explode(',',$n['photos']);
                              ?>
                                <img src="css/img/blank.gif" data-src="img/<?php echo $ph[0];?>" alt="Детская коляска Bebecar Stylo Class"
                                  style="right:40px; top:20px; z-index:2; max-height: 160px;"
                                  class="ms-layer hps-amp-img"
                                  data-type="image"
                                  data-delay="600"
                                  data-duration="2500"
                                  data-ease="easeOutExpo"
                                  data-effect="scalefrom(1.3,1.3,0,0)"
                                />

                                <h4 class="ms-layer hps-amp-more"
                                    style="left: 170px; top: 190px;"
                                    data-type="text"
                                    data-delay="100"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeInOutBack"
                                >
                                     Подробнее
                                </h4>

                                <!-- linked slide -->
                                <a href="/<?php echo $n['title_translit'];?>"><?php echo $n['title'];?></a>

                            </div>
                            <!-- end of slide -->
                            <?php
                                }
                            ?>
                           
                          </div>
                          <!-- end of masterslider -->
                          <?php
                        }
                          ?>
                        </div>

                        <div class="col-md-12">

                            <?php
                            if($hit){ 
                            ?>
                            <!-- masterslider new -->
                            <div class="master-slider ms-skin-light-5" id="mastersliderhit">
                              <?php
                              foreach ($hit as $h) {
                                  
                              ?>
                                <!-- new slide -->
                              <div class="ms-slide" data-delay="12">
                             
                               <h5 class="ms-layer hit-amp-hit"
                                    style="left: 20px; top: 20px;"
                                    data-type="text"
                                    data-delay="0"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeOutExpo"
                                >
                                    Хит продаж! 
                                </h5>

                                <h1 class="ms-layer hit-amp-name"
                                    style="left: 20px; top: 50px;"
                                    data-type="text"
                                    data-delay="0"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeOutExpo"
                                >
                                    <?php
                                    echo $h['title']; 
                                    ?>
                                </h1>

                                <h2 class="ms-layer hit-amp-info"
                                    style="left: 20px; top: 90px;"
                                    data-type="text"
                                    data-delay="50"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeOutBack"
                                >
                                <?php echo $h['small_desc'];?>
                                </h2>

                                <h3 class="ms-layer hit-amp-price"
                                    style="left: 20px; top: 180px;"
                                    data-type="text"
                                    data-delay="100"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeInOutBack"
                                >
                                     <?php
                                     echo $h['price']; 
                                     ?>
                                </h3>
                                <?php
                              $ph=explode(',',$h['photos']);
                              ?>
                                <img src="css/img/blank.gif" data-src="img/<?php echo $ph[0];?>" alt="Автокресло Cybex Pallas 2-Fix"
                                  style="right:40px; top:20px; z-index:2; max-height: 160px;"
                                  class="ms-layer hit-amp-img"
                                  data-type="image"
                                  data-delay="600"
                                  data-duration="2500"
                                  data-ease="easeOutExpo"
                                  data-effect="scalefrom(1.3,1.3,0,0)"
                                />

                                <h4 class="ms-layer hit-amp-more"
                                    style="left: 170px; top: 190px;"
                                    data-type="text"
                                    data-delay="100"
                                    data-duration="1400"
                                    data-effect="scale(2.8,2.8)"
                                    data-ease="easeInOutBack"
                                >
                                     Подробнее
                                </h4>

                                <!-- linked slide -->
                                <a href="/<?php echo $h['title_translit'];?>"><?php echo $h['title'];?></a>

                        </div>
                        <!-- end of slide -->
                             <?php
                                }
                             ?>
                            </div>
                            <!-- end of masterslider -->
                            <?php
                            } 
                            ?>
                        </div>

                    </div>

                </div>
           
           </div>
         
         
         
         <div id="footer_utp">
         
              <div class="row content_block">
           
                <div class="col-md-4">
                
                     <div class="utp1">
                     
                          <h1>Широкий ассортимент товаров</h1>
                          <p>
                          В интернет-магазине “Мамонтёнок” вы найдете товары от более 100 ведущих производителей, таких как: Maxi-Cosi, Romer, Chicco, Baby Care, Inglesina, Jetem, Avent и многих других. Ваш выбор не ограничен наличием товара на складе, мы привезем любой понравившийся вам товар. 
                          </p>
                     
                     </div>
                
                </div>
                
                <div class="col-md-4">
                     
                     <div class="utp2">
                     
                          <h1>Доставка и самовывоз</h1>
                          <p>
                          Доставка по Рязани бесплатная при покупке от 5 000 рублей, также предусмотрена возможность самовывоза заказанного товара, в удобно расположенном месте, в центре города.  
                          </p>
                     
                     </div>
                     
                </div>
                
                <div class="col-md-4">
                
                     <div class="utp3">
                     
                          <h1>Оперативная помощь</h1>
                          <p>
                          Чтобы сэкономить ваше время на долгие походы по торговым центрам и поиск информации в сети Интернет, консультанты интернет-магазина “Мамонтёнок” помогут вам быстро найти интересующие вас товары и ответят на ваши вопросы в режиме онлайн.   
                          </p>
                     
                     </div>
                
                </div>
           
              </div>
              
              <div class="row content_block">
           
                <div class="col-md-4">
                     
                     <div class="utp4">
                     
                          <h1>Удобные способы оплаты</h1>
                          <p>
                          Вы сможете оплатить ваши покупки как с помощью банковской карты или другим безналичным способом, так и наличными в момент получения товара.    
                          </p>
                          <span class="paypal"></span>
                          <span class="visa"></span>
                          <span class="mastercard"></span>
                          <span class="qiwi"></span>
                     
                     </div>
                     
                </div>
                
                <div class="col-md-4">
                     
                     <div class="utp5">
                     
                          <h1>Постоянные скидки и подарки</h1>
                          <p>
                          При первой покупке мы дарим вам подарок, который вы сами выберете. При всех последующих покупках у вас будет эксклюзивный доступ к скидкам, специальным предложениям и приятным сюрпризам от Мамонтёнка!    
                          </p>
                     
                     </div>
                     
                </div>
                
                <div class="col-md-4">
                
                     <!-- <div class="utp6">
                     
                          <div class="mamoth">
                            
                            <div onclick="" class="lz_online_text">Добро пожаловать ко мне в гости, очень рад встрече! Задавайте свои вопросы онлайн и я с радостью на них отвечу!
                              <br>
                              <center><span class="lz_question">Задать вопрос</span></center>
                            </div>
                           
                            <div id="livezilla_tracking" style="display:none"></div>
                          </div>
                          <div class="voice">

                          </div> 
                         
                     
                     </div> -->
                     
                </div>
           
              </div>
         
         </div>
         
