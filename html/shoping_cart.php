<div id="cart_information">

<div class="row content_block">

  <div class="col-md-8">
    <?php
      model::get_path_link($prod['catalog'],$tree,true);
    ?>
    <script type="text/javascript">
      $(document).ready(function() {
        //console.log($('.catalog_items [href="/<?php echo $tree->orig_key[$prod['catalog']]['translit'];?>"]'));
         $('.catalog_items [href="/<?php echo $tree->orig_key[$prod['catalog']]['translit'];?>"]').addClass('active_item').parents('ul').show();
      
      });
    </script>
  </div>
  <div class="col-md-4">
  </div>

</div>

<div class="row content_block">

    <div class="col-md-8">

        <h1 class="cat_title_cart"><?=$title_page;?></h1>
    </div>

    <div class="col-md-4">

      
       
    </div>

</div>

<div class="row content_block">

    <div class="col-md-12">

        <div class="row">

          <div class="col-md-12">

            <h4 class="cart_user_info">Ваши контактные данные</h4>

          </div>

          <div class="col-md-12">

            <div class="row" id="user_information">

              <div class="col-md-4">

                <label><span>*</span> Ваше Имя:</label>
                  
                   <input name="" id="client_name" class="form-control input-lg user_input_info" type="text" placeholder="Ваше имя" value="<?php echo $client->name;?>">

              </div>

              <div class="col-md-4">

                <label><span>*</span> Электронная почта:</label>
                  
                   <input name="" id="client_email" class="form-control input-lg user_input_info" type="text" placeholder="Ваш email" value="<?php echo $client->email;?>">
                
              </div>

              <div class="col-md-4">

                <label><span>*</span> Номер телефона:</label>
                  
                   <input name="" id="client_phone" class="form-control input-lg user_input_info" type="text" placeholder="+7 (  ) ___ __ __" pattern="+7 ([0-9]{3}) [0-9]{3} [0-9]{2} [0-9]{2}" value="<?php echo $client->phone;?>">

              </div>
              
            </div>

          </div>

          <div class="col-md-12">

            <h4 class="cart_user_info">Как вы хотите получить Ваш заказ?</h4>
              <div class="col-md-4 col-xs-12">
              <select class="form-control user_city input" id="dost">
                <?php
                if($transport){
                  foreach ($transport as $trans) {
                    ?>
                    <option price="<?php echo $trans['price'];?>" value="<?php echo $trans['id'];?>"><?php echo $trans['title'];?></option>
                    <?
                  }
                }
                ?>
                
              </select>
              <input type="hidden" id="dost_sum_inpt">
            </div>

          </div>

          <div class="col-md-12">
             <div class="space5"></div>
              <div class="cart_prod_bottom_line"></div>
              <div class="space5"></div>
          </div>

         
          <!-- <div class="col-md-12">
       

              <ul class="nav nav-tabs" role="tablist" id="shipping_tab">
                <li role="presentation" class="active">
                  <a href="#courier" aria-controls="courier" role="tab" data-toggle="tab">
                    
                    <h4 class="shipping_user_info">Курьером (500 руб)</h4>
                     
                  </a>
                </li>
              
                <li role="presentation">

                  <a href="#pickup" aria-controls="pickup" role="tab" data-toggle="tab">

                    <h4 class="shipping_user_info">Забрать из магазина</h4>

                  </a>

                </li>

              </ul>

              <div class="tab-content" id="shipping_tab_info">
                
                <div role="tabpanel" class="tab-pane active" id="courier">

                  <div class="row">

                    <div class="col-md-12">

                      <h5 class="shipping_user_description">Доставим заказ домой или в офис в удобный для Вас день. <br>Просто выберите дату, когда Вам привезти покупку.</h5>

                      <form class="form-inline">

                        <div class="form-group shipping_data_form">
                          <label class="sr-only" for="exampleInputAmount"></label>
                          <div class="input-group">
                            <div class="input-group-addon"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span></div>

                            <input type="text" class="form-control shipping_data" placeholder="Дата доставки" id="shipping_courier_data">
                            
                          </div>
                        </div>
                       
                      </form>

                    </div>

                    <div class="col-md-12">



                    </div>

                  <div class="col-md-12">

                      <h4 class="shipping_adress_info">Доставим курьером следующие товары:</h4>

                  </div>
              


                  </div>

                </div>
               
                <div role="tabpanel" class="tab-pane" id="pickup">
                  <h5 class="shipping_user_description">Заказ будет ждать в магазине, чтобы Вы могли забрать его в удобное для Вас время.<br><br>Адрес магазина: <br style="border-bottom: 1px dotted #f3f3f3">г. Рязань, ул. Горького, д. 30</h5>
                  <h4 class="shipping_adress_info">Соберем для Вас следующие товары:</h4>
                  
                </div>
              </div>

            

          </div> -->

              <div id="cart_content" class="col-md-12">
                    загрузка ждите...
              </div>

                  

                  <div class="col-md-12">
                     <div class="space20"></div>
                  </div>

                  <div class="col-md-4" id="promo_form">
                      <!-- <input name="" class="form-control input-lg user_adress_input promocode" type="text" placeholder="Промокод">
                      <span class="use_promo_cod"><a>применить</a></span> -->
                  </div>

                  <div class="col-md-4">

                    <h4 class="shipping_price_finish">Доставка: <span><span id="dost_sum">0</span> руб</span></h4>
                      
                  </div>

                   <div class="col-md-4 total_price">

                    <h4 class="total_price_finish">Итог: <span id="itog"></span></h4>
                      
                  </div>

                  <div class="col-md-12 dost_cont">
                      <h4 class="cart_user_info">Адрес доставки</h4>
                    </div>

                    <div class="col-md-12 dost_cont">

                      <form class="form-inline">
                        
                        <div class="form-group">
                          <input id="adr_city" placeholder="Укажите город"  value="<?php echo $client->city;?>" />
                          <!-- <select id="adr_city" class="form-control user_adress_input user_city input-lg">
                            
                            <option value="Рязань">Рязань</option>
                           
                          </select> -->
                        </div>

                      </form>

                    </div>

                  <div class="col-md-6 dost_cont">
                      <input id="adr_index" type="text" class="form-control input user_adress_input user_street num" id="" placeholder="Почтовый индекс" value="<?php echo $client->index;?>">
                      <input id="adr_house" type="text" class="form-control input user_adress_input user_street" id="" placeholder="Улица, дом, корпус, квартира или офис" value="<?php echo $client->adres;?>">
                  </div>

        </div>

    </div>

</div>

<div class="row content_block">

  <div class="col-md-12">

          <h4 class="cart_user_info">Комментарий к заказу</h4>


  </div>

  <div class="col-md-12 order_send">

          <textarea id="comment" class="order_more"></textarea>          

  </div>

  <!-- <div class="col-md-12">

          <h4 class="cart_user_info">Способы оплаты</h4>

  </div>

  <div class="col-md-6 order_send">

         <div class="row">
            <div class="col-md-6">
               <select id="pay_type" class="form-control user_city input">
                <option value="cash">Наличными</option>
                <option value="card">Банковкой картой</option>
              </select>
            </div>
            <div class="col-md-6">


            </div>
         </div>

  </div> -->

  <div class="col-md-6">

         <div class="row"></div>

  </div>

  <div class="col-md-6 order_send">
    <div id="mess"></div>
    <div class="product_add_to_cart order_send" id="product_add_to_cart">
          
          <div class="space20"></div>
       
          <span id="but_send" class="btn_add_to_cart">Оформить заказ</span>

   </div>

  </div>

  <div class="col-md-6"></div>

</div>

</div>

<div class="space20"></div>
<div class="space20"></div>
<div class="space20"></div>
<div class="space20"></div>

<div id="test_res" style="display:none;">
</div>

<script type="text/javascript">      
    
    if($.cookie(cart_name)==''||$.cookie(cart_name)==undefined){
      $('#cart_information').html('<div class="row content_block "> <span class="emty_cart"> Корзина пуста </span></div>');
    }
    $(document).ready(function(){
        $("#adr_city").kendoComboBox({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: [
                            { text: "Рязань", value: "Рязань" },
                            { text: "Сасово", value: "Сасово" }
                        ],
                        filter: "contains",
                        suggest: true
                    });

        $('#client_phone').mask("+7 (999) 999 99 99");
        $('#dost').change(function(event) {
          $('#dost_sum').html($(this).find('option:selected').attr('price'));
          $('#dost_sum_inpt').val($(this).find('option:selected').attr('price'));
          itog_price();
        });

        function itog_price(){
          var summ=0;
          $.each($('#cart_content .prod_row'), function(index, val) {
             
             var price=parseFloat($(val).find('.pr_prod').eq(0).val());
             var count=parseInt($(val).find('.count_prod').eq(0).val());
             summ+=count*price;
          });
          summ+=parseFloat($('#dost_sum_inpt').val());
          $('#itog .itog').html(summ);
        }
        /*$('#shipping_tab a').click(function (e) {
          e.preventDefault();
          $(this).tab('show');
        });

        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            //e.target // activated tab
            //e.relatedTarget // previous tab
            if($(e.target).attr('href')=='#pickup'){
              $('#dost_sum').html(0);
            }
            if($(e.target).attr('href')=='#courier'){
              $('#dost_sum').html(500);
              $('.dost_cont').show();
            }else{
              $('.dost_cont').hide();
            }
          })*/

        getContentCart();
        var timer;
        $('body').on('change','input.num',function (e){
          var c=parseInt($(this).val());
          if(c<1){
            $(this).val(1);
          }
          var obj=JSON.parse($(this).parents('.prod_row').eq(0).find('.prd_del').eq(0).attr('rel').replace(/'/g, '"'));
          setCount(parseInt($(this).val()),obj);
          refreshCart();
          itog_price();
          /*clearTimeout(timer);
          timer=setTimeout(
            function(){
            //synchronizCart();
            //getContentCart();
            refreshCart();
          },2000);*/
        });

        $('#but_send').click(function(event) {
          var bt=$(this);
          bt.hide();
          var st={};
          st['cart']=$.cookie(cart_name);
          st['dost']=$('#dost').val();
          st['city']=$('#adr_city').val();
          st['adres']=$('#adr_house').val();
          st['index']=$('adr_index').val();
          st['comment']=$('#comment').val();
          st['pay_type']=$('#pay_type').val();
          st['name']=$('#client_name').val();
          st['phone']=$('#client_phone').val();
          st['email']=$('#client_email').val();
          if(st['name']==''){
            $('#mess').html('<div class="alert alert-danger" role="alert">Введите Ваше Имя</div>');
            bt.show();
            return false;
          }
          if(st['phone']==''){
            $('#mess').html('<div class="alert alert-danger" role="alert">Укажите номер телефона</div>');
            bt.show();
            return false;
          }
           if(st['email']==''){
            $('#mess').html('<div class="alert alert-danger" role="alert">Укажите Ваш email-адрес</div>');
            bt.show();
            return false;
          }
          $.post('load/send_order', st, function(data) {
            try{
              data=JSON.parse(data);
              if(data.status){
                clearCart();
                window.location.href="/thankyou";
              }else{
                bt.show();
              }
            }catch(e){
              bt.show();
            }
          });
        }); 
        
    });

    $(window).load(function()
        {
             $('.shipping_data').glDatePicker();
        });
 

</script>

