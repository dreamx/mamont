<div class="row content_block">
    
             <div class="col-md-12">
                  
               <div class="row">

                        <div class="col-md-6">
                          <ol class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li class="active"><?php echo $title_page;?></li>
                          </ol>
                        </div>

                        
             
             </div>
         
         </div>
         
      </div>

      <div class="row content_block">

        <div class="col-md-8">
          <h1 class="cat_title"><?=$title_page;?></h1>
        </div>
        <div class="col-md-4">
        
        </div>

      </div>

      <div id="pr_list">
        <div style="color:#000;">
          <?php
          //model::get_path_link(61,$tree);
          //print_r($tree->get_parents(61));
          //print_r($_GET);
        //print_r($products);
          ?>
        </div>
        <?php
        
        
        if($client){
          
          //echo $client->name."<br>";
          //echo $client->phone."<br>";
          //echo $client->email."<br>";
          $adreses=$client->getAdreses();
          if($adreses){
            ?>
            Адреса: <br>
            <?php
            foreach ($adreses as $adr) {
              echo $adr['adres']."<br>";
            }
          }
          $orders=$client->getOrders();
          if($orders){
            foreach ($orders as $order) {
              ?>
              Заказ №<?php echo $order['order']['id'];?> <br>
              Дата заказа <?php echo $order['order']['date'];?><br>
              Статус заказа <?php echo $order['order']['status'];?><br>
              <?
              if($order['discount']){
                ?>
                Скидки заказа:<br>
                <?
                foreach ($order['discount'] as $disc) {
                  echo $disc['name']." ";
                  if($disc['edinici']=='rub'){
                    echo $disc['value']."руб.";
                  }else{
                    echo $disc['value']."%";
                  }
                }
              }
              if($order['items']){
                ?>
                Товары заказа:<br>
                <?
                foreach ($order['items'] as $item) {
                  echo $item['title']." ".$item['price']."руб."." ".$item['count']."шт.<br>";
                  if($item['discount']){
                    ?>
                    Скидки товара:<br>
                    <?
                    foreach ($item['discount'] as $disc) {
                      echo $disc['name']." ";
                      if($disc['edinici']=='rub'){
                        echo $disc['value']."руб.";
                      }else{
                        echo $disc['value']."%";
                      }
                    }
                  }
                }
              }
            }
          }
          
        }else{
          echo "Клиент не найден.";
          
        }
        ?>
        


    <div class="clear"></div>
</div>


<div class="row content_block">
  <div class="col-md-6">
    <h4 class="lc_user_info">Здравствуйте <?php echo $client->name; ?>!</h4>
  </div>
</div>

<div class="row content_block">
  <div class="col-md-6">
    <h4 class="cart_user_info">Персональная информация</h4>

        <div class="row" id="">
          <div class="col-md-8 lc_info">
            <label>Ваше Имя:</label>
            <input name="" id="client_name" class="form-control input user_input_info" type="text" placeholder="Ваше имя" value="<?php echo $client->name;?>" disabled>
          </div>
          <div class="col-md-8 lc_info">
            <label>Ваш email-адрес:</label>
            <input name="" id="client_email" class="form-control input user_input_info" type="text" placeholder="Ваш email" value="<?php echo $client->email;?>" disabled>
          </div>
          <div class="col-md-8 lc_info">
            <label>Номер телефона:</label>
            <input name="" id="client_phone" class="form-control input user_input_info" type="text" placeholder="+7 (  ) ___ __ __" pattern="+7 ([0-9]{3}) [0-9]{3} [0-9]{2} [0-9]{2}" value="<?php echo $client->phone;?>">
          </div>
          <div class="col-md-8 lc_info">
            <a href="#" class="btn btn_order disabled" role="button">Сохранить</a>
            <a href="#" class="btn btn_enter disabled" role="button">Изменить</a>
          </div>
          
          <div class="col-md-8 lc_info">
            <h4 class="lc_adress_info">Адрес доставки</h4>
              <div class="form-group lc_info_adress_input">
                <input id="adr_city" placeholder="Укажите город"  value="<?php echo $client->city;?>" />
              </div>
          </div>
          <div class="col-md-8 lc_info">
            <label>Почтовый индекс:</label>
            <div class="form-group ">
             <input id="adr_index" type="text" class="form-control input  user_input_info num" id="" placeholder="Почтовый индекс" value="<?php echo $client->index;?>">
            </div>
          </div>
          <div class="col-md-8 lc_info">
            <label>Адрес:</label>
            <div class="form-group ">
              <input id="adr_house" type="text" class="form-control input user_input_info" id="" placeholder="Улица, дом, корпус, квартира или офис" value="<?php echo $client->adres;?>">
            </div>
          </div>
          <div class="col-md-8 lc_info">
            <a href="#" class="btn btn_order disabled" role="button">Сохранить</a>
            <a href="#" class="btn btn_enter disabled" role="button">Изменить</a>
            <br><br>
          </div>
        </div>

    </div>
    <div class="col-md-6">
      <h4 class="cart_user_info">Информация о заказах</h4>

        <div class="row" id="">
          <div class="col-md-8 lc_info">
           ...
          </div>
        </div>
    </div>
  </div>
  
</div>
<script type="text/javascript">
$(document).ready(function(){
        $("#adr_city").kendoComboBox({
                        dataTextField: "text",
                        dataValueField: "value",
                        dataSource: [
                            { text: "Рязань", value: "Рязань" },
                            { text: "Сасово", value: "Сасово" }
                        ],
                        filter: "contains",
                        suggest: true
                    });
    });
</script>
<script type="text/javascript" src="/js/kendo.all.min.js"></script>
<link rel="stylesheet" href="/css/kendo.common-bootstrap.min.css" />
