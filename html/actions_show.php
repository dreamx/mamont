
         
           <div class="row content_block">
    
             <div class="col-md-12">
                  
               <div class="row">

                        <div class="col-md-6">
                          <ol class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li class="active">Акции</li>
                          </ol>
                        </div>

                        <div class="col-md-6">
                          <div class="cat_sort">
                          <span class="label label-warning"><?php echo count($products);?> товаров</span>
                          <?php
                          $newget=$_GET;
                          unset($newget['chpu']);// удаляем из массива урл каталога

                          
                          $v='';
                          if(isset($values)){
                            $v=key($values);
                          }
                          ?>
                            <div class="dropdown select_sort">
                                <input type="hidden" class="sel_val" rel="<?=$first['price'];?>" value="<?=$v;?>">
                                <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-expanded="true">
                                  <span class="select_cont">Сортировать по:
                                  <?php 
                                    echo $arr_sort[$sel_sort][1];
                                  ?> 
                                  </span>
                                  <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                  <?
                                     foreach ($arr_sort as $key => $value) {
                                      $newget['sort']=$key;
                                      ?>
                                      
                                      <li role="presentation"><a href="?<?=model::toGet($newget);?>"><?=$value[1];?></a></li>
                                      <?
                                     }
                                    ?>
                                </ul>
                              </div>
                          </div>
                        </div>
             
             </div>
         
         </div>
         
      </div>

      <div class="row content_block">

        <div class="col-md-8">
          <h1 class="cat_title"><?=$title_page;?></h1>
        </div>
        <div class="col-md-4">
          <?php 
          if($f_arr){
            $newget=$_GET;
            unset($newget['chpu']);// удаляем из массива урл каталога


            $old_ch='';
            foreach ($f_arr as $ch) {
                
                if($ch['char']!=$old_ch){
                  if($old_ch!=''){
                    ?>
                  </div>
                    <?
                  }
                  $old_ch=$ch['char'];
                  ?>
                  <div class="filter_char">
                    <span class="name_char"><?=$ch['ctitle'];?>:</span><br>
                  <?
                }
                $newget_char=$newget;
                if(!empty($newget_char['ch'.$ch['id']])){
                  unset($newget_char['ch'.$ch['id']]);
                  ?>
                  <a class="select_ch" href="?<?=model::toGet($newget_char);?>"><?=$ch['value_char'];?><b>(<?=$ch['prd'];?>)</b></a>&nbsp;&nbsp;
                <?php
                }else{
                $newget_char['ch'.$ch['id']]=$ch['id'];
                ?>
                <a href="?<?=model::toGet($newget_char);?>"><?=$ch['value_char'];?><b>(<?=$ch['prd'];?>)</b></a>&nbsp;&nbsp;
                <?php
                }
            }
?><br style="clear:both;">
            <?
          }
          ?>
        </div>
        </div>
      </div>
      
<br style="clear:both;">
      <div id="pr_list">

        <?php
        if($products){
          foreach ($products as $prod) {
            $ph=explode(',',$prod['photos']);
            ?>
            <div id="product_list">
          <div id="product_block_sales">
           <div class="product_img">
            <a href="/<?=$prod['title_translit'];?>">
            <img src="/img/thumbs/<?=$ph[0];?>" class="img-rounded"></a>
           </div>
           <div class="product_name">
            <a href="/<?=$prod['title_translit'];?>">
             <?=$prod['title'];?>
           </a>
           </div>
           <div class="product_bottom_line">
           </div>
           <?php
            if($prod['action']=='on'){
            ?>
          <div class="old_price" id="old_price">
             Старая цена: <span><?=intval($prod['price']);?></span> руб
          </div>
          <?php
            }
          ?>
          <div class="new_price" id="new_price">
             <span><?php if($prod['action']=='on'){echo intval($prod['action_price']);}else{echo intval($prod['price']);};?></span> руб
          </div>
           <div class="add_cart" prod="<?php echo $prod['id']?>">
            <span class="add_shoping_cart"></span>
           </div>
           <div class="clear"></div>
          </div>
        </div>
        <?
          }
        }
        ?>
        


    <div class="clear"></div>
</div>
<script type="text/javascript">
$('.add_cart').click(function(event) {
  var el=$(this);
  var fnc=arguments.callee;
  el.find('span').html('+');
  el.unbind('click', fnc).animate({opacity:'0.4','font-size':'4px'}, 200,
    function(){
      el.find('span').html('');
      addProdFast(el.attr('prod'));
      el.animate({opacity:'1','font-size':'78px'}, 200,
        function(){
          el.click(fnc);
        })
    });
  
});
</script>