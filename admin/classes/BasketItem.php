<?php

class BasketItem{

	private $table='basket_item';
	public $id;
	public $id_prod;
	private $title;
	private $price;
	public $count;
	private $basket;
	private $chars;//массив характеристик
	public $chars_val;//массив характеристик
	private $discount=array();//массив скидок
	private $summa;//стоимость товара со всеми характеристиками без скидки
	private $summa_disc;//стоимость товара со всеми характеристиками со скидками
	private static $instance;//объект класса
	private $type_temp=4;// id времменного типа скидки
	private $max_discount;//максимальная скидка, которая применяется
	private $arr_links=array();//массив ссылок на скидку
	private $DiscountBasket=array();//скидка на корзину

	private function __construct($id='',$arr_links=array()){
  		if($id!=''){
			$order_info=DB::query("SELECT `{$this->table}`.*,`p`.`title`,`p`.`price` FROM `{$this->table}` LEFT JOIN `product` AS `p` ON `p`.`id`=`{$this->table}`.`id_prod` WHERE `{$this->table}`.`id`='".intval($id)."' LIMIT 1",'a');
			if($order_info){
				$this->arr_links=$arr_links;
				$this->setInfo($order_info[0]);
			}
		}
		
	}

	/* возвращает объект класса */
	public static function getInstance($id='',$arr_links=array()){
		if(empty(self::$instance)){
			if(empty($id)){
				return false;
			}
			self::$instance=new BasketItem($id,$arr_links);
		}
		return self::$instance;
	}

	/* создаёт новый и возвращает объект класса */
	public static function getNewItem($id='',$arr_links=array()){
		
		if(empty($id)){
			return false;
		}
		self::$instance=new BasketItem($id,$arr_links);
		return self::$instance;
	}

	/* заполняет свойства объекта */
	private function setInfo($ord){
		extract($ord);
		$this->id=$id;
		$this->id_prod=$id_prod;
		$this->title=$title;
		$this->price=$price;
		$this->count=$count;
		$this->basket=$basket;
		$this->setChars();
		$this->setDiscount();
	}

	/* читает характеристики товара из базы и записывает в свойство объекта */ 
	private function setChars(){
		if(!empty($this->id)){
			$chars=DB::query("SELECT `ch`.`title`,`ch`.`si`,`l`.`value_char` AS `value`,`l`.`value_color`,`v`.`price`,`v`.`id`
									FROM `basket_item_char` AS `bi` 
									LEFT JOIN `char_value` AS `v` ON `bi`.`id_char`=`v`.`id`
									LEFT JOIN `char_list` AS `l` ON `v`.`id_char`=`l`.`id`
									LEFT JOIN `char` AS `ch` ON `l`.`id_char`=`ch`.`id`
									WHERE `bi`.`id_item`='{$this->id}'",'a');
			if($chars){
				$this->chars=$chars;
				$res=array();
				foreach ($chars as $ch) {
					$res[$ch['id']]=array('val'=>$ch['value'],'price'=>$ch['price'],'valc'=>$ch['value_color']);
				}
				$this->chars_val=$res;
			}
		}
	}

	/* проверяет наличие скидок у товара и записывает их в свойство */
	private function setDiscount(){
		if(!empty($this->id)){
			$q="SELECT `d`.`type`,`d`.`edinici`,`d`.`value`,`d`.`name`,`d`.`date_begin`,`d`.`date_end` 
				FROM `link_product_discont` AS `l`
				LEFT JOIN `{$this->table}` AS `bi` ON `l`.`id_note1`=`bi`.`id_prod`
				LEFT JOIN `discount` AS `d` ON `l`.`id_note2`=`d`.`id` 
				WHERE `bi`.`id`='{$this->id}' AND `d`.`public`='on'";
			$disc=DB::query($q,'a');
			if($disc){
				$this->discount=$disc;
			}
			
			if(count($this->arr_links)>0){
				foreach ($this->arr_links as $value) {
					if($this->id_prod==$value['id_prod']){
						$this->discount[]=$value;
					}
				}
			}
		}
	}

	/* проверяет наличие скидок на корзину и записывает в свойства объекта,
		так как скидка на корзину может быть только одна, берётся первая строка   
	*/
	public function getDiscountBasket(){
		if(empty($this->DiscountBasket)){
			$disc=DB::query("SELECT `type`,`edinici`,`value`,`name`,`date_begin`,`date_end` 
							FROM `basket_item_discount` AS `bd` 
							WHERE `bd`.`id_item`='{$this->id}' LIMIT 1",'a');
			if($disc){
				$this->DiscountBasket=$disc;
			}
		}
		return $this->DiscountBasket;
	}

	public function getTitle(){
		return $this->title;
	}
	public function getPrice(){
		return $this->price;
	}
	public function getCount(){
		return $this->count;
	}
	public function getId(){
		return $this->id;
	}
	public function get_chars(){
		return $this->chars;
	}

	public function setCount($count){
		if(	DB::update($this->table,array('count'=>intval($count))," `id`='".$this->id."'")){
			$this->count=intval($count);
		}
	}

	public function get_chars_str(){
		$str="";
		$ar=array();
		if(count($this->chars)){
			foreach ($this->chars as $ch) {
				$ar[]=$ch['title']." ".$ch['value'].$ch['si']." ".$ch['price'];
			}
			$str=implode(', ',$ar);
		}
		return $str;
	}

	public function get_discounts(){
		return $this->discount;
	}

	/* возвращает сумму отдного товара без скидки
	*/ 
	public function get_sum(){
		if(empty($this->summa)){
			if(count($this->chars)>0)
			{
				$s=$this->price;
				foreach ($this->chars as $ch) {
					$s+=$ch['price'];
				}
				$this->summa=$s;
			}else{
				$this->summa=$this->price;
			}
		}
		return $this->summa;
	}

	

	/* возвращает сумму товара без скидки умноженную на количество
	*/
	public function get_summa(){
		return $this->get_sum()*$this->count;
	}

	/*возвращает максимальную скидку на товар */
	public function getMaxDisc($date){
		if(empty($this->max_discount)&&count($this->discount)>0){
				$summ=$this->get_sum();
				$time_order=strtotime($date);
				$max_rub=0;//начальное значение максимальной скидки
				$res=false;
				foreach ($this->discount as $dis) {
					if($dis['type']==$this->type_temp){ //если скидка временная
						$tb=strtotime($dis['date_begin']); //время начала действия
						$te=strtotime($dis['date_end']); //время конца действия
						if($time_order>=$tb&&$time_order<=$te){// если заказ сделан во время действия скидки, то учитываем её
							if($dis['edinici']=='rub'){
								if($max_rub<$dis['value']){
									$res=$dis;
									$max_rub=$dis['value'];
								}
							}
							if($dis['edinici']=='perc'){
								if($max_rub<$summ*$dis['value']/100){
									$res=$dis;
									$max_rub=$summ*$dis['value']/100;
								}
							}
						}
					}else{
						if($dis['edinici']=='rub'){
							if($max_rub<$dis['value']){
								$res=$dis;
								$max_rub=$dis['value'];
							}
						}
						if($dis['edinici']=='perc'){
							if($max_rub<$summ*$dis['value']/100){
								$res=$dis;
								$max_rub=$summ*$dis['value']/100;
							}
						}
					}	
				}
				$this->max_discount=$res;
			}
		return $this->max_discount;
	}

	/* максимальная скидка строкой */
	public function getMaxDisc_str($date){
		$dis=$this->getMaxDisc($date);
		if($dis){
			if($dis['edinici']=="rub")
			{
				return $dis['value']." руб.";
			}
			if($dis['edinici']=="perc")
			{
				return $dis['value']." %";
			}
		}else{
			return '';
		}
	}

	/* возвращает стоимость товара со скидкой умноженную на количество
	сейчас выбирается наибольшая скидка действующая на указанную дату
	* @date дата 
	*/
	public function get_summa_disc($date){
		$summ=$this->get_sum();
		$dis=$this->getMaxDisc($date);
		if($dis){
			if(empty($this->summa_disc)){
				if($dis['edinici']=='rub'){
						$this->summa_disc=$summ-$dis['value'];
					}
				if($dis['edinici']=='perc'){
					$this->summa_disc=$summ-$summ*$dis['value']/100;
				}
			}
		}else{
			$this->summa_disc=$summ;
		}
		return $this->summa_disc*$this->count;
	}

	/** 
	* Функция добавляет товар в корзину
	* @id_prod
	* @arr_char 
	*/
	/* ********************** сюда добавить запись скидок по ссылкам  ********************* */
	public static function add_item($id_prod,$arr_char=array(),$count,$basket){
		//добавляем запись товара в корзину
		$new_id=DB::insert('basket_item',array('count','basket','id_prod'),array($count,$basket,$id_prod));
		if($new_id)
		{
			$new_id=$new_id[0];
			DB::query("UPDATE `basket` SET `date`=now() WHERE `id`='".intval($basket)."'");
		}else{
			return false;
		}
		// если массив характеристик не пустой
		if(count($arr_char)>0){
			$fields=array('id_item','id_char');
			$values=array();
			foreach ($arr_char as $ch) {
				$values[]=array($new_id,$ch);
			}
			DB::insert('basket_item_char',$fields,$values);//добавляем выбранные характеристики
		}
		return $new_id;
	}

	//удаляет товар из корзины
	function del_item(){
		DB::query("DELETE FROM `{$this->table}` WHERE `id`='{$this->id}'");
	}

}
?>