<?php
class Order {
	private static $instance;
	private $table='order';//таблица заказов
	private $id;// id заказа
	private $client;//клиент
	private $status;//статус
  	private $payment;//оплачен или нет
  	private $payment_summ;//сумма оплаты
  	private $promo_cod;// промо код
  	private $summ;// заказа с учетом скидок на товары и на сам заказ
  	public $date;// дата заказа
  	public $adres;//адрес доставки
  	public $comm;//комментарии покупателя
  	private $items;// массив товаров в заказе
  	private $discount=array();// массив скидок на заказ
  	private $errors=array();//массив ошибок
  	private $max_discount;//максимальная скидка, которая применяется
  	private $sum;// сумма без скидки на заказ 
  	private $s_act;// сумма товаров с акцией
  	private $s;// сумма товаров без акцией
  	private $block;//блокировка

  	private function __construct($id=''){

  		if($id!=''){
			$order_info=DB::query("SELECT * FROM `{$this->table}` WHERE `id`='".intval($id)."' LIMIT 1",'a');
			if($order_info){
				$this->setInfo($order_info[0]);
			}else{
				throw new Exception('Заказ с номером '.$id.' не найден');
			}
		}else{
			throw new Exception('Укажите номер заказа');
		}
	}

	public static function getInstance($id=''){
		if(!isset(self::$instance)){
			if(empty($id)){
				return false;
			}
			try {
				self::$instance=new Order($id);
			}catch (Exception $e){
				echo $e->getMessage();
				return false;
			}
		}
		return self::$instance;
	}

	public function setInfo($ord){
		extract($ord);
		$this->id=$id;
		$this->client=$client;
		$this->status=$status;
		$this->payment=$payment;
		$this->payment_summ=$payment_summ;
		$this->promo_cod=$promo_cod;
		$this->summ=$summ;
		$this->date=$date;
		$this->adres=$adres;
		$this->comm=$comment;
		$this->setDiscount();
		$this->block=$block;
	}

	public function get_block(){
		return $this->block;
	}

	private function setDiscount(){
		if(!empty($this->id)){
			$disc=DB::query("SELECT * FROM `order_discount` WHERE `id_order`='{$this->id}'",'a');
			if($disc){
				$this->discount=$disc;
			}
		}
	}

	public function get_discounts(){
		return $this->discount;
	}

	public function getItems(){
		if(empty($this->items)){
			$items=DB::query("SELECT `id` FROM `order_item` WHERE `order`='{$this->id}'",'a');
			if($items){
				$this->items=array();
				foreach ($items as $it) {
					$item=OrderItem::getNewItem($it['id']);
					$this->items[]=$item;
				}
			}
		}
		return $this->items;
	}

	public function getDate(){
		return $this->date;
	}

	public static function add_order($id_client,$promo){
		if(is_numeric($id_client)){
			$res=DB::insert('order',array('client','promo_cod'),array($id_client,$promo));
			if($res){
				$id=$res[0];
				if(!empty($promo)){
					$q="SELECT `d`.* 
					FROM `promo_cod` AS `p`
					LEFT JOIN `discount`AS `d` ON `p`.`discount`=`d`.`id`
					WHERE `p`.`status`='0' AND cod=:cod AND `d`.`public`='on'";
					$ins=DB::getConnect()->prepare($q);
					$ins->execute(array(':cod'=>$promo));
					$disc=$ins->fetchAll(PDO::FETCH_ASSOC);
					if($disc!==false){
						$val=array();
						foreach ($disc as $d) {
							$val=array($id,$d['type'],$d['edinici'],$d['value'],$d['name'],$d['date_begin'],$d['date_end']);
						}
						DB::insert('order_discount',array('id_order','type','edinici','value','name','date_begin','date_end'),$val);
					}
				}
				return $id;
			}
		}
		return false;
	}

	public function get_sum(){
		$this->set_sum();
		return $this->sum;
	}

	public function getMaxDisc(){
		if(empty($this->max_discount)&&count($this->discount)>0){
				$this->set_sum();
				$summ=$this->s;
				$time_order=strtotime($this->date);
				$max_rub=0;//начальное значение максимальной скидки
				$res=false;
				foreach ($this->discount as $dis) {
					if($dis['type']==$this->type_temp){ //если скидка временная
						$tb=strtotime($dis['date_begin']); //время начала действия
						$te=strtotime($dis['date_end']); //время конца действия
						if($time_order>=$tb&&$time_order<=$te){// если заказ сделан во время действия скидки, то учитываем её
							if($dis['edinici']=='rub'){
								if($max_rub<$dis['value']){
									$res=$dis;
									$max_rub=$dis['value'];
								}
							}
							if($dis['edinici']=='perc'){
								if($max_rub<$summ*$dis['value']/100){
									$res=$dis;
									$max_rub=$summ*$dis['value']/100;
								}
							}
						}
					}else{
						if($dis['edinici']=='rub'){
							if($max_rub<$dis['value']){
								$res=$dis;
								$max_rub=$dis['value'];
							}
						}
						if($dis['edinici']=='perc'){
							if($max_rub<$summ*$dis['value']/100){
								$res=$dis;
								$max_rub=$summ*$dis['value']/100;
							}
						}
					}	
				}
				$this->max_discount=$res;
			}
		return $this->max_discount;
	}

	public function getMaxDisc_str(){
		$dis=$this->getMaxDisc();
		if($dis){
			if($dis['edinici']=="rub")
			{
				return $dis['value']." руб.";
			}
			if($dis['edinici']=="perc")
			{
				return $dis['value']." %";
			}
		}else{
			return '';
		}
	}

	public function blocked(){
		$summa=$this->getSum();
		return DB::update($this->table,array('summ'=>$summa,'block'=>1),$where=" `id`='".$this->id."'");
	}

	public function set_sum(){
		$s_act=0;
		$s=0;
		if($this->sum==0){
		if(count($this->getItems())>0){
			
			foreach ($this->getItems() as $item) {
				if(count($item->get_discounts())>0){
					$s_act+=$item->get_summa_disc($this->date);
				}else{
					$s+=$item->get_summa();
				}
			}
			$this->s_act=$s_act;
			$this->s=$s;
			$this->sum=$s+$s_act;
		}
		}
	}

	public function getSum(){
		$this->set_sum();
		$s=$this->s;
		if($this->summ==0){
		if(count($this->getItems())>0){
			
			$dis=$this->getMaxDisc();
			if(count($dis)>0 && $this->s>0){ //делаем скидку заказа на сумму товаров, у которых нет скидок
				
				if($dis['edinici']=='rub'){
						$s=$this->s-$dis['value'];
					}
				if($dis['edinici']=='perc'){
					$s=$this->s-$this->s*$dis['value']/100;
				}
			}
			$this->summ=$this->s_act+$s;
		}
		}
		return $this->summ;
	}

	public function view_order_admin($block=false){
		?>
		Cумма заказа: <?php echo $this->get_sum()." руб.";?><br>
		<?php if(count($this->get_discounts())>0){?>
		Скидка на заказ(не распространяется на акционный товар): <?php echo $this->getMaxDisc_str();?><br>
		<?php } ?>
		Cумма заказа со скидкой: <?php echo $this->getSum()." руб.";?>
		<?php
		if($this->payment==1){
			echo "<br>Оплачено:".$this->payment_summ." руб.";
		}
		?>
		<table id="tab_ord" style="width:100%;" class="table table-striped table-bordered">
          <thead>
            <th>Наименование</th>
            <th>Характеристики</th>
            <th>Кол-во</th>
            <th>Цена</th>
            <th>Скидка</th>
            <th>Итого</th>
            <?php
            if(!$block){?>
            <th></th>
            <?php } ?>
          </thead>
          <tbody>
            <?php
              if(count($this->getItems())>0){
                foreach ($this->getItems() as $pr){
                  ?>
                  <tr id="it<?=$pr->getId();?>">
                    <td><?php echo $pr->getTitle();?></td>
                    <td><?php echo $pr->get_chars_str();?></td>
                    <td><?php echo $pr->getCount();?></td>
                    <td><?php echo $pr->get_sum();?></td>
                    <td><?php print_r($pr->getMaxDisc_str($this->date));?></td>
                    <td><?php echo $pr->get_summa_disc($this->date);?></td>
                    <?php if(!$block){?>
                    <td><span class="btn btn-danger del_item"><span class="glyphicon glyphicon-trash"></span> Удалить</span></td>
                  	<?php } ?>
                  </tr>
                  <?
                }
              }
            ?>
          </tbody>
        </table>
        <script type="text/javascript">
	        var tab_ord=$('#tab_ord').dataTable({
	            "language": {
	                "paging":false,
	                "lengthMenu": "",
	                "zeroRecords": "Ничего не найдено",
	                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
	                "infoEmpty": "",
	                "search": "Поиск: ",
	                "paginate": {
	                "previous": "",
	                "next": ""
	                }
	            },
	            tableTools: {
	            "aButtons": [ ]
	        }
	          });

			var api_ord=tab_ord.api();
        </script>
		<?
	}

}
?>