<?php

/**
* Класс для работы с пользователями 
* обязательно PHP 5.3 и выше
*/
class User 
{
	private static $tab_us='client';//таблица пользователей
	private static $nam_us='name';//имя пользователя
	private static $pas_us='password';//поле пароля
	private static $log_us='email';//поле логина
	private static $hash_us='hash';//поле хеша
	private static $ses_us='id_session';//поле id сессии
	private static $tab_lv='';//таблица прав
	private static $errors;

	public static function getErrors()
	{
		return self::$errors;
	}
	
	public static function setErrors($mes)
	{
		self::$errors[]=$mes;
	}

	public static function getCode($count='')
	{
		$count=$count==''?10:$count;
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		$code='';
		for ($i=0; $i < $count; $i++) { 
			str_shuffle($chars);
			$code.=$chars[mt_rand(0,(strlen($chars) - 1))];
		}
		return $code;
	}

	public static function auth($login,$pas)
	{
		if(empty($login)||empty($pas))
		{
			self::setErrors("function auth: Не задан логин и/или пароль");
			return false;
		}
		if(!session_id())
		{
			session_start();	
		}
		if(!session_id())
		{
			self::setErrors("function auth: Ошибка старта сессии");
			return false;
		}
		$q="SELECT `id` FROM `".self::$tab_us."` WHERE `".self::$log_us."`=:login AND `".self::$pas_us."`=:pass";
		$us_id=DB::query($q,'n',array('login' =>mysql_escape_string($login),'pass'=>md5(md5($pas))));
		if(count($us_id)>0)
		{
			$code=self::getCode();
			$hash=md5($code);
			if(DB::update(self::$tab_us,array(self::$ses_us=>session_id(),self::$hash_us=>$hash)," `id`='".$us_id[0][0]."'"))
			{
				$_SESSION['usid']=$us_id[0][0];
				$_SESSION['ushash']=$hash;
				return true;
			}

		}
		return false;
	}

	public static function check_auth()
	{
		if(!session_id())
		{
			session_start();	
		}
		if(empty($_SESSION['usid'])||empty($_SESSION['ushash']))
		{
			return false;
		}
		$info_id=DB::query("SELECT `".self::$ses_us."`,`".self::$hash_us."` FROM `".self::$tab_us."` WHERE `id`='".intval($_SESSION['usid'])."'",'n');
		if(count($info_id)>0)
		{
			if(session_id()!=$info_id[0][0]||$_SESSION['ushash']!=$info_id[0][1])
			{
				return false;
			}else
			{
				return true;
			}
		}
		return false;
	}

	public static function log_out()
	{
		if(!session_id())
		{
			session_start();	
		}
		DB::update(self::$tab_us,array(self::$ses_us=>'',self::$hash_us=>'')," `id`='".intval($_SESSION['usid'])."'");
		unset($_SESSION['usid']);
		unset($_SESSION['ushash']);
	}

	public static function new_pas($login,$hash='',$newPass='')
	{
		if($hash=='')
		{
			$info_id=DB::query("SELECT `id` FROM `".self::$tab_us."` WHERE `".self::$log_us."`='".mysql_escape_string($login)."'",'n');
		}else
		{
			$info_id=DB::query("SELECT `id` FROM `".self::$tab_us."` WHERE `".self::$log_us."`='".mysql_escape_string($login)."' AND `".self::$hash_us."`='".mysql_escape_string($hash)."'",'n');
		}
		if(count($info_id)>0)
		{
			
			$new_pas=$newPass==''?self::getCode(6):$newPass;
			if(DB::update(self::$tab_us,array(self::$pas_us=>md5(md5($new_pas)),self::$hash_us=>'')," `id`='".intval($info_id[0][0])."'"))
			{
				return $new_pas;
			}else
			{
				return false;
			}
		}
		return false;
	}

	public static function new_user($login,$pas,$name)
	{
		if(empty($login)||empty($pas))
		{
			self::setErrors("function auth: Не задан логин и/или пароль");
			return array(false,'Не задан логин и/или пароль');
		}
		

		$q="SELECT `id` FROM `".self::$tab_us."` WHERE `".self::$log_us."`=:login";
		$us_id=DB::query($q,'n',array('login' =>mysql_escape_string($login)));
		if($us_id)
		{
			return array(false,'Логин уже зарегистрирован воспользуйтесь восстановлнием пароля');
		}else{
			$res=DB::insert(self::$tab_us,array(self::$log_us,self::$pas_us,self::$nam_us),array($login,md5(md5($pas)),$name));
			if($res)
			{
				return array(true,'Регистрация прошла успешно');
			}
		}
		return array(false,'Неизвестная ошибка');
	}	

	public static function getBasket(){
		if(self::check_auth()){
			$id_client=intVal($_SESSION['usid']);
			$bs=DB::query("SELECT `id` FROM `basket` WHERE `client`='".$id_client."'",'a');
			if($bs){
				return Basket::getInstance($bs[0]['id']);
			}else{
				$new_id=DB::insert('basket',array('client'),array($id_client));
				if($new_id)
				{
					$new_id=$new_id[0];
					return Basket::getInstance($new_id);
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
	}
}
?>