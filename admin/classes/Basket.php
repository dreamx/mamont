<?php
class Basket {
	private static $instance;
	private $table='basket';//таблица заказов
	public $id;// id заказа
	private $client;//клиент
	private $summ;// корзины с учетом скидок на товары и на сам заказ
  	public $date;// дата заказа
  	private $items;// массив товаров в корзине
  	private $discount;// скидка на корзину
  	private $sum;// сумма без скидки на заказ 
  	private $s_act;// сумма товаров с акцией
  	private $s;// сумма товаров без акцией
  	private $arr_links=array();//массив ссылок на скидку

  	private function __construct($id=''){
  		if($id!=''){
			$order_info=DB::query("SELECT * FROM `{$this->table}` WHERE `id`='".intval($id)."' LIMIT 1",'a');
			if($order_info){
				$this->setInfo($order_info[0]);
			}else{
				throw new Exception('Корзина с номером '.$id.' не найдена');
			}
		}else{
			throw new Exception('Укажите номер корзины');
		}
	}

	public static function getInstance($id=''){
		if(!isset(self::$instance)){
			if(empty($id)){
				return false;
			}
			try {
				self::$instance=new Basket($id);
			}catch (Exception $e){
				echo $e->getMessage();
				return false;
			}
		}
		return self::$instance;
	}

	public function setInfo($ord){
		extract($ord);
		$this->id=intval($id);
		$this->client=$client;
		$this->date=$date;
		$this->setDiscount();
		$this->set_links();
	}

	private function set_links(){
		$q="SELECT '4' AS `type`,`ld`.`id_prod`,`d`.`edinici`,`d`.`value`,'скидка по ссылке' AS `name`,`ld`.`date_begin`,`ld`.`date_end`
			FROM `link_client_links` AS `l`
			LEFT JOIN `links_discount` AS `ld` ON `l`.`id_note2`=`ld`.`id`
			LEFT JOIN `discount` AS `d` ON `ld`.`id_discount`=`d`.`id`";
		$links=DB::query($q,'a');
			if($links){
				$this->arr_links=$links;
			}
	}

	/* проверяет наличие скидок на корзину и записывает в свойства объекта,
		так как скидка на корзину может быть только одна, берётся первая строка   
	*/
	private function setDiscount(){
		if(!empty($this->id)){
			$disc=DB::query("SELECT `bd`.* FROM `basket_item_discount` AS `bd` LEFT JOIN `basket_item` AS `bi` ON `bi`.`id`=`bd`.`id_item` WHERE `bi`.`basket`='{$this->id}' LIMIT 1",'a');
			if($disc){
				$this->discount=$disc[0];
			}
		}
	}


	public function get_discounts(){
		return $this->discount;
	}

	public function getItems(){
		if(empty($this->items)){
			$items=DB::query("SELECT `id` FROM `basket_item` WHERE `basket`='{$this->id}' ORDER BY `id`",'a');
			if($items){
				$this->items=array();
				foreach ($items as $it) {
					$item=BasketItem::getNewItem($it['id'],$this->arr_links);
					$this->items[]=$item;
				}
			}
		}
		return $this->items;
	}

	public function refreshItems(){
		$this->items=array();
		$items=DB::query("SELECT `id` FROM `basket_item` WHERE `basket`='{$this->id}' ORDER BY `id`",'a');
		if($items){
			foreach ($items as $it) {
				$item=BasketItem::getNewItem($it['id'],$this->arr_links);
				$this->items[]=$item;
			}
		}
		
	}

	public function clearBasket(){
		$items=$this->getItems();
		if(count($items)>0){
			foreach ($items as $it) {
				$it->del_item();
			}
			$this->refreshItems();
		}
	}

	public function getDate(){
		return $this->date;
	}

	public static function add_basket($id_client){
		if(is_numeric($id_client)){
			$res=DB::insert('basket',array('client'),array($id_client));
			if($res){
				$id=$res[0];
				return $id;
			}
		}
		return false;
	}

	public function get_sum(){
		$this->set_sum();
		return $this->sum;
	}

	

	public function getDisc_str(){
		$dis=$this->discount;
		if($dis){
			if($dis['edinici']=="rub")
			{
				return $dis['value']." руб.";
			}
			if($dis['edinici']=="perc")
			{
				return $dis['value']." %";
			}
		}else{
			return '';
		}
	}

	

	public function set_sum(){
		$s_act=0;
		$s=0;
		if($this->sum==0){
		if(count($this->getItems())>0){
			
			foreach ($this->getItems() as $item) {
				if(count($item->get_discounts())>0){
					$s_act+=$item->get_summa_disc(date("Y-m-d H:i:s",time()));//цена товара с учетом скидок действующих на данный момент
				}else{
					$s+=$item->get_summa();
				}
			}
			$this->s_act=$s_act;
			$this->s=$s;
			$this->sum=$s+$s_act;
		}
		}
	}

	/* добавляет скидку к корзине */
	public function set_discount_items($date_begin,$date_end,$edinici,$value){
		if(count($this->getItems())>0){
			if(count($this->get_discounts())>0){
				DB::query(" DELETE `bd`.* 
							FROM `basket_item_discount` AS `bd` 
							LEFT JOIN `basket_item` AS `bi` ON `bd`.`id_item`=`bi`.`id`
							WHERE `bi`.`basket`='{$this->id}' ");
			}
			if(count($this->getItems())>0){
				$values=array();
				foreach ($this->getItems() as $item) {
					if(count($item->get_discounts())>0){
						
					}else{
						$values[]=array($item->getId(),'4',$edinici,$value,'Скидка на корзину',$date_begin,$date_end);
					}
				}
				if(count($values)>0){
					$res=DB::insert('basket_item_discount',array('id_item','type','edinici','value','name','date_begin','date_end'),$values);
					if($res){return true;}else{return false;}
				}
				return false;
			}
		}else{
			return false;
		}
	}
	
	public function getSum(){
		$this->set_sum();
		$s=$this->s;
		if($this->summ==0){
		if(count($this->getItems())>0){
			
			$dis=$this->get_discounts();
			if(count($dis)>0 && $this->s>0){ //делаем скидку заказа на сумму товаров, у которых нет скидок
				$tb=strtotime($dis['date_begin']); //время начала действия
				$te=strtotime($dis['date_end']); //время конца действия
				$time=time();
				//проверяем действует ли скидка сейчас, если да то учитываем её
				if($time>=$tb&&$time<=$te){
					if($dis['edinici']=='rub'){
							$s=$this->s-$dis['value'];
						}
					if($dis['edinici']=='perc'){
						$s=$this->s-$this->s*$dis['value']/100;
					}
				}
			}
			$this->summ=$this->s_act+$s;
		}
		}
		return $this->summ;
	}

	/* переводит корзину в заказ */
	public function turn_order($adres='',$transport='',$comment='',$promo=''){
		$res=DB::insert('order',array('client','promo_cod','adres','transport','comment'),array($this->client,$promo,$adres,$transport,$comment));
		if($res){
			$id=$res[0];
			if(!empty($promo)){
				$q="SELECT `d`.* 
				FROM `promo_cod` AS `p`
				LEFT JOIN `discount`AS `d` ON `p`.`discount`=`d`.`id`
				WHERE `p`.`status`='0' AND cod=:cod AND `d`.`public`='on'";
				$ins=DB::getConnect()->prepare($q);
				$ins->execute(array(':cod'=>$promo));
				$disc=$ins->fetchAll(PDO::FETCH_ASSOC);
				if($disc!==false){
					$val=array();
					foreach ($disc as $d) {
						$val=array($id,$d['type'],$d['edinici'],$d['value'],$d['name'],$d['date_begin'],$d['date_end']);
					}
					DB::insert('order_discount',array('id_order','type','edinici','value','name','date_begin','date_end'),$val);
				}
			}
			if(count($this->getItems())>0){
				foreach ($this->getItems() as $item) {
					$id_item=DB::insert('order_item',array('title','price','count','order'),array($item->getTitle(),$item->getPrice(),$item->getCount(),$id));
					if($id_item){
						$id_item=$id_item[0];
						$chars=$item->get_chars();
						if(count($chars)>0){
							$values=array();
							foreach ($chars as $char) {
								$values[]=array($char['title'],$char['si'],$char['value'],$char['price'],$id_item);
							}
							DB::insert('order_item_char',array('title','si','value','price','id_item'),$values);
						}
						$discounts=array_merge($item->getDiscountBasket(),$item->get_discounts());
						if(count($discounts)>0){
							$values=array();
							foreach ($discounts as $disc) {
								$values[]=array($disc['type'],$disc['edinici'],$disc['value'],$disc['name'],$disc['date_begin'],$disc['date_end'],$id_item);
							}
							DB::insert('order_item_discount',array('type','edinici','value','name','date_begin','date_end','id_item'),$values);
						}
					}
				}
			}
			/* удаляем корзину */
			DB::query("DELETE FROM `basket` WHERE `id`='{$this->id}'");
			/* очищаем все свойства что бы исключить обращение к ним после удаления корзины */
			$this->instance=null;
			$this->table='';//таблица заказов
			$this->id='';// id заказа
			$this->client='';//клиент
			$this->summ='';// корзины с учетом скидок на товары и на сам заказ
		  	$this->date='';// дата заказа
		  	$this->items='';// массив товаров в корзине
		  	$this->discount='';// скидка на корзину
		  	$this->sum='';// сумма без скидки на заказ 
		  	$this->s_act='';// сумма товаров с акцией
		  	$this->s='';// сумма товаров без акцией
		  	$this->arr_links='';
		  	/* возвращаем id созданного заказа */
			return $id;
		}
		return false;
	}

	

	public function view_basket_admin(){
		?>
		Cумма корзины: <?php echo $this->get_sum()." руб.";?><br>
		Скидка на корзину(не распространяется на акционный товар): <?php $d=$this->get_discounts(); if($d){echo $this->getDisc_str()." действует до ".$d['date_end'];}?><br>
		Cумма корзины со скидкой: <?php echo $this->getSum()." руб.";?>
		
		<table id="tab_ord" style="width:100%;" class="table table-striped table-bordered">
          <thead>
            <th>Наименование</th>
            <th>Характеристики</th>
            <th>Кол-во</th>
            <th>Цена</th>
            <th>Скидка</th>
            <th>Итого</th>
            
          </thead>
          <tbody>
            <?php
              if(count($this->getItems())>0){
                foreach ($this->getItems() as $pr){
                  ?>
                  <tr id="it<?=$pr->getId();?>">
                    <td><?php echo $pr->getTitle();?></td>
                    <td><?php echo $pr->get_chars_str();?></td>
                    <td><?php echo $pr->getCount();?></td>
                    <td><?php echo $pr->get_sum();?></td>
                    <td><?php print_r($pr->getMaxDisc_str($this->date));?></td>
                    <td><?php echo $pr->get_summa_disc(date("Y-m-d H:i:s",time()));?></td>
                  </tr>
                  <?
                }
              }
            ?>
          </tbody>
        </table>
        <script type="text/javascript">
	        var tab_ord=$('#tab_ord').dataTable({
	            "language": {
	                "paging":false,
	                "lengthMenu": "",
	                "zeroRecords": "Ничего не найдено",
	                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
	                "infoEmpty": "",
	                "search": "Поиск: ",
	                "paginate": {
	                "previous": "",
	                "next": ""
	                }
	            },
	            tableTools: {
	            "aButtons": [ ]
	        }
	          });

			var api_ord=tab_ord.api();
        </script>
		<?
	}

}
?>