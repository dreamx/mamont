<?php
class tree
{
	public $arr=array();//многомерный массив дерева
	public $orig;//оригинальный массив
	public $orig_key;//оригинальный массив ключами являются идентификаторы
	public $parent;//название поля родителя
	public $id;//название поля идентификатора
	
	function __construct($parent,$id,$array){
		$this->parent=$parent;
		$this->id=$id;
		$this->orig=$array;
		$this->arr=$this->rec_round($array,0);
		$this->set_orig_key($array);
	}

	//рекурсивно строит массив дерева
	function rec_round($arr,$parent){
		$res=array();
		foreach ($arr as $val) {
			if($parent==$val[$this->parent]){
				$res[$val[$this->id]]=$val;
				$res[$val[$this->id]]['childs']=$this->rec_round($arr,$val[$this->id]);
					
			}
		}
		return $res;
	}

	function set_orig_key($arr){
		$res=array();
		foreach ($arr as $val) {
			$res[$val[$this->id]]=$val;
		}
		$this->orig_key=$res;
	}

	//возвращает массив родителей 
	function get_parents($child){
		$res=array();
		$parent_id=$this->orig_key[$child][$this->parent];
		if($parent_id!=0){
			array_push($res,$this->orig_key[$parent_id]);
			$res=array_merge($res,$this->get_parents($parent_id));
		}
		return $res;
	}
}

?>