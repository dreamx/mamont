<?php
include "DB.php";
/* Ядро системы */

class core
{
	//private $host='localhost', $login='root' ,$pasw='admin' ,$db='mineral'; 
	//private $host='p29871.mysql.ihc.ru', $login='p29871_prime' ,$pasw='qzSU8JSGLh' ,$db='p29871_prime'; 
	private $errors =  array(
		"Нет подключения к базе данных"
		);
	/*
	function __construct(argument)
	{
		# code...
	}
	*/
	function filter($field){
	    $field = stripslashes($field);
	    $field = htmlspecialchars($field);
	    return $field;
	}

	/*private function connect()
    {
        if (!@mysql_connect($this->host, $this->login, $this->pasw))
        {
            return false;
        } else
        {
            @mysql_select_db($this->db);
            @mysql_query("set names utf8");
            @mysql_query("set character_set_client='utf8'");
            @mysql_query ("set character_set_results='utf8'");
            @mysql_query ("set collation_connection='utf8_unicode_ci'");
            return true;
        }

    }

    private function disconnect()
    {
        //mysql_close();
    }*/

	function Auth()
	{
		if(isset($_SESSION['auth']) && isset($_SESSION['user_id']) && $_SESSION['auth']==true){ return true; }else{ return false; }
	}

	function _list($q)
	{
		return DB::query($q,'d');
	}

	function _query($q)
    {
    	
	        $query = DB::query($q);
	        if($query!==false)
            {
            	return true;
	        }else
	        {
	        	return false;
	        }
    	
    }
	function check_permiss($user_id,$module,$type){
		if(empty($_SESSION['user_id']) && empty($_SESSION['auth'])){
			//header('Location: /admin');
			?>
			<script>top.location.href = '/admin/';</script>
			<?php
		}
		if(DB::getConnect()){
			$user = $this->_list("select `Users_permissions` from `Users` where `id`='".$user_id."'");
			if(count($user)>0)
			{
				
		
					$permissions = json_decode($user[0]['Users_permissions'],true);
					if(array_key_exists($module,$permissions))
					{

						$perm = $permissions[$module];
						if($perm[$type]=='on'){
							return true;
						}else{
							return false;
						}
					}
					else
					{
						return false;
					}

			}
			else
			{
				return false;
			}
		}
		else
		{
			return $errors[0];
		}

	}

	function module_table($module_name,$add_button_title,$table_id,$list_query,$list_header){

		if($this->check_permiss($_SESSION['user_id'],$module_name,'create')){ ?> 

		<a class="btn btn-primary add_bt" onclick="open_note('<?php echo $module_name;?>-','from_page','add')"><span class="glyphicon glyphicon-plus"></span> <?php echo $add_button_title;?></a>
		<br><br>

		<?php }

		$block_list = $this->_list($list_query);

		?>

		<table class="table table-striped table-bordered" width="100%" cellspacing="0" id="<?php echo $table_id;?>">
		<thead>
		<tr>

		<?php 
		foreach($list_header as $th_title){
		        echo "<th>".$th_title."</th>";
		    }

		    
		    echo "<th width='250'>&nbsp;</th>";
		 ?>
		</tr>
		</thead>
		


		<tbody>
		<?php
		if($block_list){
		    foreach ($block_list as $note){

		        ?>
		        <tr id='<?php echo $module_name."-".$note['id']; ?>'>
		        <?php 
		        $this->table_string($note,$module_name);
		        ?>
		        </tr>
		        <?php

		    }
		}
		?>
		</tbody>

		</table>

		<script type="text/javascript">
			$(document).ready(function() {
		    	 mt_tb = $('#<?php echo $table_id;?>').dataTable({
		    		"language": {
		            "lengthMenu": "Выводить по _MENU_ строк на странице",
		            "zeroRecords": "Ничего не найдено",
		            "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
		            "infoEmpty": "Нет записей",
		            "search": "Поиск: ",
		            "paginate": {
		      			"previous": "предыдущая",
		      			"next": "следующая",
		    		}

		        },
                    "aaSorting":[],

		        "oTableTools": {
	              "sRowSelect": "multi",
	              "aButtons": [ 
	              {"sExtends":"select_all","sButtonText":"Выделить все"}, 
	              {"sExtends":"select_none","sButtonText":"Снять выделение"},
	              {"sExtends":"text","sButtonText":"<span id='delete_selected' class='glyphicon glyphicon-trash'></span>"}

	               ]
	         	}
		    	});
		    	mt=mt_tb.api(); 
		    	 
		    	var delete_selected = $('#delete_selected').parent('span').parent('a');
		    	delete_selected.addClass('btn-danger');
		    	delete_selected.addClass('disabled');
		    	delete_selected.click(function(){delete_selected_rows('<?php echo $module_name;?>')});
		    	$('#<?php echo $table_id;?>').find('tbody').click(function(){
		    		check_selected_notes();
		    	});
		    	$('.DTTT').click(function(){
		    		check_selected_notes();
		    	});
		    	$('#users-1').find('.btn-danger').remove();


		    	function check_selected_notes(){
		    		if($('#<?php echo $table_id;?>').find("tr.active").length){
		    			delete_selected.removeClass('disabled');
		    		}else{
		    			delete_selected.addClass('disabled');
		    		}
		    	}

			} );
			
		</script>

	<?php }
	function table_string($note,$module_name)
	{

		$rows = (count($note)/2);
		for ($i=1;$i<$rows;$i++){
			 $row = $note[$i];
			 echo "<td>".$row."</td>";
		}


		echo "<td>";
		if($this->check_permiss($_SESSION['user_id'],$module_name,'write'))
			{
			 ?> <a href="#<?php echo $note['id'];?>" class="btn btn-success" onclick="open_note(this,'from_table','edit')"><span class="glyphicon glyphicon-pencil"></span> Изменить</a>&nbsp; <?php
			}else{
			?>  <a href="#<?php echo $note['id'];?>" class="btn btn-success" onclick="open_note(this,'from_table','edit')"><span class="glyphicon glyphicon-eye-open"></span> Смотреть</a>&nbsp; <?php
			}
			if($this->check_permiss($_SESSION['user_id'],$module_name,'delete') && !($module_name=='Users' && $note['id']==1))
			{
			   ?> <a href="#" class="btn btn-danger" onclick="delete_note(this,'from_table')"><span class="glyphicon glyphicon-trash"></span> Удалить</a>&nbsp; <?php
			}
			echo "</td>";
			 
			       
	}

	function make_top_menu($modules_all){
		foreach($modules_all as $key=>$menu_item){
			if($this->check_permiss($_SESSION['user_id'],$menu_item[0],'read') && $key!=0){
				echo "<li><a href='/admin/modules/".$menu_item[0]."'>".$menu_item[1]."</a></li>";
			}
		}
	}

	function update_note($table,$fields,$values,$id){
		
		if(DB::update($table,$values,"`id`='".intval($id)."'")){
			return true;
		}else{
			return false;
		}
	}

	function delete_note_by_id($table,$id,$linked_imgs=array(),$linked_tables=array()){
		$q="delete from `".$table."` where `id`='".intval($id)."'";
			if(DB::query($q)){
				
				if(count($linked_imgs)>0){
					$this->del_linked_imgs($id,$linked_imgs);
				}
				return true;
			}else{
				return false;
			}
		
	}

	function delete_note_by_ids($table,$ids,$linked_imgs=array(),$linked_tables=array()){
		$q="delete from `".$table."` where `id` in (".$ids.")";
			if(DB::query($q)){
				$ids_arr = explode(",",$ids);
				if(count($linked_imgs)>0){
					foreach($ids_arr as $id){
					$this->del_linked_imgs($id,$linked_imgs);
					}
				}
				return true;
			}else{
				return false;
			}
		
	}

	function delete_tree_node($table,$id,$parent_name,$linked_imgs=array()){
			
			$q="delete from `".$table."` where `id`='".intval($id)."'";
			if(DB::query($q)){
				if(count($linked_imgs)>0){
					$this->del_linked_imgs($id,$linked_imgs);
				}
				$children = $this->_list("select `id` from `".$table."` where `".$parent_name."`='".$id."'");
				foreach($children as $child){
					$this->delete_tree_node($table,$child['id'],$parent_name);

				}
				return true;
			}else{
				return false;
			}
		
	}

	function del_linked_imgs($id,$linked_imgs){
		$dir = "../../img/";
		$thumb_dir = $dir."thumbs/";
		foreach($linked_imgs as $link){
			$imgs = $this->_list("select `id`,`Photos_ex` from `Photos` where `Photos_module`='".$link."' and `Photos_parent`='".$id."'");
			foreach($imgs as $img){
				if($this->delete_note_by_id('Photos',intval($img['id']))){
					@unlink($dir.intval($img['id']).".".$img['Photos_ex']);
					@unlink($thumb_dir.intval($img['id']).".".$img['Photos_ex']);
				}
			}
		}
	}



	function add_note($table,$fields_for_add){
		$fields = array();
		$values = array();
		$note = array();
		foreach($fields_for_add as $field=>$value){
			array_push($fields,$field);
			array_push($values,$value);
		}
		$res=DB::insert($table,$fields,$values);
		if($res){
			return $res[0];
		}else{
			return false;
		}
	}

	function make_field($label="",$name="",$type="text",$value="",$values=array(),$placeholder="",$readonly="",$required="",$pattern=""){
            
			switch ($type) {
				case 'text':
					$cnt = "<input pattern='".$pattern."' name='".$name."' value='".$value."' type='text' class='form-control' id='field-".$name."' placeholder='".$placeholder."' ".$readonly." ".$required.">";
					break;
				case 'textarea':
					$cnt = "<textarea pattern='".$pattern."' name='".$name."'  class='form-control' id='field-".$name."' placeholder='".$placeholder."' ".$readonly." ".$required.">".$value."</textarea>";
					break;	
				case 'password':
					$cnt = "<input pattern='".$pattern."' name='".$name."' value='".$value."' type='password' class='form-control' id='field-".$name."' placeholder='".$placeholder."' ".$readonly." ".$required.">";
					break;
				case "date":
					$cnt = "<div class='input-group date' id='datepicker-".$name."' data-date-format='YYYY-MM-DD HH:mm'>";
					$cnt .= "<input pattern='".$pattern."' name='".$name."' value='".$value."' type='text' class='form-control' id='field-".$name."' placeholder='".$placeholder."' ".$readonly." ".$required.">";
					$cnt .= "<span class='input-group-addon'><span style='font-size:18px;' class='glyphicon glyphicon-calendar'></span></span>";
					$cnt .= "</div>";
					$cnt .= "<script>
					var now = new Date();
					var formated_date = now.getFullYear()+'-'+(now.getMonth() + 1)+'-'+now.getDate() + '00:00:00';
						
						$('#datepicker-".$name."').datetimepicker({
                    		language: 'ru',
                    		pickTime: true,
                    		minDate:'2014-08-01',
                    		format: 'yyyy-mm-dd HH:ii',
                    		autoclose: true,
                    		showToday: true
                		});
					</script>";
				break;

				case 'checkbox':
					if($value == "on"){
						$checked = "checked";
					}else{
						$checked = "";
					}
					$cnt = "<input  name='".$name."' ".$checked."  type='checkbox' class='' style='height:28px;' id='field-".$name."' ".$readonly." ".$required.">";
					break;
				case 'hidden':
					$cnt = "<input type='hidden' name='".$name."' value='".$value."'' id='field-".$name."'/>";
					echo $cnt;
					return true;

				case 'select':
					$options = array();
					foreach($values as $key=>$val){
						if($key == $value){
							$selected = "selected";
						}else{
							$selected = "";
						}
						$option = "<option ".$selected." value='".$key."'>".$val."</option>";
						array_push($options,$option);
					}
					
					$cnt = "<select id='field-".$name."' name='".$name."' class='form-control'>".implode(" ", $options)."</select>
					<script>$('#field-".$name."').chosen({width:'100%'});</script>
					";
					break;
				case 'hierarchy':
					function get_child($array,$level,$parent,$value,$str){
			            $option='';
			            if(is_array($array)){
			            foreach ($array as $val) {
			              if($val['id'] == $value){
			                $selected = "selected";
			              }else{
			                $selected = "";
			              }
			              if($val['parent']==$parent){
			                $ch=get_child($array,$level+1,$val['id'],$value,$str);
			                $option .="<option ".$selected." value='".$val['id']."'>".str_repeat($str,$level).$val['title']."</option>";
			                
			                if($ch){$option.=$ch;}
			              }
			            }
			            
			            }
			            if($option==''){
			              return false;
			            }else{
			              return $option;
			            }
			          }
          					$cnt="<select id='field-".$name."' name='".$name."' class='form-control'>".get_child($values,0,$values[0]['parent'],$value,'&nbsp;')."</select>";
          				
				break;

				case 'hierarchy2':
					function get_child($array,$level,$parent,$value,$str){
			            $option='';
			            if(is_array($array)){
			            foreach ($array as $val) {
			              if($val['id'] == $value){
			                $selected = "selected";
			              }else{
			                $selected = "";
			              }
			              if($val['parent']==$parent){
			                $ch=get_child($array,$level+1,$val['id'],$value,$str);
			                if($ch){
			                	$option .="<optgroup label=\"".str_repeat($str,$level).$val['title']."\">".$ch."</optgroup>";
			                }else{
			                	$option .="<option ".$selected." value='".$val['id']."'>".str_repeat($str,$level).$val['title']."</option>";
			                }
			                //if($ch){$option.=$ch;}
			              }
			            }
			            
			            }
			            if($option==''){
			              return false;
			            }else{
			              return $option;
			            }
			          }
          					$cnt="<select id='field-".$name."' name='".$name."' class='form-control'>".get_child($values,0,$values[0]['parent'],$value,'&nbsp;')."</select>";
          				
				break;		
				
				default:
					$cnt = "<input pattern='".$pattern."' name='".$name."' value='".$value."' type='text' class='form-control' id='field-".$name."' placeholder='".$placeholder."' ".$readonly." ".$required.">";
					break;
			}
	
		?>
			<div class="form-group">
			    <label for="field-<?php echo $name;?>" class="col-sm-2 control-label"><?php echo $label;?></label>
			    <div class="col-sm-6">
			      <? echo $cnt;?>
			    </div>
			</div>
		<?php 

	}

	function make_wisiwyg($name="",$content="",$readonly){
		?>

				<textarea class="col-md-12 wisiwyg" style="height:550px;" name="<?php echo $name;?>" id="field-<?php echo $name;?>" <?php echo $readonly;?>>
				<?php echo $content;?>
				</textarea>

		<script type="text/javascript">
		var hEd = CKEDITOR.instances['field-<?php echo $name;?>'];
    	if (hEd) {
        CKEDITOR.remove(hEd);
   		}
		CKEDITOR.replace( 'field-<?php echo $name;?>', { 
			language: 'ru',
			'filebrowserBrowseUrl':'/admin/wisiwyg/kcfinder/browse.php?type=files',
  			'filebrowserImageBrowseUrl':'/admin/wisiwyg/kcfinder/browse.php?type=images',
  			'filebrowserFlashBrowseUrl':'/admin/wisiwyg/kcfinder/browse.php?type=flash',
  			'filebrowserUploadUrl':'/admin/wisiwyg/kcfinder/upload.php?type=files',
  			'filebrowserImageUploadUrl':'/admin/wisiwyg/kcfinder/upload.php?type=images',
  			'filebrowserFlashUploadUrl':'/admin/wisiwyg/kcfinder/upload.php?type=flash'


		});
		</script>
		<?php
	}

	function module_tree($module_name,$list_query,$parent_name,$title_name,$sort_name){
        echo '<div id="'.$module_name.'_tree" >';
        


        $all_notes = $this->_list($list_query);
        $this->make_tree(0,$all_notes,$parent_name,$title_name,$module_name,$sort_name);
        ?>
        <script>
        $('#<?php echo $module_name; ?>_tree').find('.tree_nodes_list:not(:first)').hide();
        </script>
        <?php
        echo "</div>"; 
      }

      function make_tree($parent,$all_notes,$parent_name,$title_name,$module_name,$sort_name){
      	$style = "";
      	if($parent!=0){
      	$style = "style='margin-left:34px;'";
        }
         if($this->check_permiss($_SESSION['user_id'],$module_name,'create') && $parent == 0){
        	echo "<div  id='".$module_name."_0'>";
        	echo "<div><button onclick='add_tree_node(this);' class='btn btn-primary'><span class='glyphicon glyphicon-save'> </span> Добавить</button></div>";
        	echo "</div><br>";
        }

        echo "<div class='tree_nodes_list' ".$style.">";
       	if($all_notes){
        foreach ($all_notes as $key => $value) {
          if($value[$parent_name] == $parent){
            echo "<div class='tree_nodes_cnt'  id='".$module_name."_".$value['id']."'>";
            $this->make_tree_node($value,$title_name,$parent_name,$all_notes,$module_name,$sort_name);
            if($this->has_tree_children($value['id'],$parent_name,$all_notes)) {
              $this->make_tree($value['id'],$all_notes,$parent_name,$title_name,$module_name,$sort_name);
            }
            echo "</div>";
          }
        }
    	}
        echo "</div>";
      }

      function make_tree_node($node,$title_name,$parent_name,$all_notes,$module_name,$sort_name){
        $plus = '&nbsp;';
        $plus_btn = 'btn-default';
        $plus_click = '';
        if($this->has_tree_children($node['id'],$parent_name,$all_notes)){
          $plus = '<span class="glyphicon glyphicon-plus"> </span>';
          $plus_btn = 'btn-success';
          $plus_click = "onclick='open_node(this)'";
        }
        $node_title = $node[$title_name];

        echo $this->draw_node($plus,$plus_click,$plus_btn,$node_title,$module_name,$node[$sort_name]);
        
      }

      function draw_node($plus,$plus_click,$plus_btn,$node_title,$module_name,$num=1){
      	$node_html = "<div class='btn-group' rel='".$num."'>
              <button ".$plus_click." type='button' class='btn ".$plus_btn." open_button' style='width:40px;'>".$plus."</button>
              <button type='button' class='btn btn-default' style='width:400px;overflow:hidden;text-align:left;'>".$node_title."</button>";
        if($this->check_permiss($_SESSION['user_id'],$module_name,'create')){
        	$node_html .= "<button onclick='add_tree_node(this);' type='button' class='btn btn-primary' style='width:40px;'><span class='glyphicon glyphicon-save'> </span></button>";
        }
        if($this->check_permiss($_SESSION['user_id'],$module_name,'write') || $this->check_permiss($_SESSION['user_id'],$module_name,'read')){
        	if($this->check_permiss($_SESSION['user_id'],$module_name,'write')){
       			$node_html .= "<button onclick=open_note(this,'from_tree','edit') type='button' class='btn btn-success' style='width:40px;'><span class='glyphicon glyphicon-pencil'> </span></button>";
    			$node_html .= "<button rel='up' onclick='sort_tree_node(this);' type='button' class='btn btn-info' style='width:40px;'><span class='glyphicon glyphicon-arrow-up'> </span></button>";
        		$node_html .= "<button rel='down' onclick='sort_tree_node(this);' type='button' class='btn btn-info' style='width:40px;'><span class='glyphicon glyphicon-arrow-down'> </span></button>";
        	}else{
    			$node_html .= "<button onclick=open_note(this,'from_tree','edit') type='button' class='btn btn-success' style='width:40px;'><span class='glyphicon glyphicon-eye-open'> </span></button>";
    		}
    	}
        if($this->check_permiss($_SESSION['user_id'],$module_name,'delete')){
        $node_html .= "<button onclick='del_tree_node(this);' type='button' class='btn btn-danger' style='width:40px;'><span class='glyphicon glyphicon-trash'> </span></button>";
        }     
        $node_html .=      "</div><br>";
        return $node_html;
      }

      function has_tree_children($id,$parent_name,$all_notes){
        $has = false;
        foreach ($all_notes as $key => $value) {
          if($value[$parent_name] == $id){
            return true;
          }
        }

          return $has;
        
      }

      function sort_imgs($link,$parent,$sort_str){
      	$ok  = false;
      	$sort_arr = explode(',', $sort_str);
      	foreach ($sort_arr as $key => $value) {
      		$sort = explode(':', $value);
      		if($this->connect()){
				if(mysql_query("update `Photos` set `Photos_num`='".intval($sort[1])."' where `Photos_module`='".$link."' and `Photos_parent`='".intval($parent)."' and `id`='".$sort[0]."'")){
					//echo(" update `Photos` set `Photos_num`='".intval($sort[1])."' where `Photos_module`='".$link."' and `Photos_parent`='".intval($sort[0])."'");
					$this->disconnect();
					$ok = true;

				}else{
					$ok  = false;
				}
			}else{
				$ok  = false;
			}
      	}
      return $ok;
      }

      /**
     * преобразует строку на русском в транслит
     * 
     */
    function translit($str)
	{
	    $str=trim($str);
	    $rus = array("ё","й","ю","ь","ч","щ","ц","у","к","е","н",
	                 "г","ш","з","х","ъ","ф","ы","в","а","п","р",
	                 "о","л","д","ж","э","я","с","м","и","т","б",
	                 "Ё","Й","Ю","Ч","Ь","Щ","Ц","У","К","Е","Н",
	                 "Г","Ш","З","Х","Ъ","Ф","Ы","В","А","П","Р",
	                 "О","Л","Д","Ж","Э","Я","С","М","И","Т","Б","/","","`","+");
	    $eng = array("yo","j","yu","","ch","shh","c","u","k","e",
	                 "n","g","sh","z","h","","f","y","v","a","p",
	                 "r","o","l","d","zh","e","ya","s","m","i","t",
	                 "b","yo","j","yu","","ch","shh","c","u","k","e",
	                 "n","g","sh","z","h","","f","y","v","a","p",
	                 "r","o","l","d","zh","e","ya","s","m","i","t",
	                 "b","_","_","","");
	    $str=preg_replace('/[\s]+/', '_',  $str);
	    $string = str_replace($rus, $eng,  $str);
	    return $string; 

	}


/* 
* rus_date("j F Y H:i ", strtotime($result['create_date'])
*/
	function rus_date() {
// Перевод
 $translate = array(
 "am" => "дп",
 "pm" => "пп",
 "AM" => "ДП",
 "PM" => "ПП",
 "Monday" => "Понедельник",
 "Mon" => "Пн",
 "Tuesday" => "Вторник",
 "Tue" => "Вт",
 "Wednesday" => "Среда",
 "Wed" => "Ср",
 "Thursday" => "Четверг",
 "Thu" => "Чт",
 "Friday" => "Пятница",
 "Fri" => "Пт",
 "Saturday" => "Суббота",
 "Sat" => "Сб",
 "Sunday" => "Воскресенье",
 "Sun" => "Вс",
 "January" => "Января",
 "Jan" => "Янв",
 "February" => "Февраля",
 "Feb" => "Фев",
 "March" => "Марта",
 "Mar" => "Мар",
 "April" => "Апреля",
 "Apr" => "Апр",
 "May" => "Мая",
 "May" => "Мая",
 "June" => "Июня",
 "Jun" => "Июн",
 "July" => "Июля",
 "Jul" => "Июл",
 "August" => "Августа",
 "Aug" => "Авг",
 "September" => "Сентября",
 "Sep" => "Сен",
 "October" => "Октября",
 "Oct" => "Окт",
 "November" => "Ноября",
 "Nov" => "Ноя",
 "December" => "Декабря",
 "Dec" => "Дек",
 "st" => "ое",
 "nd" => "ое",
 "rd" => "е",
 "th" => "ое"
 );
 // если передали дату, то переводим ее
 if (func_num_args() > 1) {
 $timestamp = func_get_arg(1);
 return strtr(date(func_get_arg(0), $timestamp), $translate);
 } else {
// иначе текущую дату
 return strtr(date(func_get_arg(0)), $translate);
 }
 }

}

$core = new core();

?>