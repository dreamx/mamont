<?php
session_start();
include "../config.php";
include "core.php";
$module = $_POST['module'];
include "../modules/".$module."/settings.php";
if($core->check_permiss($_SESSION['user_id'],$module_name,'create')){
	$max_num = $core->_list("select max(".$sort_name.") as ".$sort_name." from `".$block_table."` where `".$parent_name."`='".intval($_POST['parent'])."'");
	$fields_for_add[$parent_name] = $_POST['parent'];
	$fields_for_add[$sort_name] = intval($max_num[0][$sort_name])+1;
	$id = $core->add_note($block_table,$fields_for_add);
	
	$node_html = '';

	$node_html .= '<div class="tree_nodes_cnt" id="'.$module_name.'_'.$id.'" '.$style.'>';
	$node_html .= $core->draw_node("&nbsp;","","btn-default",$fields_for_add[$title_name],$module,$fields_for_add[$sort_name]);
	$node_html .= '</div>';

	$data = array("status","htm");
	$data['status'] = 'ok';
	$data['htm'] = $node_html; 

	echo json_encode($data);	

}else{
	echo json_encode(array("status"=>"no"));
}
?>