<?php

class Product{

	private $table='product';
	public $id;
	public $chars;//массив характеристик
	public $chars_val;//массив значений характеристик
	public $discount=array();//массив скидок
	public $price;//стоимость товара без скидки
	private static $instance;//объект класса
	private $type_temp=4;// id времменного типа скидки
	private $max_discount;//максимальная скидка, которая применяется
	private $arr_links=array();//массив ссылок на скидку
	private $photos;//массив изображений
	protected $action;
	protected $action_price;

	private function __construct($id='',$arr_links=array()){
  		if($id!=''){
			$prod_info=DB::query("SELECT `{$this->table}`.* FROM `{$this->table}` WHERE `{$this->table}`.`id`='".intval($id)."' LIMIT 1",'a');
			if($prod_info){
				$this->arr_links=$arr_links;
				$this->setInfo($prod_info[0]);
			}
		}
		
	}

	/* возвращает объект класса */
	public static function getInstance($id='',$arr_links=array()){
		if(!isset(self::$instance)){
			if(empty($id)){
				return false;
			}
			self::$instance=new Product($id,$arr_links);
		}
		return self::$instance;
	}

	/* создаёт новый и возвращает объект класса */
	public static function getNewItem($id='',$arr_links=array()){
		
		if(empty($id)){
			return false;
		}
		self::$instance=new Product($id,$arr_links);
		return self::$instance;
	}

	public function getPrice(){
		if($this->action=='on'){
			return $this->action_price;
		}else{
			return $this->price;
		}
	}

	/* заполняет свойства объекта */
	private function setInfo($ord){
		extract($ord);
		$this->id=$id;
		$this->title=$title;
		$this->price=$price;
		$this->setChars();
		$this->setDiscount();
		$this->action=$action;
		$this->action_price=$action_price;
	}

	/* читает характеристики товара из базы и записывает в свойство объекта */ 
	private function setChars(){
		if(!empty($this->id)){
			$chars=DB::query("SELECT `ch`.`title`,`ch`.`si`,`l`.`value_char`,`l`.`value_color`,`v`.`price`,`v`.`id`, concat(`ph`.`id`,'.',`ph`.`Photos_ex`) AS photo
									FROM `char_value` AS `v` 
									LEFT JOIN `char_list` AS `l` ON `v`.`id_char`=`l`.`id`
									LEFT JOIN `char` AS `ch` ON `l`.`id_char`=`ch`.`id`
									LEFT JOIN `Photos` AS `ph` ON `ph`.`Photos_parent`=`l`.`id` AND `ph`.`Photos_module`='char'
        							WHERE `v`.`id_prod`='{$this->id}' 
        							GROUP BY `v`.`id`
        							ORDER BY `ch`.`title`",'a');
			if($chars){
				$res=array();
				$res2=array();
				foreach ($chars as $ch) {
					$res[$ch['title'].' '.$ch['si']][$ch['id']]=array('val'=>$ch['value_char'],'price'=>$ch['price'],'photo'=>$ch['photo'],'valc'=>$ch['value_color']);
					$res2[$ch['id']]=array('val'=>$ch['value_char'],'price'=>$ch['price'],'photo'=>$ch['photo'],'title'=>$ch['title'],'si'=>$ch['si'],'valc'=>$ch['value_color']);
				}
				$this->chars=$res;
				$this->chars_val=$res2;
			}
		}
	}

	/* проверяет наличие скидок у товара и записывает их в свойство */
	private function setDiscount(){
		if(!empty($this->id)){
			$q="SELECT `d`.`type`,`d`.`edinici`,`d`.`value`,`d`.`name`,`d`.`date_begin`,`d`.`date_end` 
				FROM `link_product_discont` AS `l`
				LEFT JOIN `discount` AS `d` ON `l`.`id_note2`=`d`.`id` 
				WHERE `l`.`id_note1`='{$this->id}' AND `d`.`public`='on'";
			$disc=DB::query($q,'a');
			if($disc){
				$this->discount=$disc;
			}
			
			if(count($this->arr_links)>0){
				foreach ($this->arr_links as $value) {
					if($this->id_prod==$value['id_prod']){
						$this->discount[]=$value;
					}
				}
			}
		}
	}

	
	public function get_photos(){
		if(!is_array($this->photos)){
			$q="SELECT concat(`ph`.`id`,'.',`ph`.`Photos_ex`) AS photo
				FROM `Photos` AS `ph` 
				WHERE `ph`.`Photos_parent`='".$this->id."' AND `ph`.`Photos_module`='product'
				ORDER BY `Photos_num`
			";
			$photos=DB::query($q,'n');
			if($photos){
				foreach ($photos as $ph) {
					$this->photos[]=$ph[0];
				}
				
			}
		}
		return $this->photos;
	}
	
	public function get_chars(){
		return $this->chars;
	}


	
	public function get_discounts(){
		return $this->discount;
	}

	/*возвращает максимальную скидку на товар */
	public function getMaxDisc($date){
		if(empty($this->max_discount)&&count($this->discount)>0){
				$summ=$this->price;
				$time_order=strtotime($date);
				$max_rub=0;//начальное значение максимальной скидки
				$res=false;
				foreach ($this->discount as $dis) {
					if($dis['type']==$this->type_temp){ //если скидка временная
						$tb=strtotime($dis['date_begin']); //время начала действия
						$te=strtotime($dis['date_end']); //время конца действия
						if($time_order>=$tb&&$time_order<=$te){// если заказ сделан во время действия скидки, то учитываем её
							if($dis['edinici']=='rub'){
								if($max_rub<$dis['value']){
									$res=$dis;
									$max_rub=$dis['value'];
								}
							}
							if($dis['edinici']=='perc'){
								if($max_rub<$summ*$dis['value']/100){
									$res=$dis;
									$max_rub=$summ*$dis['value']/100;
								}
							}
						}
					}else{
						if($dis['edinici']=='rub'){
							if($max_rub<$dis['value']){
								$res=$dis;
								$max_rub=$dis['value'];
							}
						}
						if($dis['edinici']=='perc'){
							if($max_rub<$summ*$dis['value']/100){
								$res=$dis;
								$max_rub=$summ*$dis['value']/100;
							}
						}
					}	
				}
				$this->max_discount=$res;
			}
		return $this->max_discount;
	}

	/* максимальная скидка строкой */
	public function getMaxDisc_str($date){
		$dis=$this->getMaxDisc($date);
		if($dis){
			if($dis['edinici']=="rub")
			{
				return $dis['value']." руб.";
			}
			if($dis['edinici']=="perc")
			{
				return $dis['value']." %";
			}
		}else{
			return '';
		}
	}

	/* считает максимальную скидку на сумму $summ */
	public function calcDisc($summ){
		$disc=$this->getMaxDisc(time());
		if($disc){
			if($disc['edinici']=="rub")
			{
				return $summ-$disc['value'];
			}
			if($disc['edinici']=="perc")
			{
				return  $summ-$summ*$disc['value']/100;
			}
		}else{
			return $summ;
		}
	}


	
	

}
?>