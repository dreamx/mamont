<?php
	
	class model 
	{
	
		public static function get_path_link($id_child,tree $tree,$prod=false)
		{
			$parents=array_reverse($tree->get_parents($id_child));
			$child=$tree->orig_key[$id_child];

			?>
			<ol class="breadcrumb">
				<li><a href="/">Главная</a></li>
				<?php
				foreach ($parents as $p) {?>
					<li><a href="/<?php echo $p['translit'];?>"><?php echo $p['title'];?></a></li>
					<?php
				}
				if($prod){
					?>
					<li><a href="/<?php echo $child['translit'];?>"><?php echo $child['title'];?></a></li>
					<?php
				}else{
				?>
                <li class="active"><?php echo $child['title'];?></li>
                <?php
            	}
                ?>
            </ol>

                          <?php
		}

		//преобразует массив GET в строку GET параметров
        public static function toGet($arr){
            $res=array();
            if(count($arr)>0){
              foreach ($arr as $key => $value) {
                $res[]=$key."=".$value;
              }
            }
            return implode('&',$res);
          }
	
	}

?>