<?php
/*класс чата*/
class chat {
	protected $chat_table='chat';//таблица с сообщениями
	protected $date_mess='date';//поле дата сообщения
	protected $mess='mess';//поле сообщение
	protected $status_mess='read';//поле статус сообщения
	protected $client_mess='client';//поле клиента в талице сообщений
	protected $manager_mess='user';// поле менеджера в таблице сообщений
	protected $client_table='client';//таблица клиентов
	protected $name_client='name';//поле имя клиента
	protected $firma_client='firma';//поле компания клиента
	protected $activ_client='activ';// поле активности клиента
	protected $manager_client='user';//поле менеджера клиента
	protected $manager_table='Users';//таблица менеджеров
	protected $name_manager='Users_name';//поле имя менеджера

	//возвращает всех клиентов
	public function getAllClient($manager_id){
		$q="SELECT `id`,`{$this->name_client}`,`{$this->firma_client}` FROM `{$this->client_table}` WHERE `{$this->manager_client}`='".intval($manager_id)."'";
		//echo $q;
		return DB::query($q,'a');
	}

	//возвращает все сообщения
	public function getAllDialog($client_id){
		$q="SELECT 
				`chat`.*,
				`cl`.`{$this->name_client}` AS `client_name`,
				`cl`.`{$this->firma_client}` AS `client_firma`,
				`mn`.`{$this->name_manager}` AS `man_name`
			FROM `{$this->chat_table}` AS `chat`
			LEFT JOIN `{$this->client_table}` AS `cl` ON `chat`.`{$this->client_mess}`=`cl`.`id`
			LEFT JOIN `{$this->manager_table}` AS `mn` ON `chat`.`{$this->manager_mess}`=`mn`.`id`
			WHERE `chat`.`{$this->client_mess}`='".intval($client_id)."' 
			ORDER BY `chat`.`{$this->date_mess}`
				";
		return DB::query($q,'a');
	}

	//возвращает новые сообщения
	public function getNewDialog($client_id,$last_id){
		$q="SELECT 
				`chat`.*,
				`cl`.`{$this->name_client}` AS `client_name`,
				`cl`.`{$this->firma_client}` AS `client_firma`,
				`mn`.`{$this->name_manager}` AS `man_name`
			FROM `{$this->chat_table}` AS `chat`
			LEFT JOIN `{$this->client_table}` AS `cl` ON `chat`.`{$this->client_mess}`=`cl`.`id`
			LEFT JOIN `{$this->manager_table}` AS `mn` ON `chat`.`{$this->manager_mess}`=`mn`.`id`
			WHERE `chat`.`{$this->client_mess}`='".intval($client_id)."' AND `chat`.`id`>'".intval($last_id)."'
			ORDER BY `chat`.`{$this->date_mess}`
				";
		return DB::query($q,'a');
	}

	//возвращает соличество не прочитанных клиентом сообщений
	public function countUnread($client_id){
		$q="SELECT count(*)
		FROM `{$this->chat_table}`
		WHERE `{$this->client_mess}`='$client_id' AND `{$this->manager_mess}`!='0' AND `{$this->status_mess}`='0'";
		return DB::query($q,'n');
	}

	//возвращает последние сообщения 
	public function getLastDialog($client_id,$limit=5){
		$q="SELECT 
				`chat`.*,
				`cl`.`{$this->name_client}` AS `client_name`,
				`cl`.`{$this->firma_client}` AS `client_firma`,
				`mn`.`{$this->name_manager}` AS `man_name`
			FROM `{$this->chat_table}` AS `chat`
			LEFT JOIN `{$this->client_table}` AS `cl` ON `chat`.`{$this->client_mess}`=`cl`.`id`
			LEFT JOIN `{$this->manager_table}` AS `mn` ON `chat`.`{$this->manager_mess}`=`mn`.`id`
			WHERE `chat`.`{$this->client_mess}`='".intval($client_id)."'
			ORDER BY `chat`.`{$this->date_mess}` DESC
			LIMIT ".intval($limit)."
				";
		return array_reverse(DB::query($q,'a'));
	}

	//возвращает $limit сообщений начиная с $ferst_id 
	public function getHistoryDialog($client_id,$ferst_id,$limit=5){
		$q="SELECT 
				`chat`.*,
				`cl`.`{$this->name_client}` AS `client_name`,
				`cl`.`{$this->firma_client}` AS `client_firma`,
				`mn`.`{$this->name_manager}` AS `man_name`
			FROM `{$this->chat_table}` AS `chat`
			LEFT JOIN `{$this->client_table}` AS `cl` ON `chat`.`{$this->client_mess}`=`cl`.`id`
			LEFT JOIN `{$this->manager_table}` AS `mn` ON `chat`.`{$this->manager_mess}`=`mn`.`id`
			WHERE `chat`.`{$this->client_mess}`='".intval($client_id)."' AND `chat`.`id`<'".intval($ferst_id)."'
			ORDER BY `chat`.`{$this->date_mess}` DESC
			LIMIT ".intval($limit)."
				";
		return array_reverse(DB::query($q,'a'));
	}

	//информация о клиенте
	public function getClient($id){
		$q="SELECT `id`,`{$this->name_client}`,`{$this->firma_client}`,`{$this->name_client}`,`{$this->activ_client}`
			FROM `{$this->client_table}`
			WHERE `id`='".intval($id)."'";
			
		return DB::query($q,'a');
	}

	//новое сообщение если $user_id не задан, значит сообщение от клиента
	public function addMess($client_id,$mess,$user_id=''){
		if(is_numeric($client_id)){
			if(DB::insert($this->chat_table,array($this->client_mess,$this->mess,$this->manager_mess),array(intval($client_id),$mess,intval($user_id)))){
				return true;
			}else{
				return false;
			}
		}
	}

	//меняет статус сообщений если $type='man', то статус меняется у сообщений отправленных клиентом
	// если нет, то у сообщений отправленных менеджером
	public function setStatusMess($mess,$status,$type='man'){
		if(count($mess)>0){
			$wh=array();
			foreach ($mess as $m){
				$wh[]="`id`='".$m['id']."'";
			}
			if($type=='man'){
				DB::update($this->chat_table,array($this->status_mess=>$status)," `{$this->manager_mess}`='0' and (".implode(' or ', $wh).")");
			}else{
				DB::update($this->chat_table,array($this->status_mess=>$status)," `{$this->manager_mess}`!='0' and (".implode(' or ', $wh).")");
			}
		}
	}

	//информация о состоянии клиентов (в сети или нет,количество новых сообщений)
	public function getInfoClients($id_user){
		$q="SELECT `cl`.`id`,`cl`.`{$this->name_client}`,`cl`.`{$this->activ_client}`,count(`m`.`id`) AS count
			FROM `{$this->client_table}` AS `cl`
			LEFT JOIN `{$this->chat_table}` AS `m` ON `cl`.`id`=`m`.`{$this->client_mess}` AND `m`.`{$this->status_mess}`='0' AND `m`.`{$this->manager_mess}`='0'
			WHERE `cl`.`{$this->manager_client}`='".intval($id_user)."'
			GROUP BY `cl`.`id`
			";
		$clients=DB::query($q,'a');
		if($clients){
			$t=time();
			$res=array();
			foreach ($clients as $cl) {
				if(($t-$cl[$this->activ_client])<40){
					$cl[$this->activ_client]='online';
				}else{
					$cl[$this->activ_client]='offline';
				}
				$res[]=$cl;
			}
		}
		return json_encode($res);
	}
}
?>