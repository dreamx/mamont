<?php
	class neworder{
		protected $items=array();
		protected $client;
		protected $transport=3;
		protected $transport_price=0;
		protected $promo='';
		protected $adres;
		protected $index;
		protected $city;
		protected $comment;

		function __construct($client,$transport,$index=0,$comment,$adres='',$city='',$promo=''){
			if(!empty($client)){
				$this->client=$client;
				$q="SELECT * FROM `transport` WHERE `id`=:id LIMIT 1";
				$trans=DB::query($q,'a',array('id'=>$transport));
				if($trans){
					$this->transport=$trans[0]['id'];
					$this->transport_price=$trans[0]['price'];
				}
				$this->adres=$adres;
				$this->index=$index;
				$this->promo=$promo;
				$this->city=$city;
				$this->comment=$comment;
			}else{
				throw new Exception('Для создания заказа нужен валидный id клиента');
				return false;
			}
		}

		public function addItem($id_prod,$count){
			if(!empty($count)){
				// если такой товар уже добавлен просто увеличиваем количество
				if(isset($this->items[$id_prod])){
					$this->items[$id_prod]['count']+=$count;
					return true;
				}
				$q="SELECT `id`,`title`,`price`,`action_price`,`action`
					FROM `product`
					WHERE `public`='on' AND `id`=:id LIMIT 1";
				$prod=DB::query($q,'a',array('id'=>$id_prod));
				if($prod){
					$prod=$prod[0];
					if($prod['action']=='on'&&$prod['action_price']>0){
						$this->items[$prod['id']]=array('title'=>$prod['title'],'price'=>$prod['action_price'],'count'=>$count);
					}else{
						$this->items[$prod['id']]=array('title'=>$prod['title'],'price'=>$prod['price'],'count'=>$count);
					}
					return true;
				}else{
					throw new Exception('Добавляемый в заказ товар не найден');
					return false;
				}

			}else{
				throw new Exception('Не указанно количество товара');
				return false;
			}
		}

		public function save(){
			if(count($this->items)>0){
				$conn=DB::getConnect();
				$q="INSERT INTO `order` (`client`,`transport`,`transport_pay`,`promo_cod`,`adres`,`index`,`city`,`comment`) VALUES (:client,:transport,:transport_pay,:promo_cod,:adres,:index,:city,:comment)";
				$addOrder=$conn->prepare($q);
				$paramOrder=array(
					'client'=>$this->client,
					'transport'=>$this->transport,
					'transport_pay'=>$this->transport_price,
					'promo_cod'=>$this->promo,
					'adres'=>$this->adres,
					'index'=>$this->index,
					'city'=>$this->city,
					'comment'=>$this->comment
					);
				$q="INSERT INTO `order_item` (`title`,`price`,`count`,`order`) VALUES (:title,:price,:count,:order)";
				$addItem=$conn->prepare($q);
				try{
					$conn->beginTransaction();
					$order_res=$addOrder->execute($paramOrder);
					if(!$order_res){
						$conn->rollBack();
						throw new Exception('Ошибка сохранения нового заказа');
						return false;
					}
					$id_order=$conn->lastInsertId();
					foreach ($this->items as $id_prod=>$item) {
						$paramItem=array(
							'title'=>$item['title'],
							'price'=>$item['price'],
							'count'=>$item['count'],
							'order'=>$id_order
							);
						$resItem=$addItem->execute($paramItem);
						if(!$resItem){
							$conn->rollBack();
							throw new Exception('Ошибка сохранения товара в заказ ('.$item['title'].')');
							return false;
						}
					}
					$conn->commit();
					return true;
				}catch(PDOException $e){
					$conn->rollBack();
            		throw $e;
				}
			}else{
				throw new Exception('В заказ не добавлено ни одного товара');
				return false;
			}
		}

	}

?>