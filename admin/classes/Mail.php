<?php
class Mail{

	private $from;//обратный адрес
	private $from_name;//имя оправителя
	private $type = "text/html";//тип писма
  	private $encoding = "utf-8";//кодировка письма
  	private $to;// адрес получателя
  	private $subj;// тема писма
  	private $mess;// текст письма
  	private $attach=array();// массив прикрепляемых файлов
  	private $inline=array();// массив втавленных в текст файлов
  	private $baseboundary;//разделитель частей письма
  	private $errors=array();//массив ошибок

	function __construct($to,$mess,$subj='',$from='',$from_name=''){
		$this->to=$to;
		$this->mess=$mess;
		$this->subj=$subj;
		$this->from=$from;
		$this->from_name=$from_name;
		$this->baseboundary="------------".strtoupper(md5(uniqid(rand(), true)));
	}

	/*
		Добавляет в массив имена файлов для прикрепления
		$mail->addAttach('images/logo.png');
		$mail->addAttach(glob('images/logo.png'));	 
	*/
	function addAttach($file)
	{
		if(is_array($file))
		{
			$this->attach=array_merge($this->attach,$file);
		}else if(is_string($file))
		{
			array_push($this->attach, $file);
		}else{
			array_push($this->errors, "addAttach: Ошибка прикрепления файла, не корректные параметры.");
		}
	}

	/*
		Добавляет в массив имена файлов для прикрепления и вставки в текст письма
		$mail->addInline('images/logo.png','id');
		$mail->addInline(array('images/logo.png','id'));	 
	*/
	function addInline($file,$id='')
	{
		if(is_array($file))
		{
			$this->inline=array_merge($this->inline,$file);
		}else if(is_string($file)&&$id!='')
		{
			array_push($this->inline, array($file,$id));
		}else{
			array_push($this->errors, "addInline: Ошибка прикрепления файла, не корректные параметры.");
		}
	}

	/* возвращает массив ошибок */
	public function getErrors()
	{
		return $this->errors;
	}

	public function send()
	{
		$from = "=?utf-8?B?".base64_encode($this->from_name)."?="." <".$this->from.">";
		$subj= "=?utf-8?B?".base64_encode($this->subj)."?=";
		$headers="MIME-Version: 1.0;\n";
		$headers.="Content-type: multipart/mixed; boundary=\"{$this->baseboundary}\"\n";
		$headers.="From: {$from}\n";
		$headers.="Reply-To: ".$this->from."\n";
		$headers .= "Subject: $subj\n";
		$headers .= "Date: " . date("r") . "\n";
    	$headers .= "X-Mailer: zm php script\n";
		//прикрепляем сообщение
		$multipart = "--{$this->baseboundary}\n"; 
		$multipart .= "Content-Type: {$this->type}; charset={$this->encoding}\n";
		$multipart .= "Content-Transfer-Encoding: base64\n";    
		$multipart .= "\n";
		$multipart .= chunk_split(base64_encode($this->mess));
		/* если есть файлы для прикрепления */
		if(count($this->attach)>0)
		{
			foreach ($this->attach as $file) {
				if(file_exists($file))
				{	
					$fileContent = file_get_contents($file,true);
          			$filename=basename($file);
			        $multipart.="--{$this->baseboundary}\n";
			        $multipart.="Content-Type: application/octet-stream;\n";
			        $multipart.=" name=\"$filename\"\n";
			        $multipart.="Content-Transfer-Encoding: base64\n";
			        $multipart.="Content-Disposition: attachment;\n";
			        $multipart.=" filename=\"$filename\"\n\n";
			        $multipart.=chunk_split(base64_encode($fileContent));
				}else{
					array_push($this->errors,"send->attach: файл '".$file."' не найден и не прикреплен");
				}
			}
		}
		/* если есть файлы для прикрепления и вставки в текст */
		if(count($this->inline)>0)
		{
			foreach ($this->inline as $file) {
				if(file_exists($file[0]) && !empty($file[1]))
				{
					if(strpos($this->mess, "cid:".$file[1])===false)
					{
						array_push($this->errors,"send->inline: id (".$file[1].") файла ('".$file[0]."') для вставки не найден в тексте письма, файл будет прикреплен к письму");
					}
					$mimeType=finfo_file(finfo_open(FILEINFO_MIME_TYPE), $file[0]);
					$fileContent = file_get_contents($file[0],true);
          			$filename=basename($file[0]);
          			$id=$file[1];
			        $multipart.="--{$this->baseboundary}\n";
			        $multipart.="Content-Type: $mimeType;\n";
			        $multipart.=" name=\"$filename\"\n";
			        $multipart.="Content-Transfer-Encoding: base64\n";
			        $multipart.="Content-ID: <$id>\n";
			        $multipart.="Content-Disposition: inline;\n";
			        $multipart.=" filename=\"$filename\"\n\n";
			        $multipart.=chunk_split(base64_encode($fileContent));
				}else{
					array_push($this->errors,"send->inline: файл '".$file[0]."' не найден и не прикреплен или не задан id");
				}
			}
		}

		$multipart.="\n--{$this->baseboundary}--\n";
		//echo $headers;
		$res=mail($this->to, $subj, $multipart,$headers);
		if($res)
		{
			
		}else
		{
			array_push($this->errors,"send: Письмо не отправленно");
		}
		return $res;
	}
}
?>