<?php
	
	/**
	* Класс для работы с базой данных 
	* обязательно PHP 5.3 и выше
	*/
	class DB 
	{
		private static $con=null; //объект PDO
		protected static $host='localhost';
		protected static $user='mamont';
		protected static $dbname='mamont';
		protected static $pass='mamontpas';

		/*protected static $host='p138536.mysql.ihc.ru';
		protected static $user='p138536_m62';
		protected static $dbname='p138536_m62';
		protected static $pass='HE7ecq5piE';*/

		private static $errors;

		private function __construct()
		{
		}

		private function __clone()
		{
		}

		private function __wakeup()
		{
		}

		
		public static function setErrors($mes)
		{
			self::$errors[]=$mes;
		}

		public static function getErrors()
		{
			return self::$errors;
		}

		public static function getConnect()
		{
			if(self::$con)
			{
				return self::$con;
			}else
			{
				try{
				$options=array(
					PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'
					);
				self::$con=new PDO("mysql:host=".self::$host.";dbname=".self::$dbname.";charset=UTF8",self::$user,self::$pass,$options);
				self::$con->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
				return self::$con;
				}
				catch(PDOException $e){
					self::setErrors("Ошибка подключения ".$e->getMessage()." в строке ".$e->getLine());
					//print_r(self::$errors);
					//echo "Ошибка подключения ";
					return false;
				}
			}
		}
		
		function checkArray(array $mas, $level)
				{
					if($level>2){return false;}
					$res=$level;
					foreach ($mas as $row) {
						if(is_array($row))
						{
							$res=self::checkArray($row,$level+1);
							if(!$res)
							{
								return $res;
							}

						}
					}
					return $res;
				}

		public static function insert($table,array $fields,array $value)
		{	
			if(!empty($table))
			{ 
				
				$lev=self::checkArray($value,1);
				if(!$lev)
				{
					self::setErrors("function insert: Ошибка вставки строки, неверный массив значений");
					return false;
				}
				$result=array();
				$ins_fields="`".implode('`,`', $fields)."`";
				$ins_values= substr(str_repeat('?,',count($fields)),0,-1);
				$q="INSERT INTO `$table`($ins_fields) VALUES ($ins_values)";
				$transactRun=false;//флаг запущенной транзакции
				$cf=count($fields);// количество полей
				try
				{
					$ins=self::getConnect()->prepare($q);
					if($lev==1)
					{
						$cr=count($value);
						if($cr>$cf)
						{
							throw new PDOException(" массив значений больше чем массив полей");
							return false;
						}else if($cf>$cr)
							{
								for ($i=0; $i < $cf-$cr; $i++) { 
									$value[]='';
								}
							}
						$ins->execute($value);
						$result[]=self::getConnect()->lastInsertId();

					}else{
						$transactRun=self::getConnect()->beginTransaction();
						foreach ($value as $row) {
							if(is_array($row))
							{
								$cr=count($row);
								if($cr>$cf)
								{
									throw new PDOException(" массив значений больше чем массив полей");
									return false;
								}else if($cf>$cr)
								{
									for ($i=0; $i < $cf-$cr; $i++) { 
										$row[]='';
									}
								}
							}else{
								$row=array($row);
								for ($i=0; $i < $cf-1; $i++) { 
									$row[]='';
								}
							}
							$ins->execute($row);
							$result[]=self::getConnect()->lastInsertId();
						}
						self::getConnect()->commit();
					}
				}
				catch(PDOException $e)
				{
					if($transactRun)
					{
						self::getConnect()->rollBack();
					}
					self::setErrors("function insert: Ошибка вставки, ".$e->getMessage()." в строке ".$e->getLine());
					return false;
				}
				return $result;
			}
			self::setErrors("function insert: Ошибка вставки строки, не задана таблица");
			return false;
		}

		public static function update($table,array $fields,$where='')
		{
			
			if(!empty($table))
			{
				$result=false;
				//$ins_fields=implode('=?,', $fields)."=?";
				foreach ($fields as $key => $value) {
					$ins_fields[]="`".$key."`=:".$key."_par";
				}
				$ins_fields=implode(',',$ins_fields);
				try
				{
					if(!is_string($where))
					{
						throw new PDOException(" параметр (3) $where должен быть строкой ");
					}else if($where!=''){
						$where=" WHERE ".$where;
					}
					$q="UPDATE `$table` SET $ins_fields ".$where;
					
					$ins=self::getConnect()->prepare($q);
					foreach ($fields as $key => $value) {
						$ins->bindValue(":".$key."_par",$value);
					}
					$result=$ins->execute();
					$count=$ins->rowCount();
				}
				catch(PDOException $e)
				{
					self::setErrors("function update: Ошибка обновления, ".$e->getMessage()." в строке ".$e->getLine());
					return false;
				}
				if($count)
				{
					return $count;
				}
				return $result;
			}
			self::setErrors("function update: Ошибка обновления строки, не задана таблица");
			return false;
		}

		/**
		* выполняет запрос
		* param@ $q текст запроса
		* param@ $t тип запроса a- возвращается ассоциативный массив
		*						n- возвращается нумерованный массив
		*               не указан- просто выполняет запрос
		*	  любой другой символ- возвращает смешанный массив 
		*/
		public static function query($q,$t='',$param=array())
		{
			if(!self::getConnect())
			{
				echo "Нет подключения к базе данных (No connection to the database)";
				exit;
				return false;
			}
			if(empty($q))
			{
				self::setErrors("function query: передана пустая строка запроса");
				return false;
			}
			if($t=='')
			{
				try
				{
					$stmt=self::getConnect()->prepare($q);
					$res=$stmt->execute($param);
					if($res===false)
					{
						return false;
					}else{
						return true;
					}
				}
				catch(PDOException $e)
				{
					self::setErrors("function query: Ошибка выполнения запроса, ".$e->getMessage()." в строке ".$e->getLine());
					return false;
				}
			}else{
				try
				{
					$stmt=self::getConnect()->prepare($q);
					$stmt->execute($param);
					switch ($t) {
						case 'a':
							$res=$stmt->fetchAll(PDO::FETCH_ASSOC);
							break;
						case 'n':
							$res=$stmt->fetchAll(PDO::FETCH_NUM);
							break;
						default:
							
							$res=$stmt->fetchAll(PDO::FETCH_BOTH);
							break;
					}
					
					if($res===false)
					{
						return false;
					}else{
						/*$res_ar=array();
						foreach ($res as $value) {
							$res_ar[]=$value;
						}
						return $res_ar;*/
						return $res;
					}
				}
				catch(PDOException $e)
				{
					self::setErrors("function query: Ошибка выполнения запроса, ".$e->getMessage()." в строке ".$e->getLine());
					return false;
				}
			}
		}
	}
	
	function __autoload( $className ) 
	{
		$className = str_replace( "..", "", $className );
		try
		{ //echo $_SERVER["DOCUMENT_ROOT"]."/admin/classes/$className.php"."<br>";
			if(!@include_once( $_SERVER["DOCUMENT_ROOT"]."/admin/classes/$className.php" ))
			{
				throw new Exception(" класс {$className} не подключен");
			}	
		}
		catch(Exception $e)
		{ 
			DB::setErrors("function autoload: Ошибка подключения класса, ".$e->getMessage()." в строке ".$e->getLine());
		}
		
	}

?>