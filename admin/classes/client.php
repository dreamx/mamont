<?php

class client{
	 
	 public $id;
	 public $email;
	 public $name;
	 public $phone;
	 public $firma;
	 public $adres;
	 public $city;
	 public $index;
	 private static $_instance = null;


	private function __construct($id){
		
		$client_info=DB::query("SELECT * FROM `client` WHERE `id`='".intval($id)."' LIMIT 1",'a');
		if($client_info){
			$this->id=$client_info[0]['id'];
			$this->email=$client_info[0]['email'];
			$this->name=$client_info[0]['name'];
			$this->phone=$client_info[0]['phone'];
			$this->firma=$client_info[0]['firma'];
			$this->adres=$client_info[0]['adres'];
			$this->city=$client_info[0]['city'];
			$this->index=$client_info[0]['index'];

		}else{
			throw new Exception('Клиент с номером '.$id.' не найден');
		}
	}

	private function __clone(){
	}
	private function __wakeup()
		{
		}

	public static function new_client( $email="", $name="", $phone=""){
		$q="SELECT `id` FROM `client` WHERE `email`=:mail";
		$res=DB::query($q,'a',array('mail'=>$email));
		if($res){
			DB::update('client',array('name'=>$name,'phone'=>$phone)," `id`='".$res[0]['id']."'");
			return $res[0]['id'];
		}else{
			$res=DB::insert('client',array('email','name','phone'),array($email,$name,$phone));	
		}
		
		if($res){
			$id=$res[0];
			return $id;
		}else{
			return false;
		}
	}

	public static function getClient($id){
		if(empty($id)){
			return false;
		}
		return new self($id);
	}

	public function newOrder($dostavka,$index=0,$comment,$adres='',$city='',$promo=''){
		return new neworder($this->id,$dostavka,$index,$comment,$adres,$city,$promo);
	}

	public function getOrders(){
		$q="SELECT * FROM `order` WHERE `client`=:client";
		$orders=DB::query($q,'a',array('client'=>$this->id));
		if($orders){
			$res=array();// массив результата
			$conn=DB::getConnect(); // PDO оъект
			// $q="SELECT * FROM `order_discount` WHERE `id_order`=:id_order";
			// $disc_order=$conn->prepare($q);
			$q="SELECT * FROM `order_item` WHERE `order`=:id_order";
			$order_item=$conn->prepare($q);
			// $q="SELECT * FROM `order_item_discount` WHERE `id_item`=:id_item";
			// $disc_item=$conn->prepare($q);
			foreach ($orders as $key=>$order) {
				$res[$key]['order']=$order;
				// $disc_order->execute(array(':id_order'=>$order['id']));
				// $disc=$disc_order->fetchAll(PDO::FETCH_ASSOC);
				// $res[$key]['discount']=$disc;
				$order_item->execute(array(':id_order'=>$order['id']));
				$item=$order_item->fetchAll(PDO::FETCH_ASSOC);
				$res[$key]['items']=array();
				if($item){
					foreach ($item as $k => $it) {
						$res[$key]['items'][$k]=$it;
						// $disc_item->execute(array(':id_item'=>$it['id']));
						// $item_disc=$disc_item->fetchAll(PDO::FETCH_ASSOC);
						// $res[$key]['items'][$k]['discount']=$item_disc;
					}
				}
			}
			return $res;
		}else{
			return false;
		}
	}

	public function getAdreses(){
		$q="SELECT `adres` FROM `order` WHERE `client`=:client GROUP BY `adres`";
		return DB::query($q,'a',array('client'=>$this->id));
	}
}
?>