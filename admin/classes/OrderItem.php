<?php

class OrderItem{

	private $table='order_item';
	private $id;
	private $title;
	private $price;
	private $count;
	private $order;
	private $chars;//массив характеристик
	private $discount;//массив скидок
	private $summa;//стоимость товара со всеми характеристиками без скидки
	private $summa_disc;//стоимость товара со всеми характеристиками без скидки
	private static $instance;//объект класса
	private $type_temp=4;// id времменного типа скидки
	private $max_discount;//максимальная скидка, которая применяется

	private function __construct($id=''){
  		if($id!=''){
			$order_info=DB::query("SELECT * FROM `{$this->table}` WHERE `id`='".intval($id)."' LIMIT 1",'a');
			if($order_info){
				$this->setInfo($order_info[0]);
			}
		}
		
	}

	public static function getInstance($id=''){
		if(!isset(self::$instance)){
			if(empty($id)){
				return false;
			}
			self::$instance=new OrderItem($id);
		}
		return self::$instance;
	}

	public static function getNewItem($id=''){
		
		if(empty($id)){
			return false;
		}
		self::$instance=new OrderItem($id);
		return self::$instance;
	}

	private function setInfo($ord){
		extract($ord);
		$this->id=$id;
		$this->title=$title;
		$this->price=$price;
		$this->count=$count;
		$this->order=$order;
		$this->setChars();
		$this->setDiscount();
	}

	private function setChars(){
		if(!empty($this->id)){
			$chars=DB::query("SELECT * FROM `order_item_char` WHERE `id_item`='{$this->id}'",'a');
			if($chars){
				$this->chars=$chars;
			}
		}
	}

	private function setDiscount(){
		if(!empty($this->id)){
			$disc=DB::query("SELECT * FROM `order_item_discount` WHERE `id_item`='{$this->id}'",'a');
			if($disc){
				$this->discount=$disc;
			}
		}
	}

	public function getTitle(){
		return $this->title;
	}
	public function getCount(){
		return $this->count;
	}
	public function getId(){
		return $this->id;
	}
	public function get_chars(){
		return $this->chars;
	}


	public function get_chars_str(){
		$str="";
		$ar=array();
		if(count($this->chars)){
			foreach ($this->chars as $ch) {
				$ar[]=$ch['title']." ".$ch['value'].$ch['si']." ".$ch['price'];
			}
			$str=implode(', ',$ar);
		}
		return $str;
	}

	public function get_discounts(){
		return $this->discount;
	}

	public function get_sum(){
		if(empty($this->summa)){
			if(count($this->chars)>0)
			{
				$s=$this->price;
				foreach ($this->chars as $ch) {
					$s+=$ch['price'];
				}
				$this->summa=$s;
			}else{
				$this->summa=$this->price;
			}
		}
		return $this->summa;
	}

	public function get_summa(){
		return $this->get_sum()*$this->count;
	}

	public function getMaxDisc($date){
		if(empty($this->max_discount)&&count($this->discount)>0){
				$summ=$this->get_sum();
				$time_order=strtotime($date);
				$max_rub=0;//начальное значение максимальной скидки
				$res=false;
				foreach ($this->discount as $dis) {
					if($dis['type']==$this->type_temp){ //если скидка временная
						$tb=strtotime($dis['date_begin']); //время начала действия
						$te=strtotime($dis['date_end']); //время конца действия
						if($time_order>=$tb&&$time_order<=$te){// если заказ сделан во время действия скидки, то учитываем её
							if($dis['edinici']=='rub'){
								if($max_rub<$dis['value']){
									$res=$dis;
									$max_rub=$dis['value'];
								}
							}
							if($dis['edinici']=='perc'){
								if($max_rub<$summ*$dis['value']/100){
									$res=$dis;
									$max_rub=$summ*$dis['value']/100;
								}
							}
						}
					}else{
						if($dis['edinici']=='rub'){
							if($max_rub<$dis['value']){
								$res=$dis;
								$max_rub=$dis['value'];
							}
						}
						if($dis['edinici']=='perc'){
							if($max_rub<$summ*$dis['value']/100){
								$res=$dis;
								$max_rub=$summ*$dis['value']/100;
							}
						}
					}	
				}
				$this->max_discount=$res;
			}
		return $this->max_discount;
	}

	public function getMaxDisc_str($date){
		$dis=$this->getMaxDisc($date);
		if($dis){
			if($dis['edinici']=="rub")
			{
				return $dis['value']." руб.";
			}
			if($dis['edinici']=="perc")
			{
				return $dis['value']." %";
			}
		}else{
			return '';
		}
	}

	/* сейчас выбирается наибольшая скидка действующая на момент заказа
	* @date дата заказа
	*/
	public function get_summa_disc($date){
		$summ=$this->get_sum();
		$dis=$this->getMaxDisc($date);
		if($dis){
			if(empty($this->summa_disc)){
				if($dis['edinici']=='rub'){
						$this->summa_disc=$summ-$dis['value'];
					}
				if($dis['edinici']=='perc'){
					$this->summa_disc=$summ-$summ*$dis['value']/100;
				}
			}
		}else{
			$this->summa_disc=$summ;
		}
		return $this->summa_disc*$this->count;
	}

	/** 
	* Функция добавляет товар в заказ
	* @id_prod
	* @arr_char 
	*/
	public static function add_item($id_prod,$arr_char=array(),$count,$order,$arr_links=array()){
		$q="INSERT INTO `order_item` (`title`,`price`,`count`,`order`)
		SELECT `title`,`price`,{$count},{$order}
		FROM `product`
		WHERE `id`='".intval($id_prod)."'
		";
		
		DB::getConnect()->query($q);//добавляем запись товара в заказ
		$new_id=DB::getConnect()->lastInsertId();
		if(count($arr_char)>0){
			$wh=array();
			foreach ($arr_char as $ch) {
				$wh[]="`cv`.`id`='".$ch."'";
			}
			$q="INSERT INTO `order_item_char` (`title`,`si`,`value`,`price`,`id_item`)
				SELECT `c`.`title`,`c`.`si`,`cl`.`value_char`,`cv`.`price`,".intval($new_id)."
		      	FROM `char_value` AS  `cv`
		      	LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
		      	LEFT JOIN `char` AS `c` ON `cl`.`id_char`=`c`.`id` 
		      	WHERE ".implode(' or ',$wh)." ";
		    DB::getConnect()->query($q);//добавляем выбранные характеристики
		}

		$q="INSERT INTO `order_item_discount` (`id_item`,`type`,`edinici`,`value`,`name`,`date_begin`,`date_end`)
				SELECT ".intval($new_id).",`d`.`type`,`d`.`edinici`,`d`.`value`,`d`.`name`,`d`.`date_begin`,`d`.`date_end`
		      	FROM `link_product_discont` AS  `lpd`
		      	LEFT JOIN `discount`  AS `d` ON `lpd`.`id_note2`=`d`.`id`
		      	WHERE `lpd`.`id_note1`='".intval($id_prod)."' AND `lpd`.`status`='0' AND `d`.`public`='on'";
		DB::getConnect()->query($q);//добавляем скидки


		if(count($arr_links)>0){
			$wh=array();
			foreach ($arr_links as $ch) {
				$wh[]="`l`.`md_cod`='".$ch."'";
			}
			$q="INSERT INTO `order_item_discount` (`id_item`,`type`,`edinici`,`value`,`name`,`date_begin`,`date_end`)
				SELECT ".intval($new_id).",4,`d`.`edinici`,`d`.`value`,`d`.`name`,`l`.`date_begin`,`l`.`date_end`
		      	FROM `links_discount` AS  `l`
		      	LEFT JOIN `discount`  AS `d` ON `l`.`id_discount`=`d`.`id`
		      	WHERE `l`.`id_prod`='".intval($id_prod)."' AND `l`.`status`='0' AND (".implode(' or ',$wh).")";
			DB::getConnect()->query($q);//добавляем скидки по ссылкам (все скидки становятся временными срок действия берётся из таблицы ссылок)
		}

		return $new_id;
	}

}
?>