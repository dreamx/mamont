<?php
class Request{
	private static $instance;
	private $table='request';//таблица заказов
	private $id;// id заказа
	public $date;// дата заказа
  	public $desc;//комментарии менеджера
  	public $manager;//имя менеджера
  	public $contragent;//контрагент
  	private $items;// массив товаров в заказе
  	
  	

  	private function __construct($id=''){

  		if($id!=''){
			$order_info=DB::query("SELECT * FROM `{$this->table}` WHERE `id`='".intval($id)."' LIMIT 1",'a');
			if($order_info){
				$this->setInfo($order_info[0]);
			}else{
				throw new Exception('Заявка с номером '.$id.' не найдена');
			}
		}else{
			throw new Exception('Укажите номер заявки');
		}
	}

	public static function getInstance($id=''){
		if(!isset(self::$instance)){
			if(empty($id)){
				return false;
			}
			try {
				self::$instance=new Request($id);
			}catch (Exception $e){
				echo $e->getMessage();
				return false;
			}
		}
		return self::$instance;
	}

	public function setInfo($ord){
		extract($ord);
		$this->id=$id;
		$this->date=$date;
		$this->desc=$desc;
		$this->manager=$manager;
		$this->contragent=$contragent;
	}

	public function getItems(){
		if(empty($this->items)){
			$items=DB::query("SELECT `id`,`product`,`count` FROM `request_item` WHERE `id_request`='{$this->id}'",'a');
			if($items){
				$this->items=$items;
			}
		}
		return $this->items;
	}

	

	
	public function view_request($edit=false){
			
		?>
		<table id="tab_ord" style="width:100%;" class="table table-striped table-bordered">
          <thead>
            <th>Наименование</th>
            <th>Кол-во</th>
            <?php if(!$edit){?>  
            <th></th>
            <?php }?>
          </thead>
          <tbody>
            <?php
              if(count($this->getItems())>0){
                foreach ($this->getItems() as $pr){
                  ?>
                  <tr id="it<?=$pr['id'];?>">
                    <td><?php echo $pr['product'];?></td>
                    <td><?php echo $pr['count'];?></td>
                      <?php
                      if(!$edit){?>                
                    <td><span class="btn btn-danger del_item"><span class="glyphicon glyphicon-trash"></span> Удалить</span></td>
                  	<?php } ?>
                  </tr>
                  <?
                }
              }
            ?>
          </tbody>
        </table>
        <script type="text/javascript">
	        var tab_ord=$('#tab_ord').dataTable({
	            "language": {
	                "paging":false,
	                "lengthMenu": "",
	                "zeroRecords": "Ничего не найдено",
	                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
	                "infoEmpty": "",
	                "search": "Поиск: ",
	                "paginate": {
	                "previous": "",
	                "next": ""
	                }
	            },
	            tableTools: {
	            "aButtons": [ ]
	        }
	          });

			var api_ord=tab_ord.api();
        </script>
		<?
	}

}
?>