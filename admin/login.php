<!-- Авторизация -->
     <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }

    </style>
<div class="container">

      <form class="form-signin" method="post" action="auth.php">
        <h4 class="form-signin-heading">Вход в панель управления</h4>
        <?php if (isset($_GET['error'])){?>
              <div class="alert alert-danger">Неверный логин или пароль</div>
         <?php } ?>
        <input name="login" required type="text" class="input-block-level" placeholder="Логин">
        <input name="password" required type="password" class="input-block-level" placeholder="Пароль">
        
        <button class="btn btn-primary" type="submit">Авторизоваться</button>
      </form>

</div>

<!-- Авторизация -->