<?php

$admin_path = "/admin/";
$modules_path = "modules";
$modules_all = array(
	 array("users","Пользователи"),
	 array("prava","Права"),
	 array("type_discount","Виды скидок"),
	 array("transport","Виды доставки"),
	 array("promo_cod","Промо коды"),
	 array("discount","Скидки"),
	 array("brand","Бренды"),
	 array("catalog","Каталог"),
	 //array("char","Характеристики товара"),
	 //array("tara","Виды упаковки"),
	 //array("type","Категории"),
	 array("product","Продукция"),
	 array("present","Подарки"),
	 array("pages","Страницы"),
	 array("client","Клиенты"),
	 array("order","Заказы"),
	 array("basket","Корзины"),
	 array("return","Возврат"),
	 array("request","Заявки поставщикам"),
	// array("char_list","Значение характеристики"),
	 array("reviews","Отзывы")
	 
	);

//e10adc3949ba59abbe56e057f20f883e

/*
$permissions = array(); 
foreach($modules_all as $permiss){

  $permissions[$permiss[0]] = array("create"=>"on","read"=>"on","write"=>"on","delete"=>"on");

}


echo json_encode($permissions);
*/

?>

