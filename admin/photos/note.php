<style type="text/css">
	.jcrop-holder {
  direction: ltr;
  text-align: left;
}

/* These styles define the border lines */
.jcrop-vline,.jcrop-hline{background:#FFF url('../../img/jcrop.gif') top left repeat;font-size:0;position:absolute;}
.jcrop-vline{height:100%;width:1px!important;}
.jcrop-hline{height:1px!important;width:100%;}
.jcrop-vline.right{right:0;}
.jcrop-hline.bottom{bottom:0;}

/* Handle style - size is set by Jcrop handleSize option (currently) */
.jcrop-handle{background-color:#333;border:1px #eee solid;font-size:1px;}

/* This style is used for invisible click targets */
.jcrop-tracker
{
  height: 100%; 
  width: 100%;
  -webkit-tap-highlight-color: transparent; /* "turn off" link highlight */
  -webkit-touch-callout: none;              /* disable callout, image save panel */
  -webkit-user-select: none;                /* disable cut copy paste */
}

/* Positioning of handles and drag bars */
.jcrop-handle.ord-n{left:50%;margin-left:-4px;margin-top:-4px;top:0;}
.jcrop-handle.ord-s{bottom:0;left:50%;margin-bottom:-4px;margin-left:-4px;}
.jcrop-handle.ord-e{margin-right:-4px;margin-top:-4px;right:0;top:50%;}
.jcrop-handle.ord-w{left:0;margin-left:-4px;margin-top:-4px;top:50%;}
.jcrop-handle.ord-nw{left:0;margin-left:-4px;margin-top:-4px;top:0;}
.jcrop-handle.ord-ne{margin-right:-4px;margin-top:-4px;right:0;top:0;}
.jcrop-handle.ord-se{bottom:0;margin-bottom:-4px;margin-right:-4px;right:0;}
.jcrop-handle.ord-sw{bottom:0;left:0;margin-bottom:-4px;margin-left:-4px;}
.jcrop-dragbar.ord-n,.jcrop-dragbar.ord-s{height:7px;width:100%;}
.jcrop-dragbar.ord-e,.jcrop-dragbar.ord-w{height:100%;width:7px;}
.jcrop-dragbar.ord-n{margin-top:-4px;}
.jcrop-dragbar.ord-s{bottom:0;margin-bottom:-4px;}
.jcrop-dragbar.ord-e{margin-right:-4px;right:0;}
.jcrop-dragbar.ord-w{margin-left:-4px;}

/* The "jcrop-light" class/extension */
.jcrop-light .jcrop-vline,.jcrop-light .jcrop-hline
{
	background:#FFF;
	filter:Alpha(opacity=70)!important;
	opacity:.70!important;
}
.jcrop-light .jcrop-handle
{
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	background-color:#000;
	border-color:#FFF;
	border-radius:3px;
}

/* The "jcrop-dark" class/extension */
.jcrop-dark .jcrop-vline,.jcrop-dark .jcrop-hline
{
	background:#000;
	filter:Alpha(opacity=70)!important;
	opacity:.7!important;
}
.jcrop-dark .jcrop-handle
{
	-moz-border-radius:3px;
	-webkit-border-radius:3px;
	background-color:#FFF;
	border-color:#000;
	border-radius:3px;
}

/* Fix for twitter bootstrap et al. */
.jcrop-holder img,img.jcrop-prev_<?php echo $_GET['link'];?>{ max-width: none; }

</style>


<h3>Редактор изображения</h3>

<?php 
include "../classes/core.php";
$img_id = $_GET['id'];
$img_info = $core->_list("select * from `Photos` where `id`='".$img_id."'");
$img_info = $img_info[0];
$img_file = "../../../img/".$img_info['id'].".".$img_info['Photos_ex'];
$thumb_file = "../../../img/thumbs/".$img_info['id'].".".$img_info['Photos_ex'];
$img_params = getimagesize("../../img/".$img_info['id'].".".$img_info['Photos_ex']);
$thumb_params = getimagesize("../../img/thumbs/".$img_info['id'].".".$img_info['Photos_ex']);
$img_thumb_height=intval($_GET['ht']);
$img_thumb_width=intval($_GET['wt']);
$thumb_params=array($img_thumb_width,$img_thumb_height);
?>



<form class="form-horizontal" id="img_form">

<input type="hidden" id="x" name="x" value=""/>
<input type="hidden" id="y" name="y" value=""/>
<input type="hidden" id="w" name="w" value=""/>
<input type="hidden" id="h" name="h" value=""/>
<input type="hidden" name="thumb_img" value="<?php echo $thumb_file;?>?<?php echo time();?>"/>

  <div class="form-group">
    <label class="col-sm-2 control-label" for="field-Photos_title_<?php echo $_GET['link'];?>">Название изображения</label>
    <div class="col-sm-6">
      <div class="input-group">
        <input id="field-Photos_title_<?php echo $_GET['link'];?>" class="form-control" type="text" placeholder="" value="" name="Photos_title_<?php echo $_GET['link'];?>" pattern="">
        <span class="input-group-addon btn"><span class="glyphicon glyphicon-ok"></span> Сохранить</span>
      </div>
    </div>
    <div class="col-sm-2">
    <span id="saving_status_<?php echo $_GET['link'];?>" class="alert alert-success" style="padding: 6px 12px; margin: 0px;"></span>
    </div>
  </div>

</form>
<hr>
<button class="btn btn-primary" onclick="crop_img_<?php echo $_GET['link'];?>();" id="cr_thumb_<?php echo $_GET['link'];?>"><span class="glyphicon glyphicon-fullscreen"></span> Изменить миниатюру</button>
&nbsp;<button class="btn btn-success" id="ok_thumb_<?php echo $_GET['link'];?>"><span class="glyphicon glyphicon-ok"></span> Сохранить</button>
&nbsp;<button class="btn btn-danger" id="esc_thumb_<?php echo $_GET['link'];?>"><span class="glyphicon glyphicon-remove"></span> Отмена</button>
<br><br>
<div style="vertical-align:top;">
	<div style="display:inline-block;/display:inline;/zoom:1;vertical-align:top;">
		<img id="image_file_<?php echo $_GET['link'];?>" src="<?php echo $img_file;?>?<?php echo time();?>" />
	</div>
	&nbsp;
	<div style="width:<?echo $thumb_params[0];?>px;height:<?echo $thumb_params[1];?>px;overflow:hidden;display:inline-block;/display:inline;/zoom:1;vertical-align:top;">
		<div id="image_thumb_<?php echo $_GET['link'];?>?<?php echo time();?>">
			<img id="prev_<?php echo $_GET['link'];?>" src="<?php echo $thumb_file;?>?<?php echo time();?>" />	
		</div>
	</div>
</div>
<script>
$('#ok_thumb_<?php echo $_GET['link'];?>').hide();
$('#esc_thumb_<?php echo $_GET['link'];?>').hide();
$('#saving_status_<?php echo $_GET['link'];?>').hide();
function crop_img_<?php echo $_GET['link'];?>(){
$('#prev_<?php echo $_GET['link'];?>').attr('src',$('#image_file_<?php echo $_GET['link'];?>').attr('src'));
var jcrop_api, boundx, boundy; 
$('#image_file_<?php echo $_GET['link'];?>').Jcrop({
        onChange: updatePreview_<?php echo $_GET['link'];?>,
        onSelect: updatePreview_<?php echo $_GET['link'];?>,
       
        setSelect:   [ 0, 0, <?php echo $img_thumb_width;?>, <?php echo $img_thumb_height;?> ],
        aspectRatio:  <?php echo $img_thumb_width;?> / <?php echo $img_thumb_height;?>,
        minSize: [ <?php echo $img_thumb_width;?>, <?php echo $img_thumb_height;?> ],
      },function(){
       $('#cr_thumb_<?php echo $_GET['link'];?>').hide();
       $('#ok_thumb_<?php echo $_GET['link'];?>').show();
       $('#esc_thumb_<?php echo $_GET['link'];?>').show();
       $('#esc_thumb_<?php echo $_GET['link'];?>').click(function(){
        
       EscCrop_<?php echo $_GET['link'];?>(jcrop_api)
       });
       $('#ok_thumb_<?php echo $_GET['link'];?>').click(function(){
       save_img_<?php echo $_GET['link'];?>()
       jcrop_api.destroy();
       })
       var bounds = this.getBounds();
       boundx = bounds[0];
       boundy = bounds[1];
       $('#image_thumb_<?php echo $_GET['link'];?>').width(boundx);
       $('#image_thumb_<?php echo $_GET['link'];?>').height(boundy);
       updatePreview_<?php echo $_GET['link'];?>(this);
       updateCoords_<?php echo $_GET['link'];?>(this);
        // Store the API in the jcrop_api variable
        
        jcrop_api = this;
      });
function EscCrop_<?php echo $_GET['link'];?>(e){
        $('#image_file_<?php echo $_GET['link'];?>').css('visibility','visible');
        $('#ok_thumb_<?php echo $_GET['link'];?>').hide();
        $('#esc_thumb_<?php echo $_GET['link'];?>').hide();
        $('#cr_thumb_<?php echo $_GET['link'];?>').show();
        $('#prev_<?php echo $_GET['link'];?>').attr('src',$('input[name=thumb_img]').attr('value'));
        $('#prev_<?php echo $_GET['link'];?>').attr('style','');
        jcrop_api.destroy();
}

function updatePreview_<?php echo $_GET['link'];?>(c)
      {
        if (parseInt(c.w) > 0)
        {
        
          var rx = <?php echo $thumb_params[0];?> / c.w;
          var ry = <?php echo $thumb_params[1];?> / c.h;
          updateCoords_<?php echo $_GET['link'];?>(c)
     
          $('#prev_<?php echo $_GET['link'];?>').css({
            width: Math.round(rx * boundx) + 'px',
            height: Math.round(ry * boundy) + 'px',
            marginLeft: '-' + Math.round(rx * c.x) + 'px',
            marginTop: '-' + Math.round(ry * c.y) + 'px'
          });
        }
};
      
function updateCoords_<?php echo $_GET['link'];?>(c)
			{
				$('#x').attr('value',c.x);
				$('#y').attr('value',c.y);
				$('#w').attr('value',c.w);
				$('#h').attr('value',c.h);
			};  
}

function save_img_<?php echo $_GET['link'];?>(){
      $('#savebut').html('<img src="/images/loading.gif">')
      $.post('/admin/classes/save_thumb.php',{id:<?php echo $_GET['id'];?>,x:$('#x').attr('value'),y:$('#y').attr('value'),w:$('#w').attr('value'),h:$('#h').attr('value'),tw:<?php echo $thumb_params[0];?>,th:<?php echo $thumb_params[1];?>,format:'<?php echo $img_info['Photos_ex'];?>'},function(){
         var time = new Date();
         var cash = time.getHours()+""+time.getMinutes()+""+time.getSeconds();
        $('#image_file_<?php echo $_GET['link'];?>').css('visibility','visible');
        $("input[name=thumb_img]").attr('value','<?php echo $thumb_file;?>?'+cash);
        $('#img_list_<?php echo $_GET['link'];?>').children('.thumb_cnt').eq(<?php echo $_GET['num']-1;?>).find('.pic_bg').css("background-image","url('/img/thumbs/<?php echo $_GET['id'].".".$img_info['Photos_ex'];?>?"+cash+"')");
        $('#ok_thumb_<?php echo $_GET['link'];?>').hide();
        $('#esc_thumb_<?php echo $_GET['link'];?>').hide();
        $('#cr_thumb_<?php echo $_GET['link'];?>').show();
      })
}
</script>