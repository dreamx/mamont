<style type="text/css">
	.img_uploader{position: absolute;width:200px; height: 34px; cursor: pointer;/cursor: hand; opacity: 0;z-index: 200;}
	.thumb_cnt{width: 150px;float: left;padding-right:15px;}
	.for_upload{padding:15px; overflow: auto;}
	.thumb_cnt .pic_bg{cursor: move;}
	.pic_bg{width:125px;height:125px;background-color: #ccc;background-repeat: no-repeat;background-size: cover; background-position: center center;}
</style>
<?php
if($img_limit!=1){
	$multiselect = true;
}else{
	$multiselect = false;
}
$upload_max_filesize = ini_get('upload_max_filesize');
$post_max_size = ini_get('post_max_size');

?>
<script type="text/javascript">
	function create_thumb_<?php echo $img_link;?>(id,num,ex){
		var time = new Date();
    	var cash = time.getHours()+""+time.getMinutes()+""+time.getSeconds();
		var src = "/img/thumbs/"+id+"."+ex;
		var thumb_prev = ''+
	'<div class="thumb_cnt" rel="'+id+'" num="'+num+'">'+
		'<div class="thumbnail">'+
			'<div class="pic_bg" style=background-image:url("'+src+'?'+cash+'")></div>'+
			'<div class="caption">'+
				'<button class="btn btn-success" style="width:51px;" onclick="edit_img_<?php echo $img_link;?>(this)"><span class="glyphicon glyphicon-pencil"></span></button>&nbsp;'+
				'<button class="btn btn-danger" style="width:51px;" onclick="del_img_<?php echo $img_link;?>(this)"><span class="glyphicon glyphicon-trash"></span></button>'+
			'</div>'+
		'</div>'+
	'</div>';
	$('#img_list_<?php echo $img_link;?>').append(thumb_prev);
	}
</script>

<div id="img_index_<?php echo $img_link;?>">
	<input class="img_uploader" accept="image/jpeg,image/png,image/gif" type="file" <?php if($multiselect){?> multiple="multiple" name="img_files_<?php echo $img_link;?>[]" <?php }else{ ?> name="img_files_<?php echo $img_link;?>" <?php } ?> id="upload_img_<?php echo $img_link;?>"/>
	<?php
	$imgs = $core->_list("select `id`,`Photos_num` from `Photos` where `Photos_module`='".$img_link."' and `Photos_parent`='".$note['id']."' order by `Photos_num`");
		if(count($imgs)<$img_limit&&$img_limit!=0){
	?>
	<?php
	}
	?>
	<button class="btn btn-success" id="choose_files_<?php echo $img_link;?>">
		
		<span class="glyphicon glyphicon-picture"></span> 
		Добавить изображения
		
	</button>
	
	<button class="btn btn-primary" id="upload_to_server<?php echo $img_link;?>" onclick="send_imgs_<?php echo $img_link;?>(0)">Загрузить на сервер</button>
	&nbsp;&nbsp;<span id="uploading_status_<?php echo $img_link;?>" class="alert alert-success" style="padding: 6px 12px; margin: 0px;"></span>
	<div id="img_for_upload_<?php echo $img_link;?>" class="row for_upload"> 
		
	</div>
	<hr>
	<div class="img_list" id="img_list_<?php echo $img_link;?>">
		<?php
		if(count($imgs)>0){
			echo "<h3>Загруженные изображения</h3>";
		}
		foreach($imgs as $img){
			?>
			<script type="text/javascript">
			create_thumb_<?php echo $img_link;?>(<?php echo $img['id'];?>,<?php echo $img['Photos_num'];?>,'<?php echo $img_ex;?>');
			</script>
			<?php
		}
		?>
	</div>

</div>

<div id="img_note_<?php echo $img_link;?>">
	<button onclick="back_img_<?php echo $img_link;?>()" class="btn btn-warning"><span class="glyphicon glyphicon-arrow-left"></span> К списку изображений</button>
	<hr>
	<div class="img_note_cnt"></div>
</div>

<script>
$('#img_note_<?php echo $img_link;?>').hide();
$('#upload_to_server<?php echo $img_link;?>').hide();
$('#uploading_status_<?php echo $img_link;?>').hide();
$('#upload_img_<?php echo $img_link;?>').change(function(){
	get_upload_thumbs_<?php echo $img_link;?>(this);
});

function del_img_<?php echo $img_link;?>(el){
	var im = $(el).parent('div').parent('div').parent('div');
	var id = $(im).attr('rel');
	if(confirm('Подтвердите удаление')){
		$.post("<?php echo $admin_path;?>classes/del_img.php",{id:id,ex:'<?php echo $img_ex;?>'},function(data){
			$(im).remove();
			sort_imgs_<?php echo $img_link;?>();
			var all = $('#img_list_<?php echo $img_link;?>').children('.thumb_cnt').length;
			if(all<1){
				$('#img_list_<?php echo $img_link;?>').html('');
			}
		})
	}
}

var quere_upload_<?php echo $img_link;?> = [];
var fd_<?php echo $img_link;?> = [];

function get_upload_thumbs_<?php echo $img_link;?>(el){
	var files = el.files;
	var prev_cnt = $('#img_for_upload_<?php echo $img_link;?>');
	if(files.length>0){
		if(!prev_cnt.find('h3').length){
			prev_cnt.append('<div class="col-sm-12 col-lg-12"><h3>Файлы для загрузки</h3></div>');
		}
		
		draw_thumbs_<?php echo $img_link;?>(files,0)
		
	}
} 

function draw_thumbs_<?php echo $img_link;?>(files,i){
	var prev_cnt = $('#img_for_upload_<?php echo $img_link;?>');
	if(i!=files.length){

				file = files[i];
				draw_thumb_<?php echo $img_link;?>(prev_cnt,file,i);
				quere_upload_<?php echo $img_link;?>.push(file);
				draw_thumbs_<?php echo $img_link;?>(files,i+1);

	}else{
		if(files.length!=0){
			$('#upload_to_server<?php echo $img_link;?>').show();
			$('#upload_img_<?php echo $img_link;?>').attr('files','');
		}
		return;
	}
}

function draw_thumb_<?php echo $img_link;?>(prev_cnt,file,i){
	var src = URL.createObjectURL(file);
	var thumb_prev = ''+
	'<div class="thumb_cnt">'+
		'<div class="thumbnail">'+
			'<div class="pic_bg" style=background-image:url("'+src+'")></div>'+
			'<div class="caption">'+
				'<button class="btn btn-danger" style="width:100%;" onclick="del_from_upload_<?php echo $img_link;?>(this)"><span class="glyphicon glyphicon-trash"></span></button>'+
			'</div>'+
		'</div>'+
	'</div>';
	var img = new Image();
	img.src = src;
	
	

	img.onload = function(){
		var canvas = document.createElement('CANVAS');
		canvas.width = img.width;
		canvas.height = img.height;
    	var ctx = canvas.getContext('2d');
    	ctx.drawImage(img, 0, 0);
    	
    	var MAX_WIDTH = <?php echo $img_max_size;?>;
		var MAX_HEIGHT = <?php echo $img_max_size;?>;
		var width;
		var height;

		width = img.width;
		height = img.height;
		
		if (width > height) {
  			if (width > MAX_WIDTH) {
    		height *= MAX_WIDTH / width;
    		width = MAX_WIDTH;
  			}
		} else {
  			if (height > MAX_HEIGHT) {
    		width *= MAX_HEIGHT / height;
    		height = MAX_HEIGHT;
  			}
		}
	
	//width = Math.floor(width);
	//height = Math.floor(height);
	canvas.width = width;
	canvas.height = height;
	var ctx = canvas.getContext("2d");
	ctx.drawImage(img, 0, 0, width, height);
	var dataurl = canvas.toDataURL("<?php echo $img_mime;?>");
	/*var imgFileSize = Math.round((dataurl.length)*3/4) ;
	console.log(imgFileSize);*/
	
	fd_<?php echo $img_link;?>.push(dataurl);
	prev_cnt.append(thumb_prev);
	//console.log(fd_<?php echo $img_link;?>);
	}
}


function del_from_upload_<?php echo $img_link;?>(el){

	var parent_el = $(el).parent('div').parent('div').parent('div');
	var ind = $('#img_for_upload_<?php echo $img_link;?>').children('.thumb_cnt').index(parent_el);
	parent_el.remove();
	fd_<?php echo $img_link;?>.splice(ind,1);
	if(fd_<?php echo $img_link;?>.length<1){
        $('#img_for_upload_<?php echo $img_link;?>').html('');
        $('#upload_to_server<?php echo $img_link;?>').hide();
    }

}
function slice_str(str,n){
	var r={};
	var l=str.length;
	r['str_all']=0;
	if(l>n){
		k=0;
		for (var i = 0; i < Math.ceil(l/n); i++) {
			r['str_n'+(i+1)]=str.substr(i*n,n);
			r['str_all']++;
		};

	}else{
		r['str_n1']=str;
		r['str_all']=1;
	}
	return r;
}

function send_imgs_<?php echo $img_link;?>(i){
	$('#uploading_status_<?php echo $img_link;?>').show();
	$('#uploading_status_<?php echo $img_link;?>').animate({opacity:1},1);
	$('#upload_to_server<?php echo $img_link;?>').hide();
	$('#choose_files_<?php echo $img_link;?>').addClass('disabled');
	$('#upload_img_<?php echo $img_link;?>').hide();
	if(fd_<?php echo $img_link;?>.length==i){
                        	$('#img_for_upload_<?php echo $img_link;?>').html('');
                        	fd_<?php echo $img_link;?> = [];
                        	$('#choose_files_<?php echo $img_link;?>').removeClass('disabled');
							$('#upload_img_<?php echo $img_link;?>').show();
							$('#uploading_status_<?php echo $img_link;?>').html('Изображения загружены');
							$('#uploading_status_<?php echo $img_link;?>').animate({opacity:0},3000);
    }else{
    	$('#uploading_status_<?php echo $img_link;?>').html('Идет загрузка...');
	/*var fd = new FormData();
	fd.append('file',fd_<?php echo $img_link;?>[i]);
	fd.append('link','<?php echo $img_link;?>');
	fd.append('parent','<?php echo $note['id'];?>');
	fd.append('ex','<?php echo $img_ex;?>');
	fd.append('t_w','<?php echo $img_thumb_width;?>');
	fd.append('t_h','<?php echo $img_thumb_height;?>');console.log(fd_<?php echo $img_link;?>[i]);
	fd.append('file2',fd_<?php echo $img_link;?>[i]);*/
	var st={};
	st['link']='<?php echo $img_link;?>';
	st['parent']='<?php echo $note['id'];?>';
	st['ex']='<?php echo $img_ex;?>';
	st['t_w']='<?php echo $img_thumb_width;?>';
	st['t_h']='<?php echo $img_thumb_height;?>';
	//st['file']=fd_<?php echo $img_link;?>[i];//console.log(st);
	$.extend(st,slice_str(fd_<?php echo $img_link;?>[i],10000));//console.log(st);
	$.ajax(

                {
                    url: '<?php echo $admin_path;?>classes/img_upolader.php',
                    data: st,
                    //cache: false,
                    //contentType: false,
                    //processData: false,
                    type: 'POST',
                    //async:false,
                   	success: function (data) {//alert(data);
                        $('#img_for_upload_<?php echo $img_link;?>').children('.thumb_cnt:first').remove();
                        
                        $('#pause').animate({opacity:0},1,function(){
                        	$('#pause').animate({opacity:1},50,function(){
                        	send_imgs_<?php echo $img_link;?>(i+1);
                        	var new_num = $('#img_list_<?php echo $img_link;?>').children('.thumb_cnt').length;
                        	if(new_num<1){
                        		new_num = 1;
                        	}else{
                        		new_num = $('#img_list_<?php echo $img_link;?>').children('.thumb_cnt:last').attr('num');
                        		new_num =parseInt(new_num)+1;
                        	}
                        	create_thumb_<?php echo $img_link;?>(data,new_num,'<?php echo $img_ex;?>');
                        	$('#img_list_<?php echo $img_link;?>').sortable('refresh');
                        	})
                        })
                        

                        
                        
                    },
                    error: function (data) {

                    }
                });


    
	}
}


	
function make_sortable_<?php echo $img_link;?>(){
	$('#img_list_<?php echo $img_link;?>').sortable({
		items: '.thumb_cnt',
		axis:'x,y',
		containment: 'document',
	    opacity: 0.6,
	    cursor: 'move',
	    stop: function(){
	    	sort_imgs_<?php echo $img_link;?>();
	    }
	})
}

make_sortable_<?php echo $img_link;?>()

function sort_imgs_<?php echo $img_link;?>(){
	var sort_el = $('#img_list_<?php echo $img_link;?>').children('.thumb_cnt');
    	var sort_str = [];
    	$.each(sort_el,function(i,el){
    		var sort = $(el).attr('rel')+":"+(i+1);
    		sort_str.push(sort);
    	})
    	var sort_str = sort_str.join(",");
    	$.post("<?php echo $admin_path;?>classes/img_sorter.php",{link:'<?php echo $img_link;?>',parent:'<?php echo $note['id'];?>',sort_str:sort_str},function(data){
    		$.each(sort_el,function(i,el){
    		$(el).attr('num',i+1);    		
    		})

    	})
}

function edit_img_<?php echo $img_link;?>(el){
	var im = $(el).parent('div').parent('div').parent('div');
	var id = $(im).attr('rel');
	var num = $(im).attr('num');
	$('#img_index_<?php echo $img_link;?>').hide();

	$('#img_note_<?php echo $img_link;?>').show();
	$('#img_note_<?php echo $img_link;?>').children('.img_note_cnt').html("<img src='<?php echo $admin_path;?>img/loading.gif'/>");
	$('#img_note_<?php echo $img_link;?>').children('.img_note_cnt').load("<?php echo $admin_path;?>photos/note.php?id="+id+"&link=<?php echo $img_link;?>&num="+num+"&wt=<?=$img_thumb_width;?>&ht=<?=$img_thumb_height;?>");

}

function back_img_<?php echo $img_link;?>(){
	$('#img_index_<?php echo $img_link;?>').show();
	$('#img_note_<?php echo $img_link;?>').hide();
	$('#img_note_<?php echo $img_link;?>').children('.img_note_cnt').html('');
}


</script>