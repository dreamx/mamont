<?php
session_start();
include "../../config.php";
include "settings.php";
include "../../classes/core.php";

?>
<!DOCTYPE HTML>
<html class="no-js" xml:lang="ru" lang="ru">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Система управления контентом</title>
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/dataTables.tableTools.min.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/chosen.min.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/chosen-bootstrap.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/bootstrap-datetimepicker.css" />


    <script src="<?php echo $admin_path;?>js/vendor/modernizr.js"></script>
    <script src="<?php echo $admin_path;?>js/vendor/jquery.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery-ui.min.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery.jcrop.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap.min.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery.dataTables.js"></script>
    <script src="<?php echo $admin_path;?>js/chosen.jquery.min.js"></script>
    <script src="<?php echo $admin_path;?>js/dataTables.tableTools.min.js"></script>
    <script src="<?php echo $admin_path;?>js/dataTables.bootstrap.js"></script>
    <script src="<?php echo $admin_path;?>js/moment.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap-datetimepicker.ru.js"></script>
    <script src="<?php echo $admin_path;?>wisiwyg/ckeditor/ckeditor.js"></script>
    <script src="<?php echo $admin_path;?>wisiwyg/ckeditor/adapters/jquery.js"></script>
  
  </head>
  <body data-spy="scroll" data-target=".bs-docs-sidebar">
  <style type="text/css">
    body {
        padding-top: 70px;
        padding-bottom: 70px;
      }
  </style>

<?php 
include "../../panel.php";
?>

<div id="<?php echo $module_name;?>_module_index">

<?php

if(isset($module_header)){echo "<div class='container-fluid'><h2>".$module_header."</h2><hr></div>"; }


	echo "<div class='container-fluid'>";
  ?>
  <div id="rez"><br><br></div>
  <textarea rows="10" cols="120" id="field-news_content">
  </textarea>
<br>
  <div class="btn btn-primary" id="send_push" onclick="send_push()">отправить</div>
    
  <?php
  echo "</div>";


	



?>

</div>
<script type="text/javascript">
function send_push()
{
  $('#rez').html('<br><br>');
  $('#send_push').hide();
  $.post('/admin/modules/push_appl/send_push.php',{desc:$('#field-news_content').val()}, function(data) {
    $('#send_push').show();
    $('#rez').html(data);
    $('#field-news_content').html('');
    
  });
  /*$.post('/send_push.php',  function(data) {
    $('#rez').html(data);
  });*/
}
</script>

  </body>
</html>

<?




?>