<?php
session_start();
include "../../config.php";
include "../../classes/core.php";
include "settings.php";


?>
<!DOCTYPE HTML>
<html class="no-js" xml:lang="ru" lang="ru">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Система управления контентом</title>
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/dataTables.tableTools.min.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/chosen.min.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/chosen-bootstrap.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/bootstrap-datetimepicker.css" />


    <script src="<?php echo $admin_path;?>js/vendor/modernizr.js"></script>
    <script src="<?php echo $admin_path;?>js/vendor/jquery.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery-ui.min.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery.jcrop.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap.min.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery.dataTables.js"></script>
    <script src="<?php echo $admin_path;?>js/chosen.jquery.min.js"></script>
    <script src="<?php echo $admin_path;?>js/dataTables.tableTools.min.js"></script>
    <script src="<?php echo $admin_path;?>js/dataTables.bootstrap.js"></script>
    <script src="<?php echo $admin_path;?>js/moment.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap-datetimepicker.ru.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery.dataTables.columnFilter.js"></script>
    <script src="<?php echo $admin_path;?>wisiwyg/ckeditor/ckeditor.js"></script>
    <script src="<?php echo $admin_path;?>wisiwyg/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">
    var mt;
    var mt_tb;
    function delete_selected_rows(module){
      if(confirm("Подтвердите удаление")){
        var rows = $('#'+module+'_table').find("tr.active");
        var rids = [];
        var module;
        $.each(rows, function(i,row){
          var row_id = $(row).attr('id').split("-");
          module = row_id[0];
          if(module == 'users' && parseInt(row_id[1]) == 1){
          }else{
            rids.push(row_id[1]); 
          }


        })

        rids = rids.join(",");
        
        $.post("<?php echo $admin_path;?>classes/multidel.php",{module:module,ids:rids},function(data){
          $(rows).removeClass("active");
          $('#delete_selected').parent('span').parent('a').addClass('disabled');
          $('#delete_selected').parent('span').parent('a').parent('div').find('a').eq(0).removeClass('disabled');
          $('#delete_selected').parent('span').parent('a').parent('div').find('a').eq(1).addClass('disabled');
          $.each(rids.split(","),function(i,id){
            
            mt.row($('#'+module+"-"+id)).remove().draw();
          })
        })

      }

    }
    function add_new_row(id,module){
      var table_head = $('#'+module+'_table').find('th').length;
      var new_fields = [];
      for(i=0;i<table_head;i++){
        new_fields.push("");
      }

      var newRow = mt.row.add(new_fields).draw().node();
      $(newRow).attr('id',id);
      return $(newRow);
    }

    function open_note(el,from,mode){
      if(from == 'from_table'){
        var note_id = $(el).parent().parent().attr('id');
        note_id = note_id.split('-');

         
      }
      if(from == 'from_page'){
        var note_id = el;
        note_id = note_id.split('-');
        
        
      }
      if(from == 'from_tree'){
        var note_id = $(el).parent('div').parent('div').attr('id').split("_");
       
        
      }
      var module = note_id[0];
      note_id = note_id[1];
      


      $('#'+module+'_module_note').show();
      $('#'+module+'_module_index').hide();
      $('#'+module+'_module_note .container-fluid .cnt').html("<img src='<?php echo $admin_path;?>img/loading.gif'/>")
      $('#'+module+'_module_note .container-fluid .cnt').load("<?php echo $admin_path;?>html/note.php?id="+note_id+"&mode="+mode+"&module="+module,function(){
        top.location.href = "#"+note_id;
        if(mode=='add' && from !='from_tree'){
          //add_new_row(note_id,module);
        }
      });
    }

    function open_dop_note(el,from,mode,parent,callback){
      var parent=parent||'';
      if(from == 'from_table'){
        var note_id = $(el).parent('td').parent('tr').attr('id');
        note_id = note_id.split('-');
      }
      if(from == 'from_page'){
        var note_id = el;
        note_id = note_id.split('-');
      }
      
      $('.dop_edit').hide();
      var module = note_id[0];
      note_id = note_id[1];
      var nid='dp'+$.now();
      var new_div=$('<div/>',{
        class:"dop_edit container-fluid",
        id:nid,
      }).appendTo('body').html('<h2>Редактирование записи</h2>\
      <span onclick="close_dop(this,'+callback+');" rel="" module=""  class="btn btn-warning"><span class="glyphicon glyphicon-arrow-left"></span> Назад</span><br><br>\
    <div class="cnt">\
    </div>  ').find('.cnt');



      $('.module_note').hide()
      $('.module_index').hide();
      new_div.html("<img src='<?php echo $admin_path;?>img/loading.gif'/>")
      new_div.load("<?php echo $admin_path;?>html/note.php?id="+note_id+"&mode="+mode+"&module="+module+"&dp="+nid+"&parent="+parent,function(){
       // new_div.prepend('<div onclick="close_dop(this);" class="close_dop">закрыть</div>');
      });
    }

    function close_dop(el,fn){
      if(fn){
        fn();
      }
      $(el).parent().remove();
      var dp_edit=$('body').find('.dop_edit');
      if(dp_edit.length>0){
        $('body').find('.dop_edit').last().show();
      }else{
         $('.module_note').show();
      }
    }



    function back_note(el,mode){
      var note_id = $(el).attr("rel");
      var module = $(el).attr('module');
      $('#'+module+'_module_note').hide();
      $('#'+module+'_module_index').show();
      $('#'+module+'_module_note .container-fluid .cnt').html("");
      if(note_id!="" && note_id!=undefined){
      <?php if($viewMode == 'tree'){ ?>
        refresh_node(note_id,module);
      <?php }else{ ?>
        refresh_note(note_id,module);
      <?php } ?>
      
        
      }



    }

    function delete_note(el,from){
      if(from == 'from_table'){
        var note_id = $(el).parent('td').parent('tr').attr('id');   
      }
      if(from == 'from_page'){
        var note_id = el;
      }
      note_id = note_id.split('-');
       var module = note_id[0];
       note_id = note_id[1]; 

      if(confirm("Подтвердите удаление")){
          var count_rows = $("#"+module+"_table").find('th').length;
          var row_cnt = $("#"+module+"-"+note_id).html();
          var preoader = "<td colspan="+count_rows+"><img src='<?php echo $admin_path;?>img/loading.gif'/> Удаление записи...</td>";
          $("#"+module+"-"+note_id).html(preoader);
          $.post('<?php echo $admin_path;?>classes/delete_note.php',{module:module,id:note_id},function(data){
            data = $.parseJSON(data);
            if(data.status == "ok"){
            mt.row($("#"+module+"-"+note_id)).remove().draw();  
            }else{

            alert('Ошибка удаления');
            $("#"+module+"-"+note_id).html(row_cnt);
            }

          })
        }
    }

    

    function refresh_note(note_id,module){

      var count_rows = $("#"+module+"_table").find('th').length;
      var row=$("#"+note_id);
      if(row.length<=0)
      {
        row=add_new_row(note_id,module);
      }
      var preloader = "<td colspan="+count_rows+"><img src='<?php echo $admin_path;?>img/loading.gif'/></td>";
      row.html(preloader);
      $.post("<?php echo $admin_path;?>/classes/refresh_note.php?note="+note_id, function(data) {
        
        row.html(data);
      });
      //$("#"+note_id).load("<?php echo $admin_path;?>/classes/refresh_note.php?note="+note_id);alert(module+" sas"+note_id);
    }

    function refresh_node(note_id,module){

      var node = note_id.split('-');

      var preloader = "<img src='<?php echo $admin_path;?>img/loading.gif' height='14'/>";
      var node_el = $("#"+node[0]+"_"+node[1]).find('.btn-group:first').find('.btn').eq(1);
      node_el.html(preloader);
      node_el.load("<?php echo $admin_path;?>/classes/refresh_node.php?note="+note_id);

    }

    function open_node(el){
          var node = $(el).parent('div').parent('div');
          
          if($(node).find('.tree_nodes_list:first').is(':visible')){
            $(node).find('.tree_nodes_list:first').hide();
            $(el).html('<span class="glyphicon glyphicon-plus"> </span>');
            $(el).removeClass('btn-warning');
            
          }else{

            $(node).find('.tree_nodes_list:first').show();
            $(el).html('<span class="glyphicon glyphicon-minus"> </span>');
            $(el).addClass('btn-warning');

          }
    }

    function add_tree_node(el){
      var node = $(el).parent('div').parent('div');
      var node_id = $(node).attr('id').split('_');
      $(node).find('.tree_nodes_list:first').show();
      $(node).find('.open_button:first').html('<span class="glyphicon glyphicon-minus"> </span>');
      $(node).find('.open_button:first').addClass('btn-success');
      $(node).find('.open_button:first').addClass('btn-warning');
      $(node).find('.open_button:first').attr('onclick','open_node(this)');
      $(node).append('<div class="btn-group" style="margin-left:34px;"><button class="btn btn-default"><img src="<?php echo $admin_path;?>img/loading.gif" height="14"/> Создание</button></div>');
      $.post("<?php echo $admin_path;?>classes/add_tree_node.php",{module:node_id[0],parent:node_id[1]},function(data){
        var data = $.parseJSON(data);
        $(node).find('.btn-group:last').remove();
        if(data.status == 'ok'){

          htm = $(data.htm);
           
          if(node_id[1] == 0){
            var place = $(node).parent('div').find('.tree_nodes_list:first');
            if(!place.length){
              $(node).parent('div').append("<div class='tree_nodes_list'></div>");
              place = $(node).parent('div').find('.tree_nodes_list:first');
            }
           
          }else{
            var place = $(node).children('.tree_nodes_list'); 
            if(!place.length){
              $(node).append("<div class='tree_nodes_list' style='margin-left:34px;'></div>");
              place = $(node).children('.tree_nodes_list');
            }
          
          }
          $(place).append(htm);
        }else{
          alert("Недостаточно прав");

        }
        
      })

    }

    function sort_tree_node(el){
      var mode = $(el).attr('rel');
      var num = $(el).parent('div').attr('rel');
      var node = $(el).parent('div').parent('div');
      var node_id = $(node).attr('id').split('_');
      var pls_cnt = $(node).find('button:first').html();
      var num_el = $(node).parent('div').children('.tree_nodes_cnt');
      var num_index = $(num_el).index(node);

      var num_nodes = $(node).parent('div').children('.tree_nodes_cnt').length;
      var change = false;
      if(mode == 'up' && num > 1){
        change = true;
        var exch_node = $(node).prev();
      }
      if(mode == 'down' && num_index < num_nodes-1){
        change = true;
        var exch_node = $(node).next();
      }
      if(change){
        var ex_id = $(exch_node).attr('id').split('_');
        var change_num = $(exch_node).find('.btn-group:first').attr('rel');
        $(node).find('button:first').html('<img src="<?php echo $admin_path;?>img/loading.gif" height="14"/>');
        $(node).find('button:first').addClass('disabled');
        $.post('<?php echo $admin_path;?>classes/sort_tree_node.php',{module:node_id[0], id:node_id[1], sort_num:change_num, ex_id:ex_id[1], ex_num:num},function(data){
          var data = $.parseJSON(data);
          if(data.status == 'ok'){
            if(mode == 'down'){
            $(node).insertAfter($(exch_node));            
            }else{
              $(node).insertBefore($(exch_node));
            }

            $(exch_node).find('.btn-group:first').attr('rel',num);
            $(node).find('.btn-group:first').attr('rel',change_num);
            $(node).find('button:first').removeClass('disabled');
            $(node).find('button:first').html(pls_cnt);
          }else{
            alert("Недостаточно прав");
          }
        })
        
        
        
        
      }


    }

    function del_tree_node(el){
      if(confirm("Внимание! Также будут удалены все вложенные разделы и связанные с ними данные!")){
        var node = $(el).parent('div').parent('div');
        var node_id = $(node).attr('id').split('_');
        $.post("<?php echo $admin_path;?>classes/del_tree_node.php",{module:node_id[0],id:node_id[1]},function(data){
        var data = $.parseJSON(data);
        if(data.status == 'ok'){

          var parent_node = $(node).parent('div').parent('div');          
          var parent_div = $(node).parent('div');         
          $(node).remove();
          var node_parent = parent_div.find('.tree_nodes_cnt').length;
            
         if(node_parent == 0){
            var pls_button = $(parent_node).find('.btn-group:first').find('button:first');
            $(pls_button).removeClass('btn-success');
            $(pls_button).removeClass('btn-warning');
            $(pls_button).addClass('btn-default');
            $(pls_button).html('&nbsp;');
            parent_div.remove();
          }
        }else{
          alert("Недостаточно прав");

        }
        
      })
        
      }
    }

    function move_tree_node(node_id,parent,num,module){
      
      if(parent == 0 ){
        var place = $('#'+module+'_tree').children('.tree_nodes_list');
      }else{
        var place = $('#'+module+'_'+parent).children('.tree_nodes_list');
      }
      if(!place.length){
        if(parent!=0){
        var margin = 'style="margin-left:34px;"';
        }else{
        var margin = "";
       }
        $('#'+module+'_'+parent).append('<div class="tree_nodes_list" '+margin+'></div>');
        place = $('#'+module+'_'+parent).children('.tree_nodes_list');
      }
      place.show();
      if(num == 1){
        $('#'+module+'_'+parent).children('.btn-group').find('button').eq(0).addClass('btn-success');
        
         $('#'+module+'_'+parent).children('.btn-group').find('button').eq(0).attr('onclick','open_node(this)');
      }
      $('#'+module+'_'+parent).children('.btn-group').find('button').eq(0).addClass('btn-warning');
      $('#'+module+'_'+parent).children('.btn-group').find('button').eq(0).html('<span class="glyphicon glyphicon-minus"> </span>');
       
      $('#'+module+'_'+node_id).children('.btn-group').attr('rel',num);
      var node_html = $('#'+module+'_'+node_id).html();
      var from = $('#'+module+'_'+node_id).parent('div').parent('div');
      $('#'+module+'_'+node_id).remove();
      var nodes_in_parent = from.children('.tree_nodes_list').children('.tree_nodes_cnt').length;
      if(nodes_in_parent<1){
        from.children('.tree_nodes_list').remove();
        from.children('.btn-group').find('button').eq(0).html('&nbsp;');
        from.children('.btn-group').find('button').eq(0).removeClass('btn-success');
        from.children('.btn-group').find('button').eq(0).removeClass('btn-warning');
        from.children('.btn-group').find('button').eq(0).addClass('btn-default');
        from.children('.btn-group').find('button').eq(0).removeAttr('onclick');
      }
      
      var node_to_move = "<div id='"+module+"_"+node_id+"' class='tree_nodes_cnt'>"+node_html+"</div>";
      $(place).append(node_to_move);

    }

    </script>


  </head>
  <body data-spy="scroll" data-target=".bs-docs-sidebar">
  <style type="text/css">
    body {
        padding-top: 70px;
        padding-bottom: 70px;
      }
  </style>

<?php 
include "../../panel.php";
?>

<div id="<?php echo $module_name;?>_module_index" class="module_index">

<?php

if(isset($module_header)){echo "<div class='container-fluid'><h2>".$module_header."</h2><hr></div>"; }


	echo "<div class='container-fluid'>";
  if(!$core->check_permiss($_SESSION['user_id'],$module_name,'read')){
    ?> <script type="text/javascript">$('#<?php echo $module_name;?>_module_index > .container-fluid').eq(1).load("<?php echo $admin_path."error.php?code=0";?>")</script> <?php
  }else{
    if($viewMode == 'table'){
       $core->module_table($module_name,$add_button_title,$module_name.'_table',$list_query.$where_query.$order_query,$list_header);
    }
    if($viewMode == 'tree'){
      
        
        $core->module_tree($module_name,$list_query.$where_query.$order_query,$parent_name,$title_name,$sort_name);

        ?>
        
        <?php 
    }
  }

  echo "</div>";


	



?>

</div>

<div id="<?php echo $module_name;?>_module_note" class="module_note">
  <div class="container-fluid">
  <?php if(isset($module_header)){echo "<h2>".$module_note_title."</h2><hr>"; } ?>
    <a href="#" rel="" module="" id="back_to_list" class="btn btn-warning"><span class="glyphicon glyphicon-arrow-left"></span> <?php echo $module_back_title;?></a><br><br>
    <div class="cnt">
    
    </div>  
  </div>

</div>
<div id="pause"></div>
<script type="text/javascript">
    $(document).ready(function(){
      $("#<?php echo $module_name;?>_module_note").hide();
      var curr_url = window.location.href.split('#');
      var module = curr_url[0].split('/');
      module = module[module.length-2];
      if(curr_url[1]!="" && curr_url[1]!=undefined){
        open_note(module+"-"+curr_url[1],'from_page','edit');
      }
    })
</script>

  </body>
</html>

<?




?>