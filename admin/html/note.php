<?php
session_start();
include "../config.php";
include "../classes/core.php";
include "../modules/".$_GET['module']."/settings.php";
if($_GET['mode'] == 'edit'){
  $note = $core->_list("select * from `".$block_table."` where `id`='".intval($_GET['id'])."'");
}
if($_GET['mode'] == 'add'){
  $id = $core->add_note($block_table,$fields_for_add);
  $note = $core->_list("select * from `".$block_table."` where `id`='".intval($id)."'");
  ?>
  <?php
}
//echo $module_name;
if(!$core->check_permiss($_SESSION['user_id'],$module_name,'read')){
  header("Location: ".$admin_path."error.php?code=0"); 
}



if(count($note)<1){
  header("Location: ".$admin_path."error.php?code=1");
}
$note = $note[0];

?>

<div class="row" style="margin-top:-55px;">
<div class="col-sm-offset-4 col-sm-5">
  <div id="save_info<?=$_GET['dp'];?>" class="alert" style="padding: 6px 12px; margin:0;"></div>
</div>

 <?php  if($core->check_permiss($_SESSION['user_id'],$module_name,'write')){ ?>
<div class="col-sm-3">
<a class="btn btn-primary save_bt" onclick="save_note<?=$_GET['dp'];?>();"><span class="glyphicon glyphicon-ok"></span>  Сохранить изменения</a>
</div>
    <?php } ?>
</div>
<hr>

<?php include "../modules/".$_GET['module']."/html.php"; ?>






<?php include "../modules/".$_GET['module']."/js.php"; ?>


<script>
  $('#save_info<?=$_GET['dp'];?>').hide();
  $('#back_to_list<?=$_GET['dp'];?>').attr('rel','<?php echo $module_name."-".$note['id'];?>');
  $('#back_to_list<?=$_GET['dp'];?>').attr('onclick',"back_note(this,'<?php echo $_GET['mode'];?>')");
  $('#back_to_list<?=$_GET['dp'];?>').attr('module',"<?php echo $module_name;?>");

  function refresh_save_info<?=$_GET['dp'];?>(){
    $('#save_info<?=$_GET['dp'];?>').removeClass('alert-success');
    $('#save_info<?=$_GET['dp'];?>').removeClass('alert-danger');
    $('#save_info<?=$_GET['dp'];?>').removeClass('alert-warning');
    $('#save_info<?=$_GET['dp'];?>').removeClass('alert-info');
  }

  function save_note<?=$_GET['dp'];?>(){
    var alert_status = '';
    var alert_text = "<img height='14' src='<?php echo $admin_path;?>img/loading.gif'/> Идет сохранение";
    refresh_save_info<?=$_GET['dp'];?>()
    $('#save_info<?=$_GET['dp'];?>').addClass('alert-'+alert_status);

    $('#save_info<?=$_GET['dp'];?>').show();
    $('#save_info<?=$_GET['dp'];?>').html(alert_text);
   
    if(validate_fields<?=$_GET['dp'];?>()){
          var post_data = {};
          if('<?=$_GET['dp'];?>'){
            post_data['id'] =$('#<?=$_GET['dp'];?> #field-id').val()
          }else{
            post_data['id'] =$('#field-id').val();
          }
          
          
          post_data['mode'] = '<?php echo $_GET['mode'];?>';
          post_data['module_name'] = '<?php echo $module_name;?>';
          var fields_for_save = [
          <?php echo "'".implode("','", $fields_for_save)."'";?>
           ];
          $.each(fields_for_save,function(i,field){
            var field_el = $('#field-'+field);
            var val_el = field_el.val();

            if(field_el.hasClass('wisiwyg')){
              val_el = CKEDITOR.instances['field-'+field].getData();
            }
            if(field_el.attr('type') == 'checkbox'){
              if(field_el.prop('checked')){
                val_el = "on"
              }else{
                val_el = "";
              }
            }
            
            post_data[field] = val_el;
            
          })
    
        
          $.post("<?php echo $admin_path."classes/"; ?>save.php",post_data,function(data){
            refresh_save_info();
            data = $.parseJSON(data);
            $('#save_info<?=$_GET['dp'];?>').addClass('alert-'+data.status);
            $('#save_info<?=$_GET['dp'];?>').html(data.html);
            $('#field-'+data.translit).val(data.translit_val);
          })
          
      
    }else{
      $('#save_info<?=$_GET['dp'];?>').addClass('alert-danger');
      $('#save_info<?=$_GET['dp'];?>').html('Заполните обязательные поля');
    }
   
  }

  function validate_fields<?=$_GET['dp'];?>(){
    var valid = true;

    var fields_required = [
    <?php echo "'".implode("','", $fields_required)."'";?>
    ];

    $.each(fields_required,function(i,field){
      
        if(!$('#field-'+field).val().replace(/\s+/g, '').length){
          
          valid = false;
          $('#field-'+field).parent('div').addClass('has-error');
          
          if(field == 'Valid_login'){
            $('#field-Users_login').parent('div').addClass('has-error');
          }
          if(field == 'Valid_password'){
            $('#field-New_password').parent('div').addClass('has-error');
            $('#field-Confirm_password').parent('div').addClass('has-error');
          }
          
        }else{ 
          $('#field-'+field).parent('div').removeClass('has-error');
         
          if(field == 'Valid_login'){
            $('#field-Users_login').parent('div').removeClass('has-error');
          }
          if(field == 'Valid_password'){
            $('#field-New_password').parent('div').removeClass('has-error');
            $('#field-Confirm_password').parent('div').removeClass('has-error');
          }
         
        }

           
    })

    return valid;
  }

</script>