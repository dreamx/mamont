<?php
session_start();
include "../../config.php";
include "settings.php";
include "../../classes/core.php";

?>
<!DOCTYPE HTML>
<html class="no-js" xml:lang="ru" lang="ru">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Система управления контентом</title>
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/dataTables.tableTools.min.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/chosen.min.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/chosen-bootstrap.css" />
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/bootstrap-datetimepicker.css" />


    <script src="<?php echo $admin_path;?>js/vendor/modernizr.js"></script>
    <script src="<?php echo $admin_path;?>js/vendor/jquery.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery-ui.min.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery.jcrop.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap.min.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery.dataTables.js"></script>
    <script src="<?php echo $admin_path;?>js/chosen.jquery.min.js"></script>
    <script src="<?php echo $admin_path;?>js/dataTables.tableTools.min.js"></script>
    <script src="<?php echo $admin_path;?>js/dataTables.bootstrap.js"></script>
    <script src="<?php echo $admin_path;?>js/moment.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap-datetimepicker.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap-datetimepicker.ru.js"></script>
    <script src="<?php echo $admin_path;?>wisiwyg/ckeditor/ckeditor.js"></script>
    <script src="<?php echo $admin_path;?>wisiwyg/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo $admin_path;?>js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo $admin_path;?>js/ajaxupload.js"></script>
  </head>
  <body data-spy="scroll" data-target=".bs-docs-sidebar">
  <style type="text/css">
    body {
        padding-top: 70px;
        padding-bottom: 70px;
      }
  </style>

<?php 
include "../../panel.php";
?>

<div id="<?php echo $module_name;?>_module_index">

<?php

if(isset($module_header)){echo "<div class='container-fluid'><h2>".$module_header."</h2><hr></div>"; }


	?><div class="col-md-3 col-md-offset-1">

<?php

 include "../../modules/".$module_name."/show_file.php";
?>

    </div>
  </div>

</div>
<?php
include "../../modules/".$module_name."/js.php";
?>


  </body>
</html>

<?




?>