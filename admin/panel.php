<nav class="navbar navbar-default navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#">Cherry CMS</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-navbar-collapse">
  
      <ul class="nav navbar-nav">
      	  <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> Управление сайтом <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <?php $core->make_top_menu($modules_all);?>
          </ul>
        </li>
      </ul>

      <ul class="nav navbar-nav navbar-right">
    <?php 
      if($core->check_permiss($_SESSION['user_id'],'users','write')){
    ?>
    <li>
			<a href="<?php echo $admin_path.$modules_path; ?>/users/"><span class="glyphicon glyphicon-user"></span> Пользователи</a>
		</li>
    <?php } ?>
							
		<li>
			<a href="/"><span class="glyphicon glyphicon-home"></span> На сайт</a>
		</li>
		<li>
			<a href="<?php echo $admin_path;?>logout.php"><span class="glyphicon glyphicon-off"></span> Выход</a>
		</li>

      </ul>
    </div>
  </div>
</nav>

