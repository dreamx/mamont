<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li class=""><a href="#page_values" data-toggle="tab">Значения</a></li>
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    $core->make_field("Название*",$name="title",$type="text",$note['title'],"","",$readonly,"required");
    $core->make_field("Единица измерения",$name="si",$type="text",$note['si'],"","",$readonly,"required");
    //$core->make_field("Номер п/п",$name="num",$type="text",$note['num'],"","",$readonly,"required");
    //$core->make_field("Публиковать",$name="link",$type="checkbox",$note['link'],"","",$readonly,"required");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,""); 
  ?>
</form>

  </div>
  

  <div class="tab-pane" id="page_values">
   <a class="btn btn-primary" onclick="open_dop_note('char_list-','from_page','add','<?=$note['id'];?>','refresh_test_val')"><span class="glyphicon glyphicon-plus"></span> Добавить значение</a>
    <?php
      $q="SELECT * FROM `char_list` WHERE `id_char`='".intval($note['id'])."'";
      $values=$core->_list($q);
      
        ?>
        <table id="val_table" class="table table-striped table-bordered">
          <thead>
            <tr><th>Значение</th><th></th></tr>
          </thead>
          <?php
          if(count($values)>0){
            foreach ($values as $vl) {

             ?>
             <tr id="char_list-<?=$vl['id'];?>"><td><?=$vl['value_char'];?></td>
              <td>
                <a class="btn btn-success" onclick="open_dop_note('char_list-<?=$vl['id'];?>','from_page','edit','<?=$note['id'];?>','refresh_test_val')"><span class="glyphicon glyphicon-pencil"></span></span>Изменить</a>
                <a href="#" class="btn btn-danger" onclick="delete_dop_note(this,'from_table')"><span class="glyphicon glyphicon-trash"></span> Удалить</a>
              </td>

             </tr>
             <?php
            }
          }
          ?>
        </table>
        <?php
      
    ?>
  </div>
  


</div>