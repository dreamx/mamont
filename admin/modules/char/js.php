<script type="text/javascript">
var vl_tb = $('#val_table').DataTable({
		    		"language": {
		            "lengthMenu": "Выводить по _MENU_ строк на странице",
		            "zeroRecords": "Ничего не найдено",
		            "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
		            "infoEmpty": "Нет записей",
		            "search": "Поиск: ",
		            "paginate": {
		      			"previous": "предыдущая",
		      			"next": "следующая",
		    		}

		        },
                    "aaSorting":[],

		        "oTableTools": {
	              "sRowSelect": "multi",
	              "aButtons": []
	         	}
		    	});


function delete_dop_note(el,from){
      if(from == 'from_table'){
        var note_id = $(el).parent('td').parent('tr').attr('id');   
      }
      if(from == 'from_page'){
        var note_id = el;
      }
      note_id = note_id.split('-');
       var module = note_id[0];
       note_id = note_id[1]; 

      if(confirm("Подтвердите удаление")){
          var count_rows = $("#"+module+"_table").find('th').length;
          var row_cnt = $("#"+module+"-"+note_id).html();
          var preoader = "<td colspan="+count_rows+"><img src='<?php echo $admin_path;?>img/loading.gif'/> Удаление записи...</td>";
          $("#"+module+"-"+note_id).html(preoader);
          $.post('<?php echo $admin_path;?>classes/delete_note.php',{module:module,id:note_id},function(data){
            data = $.parseJSON(data);
            if(data.status == "ok"){
            vl_tb.row($("#"+module+"-"+note_id)).remove().draw();  
            }else{

            alert('Ошибка удаления');
            $("#"+module+"-"+note_id).html(row_cnt);
            }

          })
        }
    }


</script>
  