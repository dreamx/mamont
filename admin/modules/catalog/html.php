
<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li><a href="#page_content" data-toggle="tab">Описание</a></li>
  <li><a href="#page_pic" data-toggle="tab">Изображения</a></li>
  <li class=""><a href="#page_price_change" data-toggle="tab">Именение цены</a></li>
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">
    <form class="form-horizontal" id="note_form">
      <?php
        if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
          $readonly = "disabled";
        }else{
          $readonly = "";
        }
        
        $core->make_field("Название*",$name="title",$type="text",$note['title'],"","",$readonly,"required");
        $core->make_field("Транслит:",$name="translit",$type="text",$note['translit'],"","","disabled","required");

        //$areas=$core->_list("SELECT `id`,`title` FROM `$block_table` WHERE `$parent_name`!='".intval($note['id'])."' AND `id`!='".intval($note['id'])."'");
        $areas=$core->_list("SELECT * FROM `$block_table` ORDER BY `parent` ");
        /*$ar=array();
        $ar[0]=" Корневой раздел";
        foreach ($areas as $value) {
          $ar[$value['id']]=$value['title'];
        }*/
        array_unshift($areas,array('id'=>0,'parent'=>'-1','title'=>'Корневой раздел'));
        $core->make_field("Родительский раздел",$name=$parent_name,$type="hierarchy",$note[$parent_name],$areas,"",$readonly,"");
        
        $core->make_field("Публиковать",$name="public",$type="checkbox",$note['public'],"","",$readonly,"");
        $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");
        $core->make_field("Сортировка",$name="num",$type="text",$note['num'],"","",$readonly,"");
        ?>
        <hr>
        <?php
        $core->make_field("title SEO",$name="title_seo",$type="text",$note['title_seo'],"","","","required");
        $core->make_field("desc SEO",$name="desc_seo",$type="text",$note['desc_seo'],"","","","required");
        $core->make_field("Keywords SEO",$name="keywords_seo",$type="text",$note['keywords_seo'],"","","","required");


        ?>
    </form>
  </div>
  <div class="tab-pane col-md-12" id="page_content">
  <?php
    $core->make_wisiwyg('desc',$note['desc'],$readonly);
    echo "<hr>";
  ?>
  </div>
  <div class="tab-pane" id="page_pic">
  <?php
    $img_max_size = 800;
    $img_link = "catalog";
    $img_thumb_width = 200;
    $img_thumb_height = 300;
    $img_block_num = 1;
    $img_limit = 1;
    $img_wtermark = false;
    $img_label = true;
    $img_desc = true;
    $img_mime = 'image/png';
    $img_ex = 'png';
    include "../photos/index.php";
  ?>
  </div>
  <div class="tab-pane" id="page_price_change">
    <hr><h4>Цена изменится у всех вложенных товаров, в том числе и в дочерних каталогах</h4><hr>
    <form class="form-horizontal">
    <div class="form-group col-sm-6">
      <label class="col-sm-12">Единицы измерения</label>
    <label class="col-sm-4 control-label"><input type="radio" class="rad" name = "j" value ="proc" checked="checked" > % </label>
    <label class="col-sm-4 control-label"><input type="radio" class="rad" name = "j" value ="rub" > руб.</label> 
    <hr><br>
    <label class="col-sm-12">Операция</label>
    <label class="col-sm-4 control-label"><input type="radio" class="rad" name = "f" value ="plus" checked="checked" >+&nbsp;&nbsp;</label> 
    <label class="col-sm-4 control-label"><input type="radio" class="rad" name = "f" value ="minus" > -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </label> 
    <hr>
    <br>
    <?php
    $core->make_field("Значение",$name="val_change",$type="text",'',"","",$readonly,"required");
    ?></div>
    <div class="col-sm-7">
      <div id="update_info" class="alert" style="padding: 6px 12px; margin: 0px; display: none;"></div>
    </div>
    <a class="btn btn-primary" id="but_update" onclick="update_price();"><span class="glyphicon glyphicon-ok"></span>Изменить цены</a>
    </form>
  </div>
</div>