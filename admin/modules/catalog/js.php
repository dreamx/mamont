<script type="text/javascript">
	$('.rad').click(function(event) {
		var value=$('input:radio[name=j]:checked').val();
		/*$('#field-Pages_trigger').val(value);
		if(value=='link')
		{
			$('.hide_el').hide();
			$('.hide_el_link').show();
		}else{
			$('.hide_el').show();
			$('.hide_el_link').hide();
		}*/
		
});
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}	
$('#field-val_change').keypress(function (e){
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  }); 
function update_price(){
	var oper=$('input:radio[name=f]:checked').val();
	var ed=$('input:radio[name=j]:checked').val();
	var val=$('#field-val_change').val();
	var id=$('#field-id').val();
	var info=$('#update_info');
	var but=$('#but_update');
	var alert_text = "<img height='14' src='<?php echo $admin_path;?>img/loading.gif'/> Идет изменение цен";
	var st='Цена ';
	st+=oper=='plus'?'увеличится на '+val:'уменьшится на '+val;
	st+=ed=='proc'?'%':'руб.';
	info.hide().html('').removeClass('alert-success alert-danger alert-warning alert-info');
	but.hide();
	if(!isNumber(val)){
		info.addClass('alert-danger').html('Укажите значение').fadeIn();
		but.fadeIn();
		return false;
	}
	if(confirm(st)){
		info.html(alert_text).fadeIn();
		$.post('update_price.php',{oper:oper,ed:ed,val:val,id:id},function(data){//alert(data);
			if(data==5){
				info.addClass('alert-success').html('Цены изменены.').fadeIn();
			}else{
				info.addClass('alert-danger').html('Ошибка изменения цен').fadeIn();	
			}
			but.fadeIn();
		});
	}else{
		but.fadeIn();
	}
	
}
</script>
  