<?php
$module_name = "catalog";
$viewMode = "tree";
$module_header = "Каталог";
$module_note_title = "Редактирование";
$module_back_title = "К списку";
$block_table = "catalog";
$add_button_title = "Создать";
$parent_name = "parent";
$title_name = "title";
$sort_name = "num";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`".$title_name."`,`".$block_table."`.`".$sort_name."`,`".$block_table."`.`".$parent_name."` from `".$block_table."`";
$where_query = "";
$order_query = " ORDER BY `".$block_table."`.`".$sort_name."`";
$list_header = array("Заголовок страницы","Порядковый номер","Родитель");
$fields_for_add= array(
	"title"=>"Новый пункт"
);
$translit="translit";//поле для записи транслита
$translit_field="title";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет

$fields_for_save = array(
	"title",
	"public",
	"translit",
	"parent",
	"num",
	"title_seo",
	"desc_seo",
	"keywords_seo",
	"desc"
);

$fields_required = array(
	"title"
);



?>