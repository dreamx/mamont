<?php
session_start();
include "../../../admin/config.php";
include "../../../admin/classes/core.php";
if($core->check_permiss($_SESSION['user_id'],'catalog','write')){
  
  function get_Child($arr,$parent){
  	$res=array();
  	array_push($res,"`catalog`='".$parent."'");
  	foreach ($arr as $el) {
  		if($el['parent']==$parent){
  			$res=array_merge($res,get_Child($arr,$el['id']));
  		}
  	}
  	return $res; 
  }


  if(
  	($_POST['oper']=='plus'||$_POST['oper']=='minus')&&
  	($_POST['ed']=='proc'||$_POST['ed']=='rub')&&
  	is_numeric($_POST['val'])&&
  	is_numeric($_POST['id'])
   ){
	  	$q="SELECT `id`,`parent` FROM `catalog`";
	  	$cat=$core->_list($q);
	  	$ids_cat=array();
	  	if(count($cat)>0){
	  		$wh=get_Child($cat,$_POST['id']);
	  		if(count($wh)>0){
	  			$wh=implode(' or ',$wh );
	  			if($_POST['ed']=='proc'&&$_POST['oper']=='plus'){
	  				$v=$_POST['val']/100;
	  				$q="UPDATE `product` SET `price`=`price`+`price`*".$v." WHERE ".$wh;
	  			}else if($_POST['ed']=='proc'&&$_POST['oper']=='minus'){
	  				$v=$_POST['val']/100;
	  				$q="UPDATE `product` SET `price`=`price`-`price`*".$v." WHERE ".$wh;	
		  		}else if($_POST['ed']=='rub'&&$_POST['oper']=='plus'){
	  				$v=$_POST['val'];
	  				$q="UPDATE `product` SET `price`=`price`+".$v." WHERE ".$wh;	
		  		}else if($_POST['ed']=='rub'&&$_POST['oper']=='minus'){
	  				$v=$_POST['val'];
	  				$q="UPDATE `product` SET `price`=`price`-".$v." WHERE ".$wh;	
		  		}else{
		  			echo "error";
		  			exit;	
		  		}
	  		if($core->_query($q)){
	  			
	  			echo 5;
	  		}else{
	  			echo "error";
	  		}
	  	}else{
	  		exit;
	  	}
		}else{
		echo "error";
		}
	  //$core->_query($q);
	}else{
		echo "error";
	}
}
?>