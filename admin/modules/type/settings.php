<?php
$module_name = "type";
$viewMode = "table";
$module_header = "Категория";
$module_note_title = "Редактирование категории";
$module_back_title = "К списку категорий";
$block_table = "type";
$add_button_title = "Создать категорию";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`name` from `".$block_table."`";
$where_query = "";
$order_query = "ORDER BY `num` asc";
$list_header = array("Название");
$fields_for_add= array(
	"name"=>"Новая категория"
);
$translit="";//поле для записи транслита
$translit_field="";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"name",
	"name_en",
	"num"
);

$fields_required = array(
	"name"
);



?>