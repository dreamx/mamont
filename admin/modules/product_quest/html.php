<ul class="nav nav-tabs">
  <li class="active"><a href="#quest_value" data-toggle="tab">Главная</a></li>
  <li class="hide_el" ><a href="#quest_content"  data-toggle="tab">Вопрос</a></li>
  <li class="hide_el" ><a href="#quest_reply"  data-toggle="tab">Ответ</a></li>
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="quest_value">
    <form class="form-horizontal" id="note_form">
      <?php
        if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
          $readonly = "disabled";
        }else{
          $readonly = "";
        }
        
        
        $core->make_field("Имя",$name="name",$type="text",$note['name'],"","",$readonly,"required");
        $core->make_field("Публиковать",$name="public_quest",$type="checkbox",$note['public_quest'],"","",$readonly,"");
    
        $core->make_field("ID_продукта",$name="product",$type="hidden",$note['product'],"","",$readonly,"");
        $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

      ?>

    </form>
  </div>
  <div class="tab-pane col-md-12" id="quest_content">
  <?php
    $core->make_field("Дата вопроса",$name="date_quest",$type="date",$note['date_quest']=='0000-00-00 00:00:00'?'':$note['date_quest'],"","",$readonly,"required");
    echo "<hr>";
    $core->make_wisiwyg('quest',$note['quest'],$readonly);
    echo "<hr>";
  ?>
  </div>
  <div class="tab-pane col-md-12" id="quest_reply">
  <?php
    $core->make_field("Дата ответа",$name="date_reply",$type="date",$note['date_reply']=='0000-00-00 00:00:00'?'':$note['date_reply'],"","",$readonly,"required");
    echo "<hr>";
    $core->make_wisiwyg('reply',$note['reply'],$readonly);
    echo "<hr>";
  ?>
  </div>

  
  
  
</div>