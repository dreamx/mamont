<script type="text/javascript">
if(! $.fn.DataTable.isDataTable('#quest_table')){

var quest_tb = $('#quest_table').DataTable({
            "language": {
                "lengthMenu": "Выводить по _MENU_ строк на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
                "infoEmpty": "Нет записей",
                "search": "Поиск: ",
                "paginate": {
                "previous": "предыдущая",
                "next": "следующая",
                }
            },
                    "aaSorting":[],
            "oTableTools": {
                "sRowSelect": "multi",
                "aButtons": []
            }
          });
}
//функция заменяет кнопки редактирования
function refresh_test_val(){
  var id='<?=$note["id"];?>';
  var name_mod='product_quest';//название дополнительного модуля 
  var r=$('#'+name_mod+'-'+id);
  //console.log(r.length);
  if(r.length>0){//если есть строка, то копируем в неё значения из формы
    r.find('td').eq(0).html($('#field-name').val());
    r.find('td').eq(1).html($('#field-date_quest').val());
    r.find('td').eq(2).html($('#field-public_quest').val());
  }else{
    //console.log('fdgdf');
    var but='<a class="btn btn-success" onclick="open_dop_note(\''+name_mod+'-'+id+'\',\'from_page\',\'edit\',\'\',\'refresh_test_val\')"><span class="glyphicon glyphicon-pencil"></span></span>Изменить</a> \
              <a href="#" class="btn btn-danger" onclick="delete_dop_note(this,\'from_table\')"><span class="glyphicon glyphicon-trash"></span> Удалить</a>';
    //рисуется новая строка (нужно добавить выводимые поля)
    var newRow = quest_tb.row.add([$('#field-name').val(),$('#field-date_quest').val(),$('#field-public_quest').val(),but]).draw().node();
      $(newRow).attr('id',name_mod+'-'+id);
  }
}


</script>
  