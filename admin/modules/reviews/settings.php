<?php
$module_name = "reviews";
$viewMode = "table";
$module_header = "Отзывы о товаре";
$module_note_title = "Редактирование";
$module_back_title = "К списку отзывов";
$block_table = "reviews";
$add_button_title = "Создать";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`name`,`date`,`bal`,`p`.`title` 
				from `".$block_table."` 
				LEFT JOIN `product` AS `p` ON `".$block_table."`.`prod`=`p`.`id`";
$where_query = "";
$order_query = " ORDER BY `date`";
$list_header = array("Имя","Дата","Оценка","Товар");
$fields_for_add= array(
	"name"=>"Имя",
	"prod"=>$_GET['parent']
);
$translit="";//поле для записи транслита
$translit_field="";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"name",
	"prod",
	"date",
	"public_review",
	"review",
	"bal"
);

$fields_required = array(
	"name"
);



?>