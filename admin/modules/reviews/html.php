<ul class="nav nav-tabs">
  <li class="active"><a href="#review_value" data-toggle="tab">Главная</a></li>
  <li class="hide_el" ><a href="#review_content"  data-toggle="tab">Отзыв</a></li>
 
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="review_value">
    <form class="form-horizontal" id="note_form">
      <?php
        if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
          $readonly = "disabled";
        }else{
          $readonly = "";
        }
        
        
        $core->make_field("Имя",$name="name",$type="text",$note['name'],"","",$readonly,"required");
        $core->make_field("Дата отзыва",$name="date",$type="date",$note['date']=='0000-00-00 00:00:00'?'':$note['date'],"","",$readonly,"required");
    
        $core->make_field("Публиковать",$name="public_review",$type="checkbox",$note['public_review'],"","",$readonly,"");
        $core->make_field("Оценка",$name="bal",$type="text",$note['bal'],"","",$readonly,"required");
        $core->make_field("ID_продукта",$name="prod",$type="hidden",$note['prod'],"","",$readonly,"");
        $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

      ?>

    </form>
  </div>
  <div class="tab-pane col-md-12" id="review_content">
  <?php
    
    $core->make_wisiwyg('review',$note['review'],$readonly);
    echo "<hr>";
  ?>
  </div>
  

  
  
  
</div>