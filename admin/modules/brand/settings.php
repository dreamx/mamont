<?php
$module_name = "brand";
$viewMode = "table";
$module_header = "Бренды";
$module_note_title = "Редактирование бренда";
$module_back_title = "К списку брендов";
$block_table = "brand";
$add_button_title = "Создать бренд";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`title` from `".$block_table."`";
$where_query = "";
$order_query = "";
$list_header = array("Наименование");
$fields_for_add= array(
	"title"=>"Новый бренд"
);
$translit="";//поле для записи транслита
$translit_field="";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"title",
	"desc",
	"num"
);

$fields_required = array(
	"title"
);



?>