<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li><a href="#page_content" data-toggle="tab">Описание</a></li>
  <li><a href="#page_pic" data-toggle="tab">Изображения</a></li>
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    
    
    $core->make_field("Наименование*",$name="title",$type="text",$note['title'],"","",$readonly,"required");
     $core->make_field("Номер п/п",$name="num",$type="text",$note['num'],"","",$readonly,"required");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

  ?>

</form>

  </div>
  <div class="tab-pane col-md-12" id="page_content">
  <?php
    $core->make_wisiwyg('desc',$note['desc'],$readonly);
    echo "<hr>";
  ?>
  
  </div>
  
  <div class="tab-pane" id="page_pic">
  <?php
    $img_max_size = 400;
    $img_link = "brand";
    $img_thumb_width = 200;
    $img_thumb_height = 200;
    $img_block_num = 1;
    $img_limit = 1;
    $img_wtermark = false;
    $img_label = false;
    $img_desc = false;
    $img_mime = 'image/jpeg';
    $img_ex = 'jpg';
    include "../photos/index.php";
  ?>
  </div>


  
  
</div>