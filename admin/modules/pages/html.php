<?php
if($note['Pages_trigger']=='link')
{
  $view_bl="display:none;";
}else{
  $view_l="display:none;";
} 
?>
<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li class="hide_el" style="<?php echo $view_bl;?>"><a href="#page_content"  data-toggle="tab">Содержимое (контент)</a></li>
  <li class="hide_el" style="<?php echo $view_bl;?>"><a href="#page_small"  data-toggle="tab">Краткое содержимое (контент)</a></li>

</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    



    $core->make_field("Заголовок страницы*",$name="Pages_title",$type="text",$note['Pages_title'],"","",$readonly,"required");
    //$core->make_field("Заголовок страницы EN",$name="Pages_title_en",$type="text",$note['Pages_title_en'],"","",$readonly,"required");
    //$core->make_field("Ссылка страницы: /page/",$name="Pages_title_translit",$type="text",$note['Pages_title_translit'],"","","disabled","required");
    ?>
    <div class="form-group">
          <label for="field-Pages_title_translit" class="col-sm-2 control-label">Ссылка страницы: /page/</label>
          <div class="col-sm-6">
            <span id="field-Pages_title_translit" ><?php echo $note['Pages_title_translit'];?></span>         </div>
      </div>
    <?
    $areas=$core->_list("SELECT `id`,`Pages_title` FROM `Pages` WHERE `Pages_parent`='0' AND `id`!='".intval($note['id'])."'");
    $ar=array();
    $ar[0]=" Корневой раздел";
    foreach ($areas as $value) {
      $ar[$value['id']]=$value['Pages_title'];
    }
    $core->make_field("Родительский раздел",$name="Pages_parent",$type="select",$note['Pages_parent'],$ar,"",$readonly,"");
    

    $core->make_field("Публиковать",$name="Pages_public",$type="checkbox",$note['Pages_public'],"","",$readonly,"");
    ?>
    <!-- <div class="form-group">
    <label class="col-sm-2 control-label"><input type="radio" class="rad" name = "j" value ="page" <?php if($note['Pages_trigger']=='page'){echo 'checked';}?>> Страница </label>
    <label class="col-sm-2 control-label"><input type="radio" class="rad" name = "j" value ="link" <?php if($note['Pages_trigger']=='link'){echo 'checked';}?>> Ссылка</label> 
    </div> -->
    <?php
   // $core->make_field("Pages_trigger",$name="Pages_trigger",$type="hidden",$note['Pages_trigger'],"","",$readonly,"");
    ?>
    <div id="link_blok" class="hide_el_link" style="<?php echo $view_l;?>">
      <?php
    //$core->make_field("Адрес ссылки",$name="Pages_link",$type="text",$note['Pages_link'],"","",$readonly,"");
    ?>
    </div>
    <div id="seo_blok" class="hide_el" style="<?php echo $view_bl;?>">
    <?php
    echo "<hr><h4>SEO-блок</h4><hr>";
    $core->make_field("Заголовок (тэг title)*",$name="Pages_title_seo",$type="text",$note['Pages_title_seo'],"","",$readonly,"required");
    $core->make_field("Описание (META description)",$name="Pages_desc_seo",$type="text",$note['Pages_desc_seo'],"","",$readonly,"");
    $core->make_field("Ключевые слова (META keywords)",$name="Pages_content_seo",$type="text",$note['Pages_content_seo'],"","",$readonly,"");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");
    ?>
    </div>

</form>

  </div>
  <div class="tab-pane col-md-12" id="page_content">
  <?php
    $core->make_wisiwyg('Pages_content',$note['Pages_content'],$readonly);
    echo "<hr>";
  ?>
 <!--  <h4>EN</h4><hr>
  <?php
    $core->make_wisiwyg('Pages_content_en',$note['Pages_content_en'],$readonly);
    echo "<hr>";
  ?> -->
  </div>

  <div class="tab-pane col-md-12" id="page_small">
  <?php
    $core->make_wisiwyg('Pages_small',$note['Pages_small'],$readonly);
    echo "<hr>";
  ?>
 <!--  <h4>EN</h4><hr>
  <?php
    $core->make_wisiwyg('Pages_small_en',$note['Pages_small_en'],$readonly);
    echo "<hr>";
  ?> -->
  </div>
</div>