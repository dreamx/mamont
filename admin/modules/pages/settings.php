<?php
$module_name = "pages";
$viewMode = "tree";
$module_header = "Страницы";
$module_note_title = "Редактирование страницы";
$module_back_title = "К списку страниц";
$block_table = "Pages";
$add_button_title = "Создать страницу";
$parent_name = "Pages_parent";
$title_name = "Pages_title";
$sort_name = "Pages_num";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`".$title_name."`,`".$block_table."`.`".$sort_name."`,`".$block_table."`.`".$parent_name."`,`Pages_title_translit` from `".$block_table."`";
$where_query = "";
$order_query = " ORDER BY `".$block_table."`.`".$sort_name."`";
$list_header = array("Заголовок страницы","Порядковый номер","Родитель","Транслит");
$fields_for_add= array(
	"Pages_title"=>"Новая страница",
	"Pages_title_translit"=>"Транслит"
);
$translit="Pages_title_translit";//поле для записи транслита
$translit_field="Pages_title";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет

$fields_for_save = array(
	"Pages_title",
	"Pages_title_seo",
	"Pages_desc_seo",
	"Pages_content_seo",
	"Pages_content",
	"Pages_small",
	"Pages_public",
	"Pages_title_translit",
	"Pages_content_mobile",
	"Pages_parent",
	"Pages_trigger",
	"Pages_link",
	"Pages_title_en",
	"Pages_content_en",
	"Pages_small_en"
);

$fields_required = array(
	"Pages_title"
);



?>