<script type="text/javascript">
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}




$("#field-value").keypress(function (e){
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  }); 

$('#discountModal #add_discount').click(function(){
  var info=$('#discountModal #update_info');
  if($('#field-date_begin').val()!=''&&$('#field-date_end').val()!=''){

    if($('#field-value').val()==''){
      info.addClass('alert-danger').html('Укажите значение скидки').fadeIn();
      return false;
    }
  var str= $('#discountModal #fr_disc').serialize();
  
  info.hide().html('').removeClass('alert-success alert-danger alert-warning alert-info');
  $.post('add_discount.php', str, function(data) {
      if(isNumber(data)){
        data=parseInt(data);
        if(data==5){
        info.addClass('alert-success').html('Скидка добавлена').fadeIn();
        refresh_basket();
        return true;
        }
      }else{
        info.addClass('alert-danger').html('Ошибка добавления скидки').fadeIn();
      }
    });
}else{
  info.addClass('alert-danger').html('Укажите период действия скидки').fadeIn();
  return false;
}

});

function refresh_basket(){
  $.post('refresh_basket.php',{basket_id:$('#field-id').val()},function(data){
    $('#basket_info').html(data);
  })
  
}
</script>
  