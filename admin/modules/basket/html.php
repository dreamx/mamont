<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li><a href="#page_content" data-toggle="tab">Содержимое (контент)</a></li>
  <!-- <li><a href="#page_cat" data-toggle="tab">Скидки</a></li> -->
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    /*OrderItem::getInstance(3);
    $it=OrderItem::getInstance();
    print_r($it);*/
    $o=Basket::getInstance($note['id']);
    //var_dump($o->turn_order($adres='ewewewewewe',$transport='1',$comment='cffcdf',$promo=''));
   // print_r($o->getMaxDisc_str()); 
   // print_r($o->get_sum());
   //var_dump($o->blocked());
   //BasketItem::add_item(2,array(8),2,1) ;
    $items=$o->getItems();
    //print_r($items);
     //print_r($items);
    /*print_r(array_merge($items[0]->getDiscountBasket(),$items[0]->get_discounts()));
    print_r(array_merge($items[1]->getDiscountBasket(),$items[1]->get_discounts()));
    print_r(array_merge($items[2]->getDiscountBasket(),$items[2]->get_discounts()));
    print_r(array_merge($items[3]->getDiscountBasket(),$items[3]->get_discounts()));
    *///print_r(array_merge($items[3]->getDiscountBasket(),$items[2]->get_discounts()));
    
    // echo $items[0]->get_summa_disc($o->getDate())."<br>";
    // echo $items[1]->get_summa_disc($o->getDate())."<br>";
    // echo $o->getSum()."<br>";
    //print_r($items[3]);
   
    $core->make_field("Номер",$name="num",$type="text",$note['id'],"","","disabled","");
    $core->make_field("Дата",$name="date",$type="text",$note['date'],"","","disabled","");
    
    $q="SELECT `id`,`name` FROM `client` WHERE `id`='{$note['client']}'";
    $clients=$core->_list($q);
    $core->make_field("Клиент",$name="client_name",$type="text",$clients[0]['name'],"","","disabled","");
    $core->make_field("Клиент",$name="client",$type="hidden",$clients[0]['id'],"","","disabled","");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

  ?>
    
</form>

  </div>
  <div class="tab-pane col-md-12" id="page_content">
    <div class="row">
     
      <div class="col-md-7" style="background-color: #dff0d8;">
        <h2>Содержимое корзины</h2>
        <span class="btn btn-primary" data-toggle="modal" data-target="#discountModal" ><span class="glyphicon glyphicon-tag"></span> Сделать скидку на корзину</span><br>
        <div id="basket_info">
        <?php
        $o->view_basket_admin();
        ?> 
        </div>
        <!-- Modal -->
    <div class="modal fade" id="discountModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Добавление скидки к корзине</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="discount_cont form-horizontal col-sm-12">
                <form id="fr_disc">
                  <input name="id_basket" type="hidden" value="<?php echo $note['id'];?>">
                <?php
                $core->make_field("Дата начала",$name="date_begin",$type="date",'',"","","","");
                $core->make_field("Дата окончания",$name="date_end",$type="date",'',"","","","");
                $core->make_field("Еденицы",$name="edinici",$type="select",'',array('rub'=>'руб.','perc'=>'%'),"",$readonly,"");
                $core->make_field("Значение",$name="value",$type="text",'',"","",$readonly,"required");
              ?></form>
              </div>
              <div class="col-sm-12">
                <div id="update_info" class="alert" style="padding: 6px 12px; margin: 0px; display: none;"></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            <button type="button" id="add_discount" class="btn btn-primary">Добавить скидку</button>
          </div>
        </div>
      </div>
    </div>
      
        
      </div>
    </div>
  <?php
    //$core->make_wisiwyg('Pages_content',$note['Pages_content'],$readonly);
    //echo "<hr>";
  ?>

  </div>
  

</div>