<?php
session_start();
include "../../config.php";
include "../../classes/core.php";
include "settings.php";
?>
<!DOCTYPE HTML>
<html class="no-js" xml:lang="ru" lang="ru">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Заказ №<?php echo $_GET['order'];?></title>
    <link rel="stylesheet" href="/admin/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/admin/css/dataTables.tableTools.min.css" />
</head>
<body style="width:800px;padding:20px;">       
<?
if($core->check_permiss($_SESSION['user_id'],'order','read')){
	if(is_numeric($_GET['order']))
	{
		$o=Order::getInstance(intval($_GET['order']));
		if($o){
			echo "<h1>Заказ №".$_GET['order']."</h1>";
			echo "Дата: ".date('d-m-Y',strtotime($o->date))."<br>";
			if(!empty($o->comm)){
				echo "Комментарии: ".$o->comm."<br>";
			}
			if(!empty($o->adres)){
				echo "Адрес доставки: ".$o->adres."<br><br>";
				
			}
			$o->view_order_admin(true);
		}
	}
}else{
	$data = "<font color='red'>Недостаточно прав</font>";
	echo $data;

}
?>
</body> 