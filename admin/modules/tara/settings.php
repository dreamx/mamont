<?php
$module_name = "tara";
$viewMode = "table";
$module_header = "Виды упаковки";
$module_note_title = "Редактирование вида упаковки";
$module_back_title = "К списку упаковок";
$block_table = "tara";
$add_button_title = "Создать вид упаковки";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`name` from `".$block_table."`";
$where_query = "";
$order_query = " ORDER BY `num` asc";
$list_header = array("Название");
$fields_for_add= array(
	"name"=>"Новая упаковка"
);
$translit="";//поле для записи транслита
$translit_field="";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"name",
	"name_en",
	"num"
);

$fields_required = array(
	"name"
);



?>