<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    $core->make_field("код*",$name="cod",$type="text",$note['cod'],"","",$readonly,"required");
    //$core->make_field("Название EN",$name="name_en",$type="text",$note['name_en'],"","",$readonly,"required");
    
    $core->make_field("статус",$name="status",$type="select",$note['status'],array('0'=>'Активен','1'=>'Использован'),"",$readonly,"");
    
    $q="SELECT IF(`d`.`edinici`='perc','%','руб.') as `edinici`,`d`.`value`,`d`.`name`,`d`.`id` 
      FROM `discount` AS `d`
      WHERE `d`.`public`='on'";
    $clients=$core->_list($q);
    $options_cl=array();
    foreach ($clients as $cl) {
      $options_cl[$cl['id']]=$cl['name']." ".$cl['value'].$cl['edinici'];
    }
    $core->make_field("Скидка",$name="discount",$type="select",'',$options_cl,"",$readonly,"");

    $core->make_field("Дата окончания",$name="date",$type="date",$note['date'],"","",$readonly,"required");
    //$core->make_field("Публиковать",$name="link",$type="checkbox",$note['link'],"","",$readonly,"required");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,""); 
  ?>
</form>

  </div>
  
  


</div>