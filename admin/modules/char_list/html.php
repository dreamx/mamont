<ul class="nav nav-tabs">
  <li class="active"><a href="#page_value" data-toggle="tab">Значение</a></li>
    <li><a href="#page_pic_char" data-toggle="tab">Изображения</a></li>
  
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_value">
    <form class="form-horizontal" id="note_form">
      <?php
        if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
          $readonly = "disabled";
        }else{
          $readonly = "";
        }
        
        
        $core->make_field("Значение",$name="value_char",$type="text",$note['value_char'],"","",$readonly,"required");
        $core->make_field("Значение цвета",$name="value_color",$type="text",$note['value_color'],"","",$readonly,"required");
        $core->make_field("ID_char",$name="id_char",$type="hidden",$note['id_char'],"","",$readonly,"");
        $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

      ?>

    </form>
  </div>
  
  <div class="tab-pane" id="page_pic_char">
  <?php
    $img_max_size = 400;
    $img_link = "char";
    $img_thumb_width = 200;
    $img_thumb_height = 200;
    $img_block_num = 1;
    $img_limit = 1;
    $img_wtermark = false;
    $img_label = false;
    $img_desc = false;
    $img_mime = 'image/jpeg';
    $img_ex = 'jpg';
    include "../photos/index.php";
  ?>
  </div>


  
  
  
</div>