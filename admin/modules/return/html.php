<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    if($core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $core->make_field("Пользователь",$name="user",$type="hidden",$_SESSION['user_id'],"","",$readonly,"");
      $q="SELECT `id` FROM `order` WHERE `status`='доставлен'";
      $tp=$core->_list($q);
      $options_cl=array();
      foreach ($tp as $cl) {
        $options_cl[$cl['id']]=$cl['id'];
      }
      $core->make_field("Заказ",$name="id_order",$type="select",$note['id_order'],$options_cl,"",$readonly,"");
    }else{
      $core->make_field("Заказ",$name="id_order",$type="text",$note['id_order'],"","","disabled","required");
    }
    $core->make_field("Сумма*",$name="summa",$type="text",$note['summa'],"","",$readonly,"required");
    $core->make_field("Примечание",$name="desc",$type="textarea",$note['desc'],"","",$readonly,"");
    
    
    
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,""); 
  ?>
</form>

  </div>
  
  


</div>