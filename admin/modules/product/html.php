<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li><a href="#page_content" data-toggle="tab">Описание</a></li>
  <li><a href="#page_pic" data-toggle="tab">Изображения</a></li>
  <!-- <li><a href="#page_chars" data-toggle="tab">Характеристики</a></li> -->
  <!-- <li><a href="#page_cat" data-toggle="tab">Скидки</a></li> -->
  <!-- <li><a href="#page_link" data-toggle="tab">Ссылка на скидку</a></li> -->
  <li><a href="#page_prod" data-toggle="tab">Сопутствующие товары</a></li>
  <li><a href="#prod_quests" data-toggle="tab">Вопросы по товару</a></li>
  <li><a href="#prod_reviews" data-toggle="tab">Отзывы о товаре</a></li>
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    
    
    $core->make_field("Наименование*",$name="title",$type="text",$note['title'],"","",$readonly,"required");
    $core->make_field("Артикул",$name="art",$type="text",$note['art'],"","",$readonly,"required");
    
    $q="SELECT * FROM `catalog` ORDER BY `num` ASC";
    $category=$core->_list($q);
    $core->make_field("Категория",$name="catalog",$type="hierarchy2",$note['catalog'], $category,"",$readonly,"");
    
    $q="SELECT `id`,`title` FROM `brand` ORDER BY `num` ASC";
    $clients=$core->_list($q);
    $options_cl=array();
    foreach ($clients as $cl) {
      $options_cl[$cl['id']]=$cl['title'];
    }
    $core->make_field("Бренд",$name="brand",$type="select",$note['brand'],$options_cl,"",$readonly,"");
    
    $core->make_field("Публиковать",$name="public",$type="checkbox",$note['public'],"","",$readonly,"");
    $core->make_field("Номер п/п",$name="num",$type="text",$note['num'],"","",$readonly,"required");
    $core->make_field("Новинка",$name="new",$type="checkbox",$note['new'],"","",$readonly,"");
    $core->make_field("Хит продаж",$name="hit",$type="checkbox",$note['hit'],"","",$readonly,"");
    $core->make_field("На слайд",$name="slide",$type="checkbox",$note['slide'],"","",$readonly,"");
    
    $core->make_field("Цена",$name="price",$type="text",$note['price'],"","",$readonly,"required");
    $core->make_field("На товар распространяется акция",$name="action",$type="checkbox",$note['action'],"","",$readonly,"");
    $core->make_field("Цена по акции",$name="action_price",$type="text",$note['action_price'],"","",$readonly,"required");
    $core->make_field("Характеристики кратко",$name="small_desc",$type="textarea",$note['small_desc'],"","",$readonly,"required");

    $core->make_field("Описание кратко",$name="short_desc",$type="textarea",$note['short_desc'],"","",$readonly,"required");
    
    $core->make_field("Видео",$name="video",$type="textarea",$note['video'],"","",$readonly,"required");
    


    echo "<hr><h4>SEO-блок</h4><hr>";
    $core->make_field("Заголовок (тэг title)*",$name="title_seo",$type="text",$note['title_seo'],"","",$readonly,"required");
    $core->make_field("Описание (META description)",$name="desc_seo",$type="text",$note['desc_seo'],"","",$readonly,"");
    $core->make_field("Ключевые слова (META keywords)",$name="content_seo",$type="text",$note['content_seo'],"","",$readonly,"");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

  ?>

</form>

  </div>
  <div class="tab-pane col-md-12" id="page_content">
  <?php
    $core->make_wisiwyg('desc',$note['desc'],$readonly);
    echo "<hr>";
  ?>
  
  </div>
    
  <div class="tab-pane" id="page_pic">
  <?php
    $img_max_size = 900;
    $img_link = "product";
    $img_thumb_width = 200;
    $img_thumb_height = 216;
    $img_block_num = 1;
    $img_limit = 1;
    $img_wtermark = false;
    $img_label = true;
    $img_desc = true;
    $img_mime = 'image/png';
    $img_ex = 'png';
    include "../photos/index.php";
  ?>
  </div>


  <!-- <div class="tab-pane" id="page_cat">
    <style type="text/css">
    #property{moz-column-count: 2;
    -moz-column-gap: 20px;
    -webkit-column-count: 2;
    -webkit-column-gap: 20px; 
    -webkit-column-count: 2; 
    -webkit-column-gap: 20px;width: 700px; }
    #property .form-group{width:340px;}
    #property .col-sm-6{width:15%;}
    #property .col-sm-2{width:75%;}
    </style>

    <?php
      $q = "select `id`,`name`, IF(`edinici`='perc','%','руб.') as `edinici`,`value` from `discount` WHERE `type`='5' OR `type`='4'";
      $qqq = "select * from `link_product_discont` WHERE `id_note1` = '".intval($note['id'])."'";
      $cats = $core->_list($q,'r');  
      $catsss = $core->_list($qqq,'r');  
      ?>      
            Отметьте галочками скидки распространяемые на товар<br><br>
            <div class="form-horizontal" id="property">
     <?php 
      foreach ($cats as $value) { $checked=''; ?>
      <?php 
          if($catsss){
            foreach ($catsss as $key => $itemsss) { 
              if($itemsss['id_note2'] == $value['id']) 
                {
                  $checked='on'; 
                  unset($catsss[$key]); 
                }
              } 
          }
            $core->make_field($value['name']." ".$value['value'].$value['edinici'] ,$name="chcat_".$value['id']."_".$note['id'],$type="checkbox",$checked,"","",$readonly,"");
            ?>
               <br>
                <?php 
      } 
      ?>
            </div>
    </div> -->

    <div class="tab-pane" id="prod_quests">
       <a class="btn btn-primary" onclick="open_dop_note('product_quest-','from_page','add','<?=$note['id'];?>','refresh_test_val')"><span class="glyphicon glyphicon-plus"></span> Добавить вопрос</a>
        <?php
          $q="SELECT * FROM `product_quest` WHERE `product`='".intval($note['id'])."' ORDER BY `date_quest`";
          $values=$core->_list($q);
          
            ?>
            <table id="quest_table" class="table table-striped table-bordered">
              <thead>
                <tr><th>Имя</th><th>Дата</th><th>Опубликован</th><th></th></tr>
              </thead>
              <?php
              if(count($values)>0){
                foreach ($values as $vl) {

                 ?>
                 <tr id="product_quest-<?=$vl['id'];?>">
                  <td><?=$vl['name'];?></td>
                  <td><?=$vl['date_quest'];?></td>
                  <td><?=$vl['public_quest'];?></td>
                  <td>
                    <a class="btn btn-success" onclick="open_dop_note('product_quest-<?=$vl['id'];?>','from_page','edit','<?=$note['id'];?>','refresh_test_val')"><span class="glyphicon glyphicon-pencil"></span></span>Изменить</a>
                    <a href="#" class="btn btn-danger" onclick="delete_dop_note(this,'from_table')"><span class="glyphicon glyphicon-trash"></span> Удалить</a>
                  </td>

                 </tr>
                 <?php
                }
              }
              ?>
            </table>
            <?php
          
        ?>
    </div>

    <div class="tab-pane" id="prod_reviews">
       <a class="btn btn-primary" onclick="open_dop_note('reviews-','from_page','add','<?=$note['id'];?>','refresh_test_val')"><span class="glyphicon glyphicon-plus"></span> Добавить отзыв</a>
        <?php
          $q="SELECT * FROM `reviews` WHERE `prod`='".intval($note['id'])."' ORDER BY `date` DESC";
          $values=$core->_list($q);
          
            ?>
            <table id="reviews_table" class="table table-striped table-bordered">
              <thead>
                <tr><th>Имя</th><th>Дата</th><th>Опубликован</th><th></th></tr>
              </thead>
              <?php
              if(count($values)>0){
                foreach ($values as $vl) {

                 ?>
                 <tr id="reviews-<?=$vl['id'];?>">
                  <td><?=$vl['name'];?></td>
                  <td><?=$vl['date'];?></td>
                  <td><?=$vl['public_review'];?></td>
                  <td>
                    <a class="btn btn-success" onclick="open_dop_note('reviews-<?=$vl['id'];?>','from_page','edit','<?=$note['id'];?>','refresh_test_val')"><span class="glyphicon glyphicon-pencil"></span></span>Изменить</a>
                    <a href="#" class="btn btn-danger" onclick="delete_dop_note(this,'from_table')"><span class="glyphicon glyphicon-trash"></span> Удалить</a>
                  </td>

                 </tr>
                 <?php
                }
              }
              ?>
            </table>
            <?php
          
        ?>

    </div>

  
  <!-- <div class="tab-pane" id="page_chars">
    <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#charModal" rel="add">
        Добавить характеристику
    </button>
    <?php
      $q="SELECT `cv`.*,`c`.`title` AS c_title,`cl`.`value_char` as `c_val` 
      FROM `char_value` AS  `cv`
      LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
      LEFT JOIN `char` AS `c` ON `cl`.`id_char`=`c`.`id` 
      WHERE `id_prod`='".intval($note['id'])."'
      ORDER BY `c_title`,`c_val`";
      $values=$core->_list($q);
      
        ?>
        <table id="val_table" class="table table-striped table-bordered">
          <thead>
            <tr><th>Характеристика</th><th>Значение</th><th>Цена</th><th></th></tr>
          </thead>
          <?php
          if(count($values)>0){
            foreach ($values as $vl) {

             ?>
             <tr id="val<?=$vl['id'];?>">
              <td><?=$vl['c_title'];?></td>
              <td><?=$vl['c_val'];?></td>
              <td><?=$vl['price'];?></td>
              <td>
                <button class="btn btn-success" data-toggle="modal" data-target="#charModal" rel="edit" ><span class="glyphicon glyphicon-pencil"></span></span>Изменить</button>
                <button class="btn btn-danger" onclick="delete_value('<?=$vl['id'];?>')"><span class="glyphicon glyphicon-trash"></span> Удалить</button>
              </td>

             </tr>
             <?php
            }
          }
          ?>
        </table>

    
    <div class="modal fade" id="charModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Редактирование характеристики</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="char_cont form-horizontal col-sm-offset-1 col-sm-12">
              </div>
              <div class="col-sm-12">
                <div id="update_info" class="alert" style="padding: 6px 12px; margin: 0px; display: none;"></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            <button type="button" id="save_char_val" class="btn btn-primary">Сохранить изменения</button>
          </div>
        </div>
      </div>
    </div>
  </div> -->

  <!-- <div class="tab-pane" id="page_link">
    <?php
      $q="SELECT `l`.*,IF(`d`.`edinici`='perc','%','руб.') as `edinici`,`d`.`value`,`d`.`name` 
      FROM `links_discount` AS  `l`
      LEFT JOIN `discount` AS `d` ON `l`.`id_discount`=`d`.`id`
      WHERE `l`.`id_prod`='".intval($note['id'])."'
      ORDER BY `l`.`date_begin` asc";
      $values=$core->_list($q);
    ?>
      
      <div class="modal fade" id="discModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h4 class="modal-title" id="myModalLabel">Редактирование ссылки на скидку</h4>
            </div>
            <div class="modal-body">
              <div class="row">
                <div class="char_cont form-horizontal col-sm-offset-1 col-sm-12">
                </div>
                <div class="col-sm-12">
                  <div id="update_info" class="alert" style="padding: 6px 12px; margin: 0px; display: none;"></div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
              <button type="button" id="save_disc_link" class="btn btn-primary">Сохранить изменения</button>
            </div>
          </div>
        </div>
      </div>
    
    <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#discModal" rel="add">
        Добавить ссылку
    </button>
        <br><br>
        <table id="link_table" class="table table-striped table-bordered">
          <thead>
            <tr><th>Ссылка</th><th>Скидка</th><th>Дата начала</th><th>Дата окончания</th><th></th></tr>
          </thead>
          <?php
          if(count($values)>0){
            foreach ($values as $vl) {

             ?>
             <tr id="link<?=$vl['id'];?>">
              <td>?discount=<?=$vl['md_cod'];?></td>
              <td><?=$vl['name']." ".$vl['value'].$vl['edinici'];?></td>
              <td><?=$vl['date_begin'];?></td>
              <td><?=$vl['date_end'];?></td>
              <td>
                <button class="btn btn-success" data-toggle="modal" data-target="#discModal" rel="edit" ><span class="glyphicon glyphicon-pencil"></span></span>Изменить</button>
                <button class="btn btn-danger" onclick="delete_link(<?=$vl['id'];?>)"><span class="glyphicon glyphicon-trash"></span> Удалить</button>
              </td>

             </tr>
             <?php
            }
          }
          ?>
        </table>
  </div> -->

   <div class="tab-pane" id="page_prod">
    <?php
    $q="SELECT `id`,`title` FROM `product` WHERE `id`!='".intval($note['id'])."'";
    $list_prod=$core->_list($q);
    if(count($list_prod)){
      $opt_pr=array();
      foreach ($list_prod as $cl) {
        $opt_pr[$cl['id']]=$cl['title'];
      }
      $core->make_field("Продукция",$name="dop_prod",$type="select",'',$opt_pr,"",$readonly,"");
    }
    $q="SELECT `id`,`title` 
    FROM `product` 
    LEFT JOIN `link_product_product` AS `pp` ON `pp`.`id_note2`=`product`.`id`
    WHERE `pp`.`id_note1`='".intval($note['id'])."'";
    $values=$core->_list($q);
    ?>
    <button class="btn btn-success" id="add_dop_prod">
      <span class="glyphicon glyphicon-plus"></span>
        Добавить товар
    </button>
    <br><br>
      <table id="prod_table" class="table table-striped table-bordered">
          <thead>
            <tr><th>название</th><th></th></tr>
          </thead>
          <?php
          if(count($values)>0){
            foreach ($values as $vl) {

             ?>
             <tr id="prd<?=$vl['id'];?>">
              <td><?=$vl['title'];?></td>
              <td>
                <button class="btn btn-danger" onclick="delete_prd('<?=$vl['id'];?>')"><span class="glyphicon glyphicon-trash"></span> Удалить</button>
              </td>

             </tr>
             <?php
            }
          }
          ?>
        </table>

  </div>
 
</div>