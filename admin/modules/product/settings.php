<?php
$module_name = "product";
$viewMode = "table";
$module_header = "Продукция";
$module_note_title = "Редактирование продукции";
$module_back_title = "К списку продукции";
$block_table = "product";
$add_button_title = "Создать";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`title` from `".$block_table."`";
$where_query = "";
$order_query = "";
$list_header = array("Наименование");
$fields_for_add= array(
	"title"=>"Новая продукция"
);
$translit="title_translit";//поле для записи транслита
$translit_field="title";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"title",
	"catalog",
	"brand",
	"desc",
	"small_desc",
	"price",
	"action_price",
	"video",
	"art",
	"title_seo",
	"desc_seo",
	"content_seo",
	"public",
	"title_translit",
	"num",
	"new",
	"hit",
	"action",
	"slide",
	"short_desc"
	
);

$fields_required = array(
	"title"
);



?>