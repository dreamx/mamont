<?php
session_start();
include "../../../admin/config.php";
include "../../../admin/classes/core.php";
if($core->check_permiss($_SESSION['user_id'],'product','write')){
  if($_POST['type']=='edit'){
  	$q="SELECT `id`,`title`,`si` FROM `char`";
    $clients=$core->_list($q);
    $options_cl=array('0'=>"Выбирете характеристику");
    foreach ($clients as $cl) {
      $options_cl[$cl['id']]=$cl['title']." ".$cl['si'];
    }

    $q="SELECT `cv`.*,`c`.`title` AS c_title,`cl`.`value_char` as `c_val`,`c`.`id` as `idch`,`cl`.`id` as `idval` 
      FROM `char_value` AS  `cv`
      LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
      LEFT JOIN `char` AS `c` ON `cl`.`id_char`=`c`.`id` 
      WHERE `cv`.`id`='".intval($_POST['id'])."'
      ORDER BY `c_title`,`c_val`
      LIMIT 1";

    $values=$core->_list($q);
    $values=$values[0];
    $q="SELECT `id`,`value_char` FROM `char_list`";
    $list=$core->_list($q);
    foreach ($list as $cl) {
      $op_val[$cl['id']]=$cl['value_char']." ".$cl['si'];
    }
    $core->make_field("",$name="chars",$type="select",$values['idch'],$options_cl,"",$readonly,"");
    $core->make_field("",$name="chars_vales",$type="select",$values['id_char'],$op_val,"",$readonly,"");
    $core->make_field("Цена",$name="price_char",$type="text",$values['price'],"","",$readonly,"");
    $core->make_field("",$name="id_value",$type="hidden",$values['id'],"","",$readonly,""); 
  }else if($_POST['type']=='add'){
  	$q="SELECT `id`,`title`,`si` FROM `char`";
    $clients=$core->_list($q);
    $options_cl=array('0'=>"Выбирете характеристику");
    foreach ($clients as $cl) {
      $options_cl[$cl['id']]=$cl['title']." ".$cl['si'];
    }
    $core->make_field("",$name="chars",$type="select",0,$options_cl,"",$readonly,"");
    $core->make_field("",$name="chars_vales",$type="select",0,array(),"",$readonly,"");
    $core->make_field("Цена",$name="price_char",$type="text",'',"","",$readonly,"");
    $core->make_field("",$name="id_value",$type="hidden",'',"","",$readonly,"");        
  }

  ?>
  <script type="text/javascript">
  	$('#field-chars').change(function(event) {
	  $.post('get_select_char_value.php',{id:$(this).val()},function(data){
	  	$('#field-chars_vales').html(data).trigger("chosen:updated");
	  });
	  
	  
	});

  </script>
  <?php
  
}
?>