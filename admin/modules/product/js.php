<script type="text/javascript">
$('#property input').change(function(event) {
	var el=$(this);
	var val_el='';

	if(el.attr('type') == 'checkbox'){
              if(el.prop('checked')){
                val_el = "on"
              }else{
                val_el = "";
              }
              $.post('set_check.php', {id:el.attr('id'),val:val_el}, function(){
              	//alert(data);
              });
          }
});

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
} 
//таблица характеристик
var vl_tb = $('#val_table').DataTable({
            "language": {
                "lengthMenu": "Выводить по _MENU_ строк на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
                "infoEmpty": "Нет записей",
                "search": "Поиск: ",
                "paginate": {
                "previous": "предыдущая",
                "next": "следующая",
                }
            },
                    "aaSorting":[],
            "oTableTools": {
                "sRowSelect": "multi",
                "aButtons": []
            }
          });
//таблица ссылок на скидки
var dl_tb = $('#link_table').DataTable({
            "language": {
                "lengthMenu": "Выводить по _MENU_ строк на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
                "infoEmpty": "Нет записей",
                "search": "Поиск: ",
                "paginate": {
                "previous": "предыдущая",
                "next": "следующая",
                }
            },
                    "aaSorting":[],
            "oTableTools": {
                "sRowSelect": "multi",
                "aButtons": []
            }
          });
//таблица с сопутствующими товарами
var pr_tb = $('#prod_table').DataTable({
            "language": {
                "lengthMenu": "Выводить по _MENU_ строк на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
                "infoEmpty": "Нет записей",
                "search": "Поиск: ",
                "paginate": {
                "previous": "предыдущая",
                "next": "следующая",
                }
            },
                    "aaSorting":[],
            "oTableTools": {
                "sRowSelect": "multi",
                "aButtons": []
            }
          });

// таблица отзывов
if(! $.fn.DataTable.isDataTable('#reviews_table')){
var reviews_tb = $('#reviews_table').DataTable({
            "language": {
                "lengthMenu": "Выводить по _MENU_ строк на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
                "infoEmpty": "Нет записей",
                "search": "Поиск: ",
                "paginate": {
                "previous": "предыдущая",
                "next": "следующая",
                }
            },
                    "aaSorting":[],
            "oTableTools": {
                "sRowSelect": "multi",
                "aButtons": []
            }
          });
}

//таблица вопросов
if(! $.fn.DataTable.isDataTable('#quest_table')){

var quest_tb = $('#quest_table').DataTable({
            "language": {
                "lengthMenu": "Выводить по _MENU_ строк на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
                "infoEmpty": "Нет записей",
                "search": "Поиск: ",
                "paginate": {
                "previous": "предыдущая",
                "next": "следующая",
                }
            },
                    "aaSorting":[],
            "oTableTools": {
                "sRowSelect": "multi",
                "aButtons": []
            }
          });
}

$('#charModal').on('keypress','#field-price_char',function (e){
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  }); 

$('#charModal').on('show.bs.modal', function (e) {
  var rel=$(e.relatedTarget).attr('rel');
  if(rel=='add'){
    $('#charModal .char_cont').html("<img height='14' src='<?php echo $admin_path;?>img/loading.gif'/> Идет загрузка");
    $.post('get_select_char.php', {type:'add'}, function(data) {
      $('#charModal .char_cont').html(data);
    });
  }else{
    $('#charModal .char_cont').html("<img height='14' src='<?php echo $admin_path;?>img/loading.gif'/> Идет загрузка");
    var id=$(e.relatedTarget).parents('tr').attr('id').slice(3);
    $.post('get_select_char.php', {type:'edit',id:id}, function(data) {
      $('#charModal .char_cont').html(data);
    });
  }
});

$('#discModal').on('show.bs.modal', function (e) {
  var rel=$(e.relatedTarget).attr('rel');
  if(rel=='add'){
    $('#discModal .char_cont').html("<img height='14' src='<?php echo $admin_path;?>img/loading.gif'/> Идет загрузка");
    $.post('edit_link_discount.php', {type:'add'}, function(data) {
      $('#discModal .char_cont').html(data);
    });
  }else if(rel=='edit'){
    $('#discModal .char_cont').html("<img height='14' src='<?php echo $admin_path;?>img/loading.gif'/> Идет загрузка");
    var id=$(e.relatedTarget).parents('tr').attr('id').slice(4);
    $.post('edit_link_discount.php', {type:'edit',id:id}, function(data) {
      $('#discModal .char_cont').html(data);
    });
  }
});

$('#charModal').on('hide.bs.modal', function (e) {
  $('#charModal #update_info').hide().html('').removeClass('alert-success alert-danger alert-warning alert-info');
});

$('#save_char_val').click(function(event) {
  var info=$('#charModal #update_info');
  var val= $('#field-chars_vales').val();
  var price=$('#field-price_char').val();
  var id_val=$('#field-id_value').val();
  var id_prod=$('#field-id').val();
  info.hide().html('').removeClass('alert-success alert-danger alert-warning alert-info');
  if(isNumber(val)&&(isNumber(price)||price=='')){
    $.post('save_char_val.php',{id:id_val,price:price,val:val,id_prod:id_prod},function(data){
      if(isNumber(data)){
        data=parseInt(data);
        $('#field-id_value').val(data);
        refresh_row_val(data);
        info.addClass('alert-success').html('Изменения сохранены').fadeIn();
      }else{
        info.addClass('alert-danger').html('Ошибка сохраниения').fadeIn();
      }
    });
    
  }else{
    info.addClass('alert-danger').html('Не указано значение характеристики').fadeIn();
  }
});

$('#save_disc_link').click(function(event) {
  var info=$('#discModal #update_info');
  var disc= $('#field-disc_sel').val();
  var start=$('#field-date_begin').val();
  var stop=$('#field-date_end').val();
  var type=$('#field-edit_type').val();
  var id_link=$('#field-id_disc').val();
  var id_prod=$('#field-id').val();
  info.hide().html('').removeClass('alert-success alert-danger alert-warning alert-info');
  if(start!=''&&stop!=''){
    $.post('save_disk_link.php',{id:id_link,disc:disc,start:start,stop:stop,type:type,id_prod:id_prod},function(data){
      if(isNumber(data)){
        data=parseInt(data);
        $('#field-id_value').val(data);
        refresh_row_disc(data);
        info.addClass('alert-success').html('Изменения сохранены').fadeIn();
      }else{
        info.addClass('alert-danger').html('Ошибка сохраниения').fadeIn();
      }
    });
    
  }else{
    info.addClass('alert-danger').html('Не указан период действия').fadeIn();
  }
});

function delete_value(id){
  if(confirm("Подтвердите удаление")){
          var count_th = $("#val_table").find('th').length;
          var row_cnt = $("#val"+id).html();
          var preoader = "<td colspan="+count_th+"><img src='<?php echo $admin_path;?>img/loading.gif'/> Удаление записи...</td>";
          $("#val"+id).html(preoader);
          $.post('delete_value.php',{id:id},function(data){
            if(data == 5){
            vl_tb.row($("#val"+id)).remove().draw();  
            }else{
            alert('Ошибка удаления');
            $("#val"+id).html(row_cnt);
            }

          })
        }
}

function refresh_row_val(id){

  var r=$('#val'+id);

  //console.log(r.length);
  $.post('refresh_row_val.php', {id:id}, function(data) {
    if(r.length>0){
    r.html(data);
  }else{
    var ff=[];
    var count_th = $("#val_table").find('th').each(function(index, el) {
      ff.push('');
    });
    var newRow = vl_tb.row.add(ff).draw().node();
      $(newRow).attr('id','val'+id).html(data);
  }

  });
  
}

function refresh_row_disc(id){

  var r=$('#link'+id);
  //console.log(r.length);
  $.post('refresh_row_disc.php', {id:id}, function(data) {
    if(r.length>0){
    r.html(data);
  }else{
    var ff=[];
    var count_th = $("#link_table").find('th').each(function(index, el) {
      ff.push('');
    });
    var newRow = dl_tb.row.add(ff).draw().node();
      $(newRow).attr('id','link'+id).html(data);
  }

  });
  
}

function delete_link(id){
  if(confirm("Подтвердите удаление")){
          var count_th = $("#link_table").find('th').length;
          var row_cnt = $("#link"+id).html();
          var preoader = "<td colspan="+count_th+"><img src='<?php echo $admin_path;?>img/loading.gif'/> Удаление записи...</td>";
          $("#link"+id).html(preoader);
          $.post('delete_link.php',{id:id},function(data){
            if(data == 5){
            vl_tb.row($("#link"+id)).remove().draw();  
            }else{
            alert('Ошибка удаления');
            $("#link"+id).html(row_cnt);
            }

          })
        }
}

$('#add_dop_prod').click(function(event) {
  var id1=$('#field-id').val();
  var id2=$('#field-dop_prod').val();
  var r=$("#prd"+id2);
  if(r.length<=0){
    $.post('edit_dop_prod.php', {id1:id1,id2:id2,type:'add'}, function() {
      var ff=[];
      ff.push($('#field-dop_prod option:selected').text());
      ff.push('<button class="btn btn-danger" onclick="delete_prd(\''+id2+'\')"><span class="glyphicon glyphicon-trash"></span> Удалить</button>');
      var newRow = pr_tb.row.add(ff).draw().node();
      $(newRow).attr('id','prd'+id2); 
    });
  }else{
    alert('Этот товар уже добавлен');
  }
});

function delete_prd(id2){
  var id1=$('#field-id').val();
  $.post('edit_dop_prod.php', {id1:id1,id2:id2,type:'del'}, function() {
    pr_tb.row($("#prd"+id2)).remove().draw(); 
  });
}
</script>
  