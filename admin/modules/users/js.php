    <script type="text/javascript" src="<?php echo $admin_path;?>js/md5.js">></script>
    <script type="text/javascript">
    $('#field-Users_login').parent('div').addClass('input-group');
    $('#field-Users_login').parent('div').append('<span class="input-group-addon" id="login_mess"></span>');
    $("#field-Users_login").keyup(function(){check_login()});

    function check_login(){

      var new_login = $("#field-Users_login").val();
      if(new_login.length){
        $('#login_mess').html('<img src="<?php echo $admin_path;?>img/loading.gif" /> Проверка...');
        $.post('<?php echo $admin_path;?>modules/users/check_login.php',{'login':new_login},function(data){
            data = $.parseJSON(data);
            $('#login_mess').html(data.html);
            $('#field-Valid_login').val(data.status);
        })
      }else{
        $('#login_mess').html("<font color='red'>Логин не может быть пустым</font>");
        $('#field-Valid_login').val("");
      }
    }

    function compare_pass(){
      if($("#field-Confirm_password").val()!=$('#field-New_password').val()){
        $('#field-Users_password').attr('value','');
        $('#field-Valid_password').attr('value','');
      }else{
        if($('#field-New_password').val()=='' && $("#field-Confirm_password").val()==''){
          $('#field-Users_password').attr('value', $('#field-Temp_password').val());
        }else{
          $('#field-Users_password').attr('value', calcMD5($('#field-New_password').val()));
        }
          $('#field-Valid_password').attr('value','ok');
      }
      
    }

    $("#field-New_password").keyup(function(){compare_pass()});
    $("#field-Confirm_password").keyup(function(){compare_pass()});


    var permissions = $('#field-Users_permissions').val();
    if(permissions == "" || permissions==undefined){
      permissions = "{}";
    }
    permissions = $.parseJSON(permissions);
    

    $.each(permissions,function(module_name,perms){
      
      $.each(perms,function(pname,perm){
        if(perm == 'on'){          
          $('.module-'+module_name+' .'+pname+'-'+module_name).attr('checked','checked');
        }
      })


    });

    function encode_permissions(){
      var all_checks = $('.module-permiss');
      var json_obj = new Object();

      $.each(all_checks,function(i,field){
        var module_name = $(field).attr('rel');
        var iswrite = $(field).find('.write-'+module_name).prop("checked");
        var isread = $(field).find('.read-'+module_name).prop("checked");
        var iscreate = $(field).find('.create-'+module_name).prop("checked");
        var isdelete = $(field).find('.delete-'+module_name).prop("checked");

        if(!isread){isread="off";}else{isread="on";}
        if(!iswrite){iswrite="off";}else{iswrite="on";}
        if(!iscreate){iscreate="off";}else{iscreate="on";}
        if(!isdelete){isdelete="off";}else{isdelete="on";}
        json_obj[module_name] = {"create":iscreate,"read":isread,"write":iswrite,delete:isdelete}
      })

      var permiss_to_send = JSON.stringify(json_obj);
      console.log(permiss_to_send);
      $('#field-Users_permissions').val(permiss_to_send)
      
    }

    $('#permiss').find('input:checkbox').change(function(){encode_permissions()})

    </script>