<?php
$module_name = "users";
$viewMode = "table";
$module_header = "Пользователи системы";
$module_note_title = "Редактирование учетной записи";
$module_back_title = "К списку пользователей";
$block_table = "Users";
$add_button_title = "Добавить пользователя";
$list_query = "select `Users`.`id`,`Users`.`Users_name`,`Users`.`Users_login`,`Users`.`Users_range` from `".$block_table."`";
$where_query = "";
$order_query = "";
$list_header = array("Имя пользователя","Логин","Должность");
$fields_for_add= array(
	"Users_name"=>"Новый пользователь"
);

$fields_for_save = array(
	"Users_login",
	"Users_password",
	"Users_startpage",
	"Users_permissions",
	"Users_email",
	"Users_name",
	"Users_range",
	"Users_phone"
);

$fields_required = array(
	"Valid_login",
	"Valid_password",
	"Users_login",
	"Users_name"
);

?>