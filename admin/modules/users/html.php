<ul class="nav nav-tabs">
  <li class="active"><a href="#home" data-toggle="tab">Основное</a></li>
  <li><a href="#permiss" data-toggle="tab">Права доступа</a></li>
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="home">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    if($_GET['mode'] == 'edit' && !empty($note['Users_login'])  && !empty($note['Users_password']) ){

      $login = "disabled";
      $valid_login = "ok";
      $valid_password = "ok";
    }else{
      $login = "";
      $valid_login = "";
      $valid_password = "";
    }
    



    $core->make_field("Логин*",$name="Users_login",$type="text",$note['Users_login'],"","",$login,"required");
    $core->make_field("Имя",$name="Users_name",$type="text",$note['Users_name'],"","",$readonly,"");
    $core->make_field("Должность",$name="Users_range",$type="text",$note['Users_range'],"","",$readonly,"");
    $core->make_field("Email",$name="Users_email",$type="text",$note['Users_email'],"","",$readonly,"");
  	$core->make_field("Телефон",$name="Users_phone",$type="text",$note['Users_phone'],"","",$readonly,"");
    $core->make_field("Стартовая страница",$name="Users_startpage",$type="text",$note['Users_startpage'],"","",$readonly,"");
    echo "<hr>";
    $core->make_field("Новый пароль",$name="New_password",$type="password","","","",$readonly,"");
    $core->make_field("Подтверждение",$name="Confirm_password",$type="password","","","",$readonly,"");

    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");
    $core->make_field("Права пользователя",$name="Users_permissions",$type="hidden",$note['Users_permissions'],"","",$readonly,"");
    $core->make_field("Пароль",$name="Users_password",$type="hidden",$note['Users_password'],"","",$readonly,"");
    $core->make_field("Пароль для вставки",$name="Temp_password",$type="hidden",$note['Users_password'],"","",$readonly,"");
    $core->make_field("Правильный логин",$name="Valid_login",$type="hidden",$valid_login,"","",$readonly,"");
    $core->make_field("Правильный пароль",$name="Valid_password",$type="hidden",$valid_password,"","",$readonly,"");

  ?>

</form>

  </div>
  <div class="tab-pane col-md-offset-1" id="permiss">
    <?foreach ($modules_all as $module) {
      echo '
      <div rel="'.$module[0].'" class="module-permiss module-'.$module[0].'">
      <h3>'.$module[1].'</h3>
      <input type="checkbox" class="read-'.$module[0].'"/> Чтение&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="checkbox" class="write-'.$module[0].'"/> Запись&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="checkbox" class="create-'.$module[0].'"/> Создание&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <input type="checkbox" class="delete-'.$module[0].'"/> Удаление&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      </div>
      <hr>
      ';
    }
    $core->make_field("Права пользователя",$name="Users_permissions",$type="hidden",$note['Users_permissions'],"","",$readonly,"");
    ?>

  </div>
</div>