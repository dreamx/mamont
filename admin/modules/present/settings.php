<?php
$module_name = "present";
$viewMode = "table";
$module_header = "Подарки";
$module_note_title = "Редактирование подарка";
$module_back_title = "К списку подарков";
$block_table = "present";
$add_button_title = "Создать";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`title` from `".$block_table."`";
$where_query = "";
$order_query = "";
$list_header = array("Наименование");
$fields_for_add= array(
	"title"=>"Новый подарок"
);
$translit="title_translit";//поле для записи транслита
$translit_field="title";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"title",
	"catalog",
	"brand",
	"desc",
	"price",
	"video",
	"art",
	"title_seo",
	"desc_seo",
	"content_seo",
	"public",
	"title_translit",
	"num"
	
);

$fields_required = array(
	"title"
);



?>