<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li><a href="#page_content" data-toggle="tab">Описание</a></li>
  <li><a href="#page_pic" data-toggle="tab">Изображения</a></li>
  
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    
    
    $core->make_field("Наименование*",$name="title",$type="text",$note['title'],"","",$readonly,"required");
   // $core->make_field("Артикул",$name="art",$type="text",$note['art'],"","",$readonly,"required");
    
    /*$q="SELECT * FROM `catalog` ORDER BY `num` ASC";
    $category=$core->_list($q);
    $core->make_field("Категория",$name="catalog",$type="hierarchy2",$note['catalog'], $category,"",$readonly,"");
    */
    /*$q="SELECT `id`,`title` FROM `brand` ORDER BY `num` ASC";
    $clients=$core->_list($q);
    $options_cl=array();
    foreach ($clients as $cl) {
      $options_cl[$cl['id']]=$cl['title'];
    }
    $core->make_field("Бренд",$name="type",$type="select",$note['type'],$options_cl,"",$readonly,"");
    */
    $core->make_field("Публиковать",$name="public",$type="checkbox",$note['public'],"","",$readonly,"");
    $core->make_field("Номер п/п",$name="num",$type="text",$note['num'],"","",$readonly,"required");
    //$core->make_field("Цена",$name="price",$type="text",$note['price'],"","",$readonly,"required");
    
   // $core->make_field("Видео",$name="video",$type="textarea",$note['video'],"","",$readonly,"required");
    


    echo "<hr><h4>SEO-блок</h4><hr>";
    $core->make_field("Заголовок (тэг title)*",$name="title_seo",$type="text",$note['title_seo'],"","",$readonly,"required");
    $core->make_field("Описание (META description)",$name="desc_seo",$type="text",$note['desc_seo'],"","",$readonly,"");
    $core->make_field("Ключевые слова (META keywords)",$name="content_seo",$type="text",$note['content_seo'],"","",$readonly,"");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

  ?>

</form>

  </div>
  <div class="tab-pane col-md-12" id="page_content">
  <?php
    $core->make_wisiwyg('desc',$note['desc'],$readonly);
    echo "<hr>";
  ?>
  
  </div>
    
  <div class="tab-pane" id="page_pic">
  <?php
    $img_max_size = 900;
    $img_link = "present";
    $img_thumb_width = 208;
    $img_thumb_height = 208;
    $img_block_num = 1;
    $img_limit = 1;
    $img_wtermark = false;
    $img_label = true;
    $img_desc = true;
    $img_mime = 'image/jpeg';
    $img_ex = 'jpg';
    include "../photos/index.php";
  ?>
  </div>


  
 
</div>