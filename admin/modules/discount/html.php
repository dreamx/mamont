<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    $core->make_field("Название*",$name="name",$type="text",$note['name'],"","",$readonly,"required");
    $q="SELECT * FROM `type_discount`";
    $tp=$core->_list($q);
    $options_cl=array();
    foreach ($tp as $cl) {
      $options_cl[$cl['id']]=$cl['name'];
    }
    $core->make_field("Тип",$name="type",$type="select",$note['type'],$options_cl,"",$readonly,"");
    
    $core->make_field("Еденицы",$name="edinici",$type="select",$note['edinici'],array('rub'=>'руб.','perc'=>'%'),"",$readonly,"");
    

    $core->make_field("Значение",$name="value",$type="text",$note['value'],"","",$readonly,"required");
    //$core->make_field("Номер п/п",$name="num",$type="text",$note['num'],"","",$readonly,"required");
    $core->make_field("Дата начала",$name="date_begin",$type="date",$note['date_begin']=='0000-00-00 00:00:00'?'':$note['date_begin'],"","",$readonly,"required");
    $core->make_field("Дата окончания",$name="date_end",$type="date",$note['date_end']=='0000-00-00 00:00:00'?'':$note['date_end'],"","",$readonly,"required");
    
    $core->make_field("Действует",$name="public",$type="checkbox",$note['public'],"","",$readonly,"required");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,""); 
  ?>
</form>

  </div>
  
  


</div>