<?php
$module_name = "discount";
$viewMode = "table";
$module_header = "Скидки";
$module_note_title = "Редактирование скидки";
$module_back_title = "К списку скидок";
$block_table = "discount";
$add_button_title = "Создать скидку";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`name` from `".$block_table."`";
$where_query = "";
$order_query = " ORDER BY `num` asc";
$list_header = array("Название");
$q="SELECT `id` FROM `type_discount` LIMIT 1";
$type=$core->_list($q);

$fields_for_add= array(
	"name"=>"Новая скидка",
	"type"=>$type[0]['id']
);
$translit="";//поле для записи транслита
$translit_field="";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"name",
	"num",
	"type",
	"edinici",
	"value",
	"date_begin",
	"date_end",
	"public"
);

$fields_required = array(
	"name"
);



?>