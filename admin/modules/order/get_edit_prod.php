<?php
session_start();
include "../../../admin/config.php";
include "../../../admin/classes/core.php";

function get_selects($arr,$core){
  foreach ($arr as $key => $value) {
    $core->make_field($value[0]." (".$value[1].")",$name="chars_".$key,$type="select",0,$value[2],"",$readonly,"");
  }
}

if($core->check_permiss($_SESSION['user_id'],'order','write')){

  if(is_numeric($_POST['id']) && is_numeric($_POST['count'])){
    $q="SELECT `id`,`title`,`price`,`art` FROM `product` WHERE `id`='".intval($_POST['id'])."'";
    $prd=DB::query($q,'a');
    ?>
    <form id="fr_chars">
    <?php
    if($prd){

      $q="SELECT `c`.`id`,`c`.`title`,`c`.`si`,`cl`.`value_char`,`cv`.`price`,`cv`.`id` AS id_val
      FROM `char_value` AS  `cv`
      LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
      LEFT JOIN `char` AS `c` ON `cl`.`id_char`=`c`.`id` 
      WHERE `cv`.`id_prod`='".intval($_POST['id'])."'
      ORDER BY `id`";
      $chars=DB::query($q,'a');
      $chars_array=array();
      $id_chars=array();
      if($chars){
        foreach ($chars as $ch) {
          
          if(isset($chars_array[$ch['id']])){
            $chars_array[$ch['id']][2][$ch['id_val']]=$ch['value_char']." (".$ch['price']." руб.)";
          }else{
            $id_chars[]=$ch['id'];
            $chars_array[$ch['id']]=array($ch['title'],$ch['si'],array($ch['id_val']=>$ch['value_char']." (".$ch['price']." руб.)"));
          }
        }

        get_selects($chars_array,$core);
      }else{
        echo "нет характеристик для выбора";
      }
      $core->make_field("Количество",$name="count_pr",$type="text",$_POST['count'],"","",'',"");
      $core->make_field("id_prod",$name="id_prod",$type="hidden",$_POST['id'],"","",'',"");
      $core->make_field("id_chars",$name="id_chars",$type="hidden",implode(':',$id_chars),"","",'',"");
      $core->make_field("order",$name="order",$type="hidden",$_POST['order'],"","",'',"");
      ?>
      </form>

      <?php
      }
  }else{
    echo "Произошла ошибка!";
  }
  /*if($_POST['type']=='edit'){
  	$q="SELECT `id`,`title`,`si` FROM `char`";
    $clients=$core->_list($q);
    $options_cl=array('0'=>"Выбирете характеристику");
    foreach ($clients as $cl) {
      $options_cl[$cl['id']]=$cl['title']." ".$cl['si'];
    }

    $q="SELECT `cv`.*,`c`.`title` AS c_title,`cl`.`value_char` as `c_val`,`c`.`id` as `idch`,`cl`.`id` as `idval` 
      FROM `char_value` AS  `cv`
      LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
      LEFT JOIN `char` AS `c` ON `cl`.`id_char`=`c`.`id` 
      WHERE `cv`.`id`='".intval($_POST['id'])."'
      ORDER BY `c_title`,`c_val`
      LIMIT 1";

    $values=$core->_list($q);
    $values=$values[0];
    $q="SELECT `id`,`value_char` FROM `char_list`";
    $list=$core->_list($q);
    foreach ($list as $cl) {
      $op_val[$cl['id']]=$cl['value_char']." ".$cl['si'];
    }
    $core->make_field("",$name="chars",$type="select",$values['idch'],$options_cl,"",$readonly,"");
    $core->make_field("",$name="chars_vales",$type="select",$values['id_char'],$op_val,"",$readonly,"");
    $core->make_field("Цена",$name="price_char",$type="text",$values['price'],"","",$readonly,"");
    $core->make_field("",$name="id_value",$type="hidden",$values['id'],"","",$readonly,""); 
  }else if($_POST['type']=='add'){
  	$q="SELECT `id`,`title`,`si` FROM `char`";
    $clients=$core->_list($q);
    $options_cl=array('0'=>"Выбирете характеристику");
    foreach ($clients as $cl) {
      $options_cl[$cl['id']]=$cl['title']." ".$cl['si'];
    }
    $core->make_field("",$name="chars",$type="select",0,$options_cl,"",$readonly,"");
    $core->make_field("",$name="chars_vales",$type="select",0,array(),"",$readonly,"");
    $core->make_field("Цена",$name="price_char",$type="text",'',"","",$readonly,"");
    $core->make_field("",$name="id_value",$type="hidden",'',"","",$readonly,"");        
  }
*/
  ?>
  <script type="text/javascript">
  	
    $("#field-count_pr").keypress(function (e){console.log('dsf');
      var charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
    });
	  
	

  </script>
  <?php
  
}
?>