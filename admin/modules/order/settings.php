<?php
$module_name = "order";
$viewMode = "table";
$module_header = "Заказ";
$module_note_title = "Редактирование заказа";
$module_back_title = "К списку заказов";
$block_table = "order";
$add_button_title = "Создать заказ";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`id` AS `num`,`".$block_table."`.`date`,`status`,`transport` from `".$block_table."`";
$where_query = "";
$order_query = "ORDER BY `status`";
$list_header = array("Номер","Дата","Статус","Доставка");
$fields_for_add= array(
	"status"=>"новый"
);
$translit="";//поле для записи транслита
$translit_field="";//поле для транслита
$translit_id=false;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"client",
	"date",
	"transport",
	"transport_pay",
	"comment",
	"status",
	"adres",
	"index",
	"city"
);

$fields_required = array(
	"client"
);



?>