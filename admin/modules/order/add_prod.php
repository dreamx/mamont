<?php
session_start();
include "../../../admin/config.php";
include "../../../admin/classes/core.php";


if($core->check_permiss($_SESSION['user_id'],'order','write')){

  $chars=array();
  if(!empty($_POST['id_chars'])){
    $ids=explode(':',$_POST['id_chars']);
    foreach ($ids as $i) {
      $chars[]=$_POST['chars_'.$i];
    }
  }
  $q="SELECT * FROM `product` WHERE `id`=:id LIMIT 1";
  $prod=DB::query($q,'a',array('id'=>intval($_POST['id_prod'])));
  

  if($prod){
    $prod=$prod[0];
    $q="SELECT * FROM `order_item` WHERE `title`=:title AND `order`=:order AND `price`=:price LIMIT 1";
    $old_prod=DB::query($q,'a',array('order'=>intval($_POST['id']),'title'=>$prod['title'],'price'=>$prod['price']));
    // если товар уже есть в заказе увеличиваем количество не учитывается товар по акции
    if($old_prod){
      $old_prod=$old_prod[0];
      $q="UPDATE `order_item` SET `count`=`count`+:count WHERE `id`=:id_old";
      $params=array('count'=>intval($_POST['count']),'id_old'=>$old_prod['id']);
    }else{ //если нет добавляем товар
      $q="INSERT INTO `order_item` (`title`,`price`,`count`,`order`) VALUES (:title,:price,:count,:order)";
      $params=array('title'=>$prod['title'],'count'=>intval($_POST['count']),'order'=>intval($_POST['id']));
      if($prod['action']=='on'&&$prod['action_price']>0){
      $params['price']=$prod['action_price'];
      }else{
        $params['price']=$prod['price'];
      }
    }
    
    if(DB::query($q,'',$params)){
      echo "Товар добавлен";
    }else{
      echo "Ошибка добавления";
      print_r(DB::getErrors());
    }
  }else{
    echo "Товар не найден";
  }
  
  //echo OrderItem::add_item($_POST['id_prod'],$chars,$_POST['count_pr'],$_POST['order'],array('500ec7ed62cc0cfb5136ad9fdfbfa8aa'));
  //print_r($chars);
  //echo 4;
}
?>