<script type="text/javascript">
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

var tab_prod=$('#tab_prod').dataTable({
            "language": {
                "lengthMenu": "Выводить по _MENU_ строк на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
                "infoEmpty": "Нет записей",
                "search": "Поиск: ",
                "paginate": {
                "previous": "предыдущая",
                "next": "следующая",
                }
            },
            tableTools: {
            "aButtons": [ ]
        }
          }).columnFilter({
      sPlaceHolder: "head:before",
      aoColumns: [null,
           {sSelector:"#type_filter", type: "select" }
                     
        ]});




$(".count input").keypress(function (e){
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  }); 

function refreshOrder(){
  var order_id=$('#field-id').val();
  $.post('refresh_order.php',{order_id:order_id},function(data){
    $('#info_order').html(data);
  });
}
  $('#tab_prod .add_prod').click(function(event) {
    var par=$(this).parents('tr');
    var order_id=$('#field-id').val();
    var new_fields = [];
    new_fields.push(par.find('td.title').html(),par.find('td.tara select').val(),par.find('td.count input').val(),'<span class="btn btn-danger del_item"><span class="glyphicon glyphicon-trash"></span> Удалить</span>')
    var st={};
    st['title']=new_fields[0];
    st['tara']=new_fields[1];
    st['count']=new_fields[2];
    st['order']=order_id;
    st['type']='add';
    $.post('edit_order.php',st,function(data){
      if(isNumber(data)){
        /*var newRow = api_ord.row.add(new_fields).draw().node();
        $(newRow).attr('id',data);*/
        refreshOrder();
      }
    })
    
  });  
  refreshOrder();    

$('#info_order').on("click","#tab_ord .del_item",function(){
  var par=$(this).parents('tr');
  var st={};
  st['id']=par.attr('id').slice(2);
  st['type']='del';
  $.post('edit_order.php',st,function(data){
      if(data==5){
        //api_ord.row(par).remove().draw();
        refreshOrder();
      }
    })
  
});

$('#property input').change(function(event) {
  var el=$(this);
  var val_el='';

  if(el.attr('type') == 'checkbox'){
            if(el.prop('checked')){
              val_el = "on"
            }else{
              val_el = "";
            }
            $.post('set_check.php', {id:el.attr('id'),val:val_el}, function(){
              //alert(data);
            });
        }
}); 

/*$('#itemModal').on('show.bs.modal', function (e) {
  var tr=$(e.relatedTarget).parents('tr');
  var id=tr.attr('id').slice(3);
  var count=tr.find('.count input').val();
    $('#itemModal .char_cont').html("<img height='14' src='<?php echo $admin_path;?>img/loading.gif'/> Идет загрузка");

    $.post('get_edit_prod.php', {count:count,id:id,order:$('#field-id').val()}, function(data) {
      $('#itemModal .char_cont').html(data);
    });
  
});*/

/*$('#itemModal #add_item').click(function(){
  var info=$('#itemModal #update_info');
  var str= $('#itemModal #fr_chars').serialize();
  var st={};
  st['id']=$('#field-id').val();
  st['id_prod']=$('#field-id_prod').val();
  st['count']=$('#field-count_pr').val();
  st['shars']=str;
  info.hide().html('').removeClass('alert-success alert-danger alert-warning alert-info');
  $.post('add_prod.php', str, function(data) {
      if(isNumber(data)){
        data=parseInt(data);
        $('#field-id_value').val(data);
        info.addClass('alert-success').html('Товар добавлен, повторное нажатие на "Добавить в заказ" добавит товар ещё раз').fadeIn();
        refreshOrder();
      }else{
        info.addClass('alert-danger').html('Ошибка добавления').fadeIn();
      }
    });

});*/

$('#tab_prod').on('click','.add_to_order',function(){
  var tr=$(this).parents('tr');
  var id=tr.attr('id').slice(3);
  var count=tr.find('.count input').val();
  var st={};
  st['id']=$('#field-id').val();
  st['id_prod']=parseInt(id);
  st['count']=parseInt(count);
  
  $.post('add_prod.php', st, function(data) {
      /*if(isNumber(data)){
        data=parseInt(data);
        //$('#field-id_value').val(data);
        
        
      }else{
        
      }*/
      console.log(data);
      refreshOrder();
    });

});
</script>
  