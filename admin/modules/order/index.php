<?php
session_start();
include "../../html/template.php";
?>
<style type="text/css">
.date_range_filter{width:137px !important;}
#itemModal .form-group label.col-sm-2,
#orderModal .form-group label.col-sm-2{width:33.333%;}
</style>

<!-- Modal -->
    <div class="modal fade" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Создание заказа</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="char_cont form-horizontal col-sm-12">
              	<?
              	$q="SELECT `id`,`name` FROM `client`";
			    $clients=$core->_list($q);
			    $options_cl=array();
			    foreach ($clients as $cl) {
			      $options_cl[$cl['id']]=$cl['name'];
			    }
			    $core->make_field("Клиент",$name="client_mod",$type="select",$note['client'],$options_cl,"","","");
    			/*$q="SELECT `id`,`title` FROM `transport`";
			    $clients=$core->_list($q);
			    $options=array();
			    foreach ($clients as $cl) {
			      $options[$cl['id']]=$cl['title'];
			    }
			    $core->make_field("Вид доставки",$name="transport_mod",$type="select",'',$options,"",$readonly,"");
    			*/
    			$core->make_field("Промо код",$name="promo_mod",$type="text",'',"","","","");
    			?>
              </div>
              <div class="col-sm-12">
                <div id="update_info" class="alert" style="padding: 6px 12px; margin: 0px; display: none;"></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            <button type="button" id="add_order" class="btn btn-primary">Создать заказ</button>
          </div>
        </div>
      </div>
    </div>


<script type="text/javascript">
$(document).ready(function() {
	$('#<?php echo $module_name."_table";?> thead tr').clone().appendTo('#<?php echo $module_name."_table";?> thead').find('th').removeClass('sorting');
	mt_tb.columnFilter({
			sPlaceHolder: "head:before",
			aoColumns: [{ type: "number" },
					 { type: "date-range" },
					 { type: "select" },
				     { type: "select" },null
				             
				]});

	$('.add_bt').removeAttr('onclick').attr({
		'data-toggle': 'modal',
		'data-target': '#orderModal'
	});

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

	$('#add_order').click(function(event) {
		var el=$(this);
		var info=$('#orderModal #update_info');
		var client= $('#field-client_mod').val();
		var promo=$('#field-promo_mod').val();
		//var transport=$('#field-transport_mod').val();
		info.hide().html('').removeClass('alert-success alert-danger alert-warning alert-info');
		$.post('add_order.php',{client:client,promo:promo},function(data){
	      
	      if(isNumber(data)){
	        data=parseInt(data);
	        el.parent().parent().attr('id','order-'+data);
	        open_note(el,'from_table','edit');
	        info.addClass('alert-success').html('Заказ создан').fadeIn();
	        $('#orderModal').modal('hide');
	      }else{
	        info.addClass('alert-danger').html('Ошибка создания заказа').fadeIn();
	      }
	      
	    });
		
		
	});

	$('#order_module_note').on('click','.save_bt',function(){
		if($('#field-status').val()=='доставлен'){
			$.post('block_order.php',{id_order:$('#field-id').val()},function(){alert('Заказ заблокирован!');location.reload();});	
		}
	});


});

</script>