<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li><a href="#page_content" data-toggle="tab">Содержимое (контент)</a></li>
  <!-- <li><a href="#page_cat" data-toggle="tab">Скидки</a></li> -->
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    /*OrderItem::getInstance(3);
    $it=OrderItem::getInstance();
    print_r($it);*/
    $o=Order::getInstance($note['id']);
   // print_r($o->getMaxDisc_str()); 
   // print_r($o->get_sum());
   //var_dump($o->blocked()); 
   //  $items=$o->getItems();
    // print_r($items);
    // echo $items[0]->get_summa_disc($o->getDate())."<br>";
    // echo $items[1]->get_summa_disc($o->getDate())."<br>";
    // echo $o->getSum()."<br>";
   if($o->get_block()){
    ?>
    <div class="alert alert-danger">
      <h3>Внимание!</h3>
      Этот заказ заблокирован его содержимое нельзя изменить.
    </div>
    <?php
   }?>
   <a class="btn btn-primary" target="_blank" href="/admin/modules/order/print_order.php?order=<?php echo $note['id'];?>"><span class="glyphicon glyphicon-print"></span> Версия для печати</a>
   <?php
    $core->make_field("Номер",$name="num",$type="text",$note['id'],"","","disabled","");
    $core->make_field("Дата",$name="date",$type="text",$note['date'],"","","disabled","");
    $core->make_field("Промо код",$name="promo_cod",$type="text",$note['promo_cod'],"","","disabled","");
    if($o->get_block()){
      $q="SELECT `id`,`name` FROM `client` WHERE `id`='{$note['client']}'";
      $clients=$core->_list($q);
      $core->make_field("Клиент",$name="client_name",$type="text",$clients[0]['name'],"","","disabled","");
      $core->make_field("Клиент",$name="client",$type="hidden",$clients[0]['id'],"","","disabled","");
    }else{
      $q="SELECT `id`,`name` FROM `client`";
      $clients=$core->_list($q);
      $options_cl=array();
      foreach ($clients as $cl) {
        $options_cl[$cl['id']]=$cl['name'];
      }
      $core->make_field("Клиент",$name="client",$type="select",$note['client'],$options_cl,"","","");
    }
    ?>
    <a class="btn btn-primary" href="/admin/modules/client/#<?php echo $note['client'];?>"><span class="glyphicon glyphicon-user"></span> К клиенту</a>
    <?php
    $options=array(""=>"не указан","доставка"=>"доставка","самовывоз"=>"самовывоз");
    
    $options=array("новый"=>"новый","подтвержденный"=>"подтвержденный","доставлен"=>"доставлен","отменён"=>"отменён");
    $core->make_field("Статус",$name="status",$type="select",$note['status'],$options,"",$readonly,"");
    
    $q="SELECT `id`,`title` FROM `transport`";
    $clients=$core->_list($q);
    $options=array();
    foreach ($clients as $cl) {
      $options[$cl['id']]=$cl['title'];
    }
    $core->make_field("Вид доставки",$name="transport",$type="select",$note['transport'],$options,"",$readonly,"");
    $core->make_field("Почтовый индекс",$name="index",$type="textarea",$note['index'],"","",$readonly,"required");
    $core->make_field("Город",$name="city",$type="textarea",$note['city'],"","",$readonly,"required");
    $core->make_field("Адрес доставки",$name="adres",$type="textarea",$note['adres'],"","",$readonly,"required");
    $core->make_field("Комментарий",$name="comment",$type="textarea",$note['comment'],"","",$readonly,"required");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

  ?>

</form>

  </div>
  <div class="tab-pane col-md-12" id="page_content">
    <div class="row">
      <?php
      if(!$o->get_block()){
        ?>
      <div class="col-md-5">
        <h2>Продукция</h2>
        <?php
          $q="SELECT `p`.`id`,`p`.`title`,`c`.`title` AS `catalog`
          FROM `product` AS `p` 
          LEFT JOIN `catalog` AS `c` ON `p`.`catalog`=`c`.`id`
          WHERE `p`.`public`='on'
          GROUP BY `p`.`id`
          ";
          $prods=$core->_list($q);

          $q="SELECT * FROM `order_item` WHERE `order`='".intval($note['id'])."'";
          $order_items=$core->_list($q);
        ?>
        <p id="type_filter"></p>
        <table id="tab_prod" style="width:100%;" class="table table-striped table-bordered">
          <thead>
            <th>Наименование</th>
            <th>Категория</th>
            <th>Кол-во</th>
            <th></th>
          </thead>
          <tbody>
            <?php
              if(count($prods)>0){
                foreach ($prods as $pr){
                  ?>
                  <tr id="prd<?=$pr['id'];?>">
                    <td class="title"><?php echo $pr['title'];?></td>
                    <td class="type"><?php echo $pr['catalog'];?></td>
                    <td class="count"><input style="width:59px;" type="number" min="1" value="1"/></td>
                    <td><!-- <span class="btn btn-primary" data-toggle="modal" data-target="#itemModal" ><span class="glyphicon glyphicon-plus"></span> в заказ</span> -->
                        <span class="btn btn-primary add_to_order"><span class="glyphicon glyphicon-plus"></span> в заказ</span>
                    </td>
                  </tr>
                  <?
                }
              }
            ?>
          </tbody>
        </table>
      </div>
    <?php } ?>
      <div class="col-md-7" style="background-color: #dff0d8;">
        <h2>Состав заказа</h2>

        <!-- Modal -->
    <!-- <div class="modal fade" id="itemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Редактирование характеристики</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="char_cont form-horizontal col-sm-12">
              </div>
              <div class="col-sm-12">
                <div id="update_info" class="alert" style="padding: 6px 12px; margin: 0px; display: none;"></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            <button type="button" id="add_item" class="btn btn-primary">Добавить в заказ</button>
          </div>
        </div>
      </div>
    </div> -->
      <div id="info_order">
      </div>
        
      </div>
    </div>
  <?php
    //$core->make_wisiwyg('Pages_content',$note['Pages_content'],$readonly);
    //echo "<hr>";
  ?>

  </div>
  <!-- <div class="tab-pane" id="page_cat">
    <style type="text/css">
    #property{moz-column-count: 2;
    -moz-column-gap: 20px;
    -webkit-column-count: 2;
     -webkit-column-gap: 20px; 
     -webkit-column-count: 2; 
     -webkit-column-gap: 20px;width: 700px; }
      #property .form-group{width:340px;}
      #property .col-sm-6{width:15%;}
      #property .col-sm-2{width:75%;}
    </style>

    <?php
      $q = "select `id`,`name`, IF(`edinici`='perc','%','руб.') as `edinici`,`value` from `discount` ";
      $qqq = "select * from `link_order_discont` WHERE `id_note1` = '".intval($note['id'])."'";
      $cats = $core->_list($q,'r');  
      $catsss = $core->_list($qqq,'r');  
      ?>      
            Отметьте галочками скидки распространяемые на заказ<br><br>
            <div class="form-horizontal" id="property">
     <?php 

      foreach ($cats as $value) { $checked=''; ?>
      <?php 
          if($catsss){
            foreach ($catsss as $key => $itemsss) { 
              if($itemsss['id_note2'] == $value['id']) 
                {
                  $checked='on'; 
                  unset($catsss[$key]); 
                }
              } 
          }
            $core->make_field($value['name']." ".$value['value'].$value['edinici'] ,$name="chcat_".$value['id']."_".$note['id'],$type="checkbox",$checked,"","",$readonly,"");
            ?>
               <br>
                <?php 
      } 
      ?>
            </div>
  </div> -->

</div>