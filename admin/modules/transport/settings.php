<?php
$module_name = "transport";
$viewMode = "table";
$module_header = "Виды доставки";
$module_note_title = "Редактирование вида доставки";
$module_back_title = "К списку";
$block_table = "transport";
$add_button_title = "Создать вид доставки";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`title`,`price` from `".$block_table."`";
$where_query = "";
$order_query = "";
$list_header = array("Название","Цена");
$fields_for_add= array(
	"title"=>"Новый вид доставки"
);
$translit="";//поле для записи транслита
$translit_field="";//поле для транслита
$translit_id=true;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"title",
	"price"
);

$fields_required = array(
	"title"
);



?>