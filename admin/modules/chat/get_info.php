<?php
session_start();
include "../../classes/core.php";
include "settings.php";
$id_user=intval($_SESSION['user_id']);
if($core->check_permiss($id_user,'chat','write')){

	//выводит блоки сообщений
	function drawMess($new_mess){
		if(count($new_mess)>0){
			foreach ($new_mess as $mess){
				if($mess['man_name']!=''){
					$class_dop=" manag";
					$name=$mess['man_name'];
				}else{
					$class_dop=" clien";
					$name=$mess['client_name'];
				}
				?>
				<div class="mess well<?=$class_dop;?>" id="mes<?=$mess['id'];?>">
					<div class="well"><span class="date"><?=$mess['date']?></span><span class="name"><?=$name?></span></div>

					<?=$mess['mess'];?>
				</div>
				<?php
			}
		}
	}

	$chat=new chat();
	
	//открытие чата клиента
	if($_POST['type']=='open_chat' && !empty($_POST['client_id'])){
		$messages=$chat->getLastDialog($_POST['client_id'],3);
		$client_info=$chat->getClient($_POST['client_id']);

		?>
		<div class="panel-heading">
			<h3 class="panel-title"><?=$client_info[0]['name']."(".$client_info[0]['firma'].")"?></h3>
			</div>
		<div id="chat_cont"  class="panel-body">
			<div id="but_history" class="btn btn-default btn-lg center-block"><span class="glyphicon glyphicon-arrow-up"></span> Ещё сообщения</div>
			<?php
			drawMess($messages);
			if(count($messages)>0){
				$chat->setStatusMess($messages,1);
			}
			?>
		</div>
		<div class="input-group">
			<input type="hidden" id="client_id" value="<?=$client_info[0]['id']?>"> 
			<span class="input-group-btn">
				<button id="send_mess" class="btn btn-success"  type="button"><span class="glyphicon glyphicon-pencil"></span> Оправить</button>
			</span>
			<input id="mess_text" type="text" class="form-control">
		</div>
		<?php
	}

	//отправка сообщения клиенту
	if($_POST['type']=='send_mess' && !empty($_POST['client_id'])&& !empty($_POST['mess'])){
		$chat->addMess($_POST['client_id'],$_POST['mess'],$id_user);
	}

	//проверка новых сообщений у открытого клиента
	if($_POST['type']=='refresh' && !empty($_POST['client_id'])){
		if(!empty($_POST['last_id'])){
			$new_mess=$chat->getNewDialog($_POST['client_id'],$_POST['last_id']);
		}/*else{
			$new_mess=$chat->getLastDialog($_POST['client_id'],3);
		}*/
		drawMess($new_mess);
		if(count($new_mess)>0){
			$chat->setStatusMess($new_mess,1);
		}	
	}

	//проверка состояния клиентов
	if($_POST['type']=='refresh_clients'){
		echo $chat->getInfoClients($id_user);
	}

	//запрос предыдущих сообщений
	if($_POST['type']=='history'&&!empty($_POST['client_id'])&&!empty($_POST['ferst_id'])){
		$messages=$chat->getHistoryDialog($_POST['client_id'],$_POST['ferst_id'],3);
		drawMess($messages);
	}

}
?>