<script type="text/javascript">
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

$(document).ready(function() {
	var oldhash='';
	var panel=$('#panel_chat');
	$('#panel_chat').on('click','#send_mess',send_mess);
	$('#panel_chat').on('click','#but_history',history_mess);

	function hashrout(){
		var h=window.location.hash.slice(1);
		if(isNumber(h)){
			load_client_chat(h);
			$('#client_list li').removeClass('active');
			$('#client_list #cl_'+h).addClass('active');
			refresh_clients();
		}
	}

	setInterval(function(){
		var h=window.location.hash.slice(1);
		if(oldhash!=h){
			oldhash=h;
			hashrout();
		}
		
	},500);
	
	setInterval(refresh_chat,2000);
	setInterval(refresh_clients,2000);

	function load_client_chat(id){
		panel.html('<img src="../../img/loading.gif" />');
		$.post('get_info.php',{client_id:id,type:'open_chat'},function(data){
			panel.html(data);
			var cont_chat=$('#chat_cont');
			cont_chat.scrollTop(cont_chat.height());

		});
	}

	function send_mess(){
		$.post('get_info.php',{client_id:$('#client_id').val(),type:'send_mess',mess:$('#mess_text').val()},function(data){
			//alert(data);
			refresh_chat();
		});
	}

	function refresh_chat(){
		var lastitem=$('#chat_cont .mess').last();
		var cont_chat=$('#chat_cont');
		var last_id='';
		if(lastitem.length){
			last_id=lastitem.attr('id').slice(3);
		}
		
		$.post('get_info.php',{client_id:$('#client_id').val(),type:'refresh',last_id:last_id},function(data){
			
			if(data!=''){
			cont_chat.append(data);
			cont_chat.scrollTop(cont_chat.height());
			}
		});
	}
	
	function refresh_clients(){
		$.post('get_info.php',{type:'refresh_clients'},function(data){
			if(data.length>0){
				$('#client_list .mico ').remove();
				$.each(data,function(index, el) {
					var html='';
					if(el.count>0){
						html+='<span class="mico badge pull-right">'+el.count+'</span>';
					}
					if(el.activ=='online'){
						html+='<span class="mico glyphicon glyphicon-globe pull-right"></span>';
					}
					$('#client_list #cl_'+el.id+' a').append(html);
				});
			}
		},"json");
	}

	function history_mess(){
		var ferstitem=$('#chat_cont .mess').first();
		var cont_chat=$('#chat_cont');
		var first_id='';
		if(ferstitem.length){
			ferst_id=ferstitem.attr('id').slice(3);
		$.post('get_info.php',{client_id:$('#client_id').val(),type:'history',ferst_id:ferst_id},function(data){
			if(data!=''){
				$('#but_history').after(data);
			}else{
				$('#but_history').remove();
			}
			//cont_chat.append(data);
			//cont_chat.scrollTop(cont_chat.height());
		});
		}else{
			$('#but_history').remove();
		}
	}

});
</script>
  