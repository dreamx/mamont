<?php
session_start();
include "../../../admin/config.php";
include "../../../admin/classes/core.php";

function get_selects($arr,$core){
  foreach ($arr as $key => $value) {
    $core->make_field($value[0]." (".$value[1].")",$name="chars_".$key,$type="select",0,$value[2],"",$readonly,"");
  }
}

if($core->check_permiss($_SESSION['user_id'],'order','write')){

  if(is_numeric($_POST['id']) && is_numeric($_POST['count'])){
    $q="SELECT `id`,`title`,`price`,`art` FROM `product` WHERE `id`='".intval($_POST['id'])."'";
    $prd=DB::query($q,'a');
    ?>
    <form id="fr_chars">
    <?php
    if($prd){
      $q="SELECT `cl`.`id_char`
      FROM `char_value` AS  `cv`
      LEFT JOIN `char_list` AS `cl` ON `cv`.`id_char`=`cl`.`id`
      WHERE `cv`.`id_prod`='1'
      GROUP BY `id_char`";
      $chars_id=DB::query($q,'a');
      if($chars_id){
        $wh=array();
        foreach ($chars_id as $id) {
          $wh[]="`id_char`='".$id['id_char']."'";
        }
      }else{
        echo "нет характеристик для выбора";
        exit();
      }
      
      $q="SELECT `c`.`id`,`c`.`title`,`c`.`si`,`cl`.`value_char`,`cl`.`id` AS `id_val`
      FROM `char_list` AS `cl`
      LEFT JOIN `char` AS `c` ON `cl`.`id_char`=`c`.`id` 
      WHERE ".implode(' or ',$wh)."
      ORDER BY `id_char`";
      $chars=DB::query($q,'a');
      $chars_array=array();
      $id_chars=array();
      if($chars){
        foreach ($chars as $ch) {
          
          if(isset($chars_array[$ch['id']])){
            $chars_array[$ch['id']][2][$ch['title']." ".$ch['value_char']." ".$ch['si']]=$ch['value_char'];
          }else{
            $id_chars[]=$ch['id'];
            $chars_array[$ch['id']]=array($ch['title'],$ch['si'],array($ch['title']." ".$ch['value_char']." ".$ch['si']=>$ch['value_char']));
          }
        }

        get_selects($chars_array,$core);
      }else{
        echo "нет характеристик для выбора";
      }
      $core->make_field("Количество",$name="count_pr",$type="text",$_POST['count'],"","",'',"");
      $core->make_field("prod",$name="prod",$type="hidden",$prd[0]['title'],"","",'',"");
      $core->make_field("id_chars",$name="id_chars",$type="hidden",implode(':',$id_chars),"","",'',"");
      $core->make_field("request",$name="request",$type="hidden",$_POST['request'],"","",'',"");
      ?>
      </form>

      <?php
      }
  }else{
    echo "Произошла ошибка!";
  }
  
  ?>
  <script type="text/javascript">
  	
    $("#field-count_pr").keypress(function (e){console.log('dsf');
      var charCode = (e.which) ? e.which : e.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
    });
	  
	

  </script>
  <?php
  
}
?>