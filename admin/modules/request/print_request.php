<?php
session_start();
include "../../config.php";
include "../../classes/core.php";
include "settings.php";
?>
<!DOCTYPE HTML>
<html class="no-js" xml:lang="ru" lang="ru">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Заявка №<?php echo $_GET['request'];?></title>
    <link rel="stylesheet" href="/admin/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/admin/css/dataTables.tableTools.min.css" />
</head>
<body style="width:800px;padding:20px;">       
<?
if($core->check_permiss($_SESSION['user_id'],'request','read')){
	if(is_numeric($_GET['request']))
	{
		$o=Request::getInstance(intval($_GET['request']));
		if($o){
			echo "<h1>Заявка №".$_GET['request']."</h1>";
			echo "Дата: ".date('d-m-Y',strtotime($o->date))."<br>";
			echo "Поставщик: ".$o->contragent."<br>";
			echo "Менеджер: ".$o->manager."<br>";
			if(!empty($o->desc)){
				echo "Примечание: ".$o->desc."<br>";
			}
			
			$o->view_request(true);
		}
	}
}else{
	$data = "<font color='red'>Недостаточно прав</font>";
	echo $data;

}
?>
</body> 