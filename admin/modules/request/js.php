<script type="text/javascript">
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

var tab_prod=$('#tab_prod').dataTable({
            "language": {
                "lengthMenu": "Выводить по _MENU_ строк на странице",
                "zeroRecords": "Ничего не найдено",
                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
                "infoEmpty": "Нет записей",
                "search": "Поиск: ",
                "paginate": {
                "previous": "предыдущая",
                "next": "следующая",
                }
            },
            tableTools: {
            "aButtons": [ ]
        }
          }).columnFilter({
      sPlaceHolder: "head:before",
      aoColumns: [null,
           {sSelector:"#type_filter", type: "select" }
                     
        ]});




$(".count input").keypress(function (e){
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  }); 

function refreshOrder(){
  var request_id=$('#field-id').val();
  $.post('refresh_request.php',{request_id:request_id},function(data){
    $('#info_request').html(data);
  });
}
   
  refreshOrder();    

$('#info_request').on("click","#tab_ord .del_item",function(){
  var par=$(this).parents('tr');
  var st={};
  st['id']=par.attr('id').slice(2);
  st['type']='del';
  $.post('edit_request.php',st,function(data){
      if(data==5){
        //api_ord.row(par).remove().draw();
        refreshOrder();
      }
    })
  
});



$('#itemModal').on('show.bs.modal', function (e) {
  var tr=$(e.relatedTarget).parents('tr');
  var id=tr.attr('id').slice(3);
  var count=tr.find('.count input').val();
  var info=$('#itemModal #update_info');
  info.hide().html('').removeClass('alert-success alert-danger alert-warning alert-info');
    $('#itemModal .char_cont').html("<img height='14' src='<?php echo $admin_path;?>img/loading.gif'/> Идет загрузка");

    $.post('get_edit_prod.php', {count:count,id:id,request:$('#field-id').val()}, function(data) {
      $('#itemModal .char_cont').html(data);
    });
  
});

$('#itemModal #add_item').click(function(){
  var info=$('#itemModal #update_info');
  var str= $('#itemModal #fr_chars').serialize();
  var st={};
  st['id']=$('#field-id').val();
  st['id_prod']=$('#field-id_prod').val();
  st['count']=$('#field-count_pr').val();
  st['shars']=str;
  info.hide().html('').removeClass('alert-success alert-danger alert-warning alert-info');
  $.post('add_prod.php', str, function(data) {
      if(isNumber(data)){
        data=parseInt(data);
        $('#field-id_value').val(data);
        info.addClass('alert-success').html('Товар добавлен, повторное нажатие на "Добавить в заказ" добавит товар ещё раз').fadeIn();
        refreshOrder();
      }else{
        info.addClass('alert-danger').html('Ошибка добавления').fadeIn();
      }
    });

});
</script>
  