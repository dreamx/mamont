<?php
$module_name = "request";
$viewMode = "table";
$module_header = "Заказ поставщику";
$module_note_title = "Редактирование заказа";
$module_back_title = "К списку заказов поставщику";
$block_table = "request";
$add_button_title = "Создать заказ";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`contragent`,`".$block_table."`.`date`,`manager`,`desc` from `".$block_table."`";
$where_query = ""; 
$order_query = "ORDER BY `date` desc";
$list_header = array("Поставщик","Дата","Менеджер","Примечание");
$fields_for_add= array(
	"contragent"=>"новый"
);
$translit="";//поле для записи транслита
$translit_field="";//поле для транслита
$translit_id=false;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"desc",
	"date",
	"contragent",
	"manager"
);

$fields_required = array(
	"manager",
	"contragent"
);



?>