<?php
session_start();
include "../../../admin/config.php";
include "../../../admin/classes/core.php";


if($core->check_permiss($_SESSION['user_id'],'request','write')){
 
  if(is_numeric($_POST['request'])&&!empty($_POST['count_pr'])){
    $chars=array();
    if(!empty($_POST['id_chars'])){
      $ids=explode(':',$_POST['id_chars']);
      foreach ($ids as $i) {
        $chars[]=$_POST['chars_'.$i];
      }
      $prod=$_POST['prod']." (".implode(', ',$chars).")";
    }else{
      $prod=$_POST['prod'];
    }
    
    $res= DB::insert('request_item',array('id_request','product','count'),array(intval($_POST['request']),$prod,intval($_POST['count_pr'])));
    if($res){
      echo $res[0];
    }else{
      echo "error";
    }
  }else{
    echo "error2";
  }  
 
}
?>