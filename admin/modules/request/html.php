<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li><a href="#page_content" data-toggle="tab">Содержимое (контент)</a></li>
  
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    
   ?>
   <a class="btn btn-primary" target="_blank" href="/admin/modules/request/print_request.php?request=<?php echo $note['id'];?>"><span class="glyphicon glyphicon-print"></span> Версия для печати</a>
   <?php
    $core->make_field("Менеджер*",$name="manager",$type="text",$note['manager'],"","","","");
    $core->make_field("Поставщик*",$name="contragent",$type="text",$note['contragent'],"","","","");
    
    $core->make_field("Примечание",$name="desc",$type="textarea",$note['desc'],"","",$readonly,"required");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

  ?>

</form>

  </div>
  <div class="tab-pane col-md-12" id="page_content">
    <div class="row">
      
      <div class="col-md-5">
        <h2>Продукция</h2>
        <?php
          $q="SELECT `p`.`id`,`p`.`title`,`c`.`title` AS `catalog`
          FROM `product` AS `p` 
          LEFT JOIN `catalog` AS `c` ON `p`.`catalog`=`c`.`id`
          WHERE `p`.`public`='on'
          GROUP BY `p`.`id`
          ";
          $prods=$core->_list($q);

          $q="SELECT * FROM `order_item` WHERE `order`='".intval($note['id'])."'";
          $order_items=$core->_list($q);
        ?>
        <p id="type_filter"></p>
        <table id="tab_prod" style="width:100%;" class="table table-striped table-bordered">
          <thead>
            <th>Наименование</th>
            <th>Категория</th>
            <th>Кол-во</th>
            <th></th>
          </thead>
          <tbody>
            <?php
              if(count($prods)>0){
                foreach ($prods as $pr){
                  ?>
                  <tr id="prd<?=$pr['id'];?>">
                    <td class="title"><?php echo $pr['title'];?></td>
                    <td class="type"><?php echo $pr['catalog'];?></td>
                    <td class="count"><input style="width:59px;" type="number" min="1" value="1"/></td>
                    <td><span class="btn btn-primary" data-toggle="modal" data-target="#itemModal" ><span class="glyphicon glyphicon-plus"></span> в заказ</span></td>
                  </tr>
                  <?
                }
              }
            ?>
          </tbody>
        </table>
      </div>
    
      <div class="col-md-7" style="background-color: #dff0d8;">
        <h2>Состав заказа</h2>

        <!-- Modal -->
    <div class="modal fade" id="itemModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Редактирование характеристики</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="char_cont form-horizontal col-sm-12">
              </div>
              <div class="col-sm-12">
                <div id="update_info" class="alert" style="padding: 6px 12px; margin: 0px; display: none;"></div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
            <button type="button" id="add_item" class="btn btn-primary">Добавить в заказ</button>
          </div>
        </div>
      </div>
    </div>
      <div id="info_request">
      </div>
        
      </div>
    </div>
  

  </div>
  

</div>