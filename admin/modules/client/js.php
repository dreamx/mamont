<script type="text/javascript">
function new_pass(id){
	$.post('/load/reset_pas', {mail:$('#field-email').val()}, function(data){
              	$('#edit_pass').html(data).show();
              	setTimeout(function(){
              		$('#edit_pass').hide();
              	},5000);
              });
}
function new_order(){
	$.post('new_order.php', {id:$('#field-id').val()}, function(data){
		if($.isNumeric(data)){
              	location.href = '/admin/modules/order/#'+data;
              }else{
              	alert('Ошибка создания заказа');
              }
              });
}

$(document).ready(function() {
       $('#tab_ord thead tr').clone().appendTo('#tab_ord thead').find('th');
       var tab_ord=$('#tab_ord').dataTable({
            "language": {
                
                "lengthMenu": "",
                "zeroRecords": "Ничего не найдено",
                "info": "Показано с _START_ по _END_ запись из _TOTAL_  ",
                "infoEmpty": "",
                "search": "Поиск: ",
                "paginate": {
                "previous": "",
                "next": ""
                }
            },
            "paging":false,
            "sDom": '<"top"i>rtlp',
                               
            tableTools: {

            "aButtons": [ ]
              }
          });

    tab_ord.columnFilter({
                     sPlaceHolder: "head:before",
                     aoColumns: [{ type: "number" },
                                    { type: "date-range" },
                                    { type: "select" },
                                 { type: "select" },null
                                         
                            ]});

$('#property input').change(function(event) {
  var el=$(this);
  var val_el='';

  if(el.attr('type') == 'checkbox'){
            if(el.prop('checked')){
              val_el = "on"
            }else{
              val_el = "";
            }
            $.post('set_check.php', {id:el.attr('id'),val:val_el}, function(){
              //alert(data);
            });
        }
});    



});
</script>
  
  