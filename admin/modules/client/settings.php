<?php
$module_name = "client";
$viewMode = "table";
$module_header = "Клиенты";
$module_note_title = "Редактирование клиента";
$module_back_title = "К списку клиентов";
$block_table = "client";
$add_button_title = "Создать клиента";
$list_query = "select `".$block_table."`.`id`,`".$block_table."`.`name`,`phone`,`email`,IF(`user`='0','Новый','Одобрен') as `stat` from `".$block_table."`";

if($core->check_permiss($_SESSION['user_id'],'prava','write')){
	$where_query = " ORDER BY `stat` asc";
}else{
	$where_query = "WHERE `user`='".intval($_SESSION['user_id'])."'";
	$order_query = " ORDER BY `name` desc";
}


$list_header = array("Имя","Телефон","e-mail","Статус");
$fields_for_add= array(
	"name"=>"Новый клиент"
);
$translit="";//поле для записи транслита
$translit_field="";//поле для транслита
$translit_id=false;//приклеивать id записи к транслиту или нет
$fields_for_save = array(
	"name",
	"email",
	"phone",
	"firma",
	"user",
	"index",
	"city",
	"adres"
);

$fields_required = array(
	"name"
);



?>