<ul class="nav nav-tabs">
  <li class="active"><a href="#page_settings" data-toggle="tab">Настройки</a></li>
  <li><a href="#page_content" data-toggle="tab">Заказы</a></li>
  <!-- <li><a href="#page_cat" data-toggle="tab">Скидки</a></li> -->
</ul>

<br>

<div class="tab-content">
  <div class="tab-pane active" id="page_settings">

<form class="form-horizontal" id="note_form">
  <?php
    if(!$core->check_permiss($_SESSION['user_id'],$module_name,'write')){
      $readonly = "disabled";
    }else{
      $readonly = "";
    }
    if($core->check_permiss($_SESSION['user_id'],'prava','write')){
      $q="SELECT `id`,`Users_name` FROM `Users`";
      $maneger=$core->_list($q);
      $options_cl=array('0'=>'Не указан');
    foreach ($maneger as $cl) {
      $options_cl[$cl['id']]=$cl['Users_name'];
    }
    $core->make_field("Менеджер",$name="user",$type="select",$note['user'],$options_cl,"",$readonly,"");
    
    }
    $core->make_field("Имя*",$name="name",$type="text",$note['name'],"","",$readonly,"required");
    $core->make_field("Телефон",$name="phone",$type="text",$note['phone'],"","",$readonly,"");
    $core->make_field("e-mail",$name="email",$type="text",$note['email'],"","",$readonly,"");
    $core->make_field("Организация",$name="firma",$type="text",$note['firma'],"","",$readonly,"");
    $core->make_field("Индекс",$name="index",$type="text",$note['index'],"","",$readonly,"");
    $core->make_field("Город",$name="city",$type="text",$note['city'],"","",$readonly,"");
    $core->make_field("Адрес",$name="adres",$type="textarea",$note['adres'],"","",$readonly,"");
    //$core->make_field("Новый пароль",$name="pas",$type="text","","","",$readonly,"");
    ?>
    <div style="display:none;" id="edit_pass" class="alert alert-success">Пароль изменён</div>
    <span class="btn btn-success" onclick="new_pass()"><span class="glyphicon glyphicon-pencil"></span> Отправить пароль</span>
    <?php
    // echo "<hr><h4>SEO-блок</h4><hr>";
    // $core->make_field("Заголовок (тэг title)*",$name="news_title_seo",$type="text",$note['news_title_seo'],"","",$readonly,"required");
    // $core->make_field("Описание (META description)",$name="news_desc_seo",$type="text",$note['news_desc_seo'],"","",$readonly,"");
    // $core->make_field("Ключевые слова (META keywords)",$name="news_content_seo",$type="text",$note['news_content_seo'],"","",$readonly,"");
    $core->make_field("ID",$name="id",$type="hidden",$note['id'],"","",$readonly,"");

  ?>
</form>

  </div>
  <div class="tab-pane col-md-12" id="page_content">
    <span class="btn btn-primary" onclick="new_order()"><span class="glyphicon glyphicon-plus"></span>Создать заявку</span>
  <br><br>
  <?php
    //$core->make_wisiwyg('news_content',$note['news_content'],$readonly);
    $q="SELECT `id`,`date`,`status`,`transport` 
        FROM `order` 
        WHERE `client`='".intval($note['id'])."'";
    $orders=$core->_list($q);
    ?>
    <table id="tab_ord" style="width:100%;" class="table table-striped table-bordered">
          <thead>
            <th>Номер</th>
            <th>Дата</th>
            <th>Статус</th>
            <th>Доставка</th>
            <th></th>
          </thead>
          <tbody>
            <?php
            if(count($orders)>0){
              foreach ($orders as $item) {
                ?>
                <tr id="<?php echo $item['id'];?>">
                  <td><?php echo $item['id'];?></td>
                  <td><?php echo $item['date'];?></td>
                  <td><?php echo $item['status'];?></td>
                  <td><?php echo $item['transport'];?></td>
                  <td><a class="btn btn-success del_item" href="/admin/modules/order/#<?php echo $item['id'];?>"><span class="glyphicon glyphicon-pencil"></span>Изменить</a></td>
                </tr>
                <?php
              }
            }
            ?>
          </tbody>
        </table>
    <?php
    echo "<hr>";
  ?>

  </div>

  <div class="tab-pane" id="page_cat">
    <style type="text/css">
    #property{moz-column-count: 2;
    -moz-column-gap: 20px;
    -webkit-column-count: 2;
     -webkit-column-gap: 20px; 
     -webkit-column-count: 2; 
     -webkit-column-gap: 20px;width: 700px; }
      #property .form-group{width:340px;}
      #property .col-sm-6{width:15%;}
      #property .col-sm-2{width:75%;}
    </style>

    <?php
      $q = "select `id`,`name`, IF(`edinici`='perc','%','руб.') as `edinici`,`value` from `discount` WHERE `type`='1' OR `type`='2' OR `type`='4'";
      $qqq = "select * from `link_client_discont` WHERE `id_note1` = '".intval($note['id'])."'";
      $cats = $core->_list($q,'r');  
      $catsss = $core->_list($qqq,'r');  
      ?>      
            Отметьте галочками скидки распространяемые на клиента<br><br>
            <div class="form-horizontal" id="property">
     <?php 
      foreach ($cats as $value) { $checked=''; ?>
      <?php 
          if($catsss){
            foreach ($catsss as $key => $itemsss) { 
              if($itemsss['id_note2'] == $value['id']) 
                {
                  $checked='on'; 
                  unset($catsss[$key]); 
                }
              } 
          }
            $core->make_field($value['name']." ".$value['value'].$value['edinici'] ,$name="chcat_".$value['id']."_".$note['id'],$type="checkbox",$checked,"","",$readonly,"");
            ?>
               <br>
                <?php 
      } 
      ?>
            </div>
    </div>
  
</div>