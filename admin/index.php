<?php
session_start(); 
?>
<!doctype html>
<?php

include "config.php";
include "classes/core.php";

?>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Система управления контентом</title>
    <link rel="stylesheet" href="<?php echo $admin_path;?>css/bootstrap.min.css" />


    <script src="<?php echo $admin_path;?>js/vendor/modernizr.js"></script>

  </head>
  <body>
    
<?php 
  if(!$core->Auth()){ include "login.php"; }
  else{ 
  $user_id = $_SESSION['user_id'];
  $user = $core->_list("select * from `Users` where `Users`.`id`='".$user_id."'");
  $path = $admin_path."modules/".$user[0]['Users_startpage'];
  ?>
  <script type="text/javascript">
  top.location.href = "<?php echo $path;?>";
  </script>
  <?php
  } 
?>

<!-- Инициализация Bootstrap -->
    <script src="<?php echo $admin_path;?>js/vendor/jquery.js"></script>
    <script src="<?php echo $admin_path;?>js/bootstrap.min.js"></script>
  
  </body>
</html>
