var width_left;
var scrT;
var wh;
var timerscr;

function rs(){
  width_left=$('.content_left').eq(0).width();
  $('#rukovod #news li').width(width_left);
  $('#rukovod #news').tinycarousel({
  buttons: false,
  buttons: false,
  interval: true,
  intervalTime: 7000,
  animationTime:3000
});  

  menu_position();
}

function menu_position()
{
  var mh=$("#left_menu").height();
  scrT=$(window).scrollTop();
  wh=$(window).height();
  var mt=scrT+wh-40-mh;
  $('.cont_width').each(function(i,el){
    var elp= $(el).offset().top;
    var elh=$(el).height();
    if(elp<mt+mh && elp+elh>mt+mh)
    {console.log($(el).attr('id')+' '+elp+'  '+mt);
    var selm=$('[href=#'+$(el).attr('id')+']');

      if(selm.length>0)
      {//console.log(selm);
        $('#left_menu .select').removeClass('select');
        selm.find('.butt').addClass('select');
        //window.location.hash=$(el).attr('id');
      }
      if(elp<mt)
      {
        $("#left_menu").css('top',mt+'px');
        if($(el).is('.light'))
        {
          $("#left_menu").removeClass('dr_menu');
        }else
        {
          $("#left_menu").addClass('dr_menu');
        }
        return false;
      }else{
        mt=elp+20;
        $("#left_menu").css('top',mt+'px');
        if($(el).is('.light'))
        {
          $("#left_menu").removeClass('dr_menu');
        }else
        {
          $("#left_menu").addClass('dr_menu');
        }
        return false;
      }
    }
  });

}
$(window).scroll(function () {
  $('#left_menu').stop();
  $('#left_menu').fadeOut(200);
  clearInterval(timerscr);
  timerscr=setInterval(function(){
    menu_position();
    $('#left_menu').fadeIn(300);
    clearInterval(timerscr);
  },500);
});

$(document).ready(function() {
          $("a.rukgall,a.imggall").fancybox({
              'transitionIn': 'elastic',
              'transitionOut': 'elastic',
              hideOnContentClick: true,
              overlayShow: true,
              'overlayColor': '#000',
              overlayOpacity: 0.5,
              padding: 0,
              helpers: {
                  overlay: {
                      locked: false
                  }
              }
          });
rs();
        
$('#ruk_gal .rukovodi').hover(function(){
  $('#ruk_gal .prof').hide();
  $(this).find('.prof').show(200);
},function(){$(this).find('.prof').hide();});
$('#open_sert').click(function(){
  $(this).hide(300);
  $('#dop_sert').slideDown(300);
});
$('#open_form').click(function(){
  $(this).hide(300);
  $('#form_cont').slideDown(300,function(){
    $(document).scrollTop($(document).height(),400);
  });
});

  var image = new google.maps.MarkerImage('/images/Icons-contacti.png',
        new google.maps.Size(60,68),
        new google.maps.Point(0,0),
        new google.maps.Point(30,68));
  var maniaco = new google.maps.LatLng(55.109925,40.217396);
  var opt = {
              
              disableDefaultUI: true,
              navigationControl: false,
              center: maniaco,
              zoom: 12,
              zoomControl: true,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }
  var div = document.getElementById('map_office');
  //var map = new google.maps.Map($('#map_office'), opt);
  var map = new google.maps.Map(div, opt);
    var marker = new google.maps.Marker({
      map: map,
      draggable: false,
      animation: false,
      position: maniaco,
      icon: image,
      panControl: false,
        overviewMapControl: false,
        rotateControl: false,
        streetViewControl: false
    }); 

  $('#fr_cont #cnt_send').click(function(){
      if(sendForm($('#fr_cont'),'/load/send_contact'))
      {
        $('#fr_cont').find('#error_txt').show();
        $(this).remove();
      }else
      {
        $('.error_field').effect('shake',{ times: 7,distance:4 },400);
      }
    });
  $('.valid').focus(function(){if($(this).val()==$(this).attr('rel')){$(this).val('');}}).blur(function(){if($(this).val()==''){$(this).val(($(this).attr('rel')))}});
  $('#fr_cont input,#fr_cont textarea').focus(function(){if($(this).val()==$(this).attr('rel')){$(this).val('');}}).blur(function(){if($(this).val()==''){$(this).val(($(this).attr('rel')))}});

$('#left_menu a').click(function(){$('.butt').removeClass('select'); $(this).find('.butt').addClass('select')});
});

$(window).resize(function(event) {
  rs();
});

function refresh_capt()
{
  var sr=$('.capt').attr('src');
  var n=sr.indexOf('t=');
  $('.capt').attr('src',sr.slice(0,n)+'t='+(new Date).getTime().toString());
} 

function validForm(form)
{
  var erel=$(form).find('#error_txt');
  var valid=true;
  $(form).find('.valid').each(function(n,v){
    if($(v).val()==''||$(v).val()==$(v).attr('rel')){$(v).addClass('error_field');$(erel).html('Заполните обязательные поля');valid=false;}else{$(v).removeClass('error_field');}
    if($(v).attr('id')=='test')
    {
      $.ajax({
        url:'/load/valid_captcha',
        data:{capt:$(v).val()},
        success:function(data) {
        if(data!='ok'){$(v).addClass('error_field');$(erel).html('Введите символы изображенные на картинке');valid=false;}else{$(v).removeClass('error_field');}
        },
        async:false,
        type:'post'
      });
    }
    if($(v).is('.valid_mail'))
    {
      var email=$(v).val();
      var filter = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
      if (!filter.test(email)) {
        $(v).addClass('error_field');$(erel).html('Введите корректный e-mail');valid=false;
        }
      }
      if($(v).is('.valid_phone'))
    {
      var email=$(v).val();
      var filter = /^[_0-9-()+]*$/;
      if (!filter.test(email)) {
        $(v).addClass('error_field');$(erel).html('Введите корректный телефон');valid=false;
        }
      }
  });
  return valid;
}  

function sendForm(form,url)
{
  var erel=$(form).find('#error_txt');
  var rez=false;
  if(validForm(form)){
    var st={};
    var vf=[];
    $(form).find('input, select, textarea, checkbox').each(function(i,v){
      st[$(v).attr('name')]=$(v).val();
    });
    $(form).find('.valid').each(function(n,v){
      vf.push($(v).attr('name'));
    });
    st['valid']=vf.join(':');
    $.ajax({
      url: url,
      type: 'POST',
      data: st,
      async:false,
      success:function(data) {
              erel.html(data);
              refresh_capt();
              rez=true;
            }
    });
  }
  return rez;
}