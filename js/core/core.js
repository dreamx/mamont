var cart_name="m62";
var settcook= {path: '/'};

var fixed_off = $('.main_logo').height();

$.fn.oldtoggle = function () {
   	var b = arguments;
   	return this.each(function (i, el) {
      	var  a = function () {
	var c = 0;
	return function () {
                b[c++ % b.length].apply(el, arguments)
            	}
        	}();
        	$(el).click(a)
    })
};


$(document).ready(function(){

	$('body').on('keypress','input.num',function (e){
	    var charCode = (e.which) ? e.which : e.keyCode;
	    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
	      return false;
	    }
	}); 
	$('body').on('click','.prd_del',function (e){
	   var prod=JSON.parse($(this).attr('rel').replace(/'/g, '"'));
	   var cc=$.cookie(cart_name);
	   var cart=[];
	   if(cc!==undefined && cc!=''){
	   		try{
				cart=JSON.parse(cc);
			}catch(e){
				console.log(e);
			}
		}
		//console.log(prod);
		var f=find_arr(cart,prod);//console.log(f);
		if(f!==false){
			var newcart=cart.splice(f, 1);
		}
	   //console.log(cart);
	   $.post('load/del_prod',{prod:JSON.stringify(prod)},
		   	function(data){
		   		//console.log(data);
		   	});
	   $(this).parents('.prod_row').remove();
	   if(cart.length>0){
	   	$.cookie(cart_name,JSON.stringify(cart),settcook);
	   }else{
	   		$.cookie(cart_name,'',settcook);
	   		window.location.reload(true);
	   }
	}); 
	refreshCart();
	


	var fw = $(window).width();
	var fh = $(window).height();
	var lmw = $('#left_menu').width();
	var catalog_fw = $('#main_sliders').width();


	if (fw < 769) {
		$('#adapt_cart').removeClass('cart_right_screen');
		$('#adapt_cart').addClass('cart_left_screen');
	} else {
		$('#adapt_cart').addClass('cart_right_screen');
		$('#adapt_cart').removeClass('cart_left_screen');
	}

	//general catalog 

	
	$('#general_catalog_big').hide();
	//$('#general_catalog_big').css({'width': + (catalog_fw - 40) + 'px'});
	
	// open catalog big

		$('.open_catalog').oldtoggle(function() {
			//console.log($('#general_catalog_big').css('top'));
			$('#general_catalog_big').show(300);
			$('.general_catalog').html('<span class="wspace"></span><span class="wspace"></span><span class="wspace"></span><span class="wspace"></span><span class="wspace"></span><span class="wspace"></span><span class="wspace"></span>Закрыть каталог<span class="wspace"></span><span class="wspace"></span><span class="wspace"></span><span class="wspace"></span><span class="wspace"></span><span class="wspace"></span><span class="wspace"></span>');
			$('.general_catalog').css({'background':'#0687C8'});
		}, function() {
			$('#general_catalog_big').hide(300);
			$('.general_catalog').html('Каталог в один клик');
			$('.general_catalog').css({'background':'#e06b4a'});
		});
		$('.close_catalog').click(function(){
			$('.open_catalog').trigger('click');
		});
	

	// mobile menu
	$('#mobile_menu').hide();

	$('.mobile_collapsed').oldtoggle(function(){
		
		 
		$('#mobile_menu').show('fade', function (){   
		       
		}); 

		$('#open_close_menu').removeClass('mif-menu');
		$('#open_close_menu').addClass('mif-chevron-left');
	},function(){
		$('#mobile_menu').hide(); 
		

		$('#open_close_menu').addClass('mif-menu');
		$('#open_close_menu').removeClass('mif-chevron-left');
	});

	// online consult

	$('.mamoth').delay(1200).animate({'opacity':'1'}, 600, function() {
		$('.voice').animate({'opacity':'1'}, 600);
	});

	// adaptive

	if ( device.tablet() == true ) {
		$('.morelist').css({'display':'none'});
		$('.mainlists').removeClass('col-md-8');
		$('.mainlists').addClass('col-md-12');
	}
	if ( device.mobile() == true ) {
		$('fixIt').css({'background':'none'})
	} 

	
	/* slider adaptive */

	if (fw < 1685) {
		$('.hps-amp-new').css({'font-size':'32px'});
		$('.hps-amp-name').css({'font-size':'26px','top':'70px'});
		$('.hps-amp-info').css({'font-size':'21px','top':'110px'});
		$('.hps-amp-price').css({'font-size':'36px','top':'220px'});
		$('.hps-amp-img').css({'top':'80px'});
		$('.hps-amp-more').css({'font-size':'22px','top':'225px'});

		$('.hit-amp-hit').css({'font-size':'32px'});
		$('.hit-amp-name').css({'font-size':'26px','top':'70px'});
		$('.hit-amp-info').css({'font-size':'21px','top':'110px'});
		$('.hit-amp-price').css({'font-size':'36px','top':'220px'});
		$('.hit-amp-img').css({'top':'80px'});
		$('.hit-amp-more').css({'font-size':'22px','top':'225px'});
	}

});

function login(email,pass){
	$.post('load/login',{pass:pass,email:email},function(data){
		console.log('login='+data);
		try{
				data=JSON.parse(data);
				if(data.status){
					window.location.href="/lc";
				}else{
					$('#mess_login').html(data.mess);
				}
			}catch(e){
				$('#mess_login').html('Ошибка!');
			}
		
	});
}

function logout(){
	$.post('load/logout',function(data){
		console.log('logout='+data);
	});
}

function registr(email,pas,name){
	$.post('load/registr',{pass:pas,email:email,name:name},function(data){
		console.log('registr='+data);
		try{
				data=JSON.parse(data);
				$('#mess_reg').html(data.mess);
				
			}catch(e){

			}
	});
}

function refreshCart(){
		$.post('load/refresh_cart',{cart:$.cookie(cart_name)},function(data){
			try{
				data=JSON.parse(data);
				if(data.count>0){
					$('#shoping_cart .fixed_cart').html(data.count+' | '+data.summa+' руб');
				}else{
					$('#shoping_cart .fixed_cart').html('корзина пуста');
					clearCart();
				}
			}catch(e){
				console.log(e);
			}
		});
	}

function clearCart(){
		$.cookie(cart_name,'',settcook);
		$.post('load/clear_cart',function(data){
			console.log('clear_cart='+data);
		});

	}


function getContentCart(){
	var cont=$('#cart_content');
	var loader=$('#cart_loader');
	loader.show();
	cont.html('Загрузка ждите');
	$.post('load/content_cart',{cart:$.cookie(cart_name)},function(data){
			//cont.html(data);
			try{
				data=JSON.parse(data);
				cont.html(data.html);
				$('#itog').html(data.itogo);
			}catch(e){
				console.log(e);
				cont.html('Возникла ошибка');
			}
			loader.hide();
		});
}

function synchronizCart(){
	$.post('load/synchroniz_cart',{cart:$.cookie(cart_name)},function(data){
			//alert(data);
			$('.payment_method').html(data);
			try{
				data=JSON.parse(data);
				if(data.status=='ok'){
					$.cookie(cart_name,JSON.stringify(data.cart),settcook);
					refreshCart();
				}else{
					console.log(data.status);
				}
			}catch(e){
				console.log('Возникла ошибка синхронизации');
				console.log(e);
				
			}
			//$('.payment_method').html(data);console.log(data);
	});
}

function addProd(){
	//$.cookie(cart_name,'',settcook);
	$('#product_add_to_cart center').hide();
	$('#product_add_to_cart #mes_add').html('Товар добавлен<br> в корзину').show();
	var cc=$.cookie(cart_name);
	var cart=[];
	var pr={};
		pr['prod']=$('#prod').val();
		pr['count']=$('#count_prod').val();
		var chars=$('.product_select select option:selected,.product_select input.sel_val');
      	pr['chars']=[];
	      /*$.each(chars, function(index, val) {
	         pr['chars'].push($(val).val());
	      });*/
	if(cc!==undefined && cc!=''){
		try{
			cart=JSON.parse(cc);
		}catch(e){
			console.log(e);
		}
	}
		cart.push(pr);
	  $.cookie(cart_name,JSON.stringify(cart),settcook);
	  check_cart();
	  refreshCart();
	  setTimeout(function(){
	  	$('#product_add_to_cart #mes_add').html('').hide();
	  	$('#product_add_to_cart center').show();
		},6000);
}

function addProdFast(id_prod){
	//$.cookie(cart_name,'',settcook);
	var cc=$.cookie(cart_name);
	var cart=[];
	var pr={};
		pr['prod']=parseInt(id_prod);
		pr['count']=1;
		pr['chars']=[];
	if(cc!==undefined && cc!=''){
		try{
			cart=JSON.parse(cc);
		}catch(e){
			console.log(e);
			return false;
		}
	}
	cart.push(pr);
	  $.cookie(cart_name,JSON.stringify(cart),settcook);
	  check_cart();
	  refreshCart();
	return true;
}

/* находит объект в массиве и возвращает его индекс */
function find_arr(array,item){
	var ind=false;
	$.each(array, function(index, val) { 
		if(val.prod==item.prod&&val.chars.length==item.chars.length){
			if(val.chars.length>0){
				var ch=true;
				$.each(item.chars, function(index2, val2) {
					if(val.chars.indexOf(val2)===-1){ 
						ch=false;
						//return false;
					}
				});
				if(ch){
				ind=index;
				}
				//return false;
			}else{
				ind=index;
			}
		}else{
			//ind=false;
			//return false;
		} 
	});
	return ind;
}

function setCount(count,obj){
   var cc=$.cookie(cart_name);
   var cart=[];
   if(cc!==undefined && cc!=''){
		cart=JSON.parse(cc);
	}
	
	var f=find_arr(cart,obj);
	if(f!==false){
		cart[f]['count']=count;
	}
   $.cookie(cart_name,JSON.stringify(cart),settcook);
}

/* суммирует одинаковый товар */
function check_cart(){
	var cc=$.cookie(cart_name);
	if(cc!==undefined && cc!=''){
		var res=[];
		var cart=JSON.parse(cc);
		
		$.each(cart, function(index, val) {
			var ind=find_arr(res,val);
			if(ind===false){
				var it=JSON.parse(JSON.stringify(val));
				res.push(it);
			}else{
				res[ind].count=parseInt(res[ind].count)+parseInt(val.count);
			}
		});
		$.cookie(cart_name,JSON.stringify(res),settcook);
	}
}



$(window).resize(function() {

	var catalog_fw = $('#main_sliders').width();

	var fw = $(window).width();
	var fh = $(window).height();
	var lmw = $('#left_menu').width();

	if (fw < 769) {
		$('#adapt_cart').removeClass('cart_right_screen');
		$('#adapt_cart').addClass('cart_left_screen');
	} else {
		$('#adapt_cart').addClass('cart_right_screen');
		$('#adapt_cart').removeClass('cart_left_screen');
	}

	if (fw < 365) {
		$('.a_logo').removeClass('col-xs-6');
	} else {
		$('.a_logo').addClass('col-xs-6');
	}

	/* slider adaptive */

	if (fw < 1685) {
		$('.hps-amp-new').css({'font-size':'32px'});
		$('.hps-amp-name').css({'font-size':'26px','top':'70px'});
		$('.hps-amp-info').css({'font-size':'21px','top':'110px'});
		$('.hps-amp-price').css({'font-size':'36px','top':'220px'});
		$('.hps-amp-img').css({'top':'80px'});
		$('.hps-amp-more').css({'font-size':'22px','top':'225px'});

		$('.hit-amp-hit').css({'font-size':'32px'});
		$('.hit-amp-name').css({'font-size':'26px','top':'70px'});
		$('.hit-amp-info').css({'font-size':'21px','top':'110px'});
		$('.hit-amp-price').css({'font-size':'36px','top':'220px'});
		$('.hit-amp-img').css({'top':'80px'});
		$('.hit-amp-more').css({'font-size':'22px','top':'225px'});
	}


});

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
} 

$(window).scroll(function(){

	/*fixed_off = $('#top_bar').height();

	if ( device.mobile() == true ) {
		$('#mobile_menu').removeClass('fixIt');
	} else { 

 	var windowScroll = $(window).scrollTop();

	 	if(windowScroll > fixed_off){
	 		$('#mobile_menu').addClass('fixIt');
	 	}else{
	 		$('#mobile_menu').removeClass('fixIt');
	 	}
 	}*/
 });