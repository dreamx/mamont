$(document).ready(function(){
// sliders main page

	var slider = new MasterSlider();
	    slider.setup('masterslider' , {
	    	 width: 1100,
	    	 layout: "autofill",
	           space: 0,
	         
	           autoplay: true,
	           loop: true,
	           mouse: false,
	           view: "basic"
	            // more slider options goes here...
	        });
	    // adds Arrows navigation control to the slider.
	slider.control('arrows');
	slider.control('bullets');

	var sliderhit = new MasterSlider();
	    sliderhit.setup('mastersliderhit' , {
	    	 layout: "autofill",
	    	 width: 600,
	           space: 0,
	           speed: 85,
	           fullwidth: true,
	           autoplay: true,
	           loop: true,
	           mouse: false,
	           view: "basic"
	            // more slider options goes here...
	        });
	    // adds Arrows navigation control to the slider.
	sliderhit.control('arrows');

	var slidernew = new MasterSlider();
	    slidernew.setup('masterslidernew' , {
	    	 layout: "autofill",
	    	 width: 600,
	           space: 0,
	           speed: 85,
	           fullwidth: true,
	           autoplay: true,
	           loop: true,
	           mouse: false,
	           view: "basic"
	            // more slider options goes here...
	        });
	    // adds Arrows navigation control to the slider.
	slidernew.control('arrows');    

});