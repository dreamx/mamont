function validForm(form)
{
  var erel=$(form).find('#error_txt');
  var valid=true;
  $(erel).html('');
  $(form).find('.valid').each(function(n,v){
    if($(v).val()==''||$(v).val()==$(v).attr('rel')){$(v).addClass('error_field');$(erel).html('Заполните обязательные поля');valid=false;}else{$(v).removeClass('error_field');}
    if($(v).attr('id')=='test')
    {
      $.ajax({
        url:'/load/valid_captcha',
        data:{capt:$(v).val()},
        success:function(data) {
        if(data!='ok'){$(v).addClass('error_field');$(erel).html('Введите символы изображенные на картинке');valid=false;}else{$(v).removeClass('error_field');}
        },
        async:false,
        type:'post'
      });
    }
    if($(v).is('.valid_mail'))
    {
      var email=$(v).val();
      var filter = /^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/;
      if (!filter.test(email)) {
        $(v).addClass('error_field');$(erel).html('Введите корректный e-mail');valid=false;
        }
      }
      if($(v).is('.valid_phone'))
    {
      var email=$(v).val();
      var filter = /^[_0-9-()+]*$/;
      if (!filter.test(email)) {
        $(v).addClass('error_field');$(erel).html('Введите корректный телефон');valid=false;
        }
      }
  });
  return valid;
}  

function sendForm(form,url)
{
  var erel=$(form).find('#error_txt');
  var rez=false;
  if(validForm(form)){
    var st={};
    var vf=[];
    $(form).find('input, select, textarea, checkbox').each(function(i,v){
      st[$(v).attr('name')]=$(v).val();
    });
    $(form).find('.valid').each(function(n,v){
      vf.push($(v).attr('name'));
    });
    st['valid']=vf.join(':');
    $.ajax({
      url: url,
      type: 'POST',
      data: st,
      async:false,
      success:function(data) {
              erel.html(data);
              refresh_capt();
              rez=true;
            }
    });
  }
  return rez;
}
  
function refresh_capt()
{
  var sr=$('.capt').attr('src');
  var n=sr.indexOf('t=');
  $('.capt').attr('src',sr.slice(0,n)+'t='+(new Date).getTime().toString());
}


function clearForm(form)
{
  $(form).find('input:text, textarea').each(function(n,v){
    if($(v).attr('rel')!='undefind')
    {
      $(v).val($(v).attr('rel'));
    }else{
      $(v).val('');
    }
  });
}

jQuery(document).ready(function($) { 
  
  $('.capt').click(refresh_capt);
  //$('.valid').focus(function(){if($(this).val()==$(this).attr('rel')){$(this).val('');}}).blur(function(){if($(this).val()==''){$(this).val(($(this).attr('rel')))}});
  //$('.forms input,.forms textarea').focus(function(){if($(this).val()==$(this).attr('rel')){$(this).val('');}}).blur(function(){if($(this).val()==''){$(this).val(($(this).attr('rel')))}});

  $('body').on('.numb','keypress',function (e){
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  });
  /*$(".numb").keypress(function (e){
    var charCode = (e.which) ? e.which : e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
  });
*/
  

  $('#send_partner').click(function(event) {
    
    if(sendForm('#partner','/load/send_partner')){
      clearForm('#partner');
    $('#partner #error_txt').html('Заявка оформлена в течении 30 минут на указанный e-mail будет отправленно письмо с подтверждением.').fadeIn(200);
    }else{
      $('#partner #error_txt').fadeIn(200);
    }
    setTimeout(function(){$('#partner #error_txt').fadeOut(200)},5000);
  });

  $('#sendMess').click(function(event) {
    
    if(sendForm('#questForm','/load/send_mess')){
      clearForm('#questForm');
    $('#questForm #error_txt').html('Вопрос принят.').fadeIn(200);
    }else{
      $('#questForm #error_txt').fadeIn(200);
    }
    setTimeout(function(){$('#questForm #error_txt').fadeOut(200)},5000);
  });

  $('#send_reset').click(function(){
    if(sendForm('#resetpsw','/load/reset_pas')){
      $('#resetpsw #error_txt').fadeIn(200);
    }else{
      $('#resetpsw #error_txt').fadeIn(200);
    }
    setTimeout(function(){$('#questForm #error_txt').fadeOut(200)},5000);
  });
 
});

