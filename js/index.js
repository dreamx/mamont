var ww;
var wh;
var min_h=660;
var min_w=980;
var scr_h,scr_w;
var timerscr;
var scroll_id=false;

function isAppleDevice(){
    return (
        (navigator.userAgent.toLowerCase().indexOf("ipad") > -1) ||
        (navigator.userAgent.toLowerCase().indexOf("iphone") > -1) ||
        (navigator.userAgent.toLowerCase().indexOf("ipod") > -1)
    );
}

$(window).resize(function(event) {
	ww=$(window).width();
  wh=$(window).height();
  if (ww<min_w) {scr_w=min_w}else{scr_w=ww;}
  if (wh<min_h) {scr_h=min_h}else{scr_h=wh;};
  $('.screen').width(scr_w).height(scr_h);
});

$(window).ready(function(){

	
  ww=$(window).width();
  wh=$(window).height();
  if (ww<min_w) {scr_w=min_w}else{scr_w=ww;}
  if (wh<min_h) {scr_h=min_h}else{scr_h=wh;};
  $('.screen').width(scr_w).height(scr_h);
var act_screen=1;
var upscroll=0;
var scroll_now=false;
menu_position();
if(isAppleDevice())
	{
		//anim_scr2_int();
		anim_icon(true);
		anim_scr2();
	}else{
		anim_scr2_int();
		anim_icon(false);
	}

$(window).scroll(function () {
	var scroll = $(window).scrollTop();
	
	if(Math.abs(upscroll-scroll)>10 && !scroll_now)
	{
            if(scroll_id==false &&!isAppleDevice()) 
            {
		if(upscroll<scroll)
		{ 
			scroll_now=true;
			window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return false;};

			$("body,html").animate({"scrollTop":(parseInt(upscroll/scr_h)+1)*scr_h},500,function()
				{	
					setTimeout(function(){
						scroll_now=false;
						upscroll=$(window).scrollTop();
						window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return true;};
					},50);
				});
		}else
		{
			window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return false;};
			scroll_now=true;
			$("body,html").animate({"scrollTop":(parseInt(upscroll/scr_h)-1)*scr_h},500,function(){
				setTimeout(function(){
					scroll_now=false;
					upscroll=$(window).scrollTop();
					window.onmousewheel = document.onmousewheel = window.onscroll = document.onscroll = function (e) {return true;};
				},50);
			});
		}
            } 
	}

	
	if(scroll<10&&!isAppleDevice())
	{
		anim_scr2_int();

	}
	if(scroll==scr_h*2&&!isAppleDevice())
	{
		anim_icon(true);
		anim_scr2_int();
	}
	if(scroll==scr_h&&!isAppleDevice())
	{
		anim_icon(false);
	}

	if(scroll>scr_h*0.8&&scroll<scr_h*1.1&&!isAppleDevice())
	{
		anim_scr2();
	}
//console.log('scroll = '+scroll);
  $('#left_menu').stop(true,true);
  $('#left_menu').fadeOut(200);
  clearInterval(timerscr);
  timerscr=setInterval(function(){
    menu_position();
    $('#left_menu').fadeIn(300);
    clearInterval(timerscr);
  },500);
  
});
var anim_ico=false;
 setInterval(function(){
 	if(upscroll>scr_h*1.98)
 	{
 		anim_icon(true);
 		anim_scr2_int();
 	}
 	if(upscroll<10)
 	{
 		anim_scr2_int();
 	}
 },400);

function anim_icon(show)
{
	if(!anim_ico)
	{//console.log('start'+' '+show);
	anim_ico=true;
	anim_ico39(show,
		function(){//console.log('start1');
			anim_ico17(show,function(){
				anim_ico14(show,function(){
					anim_ico11(show,function(){
						anim_ico10(show,function(){
							anim_ico6(show,function(){
								anim_ico3(show,function(){
									anim_ico=false;
									if(show)
									{
										$('#scr3 #bg2').fadeIn(500);
									}else
									{
										$('#scr3 #bg2').fadeOut(500);
									}
								});	
							});	
						});	
					});	
				});	
			});	
		}
	);
	}
}

$('#scr1 .balls').mouseenter(function(){
	$(this).stop(true,true);
	var elw=parseFloat($(this).css('width'));
	var neww=elw/scr_w/0.01*1.03;
	var margin=(elw*1.03-elw)/-2;
	$('#scr1 .balls').stop(true,true);
	$('#hud').hide().removeClass('twist');
	$(this).animate({width:neww+'%','margin-left':margin+'px'},250,function(){
		$('#hud').css($(this).position());
		$('#hud').css({width:neww+'%','z-index':$(this).css('z-index')-1,'margin-left':$(this).css('margin-left')}).fadeIn(250).addClass('twist');
	});
}).mouseleave(function() {
	$(this).stop(true,true);
	var neww=parseFloat($(this).css('width'))/scr_w/0.01/1.03+'%';
	$('#hud').hide().removeClass('twist');
	$(this).animate({width:neww,'margin-left':'0px'},250);
});;

	$('#scr1 #bg5').mouseenter(function(){
		$('#scr1 .balls').stop(true,true);
		$('#hud').hide().removeClass('twist');
	});

$('#scr1').mousemove(function (pos) { 
    newx=pos.pageX;
    newy=pos.pageY;
    var x=(scr_w/2-newx)*-20/scr_w;
    var y=(scr_h/2-newy)*-20/scr_h;
  	$('#scr1 #bg2').css({'background-position':x+'% '+y+'%'});
  	$('#scr1 #bg3').css({'background-position':(-1.223*x)+'% '+(-1.212*y)+'%'});
  	//console.log(parseFloat(getBgposition($('#scr1 #bg2'))[0]));
  	//move_pil(pos);
  	});

$('#scr3').mousemove(function (pos) { 
    newx=pos.pageX;
    newy=pos.pageY-$(window).scrollTop();
    var x=newx/(scr_w/100);
    var y=newy*0.05/(scr_h/100);
    //console.log('x='+newx+', y='+newy);
  	//$('#scr3 #galaxy').move_bg(x,50+y,750);
  	
  	$('#scr3 #layer1').css({'background-position':(50+x*-0.01)+'% '+(70+y)+'%'});
  	$('#scr3 #layer2').css({'background-position':(50+(x*0.02))+'% '+(70+y*-0.8)+'%'});
  	$('#scr3 #layer3').css({'background-position':x*0.06+'% '+(70+y*-1)+'%'});
	});
	life_bg2(15);

	$('#scr1').click(function(event) {
		$('#o2').move_bg(-10,-20,500);
	});

	$('#tsec39').click(function(){
		//anim_ico39();
	});

	$('#scr2').click(function(event) {
		//anim_scr2();
	});

$('.sl').click(function(){
scroll_id=true;
var pos = $(this).data('sl');  
$('.sl .butt').removeClass('select');
$(this).find('.butt').addClass('select'); 
//console.log(pos+" = "+$("#"+pos).offset().top);
var scroll_now=true;
			$("body,html").animate({"scrollTop":$("#"+pos).offset().top},500,function()
				{	
					setTimeout(function(){scroll_now=false;scroll_id=false;upscroll=$(window).scrollTop();},50);
				});    
});
        //anim_scr2_int();
	
});
 
 $(window).load(function() {$("body,html").scrollTop(0);});
 

function anim_scr2_int()
{
	if(!isAppleDevice()&&!anim_scr2_run)
	{
	$('#scr2 .anim_cir').stop(true,true).css({top:'248px',left:'379px'});
	$('#scr2 .line').stop(true,true).width(0).height(0);
	setTimeout(function(){
		$('#scr2 .small_cir').stop(true,true).css({opacity:'0'});
	
	$('#zavod').hide('scale',100);
	$('.text_cir').hide('size',100);
	},550);
	}
}
var anim_scr2_run=false;
function anim_scr2()
{
	anim_scr2_run=true;
	anim_cir1();anim_cir2();anim_cir3();
		
		setTimeout(function(){
			anim_cir4();anim_cir5();anim_cir6();
		anim_cir7();
		setTimeout(function(){
			$('#lider2').addClass('twist3');
			$('#lider3').addClass('twist4');
		},300);
		$('#zavod').show('scale',300);
		$('.text_cir').show('scale',300);
		anim_scr2_run=false;
	},600);
}

function move_pil(pos)
{
	var pil=$('#scr1 #o2');
	var xb=parseFloat(getBgposition(pil)[0]);
	var yb=parseFloat(getBgposition(pil)[1]);
	var ypr=pos.pageY/(scr_h*0.01);
	var xpr=pos.pageX/(scr_w*0.01);
	//console.log(xb+'-'+xpr+'_'+yb+'-'+ypr);
	if(Math.abs(xb-xpr)<5&&Math.abs(yb-ypr)<5)
	{
		if(xpr>xb){movex=xpr-5;}else{movex=xpr+5;}
		if(ypr>yb){movey=ypr-5;}else{movey=ypr+5;}
		$('#o2').move_bg(movex,movey,100);
		
	}
}
var fadeTime=300;
var fadeTimesec=150;
var animTime=150;
var koefHide=0.5;
function anim_ico39(show,funct,option)
{
	if(show)
	{
		$('#ico_sec39').fadeIn(fadeTime);
		$('#ico_sec39').animate({
		width: '80px'},
		animTime, function() {
		$('#sec39').fadeIn(fadeTimesec,function()
		{
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		}
			);
	});	
	}else
	{
		$('#sec39').fadeOut(fadeTimesec*koefHide,function(){
			$('#ico_sec39').animate({
		width: '40px'},
		animTime*koefHide,function(){
			$('#ico_sec39').fadeOut(fadeTime*koefHide);
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		});	
		});
	}
	
}
function anim_ico17(show,funct,option)
{
	
	if(show)
	{
		$('#ico_sec17').fadeIn(fadeTime);
		$('#ico_sec17').animate({
			width: '80px'},
			animTime, function() {
			$('#sec17').fadeIn(fadeTimesec,function()
		{
			if (funct ) {
        		funct();
		    }
		}
			);
		});
	}else
	{
		$('#sec17').fadeOut(fadeTimesec*koefHide,function(){
			$('#ico_sec17').animate({
		width: '40px'},
		animTime*koefHide,function(){
			$('#ico_sec17').fadeOut(fadeTime*koefHide);
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		});	
		});
	}
}

function anim_ico14(show,funct,option)
{
	if(show)
	{
		$('#ico_sec14').fadeIn(fadeTime);
		$('#ico_sec14').animate({
			width: '80px'},
			animTime, function() {
			$('#sec14').fadeIn(fadeTimesec,function()
		{
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		}
			);
		});
	}else
	{
		$('#sec14').fadeOut(fadeTimesec*koefHide,function(){
			$('#ico_sec14').animate({
		width: '41px'},
		animTime*koefHide,function(){
			$('#ico_sec14').fadeOut(fadeTime*koefHide);
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		});	
		});
	}
}
function anim_ico11(show,funct,option)
{
	if(show)
	{
		$('#ico_sec11').fadeIn(fadeTime);
		$('#ico_sec11').animate({
			width: '105px'},
			animTime, function() {
			$('#sec11').fadeIn(fadeTimesec,function()
		{
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		}
			);
		});
	}else
	{
		$('#sec11').fadeOut(fadeTimesec*koefHide,function(){
			$('#ico_sec11').animate({
		width: '41px'},
		animTime*koefHide,function(){
			$('#ico_sec11').fadeOut(fadeTime*koefHide);
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		});	
		});
	}
}
function anim_ico10(show,funct,option)
{
	if(show)
	{
		$('#ico_sec10').fadeIn(fadeTime);
		$('#ico_sec10').animate({
			width: '105px'},
			animTime, function() {
			$('#sec10').fadeIn(fadeTimesec,function()
		{
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		}
			);
		});
	}else
	{
		$('#sec10').fadeOut(fadeTimesec*koefHide,function(){
			$('#ico_sec10').animate({
		width: '41px'},
		animTime*koefHide,function(){
			$('#ico_sec10').fadeOut(fadeTime*koefHide);
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		});	
		});
	}
}

function anim_ico6(show,funct,option)
{
	if(show)
	{
		$('#ico_sec6').fadeIn(fadeTime);
		$('#ico_sec6').animate({
			width: '80px'},
			animTime, function() {
			$('#sec6').fadeIn(fadeTimesec,function()
		{
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		}
			);
		});
	}else
	{
		$('#sec6').fadeOut(fadeTimesec*koefHide,function(){
			$('#ico_sec6').animate({
		width: '40px'},
		animTime*koefHide,function(){
			$('#ico_sec6').fadeOut(fadeTime*koefHide);
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		});	
		});
	}
}

function anim_ico3(show,funct,option)
{
	if(show)
	{
		$('#ico_sec3').fadeIn(fadeTime*koefHide);
		$('#ico_sec3').animate({
			height: '102px'},
			animTime*koefHide, function() {
			$('#sec3').fadeIn(fadeTimesec*koefHide,function()
		{
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		}
			);
		});
	}else
	{
		$('#sec3').fadeOut(fadeTimesec,function(){
			$('#ico_sec3').animate({
		height: '40px'},
		animTime,function(){
			$('#ico_sec3').fadeOut(fadeTime);
			if (funct && typeof(funct) === "function") {
        		funct();
		    }
		});	
		});
	}
}

function anim_cir1()
{
	$('#scr2 #sotrud').animate({
		top: '103px',
		left: '33px',
		opacity:'1'},
		500, function() {
		$(this).find('.rot').fadeIn('300').addClass('twist2');
	});
	setTimeout(function(){
	$('#scr2 #line1').animate({
		width:'119px',
		height:'47px'
	},350)},160);
	
}

function anim_cir2()
{
	$('#scr2 #kislorod').animate({
		top: '102px',
		left: '744px',
		opacity:'1'},
		500, function() {
		$(this).find('.rot').fadeIn('300').addClass('twist2');
	});
	setTimeout(function(){
		$('#scr2 #line2').height(54);
	$('#scr2 #line2').animate({
		width:'128px'
	},350)},160);
	
}
function anim_cir3()
{
	$('#scr2 #azot').animate({
		top: '462px',
		left: '629px',
		opacity:'1'},
		500, function() {
		$(this).find('.rot').fadeIn('300').addClass('twist2');
	});
	setTimeout(function(){
		$('#scr2 #line3').height(53);
	$('#scr2 #line3').animate({
		width:'56px'
	},350)},160);
	
}
function anim_cir4()
{
	$('#scr2 #pers').css({top:'190px',left:'119px',opacity:'0'});
	    $('#scr2 #pers').animate({
		top: '59px',
		left: '248px',
		opacity:'1'},
		500, function() {
		$(this).find('.rot').fadeIn('300').addClass('twist2');
	});
	setTimeout(function(){
		$('#scr2 #line4').height(53);
	$('#scr2 #line4').animate({
		width:'56px'
	},350)},160);
}
function anim_cir5()
{
	$('#scr2 #medal').css({top:'325px',left:'462px',opacity:'0'});
	    $('#scr2 #medal').animate({
		top: '69px',
		left: '380px',
		opacity:'1'},
		500, function() {
		$(this).find('.rot').fadeIn('300').addClass('twist2');
	});
	setTimeout(function(){
		//$('#scr2 #line5').width(31);
	$('#scr2 #line5').animate({
		height:'92px',
		width:'31px'
	},350)},160);
}
function anim_cir6()
{
	$('#scr2 #cir_n2').css({top:'544px',left:'711px',opacity:'0'});
	    $('#scr2 #cir_n2').animate({
		top: '612px',
		left: '562px',
		opacity:'1'},
		500, function() {
		$(this).find('.rot').fadeIn('300').addClass('twist3');
	});
	setTimeout(function(){
	$('#scr2 #line6').width(50);
	$('#scr2 #line6').animate({
		height:'21px'
	},350)},160);
}
function anim_cir7()
{
	$('#scr2 #cir_o2').css({top:'183px',left:'825px',opacity:'0'});
	    $('#scr2 #cir_o2').animate({
		top: '56px',
		left: '912px',
		opacity:'1'},
		500, function() {
		$(this).find('.rot').fadeIn('300').addClass('twist2');
	});
	setTimeout(function(){
	$('#scr2 #line7').height(37);
	$('#scr2 #line7').animate({
		width:'27px'
	},350)},160);
}
$.fn.move_bg=function(x,y,t)
{
	this.each(function(){
		var titer=10;
		var obj=$(this)
		var pos=obj.css('background-position').split(' ');
		var xob=/([\d\.]*)(.*)/.exec(pos[0]);
		var yob=/([\d\.]*)(.*)/.exec(pos[1]);
		var max_iter=t/titer;
		var iter=1;
		var xfl=parseFloat(xob[1]);
		var yfl=parseFloat(yob[1]);
		x=x-xfl;
		y=y-yfl;
		var step_x=(x)/max_iter;
		var step_y=(y)/max_iter;
		var st=$.now();
		var timer=setInterval(function(){
			obj.css('background-position',(xfl+step_x*iter)+xob[2]+' '+(yfl+step_y*iter)+yob[2]);
			//obj.css()
			if(iter>=max_iter)
			{
				clearInterval(timer);//console.log(($.now()-st)+' '+iter);
			}//console.log((yfl+step_y*iter)+yob[2]+ yfl+yob[2]);
			
			iter++;
		},titer);
	});
}



function getBgposition(el)
{
	var pos=el.css('background-position');
	return pos.split(' ');
}
/*
function life_bg(k)
{
	setTimeout(function(){
		var xb=parseFloat(getBgposition($('#scr1 #bg1'))[0]);
		var yb=parseFloat(getBgposition($('#scr1 #bg1'))[1]);
		var xb4=parseFloat(getBgposition($('#scr1 #bg4'))[0]);
		var yb4=parseFloat(getBgposition($('#scr1 #bg4'))[1]);
		//console.log(k);
	if( (0<k&& k <0.0002)|| (0>k&& k>-0.0002)|| k==0)
	{
		z=Math.random();
		if(xb>15||z>0.5){r=-1;}else{r=1;}
		if(xb<5||z>0.5){r=1;}else{r=-1;}
		life_bg(Math.floor((Math.random() * r * 20) + 1));
	}else if(k<0)
	{
		$('#scr1 #bg1').css({'background-position':(xb-0.14921)+'% '+yb+'%'});
		$('#scr1 #bg4').css({'background-position':(xb4+0.2032)+'% '+yb+'%'});
		life_bg(k+0.1);
	}else
	{
		$('#scr1 #bg1').css({'background-position':(xb+0.1424)+'% '+yb+'%'});
		$('#scr1 #bg4').css({'background-position':(xb4-0.2012)+'% '+yb+'%'});
		life_bg(k-0.1);
	}
		
	},4);
}
*/
function life_bg2(k)
{//console.log(k);
		var xb=parseFloat(getBgposition($('#scr1 #bg1'))[0]);
		var yb=parseFloat(getBgposition($('#scr1 #bg1'))[1]);
		var xb4=parseFloat(getBgposition($('#scr1 #bg4'))[0]);
		var yb4=parseFloat(getBgposition($('#scr1 #bg4'))[1]);
		z=Math.random();
		if(xb>15||z>0.5){r=-1;}else{r=1;}
		if(xb<5||z>0.5){r=1;}else{r=-1;}
		
		$('#scr1 #bg1').animate({'background-position':(k/-1)+'% 0%'},4800);
		$('#scr1 #bg4').animate({'backgroundPosition':k+'% 0%'},5000,
			function(){
			life_bg2(Math.floor((Math.random() * r * 20) + 1));
		});
		
}

function menu_position()
{
  var mh=$("#left_menu").height();
  scrT=$(window).scrollTop();
  wh=$(window).height();
  var mt=scrT+wh-40-mh;
  $('.screen').each(function(i,el){
    var elp= $(el).offset().top;
    var elh=$(el).height();
    if(elp<mt+mh && elp+elh>mt+mh)
    {
    var selm=$('[data-sl='+$(el).attr('id')+']');

      if(selm.length>0)
      {//console.log(selm);
        $('#left_menu .select').removeClass('select');
        selm.find('.butt').addClass('select');
        //window.location.hash=$(el).attr('id');
      }
      
        $("#left_menu").css('top',mt+'px');
        if($(el).is('.light'))
        {
          $("#left_menu").removeClass('dr_menu');
        }else
        {
          $("#left_menu").addClass('dr_menu');
        }      


    }
  });

}
