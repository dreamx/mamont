function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

function send_mess(){
  var mescont=$('#mess_text').val();
  if(mescont!=''){
    $.post('/lc/load/get_info',{type:'send_mess',mess:mescont},function(data){
      refresh_chat();
      $('#mess_text').val('');
    });
  }
}

function refresh_chat(){
    var lastitem=$('#contMess .message').last();
    var cont_chat=$('#contMess');
    var last_id='';
    if(lastitem.length){
      last_id=lastitem.attr('id').slice(3);
    }
    
    $.post('/lc/load/get_info',{type:'refresh',last_id:last_id},function(data){
      
      if(data!=''){
      cont_chat.append(data);
      cont_chat.scrollTop(cont_chat.height());
      }
    });
  }

function history_mess(){
    var ferstitem=$('#contMess .message').first();
    var cont_chat=$('#contMess');
    var first_id='';
    if(ferstitem.length){
      ferst_id=ferstitem.attr('id').slice(3);
    $.post('/lc/load/get_info',{type:'history',ferst_id:ferst_id},function(data){
      if(data!=''){
        cont_chat.prepend(data);
      }else{
        $('#but_history').remove();
      }
      
    });
    }else{
      $('#but_history').remove();
    }
  }

function refresh_status(){
  $.post('/lc/load/get_info',{type:'active'},function(data){
        if(isNumber(data)){
          $('#unread').html(data);
        }
      });
}

$(document).ready(function() {
  refresh_status();
    setInterval(refresh_status,10000);

    setInterval(refresh_chat,3000);
});