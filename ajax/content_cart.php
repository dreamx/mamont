<?php
defined('_LCACCESS') or die;
if(!empty($_POST['cart'])){
	
	$prods_obj=json_decode($_POST['cart']);
	$count=0;
	$itogo=0;
	$html='';
	if(count($prods_obj)>0){
		foreach ($prods_obj as $obj) {
			$pr=Product::getNewItem($obj->prod);//объект класса
			
			$count+=$obj->count;//прибавляем количество товара к общему числу
			$photos=$pr->get_photos();
			
			if(count($photos)>0){
				$src="/img/thumbs/".$photos[0];
			}else{
				$src="/images/nophoto.jpg";
			}
			$summ=$pr->getPrice();//цена товара
			
			$html.='<div class="col-md-12 prod_row">
                    <div id="cart_products">
                        <ul class="cart_table_list">
                          <div class="row">
                            <div class="col-md-2 col-xs-2"><li class="prd_img img-thumbnail"><img src="'.$src.'"></li></div>
                            <div class="col-md-4 col-xs-4"><li class="prd_name">'.$pr->title.' </li></div>
                            <div class="col-md-2">
                              <li class="prd_quantity">
                                    <input type="number" class="form-control tcenter count_prod num"  placeholder="1" value="'.$obj->count.'">
                              </li>
                            </div>
                            <div class="col-md-3"><li class="prd_price">'.number_format($summ,0,',', ' ').'руб.<span></span></li></div>
                            <div class="col-md-1"><li rel="'.str_replace('"',"'",json_encode($obj)).'" class="prd_del" title="Удалить товар из корзины"><span class="glyphicon glyphicon-remove-circle" aria-hidden="true"></span></li></div>
                          	<input type="hidden" class="pr_prod" value="'.$summ.'">
                          </div>
                        </ul>
                    </div>
                    <div class="space5"></div>
                    <div class="cart_prod_bottom_line"></div>
                    <div class="space5"></div>
                  </div>';
			$itogo+=$summ;
		}
	}
	//echo $html;
	$itogo='<span class="itog">'.$itogo.'</span> руб.';
}
echo json_encode(array('html'=>$html,'itogo'=>$itogo));
?>
