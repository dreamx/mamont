<?php
session_start();
require_once("admin/classes/mail.php");
require_once("admin/classes/core.php");
function filter($field){
    $field = stripslashes($field);
    $field = preg_replace("(\r?\n)", "", $field);
    $field = str_replace("<", "", $field);
    $field = str_replace(">", "", $field);
    $field = htmlspecialchars($field);
    return $field;
}

$from="noreply@prime-med.ru<noreply@prime-med.ru>";
$from_name="Сайт prime-med.ru";
$theme = 'Сообщение с сайта';
$email="sport60@yandex.ru";
//$email="cherry-line@mail.ru";
//$email="info@victoriaplaza.ru";

$mess='';
$valid=true;
if($_POST['valid']!='')
{
  $mas_vf=explode(":", $_POST['valid']);
  foreach ($mas_vf as $value) {
    if(empty($_POST[$value]))
    {
      $valid=false;
    }
  }
}

    if($valid)
    {
    
      if(mb_strtoupper($_POST['test'])==$_SESSION['capcha'])
      {
        $mess.='Имя: '.$_POST['name'];
        if($_POST['type']=="quest") {
          $mess.=!empty($_POST['mail'])?'<br>e-mail: '.$_POST['mail']:'';
          $theme = 'Вопрос с сайта';
        }else if($_POST['type']=="zapis") {
          $mess.=!empty($_POST['phone'])?'<br>Телефон: '.$_POST['phone']:'';
          $mess.=!empty($_POST['date'])?'<br>Дата: '.$_POST['date']:'';
          $theme = 'Запись на прием с сайта';
        }else if($_POST['type']==0){
          $theme = 'Отзыв о клинике с сайта';
        }else if($_POST['type']==1){
          $theme = 'Отзыв о услуге с сайта';
          if(is_numeric($_POST['type_id'])){
            $q="SELECT `Pages_title` AS 'title' FROM `uslugi` WHERE `id`='".intval($_POST['type_id'])."' LIMIT 1";
            $info=$core->_list($q);
            if(count($info)>0)
            {
              $mess.="<br>Услуга: ".$info[0]['title'];
            }
          }
        }else if($_POST['type']==2){
          $theme = 'Отзыв о враче с сайта';
          if(is_numeric($_POST['type_id'])){
            $q="SELECT `Pages_title` AS 'title' FROM `personal` WHERE `id`='".intval($_POST['type_id'])."' LIMIT 1";
            $info=$core->_list($q);
            if(count($info)>0)
            {
              $mess.="<br>Доктор: ".$info[0]['title'];
            }
          }
        }
        $mess.=!empty($_POST['comment'])?'<br>Сообщение: '.$_POST['comment']:'';

        
        /*$mess.='Имя: '.$_POST['name'];
        $mess.=!empty($_POST['fam'])?'<br>Фамилия: '.$_POST['fam']:'';
        $mess.=!empty($_POST['mail'])?'<br>e-mail: '.$_POST['mail']:'';
        $mess.=!empty($_POST['phone'])?'<br>телефон: '.$_POST['phone']:'';
        $mess.=!empty($_POST['comment'])?'<br>Сообщение: '.$_POST['comment']:'';
*/
        $mail=new Mail($email,$mess,$theme,$from,$from_name);

      if($mail->send())
      {
        if($_POST['type']==0||$_POST['type']==1||$_POST['type']==2){
          $fields_for_add=array(
            'name'=>$_POST['name'],
            'desc'=>$_POST['comment'],
            'type'=>$_POST['type'],
            'id_type'=>$_POST['type_id']
            );
          $core->add_note('reviews',$fields_for_add);    
        }
        echo "Сообщение отправленно";
        $_SESSION['capcha']='';
      }else
      {
        echo "Сообщение не удалось отправить, повторите попытку позже";
      }
    }else
    {
      echo "Введите символы изображённые на картинке";
    }
      
    }else
    {
      echo "Заполните все обязательные поля";
      
    }
  

?>