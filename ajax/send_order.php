<?php 
defined('_LCACCESS') or die;
if(!empty($_POST['cart'])){ 
	
	$prods=json_decode($_POST['cart'],true);
	$html='';

	if(count($prods)>0){ 
		$client_id=client::new_client($_POST['email'],$_POST['name'],$_POST['phone']);
		$client=client::getClient($client_id);
		$dostavka=intval($_POST['dost']);
		$adres=htmlspecialchars($_POST['adres'],ENT_QUOTES);
		$city=htmlspecialchars($_POST['city'],ENT_QUOTES);
		$comment=htmlspecialchars($_POST['comment'],ENT_QUOTES);
		$index=intval($_POST['index']);;
		$promo=$_POST['promo'];
		$order=$client->newOrder($dostavka,$index,$comment,$adres,$city,$promo);

		if($order){
		
			foreach ($prods as $item) {
				if(!$order->addItem(intval($item['prod']),intval($item['count']))){
					$status=false;
					break;
				}
			}
			
			if($order->save()){
				$status=true;
			}else{
				$status=false;
			}
			
		}else{
			$status=false;
		}	
		
	}else{
		$status=false;
	}

	
}else{
	$status=false;
}
echo json_encode(array('status'=>$status,'addprod'=>$ads_prod));
?>
