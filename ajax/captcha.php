<?php 
// Задаем список символов, используемых в капче 
//$capletters=array('A','B','C','D','E','F','G','K','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9');
$capletters=array('1','2','3','4','5','6','7','8','9');
//$capletters = 'ABCDEFGKIJKLMNOPQRSTUVWXYZ123456789';  
// Длина капчи 7 знаков 
$captlen = 5;  

// Устанавливаем размеры изображения 
$capwidth = 157; $capheight = 33;  

// Подключаем шрифт 
$capfont = 'font/OpenSans.ttf';  

 // Размер нашего текста 
$capfontsize = 14; 
session_start(); 
// Переопределяем HTTP заголовок, чтобы контент нашего  
// скрипта представлял собой не текст, а изображение 
header('Content-type: image/png');  

// Формируется изображение с указанными ранее размерами 
$capim = imagecreatetruecolor($capwidth, $capheight);  

// Устанавливаем необходимость применения альфа канала (прозрачности) 
imagesavealpha($capim, true);  

// Устанавливаем цвет фона, в нашем случае - прозрачный 
//$capbg = imagecolorallocatealpha($capim, 0, 0, 0,127 ) ; 
$capbg = imagecolorallocatealpha($capim, 255, 255, 255,0 ) ; 
$capbg = imagecolorallocatealpha($capim, 0, 0, 0,127 ) ;
// Устанавливаем цвет фона изображения 
imagefill($capim, 0, 0, $capbg);  

// Задаем начальное значение капчи 
$capcha = ''; 

// Запускаем цикл заполнение изображения 
for ($i = 0; $i < $captlen; $i++){ 
	$capfontsize = rand(14,20); 

shuffle($capletters);
// Из нашего списка берем «рендомный» символ и добавляем в капчу 
$capcha .= $capletters[rand(0, count($capletters)-1) ];  

// Вычисление положения символа по X оси 
$x = ($capwidth - 20) / $captlen * $i ; 

// Добавим «рендомности» в это положение. 
$x = rand($x, $x+5);  

// Находим положение по Y оси 
$y = $capheight - ( ($capheight - $capfontsize) / 2 );  

// Укажем случайный цвет для символа. 
//$capcolor = imagecolorallocate($capim, rand(0, 100), rand(0, 100), rand(0, 100) );  

$capcolor = imagecolorallocate($capim, 51, 51, 51 );  

// Наклон для символа 
$capangle = rand(-25, 25);  

// Рисуем созданный символ, применяя все описанные параметры 
imagettftext($capim, $capfontsize, $capangle, $x, $y, $capcolor, $capfont, $capcha[$i]); 

} // Закрываем цикл 


$_SESSION['capcha']= $capcha; 
//рисуем линии
for ($k=0; $k < 4; $k++) { 
	//$capcolor = imagecolorallocate($capim, rand(0, 100), rand(0, 120), rand(0, 100) );
	$capcolor = imagecolorallocate($capim,  51, 51, 51 );
	imageline($capim,rand(0,$capwidth), rand(0,$capheight), rand(0,$capwidth),rand(0,$capheight), $capcolor);

}
// Создаем переменную, куда будет сохранена капча,  
// с ней будет сравниваться введенный пользователем текст 


imagepng($capim); // Выводим картинку. 

imagedestroy($capim); // Очищаем память. 

?>
