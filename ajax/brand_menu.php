<?php
defined('_LCACCESS') or die;
if(!empty($_POST['id'])){
	$q="SELECT `id`,`parent` FROM `catalog` WHERE `public`='on' ";
	$cats=DB::query($q,'a');
	$select_cat=array(array('id'=>intval($_POST['id'])));
	function get_child_cat($mass,$id){
		$res=array();
		foreach ($mass as $k=>$row) {
			if($row['parent']==$id)
			{
				array_push($res, $row);
				unset($mass[$k]);
				$res=array_merge($res,get_child_cat($mass,$row['id']));
			}
		}
		return $res;
	}
	//print_r(get_child_cat($cats,$_POST['id']));
	$select_cat=array_merge($select_cat,get_child_cat($cats,$_POST['id']));
	$wh=array();
	foreach ($select_cat as $cat) {
		array_push($wh,"`p`.`catalog`='".$cat['id']."'");
	}

	$q="SELECT `b`.`id`,`b`.`title`
	FROM `brand` AS `b`
	LEFT JOIN `product` AS `p` ON `p`.`brand`=`b`.`id`
	WHERE `p`.`public`='on' AND (".implode(' or ',$wh).")
	GROUP BY `b`.`id`
	ORDER BY `b`.`num`";
	$brand=DB::query($q,'a');
	if($brand){
		$brand_html='<div class="big_cat_name">Бренд</div>';
		$brand_html.='<ul>';
		foreach ($brand as $b) {
			$brand_html.='<li class="br_item" id="br_'.$b['id'].'">
                                    <span class="small_cat_name">'.$b['title'].'</span> 
                                    <div class="clear"></div>
                                  </li>';
		}
		$brand_html.='</ul>';
	}
	//echo $brand_html;

// проверяет наличие скидок только для товаров(`type`='5')
	$q="SELECT `p`.`id`,`p`.`title`,IF(`d`.`edinici` = 'perc', '%', 'руб.') as `edinici`,`d`.`value`
	FROM `discount` AS `d`
	LEFT JOIN `link_product_discont` AS `ld` ON `ld`.`id_note2`=`d`.`id` 
	LEFT JOIN `product` AS `p` ON `p`.`id`=`ld`.`id_note1`
	WHERE `p`.`public`='on' AND (".implode(' or ',$wh).") AND `d`.`type`='5'
	GROUP BY `p`.`id`
	LIMIT 10";
	$disc=DB::query($q,'a');
	if($disc){
		$disc_html='<div class="big_cat_name">Акции</div>';
		$disc_html.='<ul>';
		foreach ($disc as $d) {
			$disc_html.='<li class="ds_item" id="ds_'.$d['id'].'">
                                    <span class="small_cat_name">'.$d['title'].' скидка '.$d['value'].' '.$d['edinici'].'</span> 
                                    <div class="clear"></div>
                                  </li>';
		}
		$disc_html.='</ul>';
	}
	//echo $q;
	//echo $disc_html;
}
echo json_encode(array("brand_html"=>$brand_html,"disc_html"=>$disc_html));