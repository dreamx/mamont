<?php
defined('_LCACCESS') or die;
if(!empty($id_client)){

	//выводит блоки сообщений
	function drawMess($new_mess){
		if(count($new_mess)>0){
			foreach ($new_mess as $mess){
				if($mess['man_name']!=''){
					$class_dop=" rightMess";
					$name=$mess['man_name'];
				}else{
					$class_dop=" leftMess";
					$name=$mess['client_name'];
				}
				?>
				<div class="row message <?=$class_dop;?>" id="mes<?=$mess['id'];?>">
					<span class="messageHeader"><span class="date"><?=$mess['date']?></span>-<span class="name"><?=$name?></span></span>
					<span class="messageBlack">
					<?=$mess['mess'];?>
					</span>
				</div>
				<?php
			}
		}
	}

	$chat=new chat();
	
	//отправка сообщения клиенту
	if($_POST['type']=='active'){
		DB::update('client',array('activ'=>time())," `id`='".$id_client."'");
		$count=$chat->countUnread($id_client);
		if($count){
			echo $count[0][0];
		}else{
			echo 0;
		}

	}

	//открытие чата клиента
	if($_POST['type']=='open_chat'){
		$messages=$chat->getLastDialog($id_client,3);
		drawMess($messages);
		if(count($messages)>0){
			$chat->setStatusMess($messages,1,'client');
		}
	}

	//отправка сообщения менеджеру
	if($_POST['type']=='send_mess' && !empty($_POST['mess'])){
		$chat->addMess($id_client,$_POST['mess']);
	}

	//проверка новых сообщений у клиента
	if($_POST['type']=='refresh'){
		if(!empty($_POST['last_id'])){
			$new_mess=$chat->getNewDialog($id_client,$_POST['last_id']);
		}/*else{
			$new_mess=$chat->getLastDialog($_POST['client_id'],3);
		}*/
		drawMess($new_mess);
		if(count($new_mess)>0){
			$chat->setStatusMess($new_mess,1,'client');
		}	
	}

	//запрос предыдущих сообщений
	if($_POST['type']=='history' && !empty($_POST['ferst_id'])){
		$messages=$chat->getHistoryDialog($id_client,$_POST['ferst_id'],3);
		drawMess($messages);
	}

}
?>