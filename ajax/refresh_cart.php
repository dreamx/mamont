<?php
defined('_LCACCESS') or die;
if(!empty($_POST['cart'])){
	
	$prods_obj=json_decode($_POST['cart']);
	$count=0;
	$itogo=0;
	if(count($prods_obj)>0){
		foreach ($prods_obj as $obj) {
			$pr=Product::getNewItem($obj->prod);//объект класса
			$summ_chars=0;//сумма стоимости характеристик
			$count+=$obj->count;//прибавляем количество товара к общему числу
			if(count($obj->chars)>0){
				foreach ($obj->chars as $ch) {// считаем стоимость характеристик
					$summ_chars+=intVal($pr->chars_val[$ch]['price']);
				}
			}
			$summ=$pr->price;//цена товара
			$summ_all=$pr->calcDisc($summ+$summ_chars)*$obj->count;//стоимость товара с учетом скидок и количества
			$itogo+=$summ_all;
		}
	}
	
}
echo json_encode(array("count"=>$count,"summa"=>intVal($itogo)));