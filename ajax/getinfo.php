<?php
$send_ar=array();
$img_url='http://prime.cs62.ru/img/';
$no_img='http://prime.cs62.ru/images/';

  /* озывы */

  $q="SELECT *,DATE_FORMAT(`date`, '%d.%m.%Y') AS `revdate`  FROM `reviews` WHERE public='on'";
  $reviews=$core->_list($q);
  $rev=array();
  if(count($reviews)>0){
    foreach ($reviews as $r) {
      if(isset($rev[$r['type']][$r['id_type']])){
        array_push($rev[$r['type']][$r['id_type']], $r);
      }else{
        $rev[$r['type']][$r['id_type']]=array($r);
      }
    }
  }
  /* конец озывы */

  /* врачи */
  	$q="SELECT `p`.`id`,`p`.`Pages_title` AS name,`p`.`Pages_content` AS content,  
                concat(`ph`.`id`,'.',`ph`.`Photos_ex`) AS `photo`,
                GROUP_CONCAT( `prof`.`name` ORDER BY `l`.`id_note2` ASC SEPARATOR ', ') as filter,
                `p`.`Pages_title_translit` AS `link`      
                FROM `personal` AS `p` 
                LEFT JOIN `Photos` AS `ph` ON `p`.`id`=`ph`.`Photos_parent` AND `Photos_module`='pers'
                LEFT JOIN `link_personal_proffes` AS l ON `p`.`id`=`l`.`id_note1`
                LEFT JOIN `proffes` AS `prof` ON `l`.`id_note2`=`prof`.`id`
                WHERE `p`.`Pages_public`='on' and `prof`.`name` IS NOT NULL
                GROUP BY `p`.`id`
                ORDER BY `p`.`Pages_num` DESC";
    $events=$core->_list($q);
    $events_ar=array();
    if(count($events)>0)
    {
    	
    	foreach ($events as $v) {
        $doc_rev=array();
        if(isset($rev[2][$v['id']])){
          foreach ($rev[2][$v['id']] as $review) {
            $doc_rev[]=array(
              'name'=>$review['name'],
              'date'=>$review['revdate'],
              'message'=>$review['desc']
            );
          }
        }
        
        $ar_d=array(
          'id'=>$v['id'],
          'name'=>$v['name'],
          'image'=>$img_url.$v['photo'],
          'description'=>html_entity_decode(strip_tags($v['content']),ENT_QUOTES),
          'specialization'=>$v['filter']
        );
        if(count($doc_rev)>0){
          $ar_d['feedback']=$doc_rev;
        }
    		array_push($events_ar, $ar_d);
    	}
    }
    $send_ar['doctors']=$events_ar;
  /* врачи конец*/ 

  /* специальности */

  $q="SELECT * FROM `proffes` ORDER BY `num`";
  $spec=$core->_list($q);
  $sp=array();
  if(count($spec)>0){
    foreach ($spec as $r) {
      $sp[]=$r['name'];
    }
  }
$send_ar['specializations']=$sp;

  /* конец специальности */

  /* акции */

  $q="SELECT * FROM `Action` WHERE `Pages_public`='on' AND TO_DAYS(NOW()) <= TO_DAYS(`Pages_date_end`) ORDER BY `Pages_date_begin`";
  $actions=$core->_list($q);
  $ac=array();
  if(count($actions)>0){
    foreach ($actions as $r) {
      $ac[]=array(
          'name'=>$r['Pages_title'],
          'description'=>html_entity_decode(strip_tags($r['Pages_content']),ENT_QUOTES)
        );
    }
  }
  $send_ar['bonuses']=$ac;

  /* конец акции */
  
  /* услуги */
    $q="SELECT `p`.`id`,`p`.`Pages_title` AS name,`p`.`Pages_content` AS content,  
                concat(`ph`.`id`,'.',`ph`.`Photos_ex`) AS `photo`,
                GROUP_CONCAT( `prof`.`Pages_title` ORDER BY `prof`.`Pages_num` ASC SEPARATOR ', ') as filter,
                `p`.`Pages_title_translit` AS `link`      
                FROM `uslugi` AS `p` 
                LEFT JOIN `Photos` AS `ph` ON `p`.`id`=`ph`.`Photos_parent` AND `Photos_module`='usluga_app'
                LEFT JOIN `link_uslugi_personal` AS l ON `p`.`id`=`l`.`id_note1`
                LEFT JOIN `personal` AS `prof` ON `l`.`id_note2`=`prof`.`id`
                WHERE `p`.`Pages_public`='on' and `prof`.`Pages_title` IS NOT NULL
                GROUP BY `p`.`id`
                ORDER BY `p`.`Pages_num` DESC";
    $events=$core->_list($q);

    $q="SELECT * FROM `price` ORDER BY `num`";
    $prices=$core->_list($q);

    $events_ar=array();
    if(count($events)>0)
    {
      
      foreach ($events as $v) {
        $doc_rev=array();
        if(isset($rev[1][$v['id']])){
          foreach($rev[1][$v['id']] as $review) {
            $doc_rev[]=array(
              'name'=>$review['name'],
              'date'=>$review['revdate'],
              'message'=>$review['desc']
            );
          }
        }
        
        $price=array();
        if(isset($prices)>0){
          foreach ($prices as $pr) {
            $price[]=array(
                'service_name'=>$pr['title'],
                'price'=>$pr['price']
              );
          }
        }

        $ar_d=array(
          'id'=>$v['id'],
          'name'=>$v['name'],
          'description'=>html_entity_decode(strip_tags($v['content']),ENT_QUOTES),
          'doctors'=>$v['filter']
        );

        if($v['photo']!=''){
          $ar_d['image']=$img_url.$v['photo'];
        }
        if(count($doc_rev)>0){
          $ar_d['feedback']=$doc_rev;
        }
        if(count($price)>0){
          $ar_d['subservices']=$price;
        }
        array_push($events_ar, $ar_d);
      }
    }
    $send_ar['services']=$events_ar;
  /* услуги конец*/ 

  /* отзывы о клинике */
    $events_ar=array();
    $doc_rev=array();
    if(isset($rev[1][$v['id']])){
      foreach($rev[1][$v['id']] as $review) {
        $doc_rev[]=array(
          'name'=>$review['name'],
          'date'=>$review['revdate'],
          'message'=>$review['desc']
        );
      }
    }
    $send_ar['feedback']=$doc_rev;
  /* конец отзывы о клинике */
  

    $send_ar['clinic']= "Клиника 'Прайм-Стоматология' на Есенина, 100 - самая крупная негосударственная стоматология в Рязани и Рязанской области. На базе одной клиники работают 38 квалифицированных стоматологов всех специализаций, установлено уникальное диагностическое и лечебное оборудование, функционирует десткое отделение и собственная дентальная лаборатория\n \n\n\n Специалисты клиники проводят сложнейшие зубосохранящие операции, имплантацию зубов, лечение заболеваний десен лазером, лечение под микроскопом. В клинике прием ведут 4 кандидата медицинских наук, получены патенты на способы лечения, проводятся областные стоматологически конференции.\n \n\n\n 'Прайм-стоматология' имеет лицензию на оказание всех видов помощи, включая лечение под наркозом и выдачу больничных листов.";
    $send_ar['contacts']= "Мы находимся в самом центре Рязани, напротив остановки 'Завод САМ' на улице Есенина, 110. К нам легко подъехать из любого район города, как на общественном транспорте, так и на собственном автомобиле. оставив его на большой прилегающей стоянке.";




  print_r(json_encode($send_ar));//вывод масива в строку

?>