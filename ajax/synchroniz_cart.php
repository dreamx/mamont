<?php
defined('_LCACCESS') or die;

	$status="error=1";
if(User::check_auth()){
		
		$basket=User::getBasket();
		$items=$basket->getItems();
		$array_update=array();//массив для обновления количества на сервере
		$array_insert=array();//массив для добавления в корзину сервера
		$keys=array();//массив ключей товаров, которые есть в обоих корзинах
		$new_arr=array();//совмещённая корзина
		function find_it($it,$prods_obj){
			if(count($prods_obj)>0){
			foreach ($prods_obj as $key_ob => $pr) {
					if($pr->prod==$it->id_prod && count($it->chars_val)==count($pr->chars)){
						if(count($pr->chars)>0){
							$ch=true;
							foreach ($it->chars_val as $key => $value) {
								if(array_search($key,$pr->chars)===false){
									$ch= false;
								}
							}
							if($ch){
								return $key_ob;
							}
						}else{
							return $key_ob;
						}
					}
				}
			}
				return false;
		}

		if(!empty($_POST['cart'])){ //если в козине на клиенте есть товары
			$prods_obj=json_decode($_POST['cart']);
			$new_arr=$prods_obj;//совмещённая корзина
			if(count($items)>0){ // если в корзине на сервере есть товары, то будем их совмещать  
				foreach ($items as $it){
					$fc=find_it($it,$prods_obj);
					if($fc!==false){
						$keys[]=$fc;
						$array_update[$it->id]['newcount']=$prods_obj[$fc]->count;
						$array_update[$it->id]['item']=$it;
					}else{
						$obj= new stdClass();
						$obj->prod=$it->id_prod;
						$obj->count=$it->count;
						$obj->chars=array();
						if(count($it->chars_val)>0){
							foreach($it->chars_val AS $ch_id => $val){
								array_push($obj->chars,$ch_id);
							}
						}
						array_push($new_arr, $obj);
						
					}	
				}
				/* ************ обновление количества существующих товаров ************** */
				foreach ($array_update as $updvalue) {
					$updvalue['item']->setCount($updvalue['newcount']);
				}
			}	
				//формируем массив новых товаров для вставки в базу 
				foreach ($prods_obj as $key => $value) {
					if(array_search($key, $keys)===false){
						$array_insert[]= array('id'=>$value->prod,'count'=>$value->count,'chars'=>$value->chars);
					}
				}


				/* ************ добавление в базу товаров, которых не было в корзине на сервере ************** */
				foreach ($array_insert as $insvalue) {
					BasketItem::add_item($insvalue['id'],$insvalue['chars'],$insvalue['count'],$basket->id);
				}
				
			//вывод для отладки
		/*print_r($keys);
		print_r($new_arr);
		echo "<br>cookei";print_r($prods_obj);
		echo "update";print_r($array_update);
		echo "<br>insert";print_r($array_insert);
		print_r($items);*/
		}else{ //если в козине на клиенте нет товаров, то будем выводить товары с сервера
			
			if(count($items)>0){
				foreach ($items as $it) {
					$obj= new stdClass();
					$obj->prod=$it->id_prod;
					$obj->count=$it->count;
					$obj->chars=array();
					array_push($new_arr, $obj);
				}
				//echo json_encode($new_arr);
				$status = 'ok';
			}
		
	}
	//echo json_encode(array('status'=>$status,'cart'=>$new_arr));
	$status = 'ok';
}
echo json_encode(array('status'=>$status,'cart'=>$new_arr));	

?>
